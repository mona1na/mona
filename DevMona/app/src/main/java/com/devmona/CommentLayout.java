package com.devmona;

/**
 * Created by sake on 2017/07/27.
 */

public class CommentLayout {
    String comment;
    int layoutId;

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
