package com.devmona;


import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewFutureFragment extends Fragment {


    public NewFutureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("LifeTest NewFutureFragm", "onCreateView");
        // Inflate the layout for this fragment
        //
        View v = inflater.inflate(R.layout.fragment_new_future, container, false);
//        Button button = (Button) v.findViewById(R.id.new_future_btn_ok);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Base2Activity activity = (Base2Activity) getActivity();
//                if (activity != null) {
//                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
//                    fragmentTransaction.remove(NewFutureFragment.this).commit();
//                    TabLayout tabLayout = activity.findViewById(R.id.tabLayout);
//                    TabLayout.Tab tab = tabLayout.getTabAt(0);
//                    tab.select();
//                }
//            }
//        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("LifeTest NewFutureFragm", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("LifeTest NewFutureFragm", "onPause");
    }
}
