package com.devmona;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MyThread extends Thread {

    private Context context;
    public static final String TAG = "MyThread";

    public MyThread(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        super.run();
        int reConnectCount = TestShared.getInstance(context).getReconectCount();
        boolean reConnecting = false;
        Log.d("killTest", "MyThread:run()");
        for (int i = 0; i < reConnectCount; i++) {
            Log.d("killTest", i +"");
            if (i > 0) {
                Log.d("killTest", "i > 0");
                reConnecting = true;
            }
            if (connectSuccess(reConnecting)) {
                Log.d("killTest", "if (connectSuccess(reConnecting))");
                // 接続できたら抜ける
                Log.d(TAG, "killTest" + (i + 1) + "回目");
//                showToast("接続");
                showTate2Text("接続");
                break;
            } else {
                Log.d("killTest", "else");
                // 最後の接続なら
                if (i == reConnectCount - 1) {
                    /**
                     * 完全切断処理
                     */
                    Log.d("killTest", "i == reConnectCount - 1");
                    Log.d(TAG, "can't connect count:" + (i + 1) + "回目");
                    TestConnect.getInstance(context).stopAndClose(context);
                    showToast("再接続回数上限に達しました。切断します。");
                    showTate2Text("切断");
                    syuryoDousa();
                } else {
                    // 再接続ループ処理中
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, "run reconnect count:" + (i + 1) + "回目");
                    showToast("再接続:" + (i + 1) + "回目");
                    showTate2Text("再接続中...");
                }
            }
        }
    }

    private void syuryoDousa() {
//        Intent intent = new Intent(context, com.devmona.ProgressFinishReceiver.class);
//        context.sendBroadcast(intent);
        Log.d(TAG, "syuryoDousa");
        ProgressFragment progressFragment = UtilInfo.getProgressFragment(context);
        Base2Activity base2Activity = (Base2Activity) context;
        if (progressFragment != null) {
            Log.d(TAG, "syuryoDousa:progress finish");
            base2Activity.getSupportFragmentManager().beginTransaction().remove(progressFragment).commit();
        }
    }

    private void showToast(final String message) {
        Base2Activity base2Activity = (Base2Activity) context;
        base2Activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showTate2Text(final String message) {
        Base2Activity base2Activity = (Base2Activity) context;
        Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
        if (tate2Fragment != null) {
            final TextView textView = tate2Fragment.getView().findViewById(R.id.tate2_connect);
            base2Activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(message);
                }
            });
        }
        try {

        } catch (NullPointerException e) {
            base2Activity = (Base2Activity) context;
            tate2Fragment = UtilInfo.getTate2Fragment(context);
            if (tate2Fragment != null) {
                final TextView textView = tate2Fragment.getView().findViewById(R.id.tate2_connect);
                base2Activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(message);
                    }
                });
            }
        }

    }

//    private void showTate2Text(final String message) {
//        Base2Activity base2Activity = (Base2Activity) context;
//        Main4Fragment main4Fragment = UtilInfo.getMain4Fragment(context);
//        if (main4Fragment != null) {
//            final TextView textView = main4Fragment.getView().findViewById(R.id.tate2_connect);
//            base2Activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    textView.setText(message);
//                }
//            });
//        }
//
//
//    }

    private boolean connectSuccess(boolean reConnectting) {
        boolean check;
        Log.d("killTest", "connectSuccess");
        TestConnect testConnect = TestConnect.getInstance(context);
        testConnect.stopAndClose(context);
        check = testConnect.createConnect(context, reConnectting);
        if (!check) {
            Log.d("killTest", "connectSuccess if (!check)");
            return false;
        }
        WaitSendThread waitSendThread = new WaitSendThread(this, context);
        Log.d("killTest", "WaitSendThread");
        waitSendThread.start();
        Log.d("killTest", "WaitSendThread start");
//        testConnect.sendTest(context);
//        Log.d(TAG, "connect ok send");
//        TestConnect testConnect = TestConnect.getInstance(context);
//        testConnect.sendRoom(context);

        if (testConnect.readRoomThread != null) {
            check = sy(testConnect.readRoomThread);
            Log.d("killTest", "testConnect.readRoomThread != null");
        } else if (testConnect.readMainThread != null) {
            Log.d("killTest", "testConnect.readMainThread != null");
            check = sy(testConnect.readMainThread);

        } else {
            Log.d(TAG, "connectSuccess error");
            Log.d("killTest", "connectSuccess else");
            return false;
        }
        Log.d("killTest", "connectSuccess:" + check);
        return check;
    }

    private boolean sy(ReadAbstract readAbstract) {

        if (readAbstract instanceof ReadRoomThread) {
            ReadRoomThread readRoomThread = (ReadRoomThread) readAbstract;
            synchronized (this) {
                try {
                    Log.d(TAG, "test3 readRoomThread syn wait before");
                    this.wait();
                    Log.d(TAG, "test3 readRoomThread syn wait after connect");
                } catch (InterruptedException e) {
                    /**
                     * こっちなら切断されました
                     */
                    Log.d(TAG, "test3 readRoomThread syn catch not connect interrupted");
                    return false;
                }
            }
        } else if (readAbstract instanceof ReadMainThread) {
            ReadMainThread readMainThread = (ReadMainThread) readAbstract;
//            readMainThread.setMyThread(this);
            synchronized (this) {
                try {
                    Log.d(TAG, "test3 readMainThread syn wait before");
                    this.wait();
                    Log.d(TAG, "test3 readMainThread syn wait after connect");
                } catch (InterruptedException e) {
                    /**
                     * こっちなら切断されました
                     */
                    Log.d(TAG, "test3 readMainThread syn catch not connect interrupted");
                    return false;
                }
            }
        }
        return true;
    }
}
