// test
package com.devmona;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.FrameLayout;

import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;

public class Calc {
    public static int widthDp = 71;
    public static int heightDp = 588;
    public static final float aaHiritsu = 0.20f;
    public static int aasizeDp;
    public static int aasizePixcel;
    public static int regularTopMargin;
    public static int putY;
    public static int charWholePixcel;

    /**
     * 80 / 660で求めたheightRegularの為の比率
     */
    public static float widthHiritsu = 0.12f;

    /**
     * 画面のwidth(pixel)
     */
    public static int width;

    /**
     * 画面のheight(pixel)
     */
    public static int height;

    /**
     * width - aasizePixcel で実際の動ける範囲
     */
    public static int widthRegular;

    /**
     * widthRegular * widthHiritsu で実際の動ける範囲
     */
    public static int heightRegular;
    public static int etcHeight;
    public static int actionBarHeight;

    public static void onWindowFocusChanged(FrameLayout wholeView) {
        width = wholeView.getWidth();
        height = wholeView.getHeight();
//        regularTopMargin = (int) (height / 2.2);
    }

    public static void updateSizeTate2() {
        aasizeDp = (int) (heightDp * aaHiritsu);
    }

    /**
     * @param monaId
     * @param x
     * @param y
     * @param scl
     * @return
     */
    public static int[] testCal(int monaId, int x, int y, int scl) {
        int androidY2 = CalPositionUtil.test1(Calc.heightDp - Calc.aasizeDp, x);
        int androidX2 = CalPositionUtil.test2((int) Calc.widthDp, y);

        /**
         * picをx座標0に置いたときのはみ出し分も考慮して画像サイズを開けた分の関数を出力した
         *
         * 次は限界値で、
         *
         *
         *
         *
         *
         */

        int cX2 = testX(androidX2, (int) Calc.widthDp, Calc.aasizeDp);
        int cY2 = testY(androidY2, (int) (Calc.heightDp - Calc.aasizeDp));

        int[] cx2cy2scl = new int[]{
                monaId,
                cX2,
                cY2,
                scl
        };
        return cx2cy2scl;
    }

    public static int testX(int x, int width08, float aasizeDp) {

        if (x <= 0) {
            x = 0;
        } else if (x > width08) {
            x = width08;
        }

        return x;
    }

    public static int testY(int y, int height08) {

        if (y <= 0) {
            y = 0;
        } else if (y > height08) {
            y = height08;
        }
        return y;
    }

    /**
     * pxに変換
     *
     * @param dp
     * @param context
     * @return
     */
    public static float Dp2Px(float dp, Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return dp * metrics.density;
    }

    /**
     * dpに変換する
     *
     * @param px
     * @param context
     * @return
     */
    public static float convertPx2Dp(int px, Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return px / metrics.density;
    }

    /**
     * 現行
     *
     * @param widthDp
     * @param cx2
     * @return
     */
    public static int convertTestX2(int widthDp, int cx2) {

        return -80 * cx2 / widthDp + 320;
    }

    /**
     * 現行
     *
     * @param m_height
     * @param cy2
     * @return
     */
    public static int convertTestY2(int m_height, int cy2) {
        return 660 * cy2 / m_height + 30;
    }

    /**
     * もなちゃとから受信したxをandroidXに変換する
     *
     * @param monaX
     * @param widthRegular
     * @return
     */
    public static int convertToRegularX(int monaX, int widthRegular) {
        return ((widthRegular * monaX) / 660) - (30 * widthRegular / 660);
    }

    /**
     * もなちゃとから受信したyをandroidYに変換する
     * もなちゃとyが240の場合,androidYは0になる
     *
     * @param max
     * @param y
     * @return
     */
    public static int convertToRegularY(int max, int y) {
        int maxY = max;
        return ((80 * y) / maxY) - (19200 / maxY);
    }

    /**
     * アンドロイドから出力したXをもなちゃとxに変換
     *
     * @param androidX
     * @param maxWidth
     * @return
     */
    public static int convertFromAndroidX(int androidX, int maxWidth) {
        return (660 * androidX / maxWidth) + 30;
    }

    /**
     * アンドロイドから出力したYをもなちゃとYに変換
     *
     * @param maxY
     * @param y
     * @return
     */
    public static int convertFromAndroidY(int maxY, int y) {
        return ((80 * y) / maxY) + 240;
    }

    /**
     * レギュラーに関わるpixelを取得する
     */
    public static void updatePixel() {
        aasizePixcel = (int) (width * aaHiritsu);

        //レキュラーwidthを取得する
        widthRegular = width - aasizePixcel;
        heightRegular = (int) (widthRegular * widthHiritsu);

    }

    public static int[] test2Cal(int tempMonaId, int tempX, int tempY, int tempScl) {
        tempX = Calc.limitX(tempX);
        int cxr = convertToRegularX(tempX, Calc.widthRegular);


        int cyr = convertToRegularY(tempY, Calc.heightRegular);


        return new int[]{
                tempMonaId,
                cxr,
                cyr,
                tempScl
        };
    }


    public static HashMap<String, Integer> calGetSetMap(String strId, String strX, String strY, String strScl) {

        HashMap<String, Integer> hashMap = new HashMap<>();

        int id = Integer.parseInt(strId);
        hashMap.put("id", id);
        if (strX == null || strX.equals("")) {
            strX = "100";
        }

        if (strY == null || strY.equals("")) {
            strY = "100";
        }


        int x;
        if (strX.contains(".")) {
            strX = strX.substring(0,strX.indexOf("."));
        }
        int y;
        if (strY.contains(".")) {
            strY = strY.substring(0,strY.indexOf("."));
        }

        try {
            x = Integer.parseInt(strX);
        } catch (NumberFormatException e) {
            String position16 = strX.replaceAll("0x", "");
            try {
                x = Integer.parseInt(position16, 16);
            } catch (NumberFormatException e1) {
                x = 100;
            }

        }
        hashMap.put("x", x);
        try {
            y = Integer.parseInt(strY);
        } catch (NumberFormatException e) {
            String position16 = strY.replaceAll("0x", "");
            try {
                y = Integer.parseInt(position16, 16);
            } catch (NumberFormatException e1) {
                y = 100;
            }
        }

        hashMap.put("y", y);
        int scl = test7(strScl);
        hashMap.put("scl", scl);

        return hashMap;
    }

    public static int test7(String strScl) {

        int result = 100;

        try {
            result = Integer.parseInt(strScl);
        } catch (NumberFormatException e) {
            Log.d("lineStopX", "sclChangeError値 :" + strScl);
            return 1;
        }

        if (result == 100) {
            return 1;
        } else if (result == -100) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * xの限界値
     * 30 > x のとき強制的に30にする.
     * 690 < xのとき強制的に690にする.
     *
     * @param x
     * @return
     */
    public static int limitX(int x) {
        if (30 > x) {
            x = 30;
        }

        if (690 < x) {
            x = 690;
        }
        return x;
    }

    public static int limitY(Integer y) {
        return 320;
    }
}
