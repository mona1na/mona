package com.devmona;

import java.io.Serializable;

/**
 * Created by sake on 2018/01/21.
 */

public class MainSetting implements Serializable {
    private float alpha;
    private int black;
    private int touchOk;
    private float alpha01;
    private int alphaSeek;
    private int blackSeek;

    public MainSetting() {
        alpha = 0;
        black = 255;
        touchOk = 1;
        alpha01 = 0;
        alphaSeek = 0;
        blackSeek = 0;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void setAlphaSeek(int alphaSeek) {
        this.alphaSeek = alphaSeek;
    }

    public void setBlackSeek(int blackSeek) {
        this.blackSeek = blackSeek;
    }

    public int getAlphaSeek() {
        return alphaSeek;
    }

    public int getBlackSeek() {
        return blackSeek;
    }

    public float getAlpha01() {
        return alpha01;
    }

    public void setAlpha01(float alpha01) {
        this.alpha01 = alpha01;
    }

    public float getAlpha() {
        return alpha;
    }

    public int getBlack() {
        return black;
    }


    public boolean getTouchOk() {
        if (this.touchOk == 1) {
            return true;
        } else if(this.touchOk == 0){
            return false;
        } else {
            throw new MyException("setErrorString");
        }
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public void setBlack(int black) {
        this.black = black;
    }

    public void setTouchOk(boolean touchOk) {
        if (touchOk == true) {
            this.touchOk = 1;
        } else if (touchOk == false){
            this.touchOk = 0;
        }
    }
}
