package com.devmona;

/**
 * Created by sake on 2017/06/18.
 */

public class MyException extends RuntimeException {
    public MyException(String error) {
        super(error);
    }
}
