package com.devmona;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Calendar;

/**
 * Created by sake on 2017/05/06.
 */

public class Util {
//
//    public static final int DIALOG_STATE1 = 1;
//    public static final int DIALOG_STATE2 = 2;
//    public static final int DIALOG_STATE3 = 3;
//    public static final int DIALOG_STATE4 = 4;
//    public static final int DIALOG_STATE5 = 5;
//    public static final int DIALOG_STATE6 = 6;
//    public static final int DIALOG_STATE7 = 7;
//    public static final int DIALOG_STATE8 = 8;
//    public static final int DIALOG_STATE9 = 9;
//    public static final int DIALOG_STATE10 = 10;
//    public static final int DIALOG_STATE11 = 11;
//    public static final int DIALOG_STATE12 = 12;
//    public static final int DIALOG_STATE13 = 13;
//    public static final int DIALOG_STATE14 = 14;
//    public static final int DIALOG_STATE15 = 15;
//    public static final int DIALOG_STATE16 = 16;
//    public static final int DIALOG_STATE17 = 17;
//    public static final int DIALOG_STATE18 = 18;
//
//    public static HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
//    public static HashMap<Integer, String> aatypeMap = new HashMap<Integer, String>();
////    public static HashMap<String, Integer> searchResNoFromAaname = new HashMap<String, Integer>();
//    public static HashMap<String, Bitmap> bitmapCanvasMap = new HashMap<>();
//    public static HashMap<String, Bitmap> bitmapOther = new HashMap<>();
//    public static int aaNowbtnId = 0;
////    public static int aaNowResNo;
////    public static int aaColor;
//
//    public static HashMap searchtypeAA;
//
//
//    /**
//     * ダイアログのページを取得し
//     * imageGroupで画像を設定
//     * changePixelsで色を変更する
//     *
//     * @param context
//     * @param state
//     * @param color
//     * @return
//     */
//
//    public static Bitmap[] dialogState(Context context, int state, int color) {
//
//        Bitmap[] bitmaps = null;
//        switch (state) {
//            case DIALOG_STATE1:
//                bitmaps = Util.imageGroup1(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE2:
//                bitmaps = Util.imageGroup2(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE3:
//                bitmaps = Util.imageGroup3(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE4:
//                bitmaps = Util.imageGroup4(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE5:
//                bitmaps = Util.imageGroup5(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE6:
//                bitmaps = Util.imageGroup6(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE7:
//                bitmaps = Util.imageGroup7(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE8:
//                bitmaps = Util.imageGroup8(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE9:
//                bitmaps = Util.imageGroup9(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE10:
//                bitmaps = Util.imageGroup10(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE11:
//                bitmaps = Util.imageGroup11(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE12:
//                bitmaps = Util.imageGroup12(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE13:
//                bitmaps = Util.imageGroup13(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE14:
//                bitmaps = Util.imageGroup14(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE15:
//                bitmaps = Util.imageGroup15(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE16:
//                bitmaps = Util.imageGroup16(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE17:
//                bitmaps = Util.imageGroup17(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//
//            case DIALOG_STATE18:
//                bitmaps = Util.imageGroup18(context);
//                bitmaps = Util.changePixels(bitmaps, color);
//                return bitmaps;
//        }
//        return bitmaps;
//    }
//
//
//    /**
//     * @param context
//     * @return
//     */
//    public static Bitmap[] imageGroup1(Context context) {
//
//
//        Bitmap[] bitmaps = new Bitmap[8];
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mona).copy(Bitmap.Config.ARGB_8888, true);
//
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.marumimi).copy(Bitmap.Config.ARGB_8888, true);
//
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.shodai).copy(Bitmap.Config.ARGB_8888, true);
//
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.oomimi).copy(Bitmap.Config.ARGB_8888, true);
//
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kuromimi).copy(Bitmap.Config.ARGB_8888, true);
//
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.polygon).copy(Bitmap.Config.ARGB_8888, true);
//
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.agemona).copy(Bitmap.Config.ARGB_8888, true);
//
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.anamona).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.mona);
//        map.put(R.id.aadialog_img2, R.mipmap.marumimi);
//        map.put(R.id.aadialog_img3, R.mipmap.shodai);
//        map.put(R.id.aadialog_img4, R.mipmap.oomimi);
//        map.put(R.id.aadialog_img5, R.mipmap.kuromimi);
//        map.put(R.id.aadialog_img6, R.mipmap.polygon);
//        map.put(R.id.aadialog_img7, R.mipmap.agemona);
//        map.put(R.id.aadialog_img8, R.mipmap.anamona);
//
//        aatypeMap.put(R.id.aadialog_img1, "mona");
//        aatypeMap.put(R.id.aadialog_img2, "marumimi");
//        aatypeMap.put(R.id.aadialog_img3, "shodai");
//        aatypeMap.put(R.id.aadialog_img4, "oomimi");
//        aatypeMap.put(R.id.aadialog_img5, "kuromimi");
//        aatypeMap.put(R.id.aadialog_img6, "polygon");
//        aatypeMap.put(R.id.aadialog_img7, "agemona");
//        aatypeMap.put(R.id.aadialog_img8, "anamona");
//
//
//        return bitmaps;
//    }
//
//
//    /**
//     * @param context
//     * @return
//     */
//    public static Bitmap[] imageGroup2(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mora).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.urara).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.fuun).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.iiajan).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.nida).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.gerara).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.sens).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.gari).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.mora);
//        map.put(R.id.aadialog_img2, R.mipmap.urara);
//        map.put(R.id.aadialog_img3, R.mipmap.fuun);
//        map.put(R.id.aadialog_img4, R.mipmap.iiajan);
//        map.put(R.id.aadialog_img5, R.mipmap.nida);
//        map.put(R.id.aadialog_img6, R.mipmap.gerara);
//        map.put(R.id.aadialog_img7, R.mipmap.sens);
//        map.put(R.id.aadialog_img8, R.mipmap.gari);
//
//
//        aatypeMap.put(R.id.aadialog_img1, "mora");
//        aatypeMap.put(R.id.aadialog_img2, "urara");
//        aatypeMap.put(R.id.aadialog_img3, "fuun");
//        aatypeMap.put(R.id.aadialog_img4, "iiajan");
//        aatypeMap.put(R.id.aadialog_img5, "nida");
//        aatypeMap.put(R.id.aadialog_img6, "gerara");
//        aatypeMap.put(R.id.aadialog_img7, "sens");
//        aatypeMap.put(R.id.aadialog_img8, "gari");
//
//        return bitmaps;
//    }
//
//    /**
//     * @param context
//     * @return
//     */
//    public static Bitmap[] imageGroup3(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.yamazaki1).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.yamazaki2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.boljoa3).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.boljoa4).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.puru).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mosamosa1).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mosamosa2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mosamosa3).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.yamazaki1);
//        map.put(R.id.aadialog_img2, R.mipmap.yamazaki2);
//        map.put(R.id.aadialog_img3, R.mipmap.boljoa3);
//        map.put(R.id.aadialog_img4, R.mipmap.boljoa4);
//        map.put(R.id.aadialog_img5, R.mipmap.puru);
//        map.put(R.id.aadialog_img6, R.mipmap.mosamosa1);
//        map.put(R.id.aadialog_img7, R.mipmap.mosamosa2);
//        map.put(R.id.aadialog_img8, R.mipmap.mosamosa3);
//
//        aatypeMap.put(R.id.aadialog_img1, "yamazaki1");
//        aatypeMap.put(R.id.aadialog_img2, "yamazaki2");
//        aatypeMap.put(R.id.aadialog_img3, "boljoa3");
//        aatypeMap.put(R.id.aadialog_img4, "boljoa4");
//        aatypeMap.put(R.id.aadialog_img5, "puru");
//        aatypeMap.put(R.id.aadialog_img6, "mosamosa1");
//        aatypeMap.put(R.id.aadialog_img7, "mosamosa2");
//        aatypeMap.put(R.id.aadialog_img8, "mosamosa3");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup4(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mosamosa4).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kamemona).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.oni).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.wachoi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.oniini).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tofu).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.niku).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.iyou).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.mosamosa4);
//        map.put(R.id.aadialog_img2, R.mipmap.kamemona);
//        map.put(R.id.aadialog_img3, R.mipmap.oni);
//        map.put(R.id.aadialog_img4, R.mipmap.wachoi);
//        map.put(R.id.aadialog_img5, R.mipmap.oniini);
//        map.put(R.id.aadialog_img6, R.mipmap.tofu);
//        map.put(R.id.aadialog_img7, R.mipmap.niku);
//        map.put(R.id.aadialog_img8, R.mipmap.iyou);
//
//        aatypeMap.put(R.id.aadialog_img1, "mosamosa4");
//        aatypeMap.put(R.id.aadialog_img2, "kamemona");
//        aatypeMap.put(R.id.aadialog_img3, "oni");
//        aatypeMap.put(R.id.aadialog_img4, "wachoi");
//        aatypeMap.put(R.id.aadialog_img5, "oniini");
//        aatypeMap.put(R.id.aadialog_img6, "tofu");
//        aatypeMap.put(R.id.aadialog_img7, "niku");
//        aatypeMap.put(R.id.aadialog_img8, "iyou");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup5(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ppa2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.foppa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.charhan).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.aramaki).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ichi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ichi2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ichineko).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hat2).copy(Bitmap.Config.ARGB_8888, true);
//
//
//        map.put(R.id.aadialog_img1, R.mipmap.ppa2);
//        map.put(R.id.aadialog_img2, R.mipmap.foppa);
//        map.put(R.id.aadialog_img3, R.mipmap.charhan);
//        map.put(R.id.aadialog_img4, R.mipmap.aramaki);
//        map.put(R.id.aadialog_img5, R.mipmap.ichi);
//        map.put(R.id.aadialog_img6, R.mipmap.ichi2);
//        map.put(R.id.aadialog_img7, R.mipmap.ichineko);
//        map.put(R.id.aadialog_img8, R.mipmap.hat2);
//
//        aatypeMap.put(R.id.aadialog_img1, "ppa2");
//        aatypeMap.put(R.id.aadialog_img2, "foppa");
//        aatypeMap.put(R.id.aadialog_img3, "charhan");
//        aatypeMap.put(R.id.aadialog_img4, "aramaki");
//        aatypeMap.put(R.id.aadialog_img5, "ichi");
//        aatypeMap.put(R.id.aadialog_img6, "ichi2");
//        aatypeMap.put(R.id.aadialog_img7, "ichineko");
//        aatypeMap.put(R.id.aadialog_img8, "hat2");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup6(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hati4).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hati).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hati3).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.remona).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.monaka).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.riru).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.alice).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.batu).copy(Bitmap.Config.ARGB_8888, true);
//
//
//        map.put(R.id.aadialog_img1, R.mipmap.hati4);
//        map.put(R.id.aadialog_img2, R.mipmap.hati);
//        map.put(R.id.aadialog_img3, R.mipmap.hati3);
//        map.put(R.id.aadialog_img4, R.mipmap.remona);
//        map.put(R.id.aadialog_img5, R.mipmap.monaka);
//        map.put(R.id.aadialog_img6, R.mipmap.riru);
//        map.put(R.id.aadialog_img7, R.mipmap.alice);
//        map.put(R.id.aadialog_img8, R.mipmap.batu);
//
//        aatypeMap.put(R.id.aadialog_img1, "hati4");
//        aatypeMap.put(R.id.aadialog_img2, "hati");
//        aatypeMap.put(R.id.aadialog_img3, "hati3");
//        aatypeMap.put(R.id.aadialog_img4, "remona");
//        aatypeMap.put(R.id.aadialog_img5, "monaka");
//        aatypeMap.put(R.id.aadialog_img6, "riru");
//        aatypeMap.put(R.id.aadialog_img7, "alice");
//        aatypeMap.put(R.id.aadialog_img8, "batu");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup7(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mina).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.miwa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kabin).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.giko).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tatigiko).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.zuza).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.sugoi3).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ging).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.mina);
//        map.put(R.id.aadialog_img2, R.mipmap.miwa);
//        map.put(R.id.aadialog_img3, R.mipmap.kabin);
//        map.put(R.id.aadialog_img4, R.mipmap.giko);
//        map.put(R.id.aadialog_img5, R.mipmap.tatigiko);
//        map.put(R.id.aadialog_img6, R.mipmap.zuza);
//        map.put(R.id.aadialog_img7, R.mipmap.sugoi3);
//        map.put(R.id.aadialog_img8, R.mipmap.ging);
//
//        aatypeMap.put(R.id.aadialog_img1, "mina");
//        aatypeMap.put(R.id.aadialog_img2, "miwa");
//        aatypeMap.put(R.id.aadialog_img3, "kabin");
//        aatypeMap.put(R.id.aadialog_img4, "giko");
//        aatypeMap.put(R.id.aadialog_img5, "tatigiko");
//        aatypeMap.put(R.id.aadialog_img6, "zuza");
//        aatypeMap.put(R.id.aadialog_img7, "sugoi3");
//        aatypeMap.put(R.id.aadialog_img8, "ging");
//
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup8(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.maturi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.usi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.nezumi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.abogado).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.bana).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.uma).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.sika).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kato).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.maturi);
//        map.put(R.id.aadialog_img2, R.mipmap.usi);
//        map.put(R.id.aadialog_img3, R.mipmap.nezumi);
//        map.put(R.id.aadialog_img4, R.mipmap.abogado);
//        map.put(R.id.aadialog_img5, R.mipmap.bana);
//        map.put(R.id.aadialog_img6, R.mipmap.uma);
//        map.put(R.id.aadialog_img7, R.mipmap.sika);
//        map.put(R.id.aadialog_img8, R.mipmap.kato);
//
//        aatypeMap.put(R.id.aadialog_img1, "maturi");
//        aatypeMap.put(R.id.aadialog_img2, "usi");
//        aatypeMap.put(R.id.aadialog_img3, "nezumi");
//        aatypeMap.put(R.id.aadialog_img4, "abogado");
//        aatypeMap.put(R.id.aadialog_img5, "bana");
//        aatypeMap.put(R.id.aadialog_img6, "uma");
//        aatypeMap.put(R.id.aadialog_img7, "sika");
//        aatypeMap.put(R.id.aadialog_img8, "kato");
//        return bitmaps;
//    }
//
//    //ここから
//    public static Bitmap[] imageGroup9(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.haka).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tokei).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.gyouza).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.papi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tibigiko).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.fusa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.suwarifusa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tibifusa).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.haka);
//        map.put(R.id.aadialog_img2, R.mipmap.tokei);
//        map.put(R.id.aadialog_img3, R.mipmap.gyouza);
//        map.put(R.id.aadialog_img4, R.mipmap.papi);
//        map.put(R.id.aadialog_img5, R.mipmap.tibigiko);
//        map.put(R.id.aadialog_img6, R.mipmap.fusa);
//        map.put(R.id.aadialog_img7, R.mipmap.suwarifusa);
//        map.put(R.id.aadialog_img8, R.mipmap.tibifusa);
//
//        aatypeMap.put(R.id.aadialog_img1, "haka");
//        aatypeMap.put(R.id.aadialog_img2, "tokei");
//        aatypeMap.put(R.id.aadialog_img3, "gyouza");
//        aatypeMap.put(R.id.aadialog_img4, "papi");
//        aatypeMap.put(R.id.aadialog_img5, "tibigiko");
//        aatypeMap.put(R.id.aadialog_img6, "fusa");
//        aatypeMap.put(R.id.aadialog_img7, "suwarifusa");
//        aatypeMap.put(R.id.aadialog_img8, "tibifusa");
//
//        return bitmaps;
//    }
//
//    //TODO これいらない
//    public static Bitmap[] imageGroup10(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.haka).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tokei).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.gyouza).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.papi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tibigiko).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.fusa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.suwarifusa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tibifusa).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.haka);
//        map.put(R.id.aadialog_img2, R.mipmap.tokei);
//        map.put(R.id.aadialog_img3, R.mipmap.gyouza);
//        map.put(R.id.aadialog_img4, R.mipmap.papi);
//        map.put(R.id.aadialog_img5, R.mipmap.tibigiko);
//        map.put(R.id.aadialog_img6, R.mipmap.fusa);
//        map.put(R.id.aadialog_img7, R.mipmap.suwarifusa);
//        map.put(R.id.aadialog_img8, R.mipmap.tibifusa);
//
//        aatypeMap.put(R.id.aadialog_img1, "haka");
//        aatypeMap.put(R.id.aadialog_img2, "tokei");
//        aatypeMap.put(R.id.aadialog_img3, "gyouza");
//        aatypeMap.put(R.id.aadialog_img4, "papi");
//        aatypeMap.put(R.id.aadialog_img5, "tibigiko");
//        aatypeMap.put(R.id.aadialog_img6, "fusa");
//        aatypeMap.put(R.id.aadialog_img7, "suwarifusa");
//        aatypeMap.put(R.id.aadialog_img8, "tibifusa");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup11(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.sii2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tibisii).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tuu).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hokkyoku6).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.jien).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kita).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.haa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.nyog).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.sii2);
//        map.put(R.id.aadialog_img2, R.mipmap.tibisii);
//        map.put(R.id.aadialog_img3, R.mipmap.tuu);
//        map.put(R.id.aadialog_img4, R.mipmap.hokkyoku6);
//        map.put(R.id.aadialog_img5, R.mipmap.jien);
//        map.put(R.id.aadialog_img6, R.mipmap.kita);
//        map.put(R.id.aadialog_img7, R.mipmap.haa);
//        map.put(R.id.aadialog_img8, R.mipmap.nyog);
//
//        aatypeMap.put(R.id.aadialog_img1, "sii2");
//        aatypeMap.put(R.id.aadialog_img2, "tibisii");
//        aatypeMap.put(R.id.aadialog_img3, "tuu");
//        aatypeMap.put(R.id.aadialog_img4, "hokkyoku6");
//        aatypeMap.put(R.id.aadialog_img5, "jien");
//        aatypeMap.put(R.id.aadialog_img6, "kita");
//        aatypeMap.put(R.id.aadialog_img7, "haa");
//        aatypeMap.put(R.id.aadialog_img8, "nyog");
//
//
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup12(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.gaku).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.shob).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.shak).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.boljoa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mouk).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mouk1).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mouk2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hikk).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.gaku);
//        map.put(R.id.aadialog_img2, R.mipmap.shob);
//        map.put(R.id.aadialog_img3, R.mipmap.shak);
//        map.put(R.id.aadialog_img4, R.mipmap.boljoa);
//        map.put(R.id.aadialog_img5, R.mipmap.mouk);
//        map.put(R.id.aadialog_img6, R.mipmap.mouk1);
//        map.put(R.id.aadialog_img7, R.mipmap.mouk2);
//        map.put(R.id.aadialog_img8, R.mipmap.hikk);
//
//        aatypeMap.put(R.id.aadialog_img1, "gaku");
//        aatypeMap.put(R.id.aadialog_img2, "shob");
//        aatypeMap.put(R.id.aadialog_img3, "shak");
//        aatypeMap.put(R.id.aadialog_img4, "boljoa");
//        aatypeMap.put(R.id.aadialog_img5, "mouk");
//        aatypeMap.put(R.id.aadialog_img6, "mouk1");
//        aatypeMap.put(R.id.aadialog_img7, "mouk2");
//        aatypeMap.put(R.id.aadialog_img8, "hikk");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup13(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.dokuo).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.dokuo2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hosh).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.siranaiwa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tiraneyo).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ginu).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.unko).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kasiwa).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.dokuo);
//        map.put(R.id.aadialog_img2, R.mipmap.dokuo2);
//        map.put(R.id.aadialog_img3, R.mipmap.hosh);
//        map.put(R.id.aadialog_img4, R.mipmap.siranaiwa);
//        map.put(R.id.aadialog_img5, R.mipmap.tiraneyo);
//        map.put(R.id.aadialog_img6, R.mipmap.ginu);
//        map.put(R.id.aadialog_img7, R.mipmap.unko);
//        map.put(R.id.aadialog_img8, R.mipmap.kasiwa);
//
//        aatypeMap.put(R.id.aadialog_img1, "dokuo");
//        aatypeMap.put(R.id.aadialog_img2, "dokuo2");
//        aatypeMap.put(R.id.aadialog_img3, "hosh");
//        aatypeMap.put(R.id.aadialog_img4, "siranaiwa");
//        aatypeMap.put(R.id.aadialog_img5, "tiraneyo");
//        aatypeMap.put(R.id.aadialog_img6, "ginu");
//        aatypeMap.put(R.id.aadialog_img7, "unko");
//        aatypeMap.put(R.id.aadialog_img8, "kasiwa");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup14(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kappappa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.asou).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.hiyoko).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.nin3).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kunoichi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.osa).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.cock).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.coc2).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.kappappa);
//        map.put(R.id.aadialog_img2, R.mipmap.asou);
//        map.put(R.id.aadialog_img3, R.mipmap.hiyoko);
//        map.put(R.id.aadialog_img4, R.mipmap.nin3);
//        map.put(R.id.aadialog_img5, R.mipmap.kunoichi);
//        map.put(R.id.aadialog_img6, R.mipmap.osa);
//        map.put(R.id.aadialog_img7, R.mipmap.cock);
//        map.put(R.id.aadialog_img8, R.mipmap.coc2);
//
//        aatypeMap.put(R.id.aadialog_img1, "kappappa");
//        aatypeMap.put(R.id.aadialog_img2, "asou");
//        aatypeMap.put(R.id.aadialog_img3, "hiyoko");
//        aatypeMap.put(R.id.aadialog_img4, "nin3");
//        aatypeMap.put(R.id.aadialog_img5, "kunoichi");
//        aatypeMap.put(R.id.aadialog_img6, "osa");
//        aatypeMap.put(R.id.aadialog_img7, "cock");
//        aatypeMap.put(R.id.aadialog_img8, "coc2");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup15(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ri_man).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.chichon).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.nanyo).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.tahara).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.maji).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kikko2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.sumaso2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.niraneko).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.ri_man);
//        map.put(R.id.aadialog_img2, R.mipmap.chichon);
//        map.put(R.id.aadialog_img3, R.mipmap.nanyo);
//        map.put(R.id.aadialog_img4, R.mipmap.tahara);
//        map.put(R.id.aadialog_img5, R.mipmap.maji);
//        map.put(R.id.aadialog_img6, R.mipmap.kikko2);
//        map.put(R.id.aadialog_img7, R.mipmap.sumaso2);
//        map.put(R.id.aadialog_img8, R.mipmap.niraneko);
//
//        aatypeMap.put(R.id.aadialog_img1, "ri_man");
//        aatypeMap.put(R.id.aadialog_img2, "chichon");
//        aatypeMap.put(R.id.aadialog_img3, "nanyo");
//        aatypeMap.put(R.id.aadialog_img4, "tahara");
//        aatypeMap.put(R.id.aadialog_img5, "maji");
//        aatypeMap.put(R.id.aadialog_img6, "kikko2");
//        aatypeMap.put(R.id.aadialog_img7, "sumaso2");
//        aatypeMap.put(R.id.aadialog_img8, "niraneko");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup16(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.niraime).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.niraime2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.niramusume).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.chotto1).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.chotto2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.chotto3).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.mossari).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.yokan).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.niraime);
//        map.put(R.id.aadialog_img2, R.mipmap.niraime2);
//        map.put(R.id.aadialog_img3, R.mipmap.niramusume);
//        map.put(R.id.aadialog_img4, R.mipmap.chotto1);
//        map.put(R.id.aadialog_img5, R.mipmap.chotto2);
//        map.put(R.id.aadialog_img6, R.mipmap.chotto3);
//        map.put(R.id.aadialog_img7, R.mipmap.mossari);
//        map.put(R.id.aadialog_img8, R.mipmap.yokan);
//
//        aatypeMap.put(R.id.aadialog_img1, "niraime");
//        aatypeMap.put(R.id.aadialog_img2, "niraime2");
//        aatypeMap.put(R.id.aadialog_img3, "niramusume");
//        aatypeMap.put(R.id.aadialog_img4, "chotto1");
//        aatypeMap.put(R.id.aadialog_img5, "chotto2");
//        aatypeMap.put(R.id.aadialog_img6, "chotto3");
//        aatypeMap.put(R.id.aadialog_img7, "mossari");
//        aatypeMap.put(R.id.aadialog_img8, "yokan");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup17(Context context) {
//        Bitmap[] bitmaps = new Bitmap[8];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.koit).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.koya).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ranta).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kyaku).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.taxi).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.moudamepo).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[6] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.sai).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[7] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.shaitama).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.koit);
//        map.put(R.id.aadialog_img2, R.mipmap.koya);
//        map.put(R.id.aadialog_img3, R.mipmap.ranta);
//        map.put(R.id.aadialog_img4, R.mipmap.kyaku);
//        map.put(R.id.aadialog_img5, R.mipmap.taxi);
//        map.put(R.id.aadialog_img6, R.mipmap.moudamepo);
//        map.put(R.id.aadialog_img7, R.mipmap.sai);
//        map.put(R.id.aadialog_img8, R.mipmap.shaitama);
//
//        aatypeMap.put(R.id.aadialog_img1, "koit");
//        aatypeMap.put(R.id.aadialog_img2, "koya");
//        aatypeMap.put(R.id.aadialog_img3, "ranta");
//        aatypeMap.put(R.id.aadialog_img4, "kyaku");
//        aatypeMap.put(R.id.aadialog_img5, "taxi");
//        aatypeMap.put(R.id.aadialog_img6, "moudamepo");
//        aatypeMap.put(R.id.aadialog_img7, "sai");
//        aatypeMap.put(R.id.aadialog_img8, "shaitama");
//        return bitmaps;
//    }
//
//    public static Bitmap[] imageGroup18(Context context) {
//        Bitmap[] bitmaps = new Bitmap[6];
//
//        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.welneco2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.kagami).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.gakuri).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.unknown2).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.sira).copy(Bitmap.Config.ARGB_8888, true);
//        bitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.mipmap.zonu).copy(Bitmap.Config.ARGB_8888, true);
//
//        map.put(R.id.aadialog_img1, R.mipmap.welneco2);
//        map.put(R.id.aadialog_img2, R.mipmap.kagami);
//        map.put(R.id.aadialog_img3, R.mipmap.gakuri);
//        map.put(R.id.aadialog_img4, R.mipmap.unknown2);
//        map.put(R.id.aadialog_img5, R.mipmap.sira);
//        map.put(R.id.aadialog_img6, R.mipmap.zonu);
//
//        aatypeMap.put(R.id.aadialog_img1, "welneco2");
//        aatypeMap.put(R.id.aadialog_img2, "kagami");
//        aatypeMap.put(R.id.aadialog_img3, "gakuri");
//        aatypeMap.put(R.id.aadialog_img4, "unknown2");
//        aatypeMap.put(R.id.aadialog_img5, "sira");
//        aatypeMap.put(R.id.aadialog_img6, "zonu");
//        return bitmaps;
//    }
//
//

    /**
     * 元の画像:
     * 透明部分（外）0 255 255 255
     * キャラ（中） 255 255 255 255
     * 枠灰色      255 150 150 150
     * 黒色        255  0   0   0
     *
     * @param bitmaps
     * @param changeColor
     * @return
     */
    public static Bitmap[] changePixels(Bitmap[] bitmaps, int changeColor) {

        for (int i = 0; i < bitmaps.length; i++) {
            int width = bitmaps[i].getWidth();
            int height = bitmaps[i].getHeight();

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int pixcel = bitmaps[i].getPixel(x, y);

                    int a = Color.alpha(pixcel);
                    int r = Color.red(pixcel);
                    int g = Color.green(pixcel);
                    int b = Color.blue(pixcel);

                    if (a == 0 && r == 0 && g == 0 && b == 0) {
                        bitmaps[i].setPixel(x, y, 0x00FFFFFF);
                    } else if (a == 255 && r == 255 && g == 255 && b == 255) {
                        bitmaps[i].setPixel(x, y, changeColor);
                    } else if (a == 255 && r == 150 && g == 150 && b == 150) {
                        bitmaps[i].setPixel(x, y, 0xFF646464);
                    } else if (a == 255 && r == 0 && g == 0 && b == 0) {
                        bitmaps[i].setPixel(x, y, 0xFF000000);
                    }

                }
            }
        }

        return bitmaps;

    }

    /**
     * 元の画像:
     * 透明部分（外）0 255 255 255
     * キャラ（中） 255 255 255 255
     * 枠灰色      255 150 150 150
     * 黒色        255  0   0   0
     *
     * @param bitmap
     * @param color
     * @return
     */

    public static Bitmap getChangeColorBitmap(Bitmap bitmap, int color) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int changeColor = Color.argb(Color.alpha(color), Color.red(color), Color.green(color), Color.blue(color));

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int pixcel = bitmap.getPixel(x, y);

                int a = Color.alpha(pixcel);
                int r = Color.red(pixcel);
                int g = Color.green(pixcel);
                int b = Color.blue(pixcel);

                if (a == 0 && r == 0 && g == 0 && b == 0) {
//                    bitmap.setPixel(x, y, 0x00FFFFFF);
                    bitmap.setPixel(x, y, 0x00000000);
                } else if (a == 255 && r == 255 && g == 255 && b == 255) {
                    bitmap.setPixel(x, y, changeColor);
                } else if (a == 255 && r == 150 && g == 150 && b == 150) {
                    bitmap.setPixel(x, y, 0xFF646464);
                } else if (a == 255 && r == 0 && g == 0 && b == 0) {
                    bitmap.setPixel(x, y, 0xFF000000);
                }

//                if (red == 0 && green == 0 && blue == 255) {
//                    bitmap.setPixel(x, y, 0xFFFFFFFF);
//                } else if (red == 255 && green == 0 && blue == 0) {
//                    bitmap.setPixel(x, y, changeColor);
//                } else {
//                    bitmap.setPixel(x, y, 0xFF000000);
//                }
            }
        }

        return bitmap;
    }

    /**
     * @param context
     * @param resource
     * @param color
     * @return
     */
    public static Bitmap getChangeColorResource(Context context, int resource, int color) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resource).copy(Bitmap.Config.ARGB_8888, true);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int pixcel = bitmap.getPixel(x, y);

                int a = Color.alpha(pixcel);
                int r = Color.red(pixcel);
                int g = Color.green(pixcel);

                int b = Color.blue(pixcel);


                if (r == 0 && g == 0 && b == 255) {
                    bitmap.setPixel(x, y, 0xFFFFFFFF);
                } else if (r == 255 && g == 0 && b == 0) {
                    bitmap.setPixel(x, y, color);
                } else {
                    bitmap.setPixel(x, y, 0xFF000000);
                }
            }
        }

        return bitmap;
    }


    public static ImageView[] setImages(ImageView[] ImageViews, Bitmap[] bitmaps) {
        for (int i = 0; i < bitmaps.length; i++) {
            ImageViews[i].setImageBitmap(bitmaps[i]);

        }
        return ImageViews;
    }

    public static ImageButton setImage(ImageButton imageButton, Bitmap bitmap) {

        imageButton.setImageBitmap(bitmap);

        return imageButton;
    }

    //
//
//
////    public static Button[] roomBtnsCreate(RoomsActivity m_roomsActivity, Button[] btns) {
////
////
////
////        return btns;
////    }
//
//    public static Button[] setRoomNames(Button[] btns, HashMap<Integer, Room> roomsMap, RoomsActivity roomsActivity) {
//        btns[0].setText(roomsMap.get(R.id.roomsActivity_monachat1).getRoomName());
//        btns[1].setText(roomsMap.get(R.id.roomsActivity_monachat2).getRoomName());
//        btns[2].setText(roomsMap.get(R.id.roomsActivity_monachat3).getRoomName());
//        btns[3].setText(roomsMap.get(R.id.roomsActivity_monachat4).getRoomName());
//        btns[4].setText(roomsMap.get(R.id.roomsActivity_monachat5).getRoomName());
//        btns[5].setText(roomsMap.get(R.id.roomsActivity_monachat6).getRoomName());
//        btns[6].setText(roomsMap.get(R.id.roomsActivity_monachat7).getRoomName());
//        btns[7].setText(roomsMap.get(R.id.roomsActivity_monachat8).getRoomName());
//        btns[8].setText(roomsMap.get(R.id.roomsActivity_monachat9).getRoomName());
//        btns[9].setText(roomsMap.get(R.id.roomsActivity_monachat10).getRoomName());
//        btns[10].setText(roomsMap.get(R.id.roomsActivity_monachat11).getRoomName());
//        btns[11].setText(roomsMap.get(R.id.roomsActivity_monachat12).getRoomName());
//        btns[12].setText(roomsMap.get(R.id.roomsActivity_monachat13).getRoomName());
//        btns[13].setText(roomsMap.get(R.id.roomsActivity_monachat14).getRoomName());
//        btns[14].setText(roomsMap.get(R.id.roomsActivity_monachat15).getRoomName());
//        btns[15].setText(roomsMap.get(R.id.roomsActivity_monachat16).getRoomName());
//        btns[16].setText(roomsMap.get(R.id.roomsActivity_monachat17).getRoomName());
//        btns[17].setText(roomsMap.get(R.id.roomsActivity_monachat18).getRoomName());
//        btns[18].setText(roomsMap.get(R.id.roomsActivity_monachat19).getRoomName());
//        btns[19].setText(roomsMap.get(R.id.roomsActivity_monachat20).getRoomName());
//        btns[20].setText(roomsMap.get(R.id.roomsActivity_monachat21).getRoomName());
//        btns[21].setText(roomsMap.get(R.id.roomsActivity_monachat22).getRoomName());
//        btns[22].setText(roomsMap.get(R.id.roomsActivity_monachat23).getRoomName());
//        btns[23].setText(roomsMap.get(R.id.roomsActivity_monachat24).getRoomName());
//        btns[24].setText(roomsMap.get(R.id.roomsActivity_monachat25).getRoomName());
//        btns[25].setText(roomsMap.get(R.id.roomsActivity_monachat26).getRoomName());
//        btns[26].setText(roomsMap.get(R.id.roomsActivity_monachat27).getRoomName());
//        btns[27].setText(roomsMap.get(R.id.roomsActivity_monachat28).getRoomName());
//        btns[28].setText(roomsMap.get(R.id.roomsActivity_monachat29).getRoomName());
//        btns[29].setText(roomsMap.get(R.id.roomsActivity_monachat30).getRoomName());
//        btns[30].setText(roomsMap.get(R.id.roomsActivity_monachat31).getRoomName());
//        btns[31].setText(roomsMap.get(R.id.roomsActivity_monachat32).getRoomName());
//        btns[32].setText(roomsMap.get(R.id.roomsActivity_monachat33).getRoomName());
//        btns[33].setText(roomsMap.get(R.id.roomsActivity_monachat34).getRoomName());
//        btns[34].setText(roomsMap.get(R.id.roomsActivity_monachat35).getRoomName());
//        btns[35].setText(roomsMap.get(R.id.roomsActivity_monachat36).getRoomName());
//        btns[36].setText(roomsMap.get(R.id.roomsActivity_monachat37).getRoomName());
//        btns[37].setText(roomsMap.get(R.id.roomsActivity_monachat38).getRoomName());
//        btns[38].setText(roomsMap.get(R.id.roomsActivity_monachat39).getRoomName());
//        btns[39].setText(roomsMap.get(R.id.roomsActivity_monachat40).getRoomName());
//        btns[40].setText(roomsMap.get(R.id.roomsActivity_monachat41).getRoomName());
//        btns[41].setText(roomsMap.get(R.id.roomsActivity_monachat42).getRoomName());
//        btns[42].setText(roomsMap.get(R.id.roomsActivity_monachat43).getRoomName());
//        btns[43].setText(roomsMap.get(R.id.roomsActivity_monachat44).getRoomName());
//        btns[44].setText(roomsMap.get(R.id.roomsActivity_monachat45).getRoomName());
//        btns[45].setText(roomsMap.get(R.id.roomsActivity_monachat46).getRoomName());
//        btns[46].setText(roomsMap.get(R.id.roomsActivity_monachat47).getRoomName());
//        btns[47].setText(roomsMap.get(R.id.roomsActivity_monachat48).getRoomName());
//        btns[48].setText(roomsMap.get(R.id.roomsActivity_monachat49).getRoomName());
//        btns[49].setText(roomsMap.get(R.id.roomsActivity_monachat50).getRoomName());
//        btns[50].setText(roomsMap.get(R.id.roomsActivity_monachat51).getRoomName());
//        btns[51].setText(roomsMap.get(R.id.roomsActivity_monachat52).getRoomName());
//        btns[52].setText(roomsMap.get(R.id.roomsActivity_monachat53).getRoomName());
//        btns[53].setText(roomsMap.get(R.id.roomsActivity_monachat54).getRoomName());
//        btns[54].setText(roomsMap.get(R.id.roomsActivity_monachat55).getRoomName());
//        btns[55].setText(roomsMap.get(R.id.roomsActivity_monachat56).getRoomName());
//        btns[56].setText(roomsMap.get(R.id.roomsActivity_monachat57).getRoomName());
//        btns[57].setText(roomsMap.get(R.id.roomsActivity_monachat58).getRoomName());
//        btns[58].setText(roomsMap.get(R.id.roomsActivity_monachat59).getRoomName());
//        btns[59].setText(roomsMap.get(R.id.roomsActivity_monachat60).getRoomName());
//        btns[60].setText(roomsMap.get(R.id.roomsActivity_monachat61).getRoomName());
//        btns[61].setText(roomsMap.get(R.id.roomsActivity_monachat62).getRoomName());
//        btns[62].setText(roomsMap.get(R.id.roomsActivity_monachat63).getRoomName());
//        btns[63].setText(roomsMap.get(R.id.roomsActivity_monachat64).getRoomName());
//        btns[64].setText(roomsMap.get(R.id.roomsActivity_monachat65).getRoomName());
//        btns[65].setText(roomsMap.get(R.id.roomsActivity_monachat66).getRoomName());
//        btns[66].setText(roomsMap.get(R.id.roomsActivity_monachat67).getRoomName());
//        btns[67].setText(roomsMap.get(R.id.roomsActivity_monachat68).getRoomName());
//        btns[68].setText(roomsMap.get(R.id.roomsActivity_monachat69).getRoomName());
//        btns[69].setText(roomsMap.get(R.id.roomsActivity_monachat70).getRoomName());
//        btns[70].setText(roomsMap.get(R.id.roomsActivity_monachat71).getRoomName());
//        btns[71].setText(roomsMap.get(R.id.roomsActivity_monachat72).getRoomName());
//        btns[72].setText(roomsMap.get(R.id.roomsActivity_monachat73).getRoomName());
//        btns[73].setText(roomsMap.get(R.id.roomsActivity_monachat74).getRoomName());
//        btns[74].setText(roomsMap.get(R.id.roomsActivity_monachat75).getRoomName());
//        btns[75].setText(roomsMap.get(R.id.roomsActivity_monachat76).getRoomName());
//        btns[76].setText(roomsMap.get(R.id.roomsActivity_monachat77).getRoomName());
//        btns[77].setText(roomsMap.get(R.id.roomsActivity_monachat78).getRoomName());
//        btns[78].setText(roomsMap.get(R.id.roomsActivity_monachat79).getRoomName());
//        btns[79].setText(roomsMap.get(R.id.roomsActivity_monachat80).getRoomName());
//        btns[80].setText(roomsMap.get(R.id.roomsActivity_monachat81).getRoomName());
//        btns[81].setText(roomsMap.get(R.id.roomsActivity_monachat82).getRoomName());
//        btns[82].setText(roomsMap.get(R.id.roomsActivity_monachat83).getRoomName());
//        btns[83].setText(roomsMap.get(R.id.roomsActivity_monachat84).getRoomName());
//        btns[84].setText(roomsMap.get(R.id.roomsActivity_monachat85).getRoomName());
//        btns[85].setText(roomsMap.get(R.id.roomsActivity_monachat86).getRoomName());
//        btns[86].setText(roomsMap.get(R.id.roomsActivity_monachat87).getRoomName());
//        btns[87].setText(roomsMap.get(R.id.roomsActivity_monachat88).getRoomName());
//        btns[88].setText(roomsMap.get(R.id.roomsActivity_monachat89).getRoomName());
//        btns[89].setText(roomsMap.get(R.id.roomsActivity_monachat90).getRoomName());
//        btns[90].setText(roomsMap.get(R.id.roomsActivity_monachat91).getRoomName());
//        btns[91].setText(roomsMap.get(R.id.roomsActivity_monachat92).getRoomName());
//        btns[92].setText(roomsMap.get(R.id.roomsActivity_monachat93).getRoomName());
//        btns[93].setText(roomsMap.get(R.id.roomsActivity_monachat94).getRoomName());
//        btns[94].setText(roomsMap.get(R.id.roomsActivity_monachat95).getRoomName());
//        btns[95].setText(roomsMap.get(R.id.roomsActivity_monachat96).getRoomName());
//        btns[96].setText(roomsMap.get(R.id.roomsActivity_monachat97).getRoomName());
//        btns[97].setText(roomsMap.get(R.id.roomsActivity_monachat98).getRoomName());
//        btns[98].setText(roomsMap.get(R.id.roomsActivity_monachat99).getRoomName());
//        btns[99].setText(roomsMap.get(R.id.roomsActivity_monachat100).getRoomName());
//
//        for (int i = 0; i < 100; i++) {
//            btns[i].setTextSize(roomsActivity.getResources().getDimensionPixelSize(R.dimen.cmn_size9));
//        }
//
//        return btns;
//    }
//
//
//
//    public static void setBtnsMargin(RoomsActivity roomsActivity, Button[] btns, float dimension) {
//
//    }
//
    public static String getRoomName(String name) {

        name = name.replaceAll("\n", "");
        return name;
    }

    public static String getDate() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int week = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        calendar.getTime();

//        String result = "" + year + "年" + month + "月" + day + "日　" + hour + "時" + minute + "分";
        String result = hour + "時" + minute + "分" + second + "秒";
        return result;
    }

    /**
     * android用に100を255に変換する
     *
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public static int change100to255(int red, int green, int blue) {

        float resultRed = 255f / 100f * red;
        float resultGreen = 255f / 100f * green;
        float resultBlue = 255f / 100f * blue;
        int resultR = (int) resultRed;
        int resultG = (int) resultGreen;
        int resultB = (int) resultBlue;
        int result = Color.argb(255, resultR, resultG, resultB);
        return result;
    }

    /**
     * android用に100を255に変換する
     *
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public static int change100toHukidashi(int red, int green, int blue) {
        int resultR = (int) Math.floor((red + 150) / 2.5);
        int resultG = (int) Math.floor((green + 150) / 2.5);
        int resultB = (int) Math.floor((blue + 150) / 2.5);
        return change100to255(resultR, resultG, resultB);
    }

    public static int[] change255to100(int red, int green, int blue) {

        int[] result = new int[3];
        float henkanowariai = 100f / 255f;
        result[0] = (int) (henkanowariai * red);
        result[1] = (int) (henkanowariai * green);
        result[2] = (int) (henkanowariai * blue);
        return result;
    }


    public static Bitmap test10(Context context, int aaMipMapId, int size) {

        BitmapFactory.Options imOptions = new BitmapFactory.Options();

        imOptions.inSampleSize = size;
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), aaMipMapId, imOptions);

        return bitmap;
    }

    public static int inverteColor(int color255) {
        return (0xFFFFFF - color255) | 0xFF000000;
//        return  0xFFFFFF - color255;
    }

//    public static Bitmap resize(Bitmap bitmap) {
//
//        SentakuDB sentakuDB = new SentakuDB();
//        SizeTate sizeTate = sentakuDB.getSizeTate();
//        int witdh08 = sizeTate.getWidth08();
//
//        /**
//         * 画像のsizeを取得する
//         */
//        float picSize = witdh08 * 0.1f;
//        float bai = picSize / bitmap.getWidth();
//        Matrix matrix = new Matrix();
//        matrix.postScale(bai, bai);
//
//        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
//        return bitmap;
//    }

//    /**
//     * キャンバス描画用のbitmapをbitmapCanvasMapを取得する
//     *
//     * @param strType
//     * @return
//     */
//    public static synchronized Bitmap bitmapfromType(String strType) {
//
//        Bitmap bitmap = null;
//        try {
//            bitmap = Util.bitmapCanvasMap.get(strType).copy(Bitmap.Config.ARGB_8888, true);
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//            if (bitmap == null) {
//                bitmap = Util.bitmapCanvasMap.get("mona").copy(Bitmap.Config.ARGB_8888, true);
//            }
//        }
//        return bitmap;
//    }

//    /**
//     * line用のbitmapを取得する
//     * @param strType
//     * @return
//     */
//    public static synchronized Bitmap bitmapfromType2(String strType) {
//        Bitmap bitmap = null;
//        try {
//            bitmap = Util.bitmapOther.get(strType).copy(Bitmap.Config.ARGB_8888, true);
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//            if (bitmap == null) {
//                bitmap = Util.bitmapOther.get("mona").copy(Bitmap.Config.ARGB_8888, true);
//            }
//        }
//        return bitmap;
//    }

//    public static void testCharacterGenerate(Context context) {
//        Util.bitmapCanvasMap.put("mona", Util.resize(BitmapFactory.decodeResource(context.getResources(), R.mipmap.mona).copy(Bitmap.Config.ARGB_8888, true)));
//    }
//
//    public static Bitmap resize2(Bitmap bitmap, int dpi) {
//        SentakuDB sentakuDB = new SentakuDB();
//        SizeTate sizeTate = sentakuDB.getSizeTate();
//        int witdh08 = sizeTate.getWidth08();
//
//        float sizex = witdh08 / 910f;
////        float sizey = 145f / f * density;
////        float realWidth = (float) (bitmap.getWidth() * CalPositionUtil.canvasRealWidth) / 910;
////        float width = bitmap.getWidth();
////        float size = CalPositionUtil.canvasRealWidth / 660f;
////        float size2 = CalPositionUtil.canvasRealheight /80f;
////        float size =(float) 0.08;
//
//        float baisuu = (float) dpi / 240f;
//
//
//        Matrix matrix = new Matrix();
//        matrix.postScale(sizex, sizex);
//
//        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
//        return bitmap;
//    }
}
