package com.devmona;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFcmListenerService extends FirebaseMessagingService {

    private final static String TAG = "MyFcmListenerService";
    public static int id = 1;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d(TAG, "onNewToken:" + s);
        TestShared.getInstance(getApplicationContext()).setToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "onMessageReceived:" + remoteMessage);

//        String from = remoteMessage.getFrom();
//        Map data = remoteMessage.getData();

//        for (int i = 0; i < data.size(); i++) {
//            Log.d("","");
//        }


//        Log.d(TAG, "from:" + from);
//        Log.d(TAG, "data:" + data.toString());

//        if (data.size() == 0) {
//            return;
//        }

//        String msg = data.get("data").toString();
        Log.d("testReceive", "onMessageReceived");
//        sendNotification("aiueo");
        sendNotification("aaaa");
//        sendN2();
    }

    private void sendN2() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelId = "updates"; // 通知チャンネルのIDにする任意の文字列
            String name = "更新情報"; // 通知チャンネル名
            int importance = NotificationManager.IMPORTANCE_HIGH; // デフォルトの重要度
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription("通知チャンネルの説明"); // 必須ではない

// 通知チャンネルの設定のデフォルト値。設定必須ではなく、ユーザーが変更可能。
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableVibration(true);
            channel.enableLights(true);
            channel.setLightColor(Color.argb(255, 255, 255, 255));
//            channel.setSound(uri, audioAttributes);
            channel.setShowBadge(false); // ランチャー上でアイコンバッジを表示するかどうか

// NotificationManagerCompatにcreateNotificationChannel()は無い。
            NotificationManager nm = getSystemService(NotificationManager.class);
            nm.createNotificationChannel(channel);
            NotificationCompat.Builder builder
                    = new NotificationCompat.Builder(getApplicationContext(), channelId)
                    .setContentTitle("タイトル")
                    .setContentText("テキスト")
                    .setSmallIcon(R.mipmap.mona_icon_kadomaru2);
            NotificationManagerCompat.from(getApplicationContext()).notify(id, builder.build());
        }
    }

    private void sendTest() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "" + id++;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
        notification.setAutoCancel(true);
        notification.setContentTitle("testHello");
//                notification.setSubText("aiueo");
//                notification.setContentIntent(pi);
//        notification.setLargeIcon();
//        getBitmap(myMain3Activity,R.drawable.ic_svg_mona);

        notification.setSmallIcon(R.mipmap.mona_icon_kadomaru2);
        notification.build();
    }

    private void sendNotification(String message) {
        Log.d("testReceive", "ok");
        Intent intent = new Intent(this, Base2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Push通知のタイトル")
                .setSubText("Push通知のサブタイトル")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
