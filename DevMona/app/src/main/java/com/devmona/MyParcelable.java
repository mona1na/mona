package com.devmona;

import android.os.Parcel;
import android.os.Parcelable;

public class MyParcelable implements Parcelable {

    private String name;
    private int age;
    private String address;

    protected MyParcelable(Parcel in) {
        name = in.readString();
        age = in.readInt();
        address = in.readString();
    }

    public static final Creator<MyParcelable> CREATOR = new Creator<MyParcelable>() {
        @Override
        public MyParcelable createFromParcel(Parcel in) {
            return new MyParcelable(in);
        }

        @Override
        public MyParcelable[] newArray(int size) {
            return new MyParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(name);
        out.writeInt(age);
        out.writeString(address);
    }

    public MyParcelable(String name, int age, String address) {
        this.name = name;
        this.age  = age;
        this.address = address;
    }

}
