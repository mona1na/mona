package com.devmona;

import androidx.viewpager.widget.ViewPager;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Space;

/**
 * Created by sake on 2018/06/18.
 */

public class Main3PagerListener extends ViewPager.SimpleOnPageChangeListener {

    private final LinearLayout linearLayout;
    private final EditText editText;
    private final Space space;
    private final Button button;
    private final Main4Fragment main4Fragment;
    private final Drawable mLinearBack;
    private final ColorStateList mEditTextTextColors;
    private final Drawable mEditTextBack;
    private final Drawable mSpageBack;
    private final ColorStateList mButtonTextColors;
    private final Drawable mButtonBack;

    public Main3PagerListener(Main4Fragment main4Fragment) {
        this.main4Fragment = main4Fragment;
        View view = main4Fragment.getView();
        linearLayout =(LinearLayout) view.findViewById(R.id.main3_linear);
        mLinearBack = linearLayout.getBackground();
        editText = (EditText) view.findViewById(R.id.edt_main3_com);
        mEditTextBack = editText.getBackground();
        mEditTextTextColors = editText.getTextColors();
        space = (Space) view.findViewById(R.id.main3_space);
        mSpageBack = space.getBackground();
        button = (Button) view.findViewById(R.id.btn_main3_send_comment);
        mButtonTextColors = button.getTextColors();
        mButtonBack = button.getBackground();
    }

    @Override
    public void onPageSelected(int position) {
        super.onPageSelected(position);
        int page = position;
        if (page == 0) {
//            linearLayout.setBackgroundColor(main4Fragment.getResources().getColor(R.color.commonWhite));
//            editText.setBackground(main4Fragment.getResources().getDrawable(R.drawable.frame_tate2_edit_btn));
//            editText.setTextColor(main4Fragment.getResources().getColor(R.color.commonBlack));
//            space.setBackgroundColor(main4Fragment.getResources().getColor(R.color.commonWhite));
//            button.setBackground(main4Fragment.getResources().getDrawable(R.drawable.frame_tate2_edit_btn));
//            button.setTextColor(main4Fragment.getResources().getColor(R.color.commonBlack));

            linearLayout.setBackground(mLinearBack);
            editText.setBackground(mEditTextBack);
            editText.setTextColor(mEditTextTextColors);
            space.setBackground(mSpageBack);
            button.setBackground(mButtonBack);
            button.setTextColor(mButtonTextColors);
        } else if(page == 1) {
            linearLayout.setBackgroundColor(main4Fragment.getResources().getColor(R.color.commonBlack));
            editText.setBackgroundColor(main4Fragment.getResources().getColor(R.color.commonBlack));
            editText.setTextColor(main4Fragment.getResources().getColor(R.color.commonWhite));
            space.setBackgroundColor(main4Fragment.getResources().getColor(R.color.commonBlack));
            button.setBackgroundColor(main4Fragment.getResources().getColor(R.color.commonBlack));
            button.setTextColor(main4Fragment.getResources().getColor(R.color.commonGray));
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        super.onPageScrolled(position, positionOffset, positionOffsetPixels);
    }
}
