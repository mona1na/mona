package com.devmona;

import java.io.Serializable;

/**
 * Created by sake on 2017/12/02.
 */

public class DBStatus implements Serializable{
    private int sLoginId;
    private int sPId;
    private int sStartId;
    private int sRoomId;
    private int sCmtId;

    private int frommid;
    private int tomid;

    public int getsCmtId() {
        return sCmtId;
    }

    public void setsCmtId(int sCmtId) {
        this.sCmtId = sCmtId;
    }

    public void setsRoomId(int sRoomId) {
        this.sRoomId = sRoomId;
    }

    public int getsRoomId() {
        return sRoomId;
    }

    public int getFrommid() {
        return frommid;
    }

    public void setFrommid(int frommid) {
        this.frommid = frommid;
    }

    public int getTomid() {
        return tomid;
    }

    public void setTomid(int tomid) {
        this.tomid = tomid;
    }

    public void setsLoginId(int sLoginId) {
        this.sLoginId = sLoginId;
    }

    public void setsStartId(int sStartId) {
        this.sStartId = sStartId;
    }

    public void setsPId(int sPId) {
        this.sPId = sPId;
    }

    public int getsLoginId() {
        return sLoginId;
    }

    public int getsStartId() {
        return sStartId;
    }

    public int getsPId() {
        return sPId;
    }
}
