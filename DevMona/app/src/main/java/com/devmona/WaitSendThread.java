package com.devmona;

import android.content.Context;
import android.util.Log;

public class WaitSendThread extends Thread {


    private Context context;
    private MyThread myThread;
    private static final String TAG = "WaitSendThread";

    public WaitSendThread(MyThread myThread, Context context) {
        this.myThread = myThread;
        this.context = context;
    }

    @Override
    public void run() {
        super.run();
        Log.d(TAG, "run");
        boolean check = true;
        do {
            if (myThread.getState() == State.WAITING || myThread.getState() == State.TIMED_WAITING) {
                Log.d(TAG, "myThread is waiting");
                TestConnect testConnect = TestConnect.getInstance(context);
                testConnect.sendTest(context);
                testConnect.sendRoom(context);
                break;
            } else {
                Log.d(TAG, "myThread is running");
            }
        } while (check);
    }
}
