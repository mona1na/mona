package com.devmona;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;

import androidx.preference.PreferenceScreen;
import androidx.preference.SeekBarPreference;
import androidx.preference.SwitchPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * Created by sake on 2017/08/20.
 */

public class SettingFragment extends PreferenceFragmentCompat {


    private Main2DB main2DB;
    //    private Main3Activity mainActivity;
//    private MyFrameLayout myFrameLayout;
    private Tate2Fragment tate2Fragment;
    private SeekBar seekBarAlpha;
    private SeekBar seekBarBlack;
    private ToggleButton toggleButtonMove;
    private ToggleButton toggleButtonFrameMove;
    private FrameLayout frame2;
    private MainSetting mainSetting;
    private Person me;
    private Button btnCancel;
    private Button btnOk;
    private Dialog dialog;
    private SeekBar seekBarBlackWhite;
    private ScrollView scrollView;
    private int tempProgress;
    private ToggleButton tateYoko;
    private Button btnApply;
    private CheckBox checkBox;
    //    private SharedPreferences sp;
    private int mPosition;
    private Button btnClose;
    private CheckBox reconnectCheckBox;
    private NumberPicker numberPicker;
    private ListPreference rapidRoomList;
    private SwitchPreference switchRapid;
    private ListPreference reconnectList;
    private SwitchPreference switchCommentChange;
    public static final String TAG = "SettingFragment";
    private PreferenceCategory preferenceCategory;
    private SeekBarPreference seekBarPreference;
    private LinearLayout activityBack;
    private SwitchPreference switchBackgroundChange;

    @SuppressLint("ValidFragment")
    public SettingFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        view.setBackgroundColor(getResources().getColor(R.color.commonWhite));
        return view;
    }



    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.setting2, rootKey);

        final TestShared testShared = TestShared.getInstance(getContext());
        /**
         * change comment
         */

        PreferenceScreen preferenceScreen = findPreference("setting2_test");
        preferenceScreen.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Log.d("monamisa", "aaa");
                return true;
            }
        });

        switchCommentChange = (SwitchPreference) findPreference("comment_color_switch");
        switchCommentChange.setChecked(Stub.commentChange);
        switchCommentChange.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean check = (boolean) newValue;
                testShared.setChangeColorComment(check);
                Log.d(TAG, "setChangeColorComment:" + check);
                return true;
            }
        });


        /**
         *
         */
        seekBarPreference = (SeekBarPreference) findPreference("background_seek_bar");
        seekBarPreference.setMin(0);
        seekBarPreference.setMax(153);
        activityBack =(LinearLayout) getActivity().findViewById(R.id.activity_back);
        seekBarPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activityBack.setBackgroundColor(Color.argb((int)newValue,0,0,0));
                return true;
            }
        });


        /**
         * rapid mode
         */
        boolean rapidCheck = TestShared.getInstance(getContext()).getRapidCheck();
        switchRapid = (SwitchPreference) findPreference("rapid_login_switch");
        rapidRoomList = (ListPreference) findPreference("rapid_login_list");
        switchRapid.setChecked(rapidCheck);
        if (rapidCheck) {
            rapidRoomList.setEnabled(true);
        } else {
            rapidRoomList.setEnabled(false);
        }

        switchRapid.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean check = (boolean) newValue;
                TestShared.getInstance(getContext()).setRapidCheck(check);
                rapidRoomList.setEnabled(check);
                if (check) {
                    Toast.makeText(getContext(), "即入室(rapid login)を有効にしました。", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "解除しました。", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
        rapidRoomList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                ListPreference listPreference = (ListPreference) preference;
                int value = Integer.parseInt((String) newValue);
                TestShared.getInstance(getContext()).setRapidRoomValue(value);
                listPreference.setValue((String) newValue);
                CharSequence[] temp = listPreference.getEntries();
                String entry = (String) temp[value - 1];
                listPreference.setSummary(entry);
                return true;
            }
        });


        reconnectList = (ListPreference) findPreference("reconnect_list");
        reconnectList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                int value = Integer.parseInt((String) newValue);
                ListPreference listPreference = (ListPreference) preference;
                listPreference.setValue((String) newValue);
                listPreference.setSummary(String.valueOf(newValue));
                TestShared.getInstance(getContext()).setReconectCount(value);
                return true;
            }
        });
        initSet();
    }

    private void initSet() {
        int roomValue = TestShared.getInstance(getContext()).getRapidRoomValue();
        if (roomValue == -1) {
            rapidRoomList.setSummary("未選択");
        } else {
            CharSequence[] temp = rapidRoomList.getEntries();
            String entry = (String) temp[roomValue - 1];
            rapidRoomList.setSummary(entry);
        }
        int reconectCount = TestShared.getInstance(getContext()).getReconectCount();
        reconnectList.setSummary(String.valueOf(reconectCount));
        reconnectList.setValue(String.valueOf(reconectCount));
    }
}
