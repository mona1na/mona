package com.devmona;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class MyCommentData implements Parcelable {

    private ArrayList<Tate3Fragment.PersonEnum> mCommentList;

    public MyCommentData(){
        mCommentList = new ArrayList<>();
    }

    public void setmCommentList(ArrayList<Tate3Fragment.PersonEnum> mCommentList) {
        this.mCommentList = mCommentList;
    }

    public ArrayList<Tate3Fragment.PersonEnum> getmCommentList() {
        return mCommentList;
    }

    protected MyCommentData(Parcel in) {
        this.mCommentList = in.createTypedArrayList(Tate3Fragment.PersonEnum.CREATOR);
    }

    public static final Creator<MyCommentData> CREATOR = new Creator<MyCommentData>() {
        @Override
        public MyCommentData createFromParcel(Parcel in) {
            return new MyCommentData(in);
        }

        @Override
        public MyCommentData[] newArray(int size) {
            return new MyCommentData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mCommentList);
    }
}
