package com.devmona;


import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationSettingFragment extends Fragment {
    private LinearLayout linearLayout;

//    HashMap<Integer, String> hashMap;


    public NotificationSettingFragment() {
        // Required empty public constructor
        //
//        Main2DB main2DB = new Main2DB();
//        hashMap = main2DB.getNotification();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View v = inflater.inflate(R.layout.fragment_notification_setting, container, false);
        linearLayout = (LinearLayout) v.findViewById(R.id.fragment_notification_linear);
        Button buttonOk = (Button) v.findViewById(R.id.notification_ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int j = 0;
                ArrayList<String> arrayList = new ArrayList<>();
                for (int i = 0; i < 10; i++) {
                    LinearLayout childView = (LinearLayout) linearLayout.getChildAt(i);
                    EditText editText = (EditText) childView.getChildAt(0);
                    String result = editText.getText().toString();
                    if (!result.equals("")) {
                        arrayList.add(result);
                        j++;
                    } else {
                        arrayList.add(null);
                    }
                }
                TestShared testShared = TestShared.getInstance(getContext());
                testShared.setNotify(arrayList);
//                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
//                fragmentTransaction.remove(NotificationSettingFragment.this).commit();
                Toast.makeText(getContext(), j + "個のワードを登録しました", Toast.LENGTH_LONG).show();
                Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(getContext());
                TabLayout tabLayout = (TabLayout) tate2Fragment.getView().findViewById(R.id.tabLayout2);
                TabLayout.Tab tab = tabLayout.getTabAt(0);
                tab.select();
            }
        });

        Button buttonCancel = (Button) v.findViewById(R.id.notification_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
//                fragmentTransaction.remove(NotificationSettingFragment.this).commit();
                Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(getContext());
                TabLayout tabLayout = (TabLayout) tate2Fragment.getView().findViewById(R.id.tabLayout2);
                TabLayout.Tab tab = tabLayout.getTabAt(0);
                tab.select();
            }
        });
        TestShared testShared = TestShared.getInstance(getContext());
        ArrayList<String> arrayNotity = testShared.getNotify();
        for (int i = 0; i < arrayNotity.size(); i++) {
            LinearLayout childView = (LinearLayout) linearLayout.getChildAt(i);
            EditText editText = (EditText) childView.getChildAt(0);
            editText.setText(arrayNotity.get(i));
        }
        return v;
    }

}
