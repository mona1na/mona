package com.devmona;

import android.content.Context;
import android.database.Cursor;

import net.sqlcipher.database.SQLiteException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sake on 2018/03/19.
 */

public class Main2DB {
    public Person getMeOfMonaId() {
        String SQL = "SELECT * FROM `person` WHERE is_me = 1";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        Person person = new Person();
        boolean next = cursor.moveToFirst();
        while (next) {
            int monaId = cursor.getInt(cursor.getColumnIndex("id"));
            person.setId(monaId);
            next = cursor.moveToNext();
        }
        cursor.close();
        return person;
    }

    public Person getCXCYById(int monaId) {
        String SQL = "SELECT * FROM `person` WHERE mona_id = ?";
        String[] S = new String[]{
                String.valueOf(monaId)
        };
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, S);
        Person person = new Person();
        boolean next = cursor.moveToFirst();
        while (next) {
            person.setcX(cursor.getInt(cursor.getColumnIndex("cx")));
            person.setcX(cursor.getInt(cursor.getColumnIndex("cy")));
            next = cursor.moveToNext();
        }
        cursor.close();
        return person;
    }

    public Person getPersonName(int monaId) {

        String SQL = "SELECT `name` FROM `person` WHERE `mona_id` = ?";
        String[] S = new String[]{
                String.valueOf(monaId)
        };

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, S);
        Person person = new Person();
        boolean next = cursor.moveToFirst();
        while (next) {
            person.setName(cursor.getString(cursor.getColumnIndex("name")));
            next = cursor.moveToNext();
        }
        cursor.close();
        return person;
    }

//    /**
//     * deletes person with monaId
//     *
//     * @param monaId
//     */
//    public void deletePerson(int monaId) {
//        String SQL = "DELETE FROM `person` WHERE `mona_id` = ? AND room_id = (SELECT `room_id` FROM current_status)";
//        String[] S = new String[]{
//                String.valueOf(monaId)
//        };
//        UtilInfo.m_db.execSQL(SQL, S);
//    }

    public Person getMe() {
        String SQL = "SELECT * FROM `person` WHERE `room_id` = (SELECT MAX(id) FROM room) AND `is_me` = 1 " +
                "GROUP BY `shirotori` HAVING MAX(`id`)";

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        Person person = new Person();
        person.setId(-1);
        while (next) {
            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
            person.setName(cursor.getString(cursor.getColumnIndex("name")));
            person.setColor255(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
            person.setRed(cursor.getInt(cursor.getColumnIndex("red")));
            person.setGreen(cursor.getInt(cursor.getColumnIndex("green")));
            person.setBlue(cursor.getInt(cursor.getColumnIndex("blue")));
            person.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
            person.setStat(cursor.getString(cursor.getColumnIndex("stat")));
            person.setShirotori6(cursor.getString(cursor.getColumnIndex("shirotori6")));
            person.setKurotori(cursor.getString(cursor.getColumnIndex("kurotori")));
            person.setShirotori(cursor.getString(cursor.getColumnIndex("shirotori")));
            person.setMonaId(cursor.getInt(cursor.getColumnIndex("mona_id")));
            person.setcX(cursor.getInt(cursor.getColumnIndex("cx")));
            person.setcY(cursor.getInt(cursor.getColumnIndex("cy")));
            person.setcX2(cursor.getInt(cursor.getColumnIndex("cx2")));
            person.setcY2(cursor.getInt(cursor.getColumnIndex("cy2")));

            next = cursor.moveToNext();
        }
        cursor.close();
        return person;
    }

//    public Room getCurrentRoom() {
//        String SQL = "SELECT * FROM `room` WHERE `id` = (SELECT `login_id` FROM `current_status`)";
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        boolean next = cursor.moveToFirst();
//        Room room = new Room();
//        while (next) {
//            int id = cursor.getInt(cursor.getColumnIndex("id"));
//            room.setId(id);
//            next = cursor.moveToNext();
//        }
//        cursor.close();
//        return room;
//    }

//    public ArrayList<Person> getRoomPersonByRoomId() {
//
//        String SQL = "SELECT * FROM `person` WHERE `exit_flag` = 0 AND room_id = (SELECT MAX(id) FROM room)";
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//
//        ArrayList<Person> arrayList = new ArrayList();
//        boolean next = cursor.moveToFirst();
//        while (next) {
//            Person person = new Person();
//            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
//            String strResNo = UtilInfo.aaNameaaResNoMap.get(cursor.getString(cursor.getColumnIndex("aa_name")));
//            person.setAaresno(Integer.parseInt(strResNo));
//            String name = cursor.getString(cursor.getColumnIndex("name"));
//            person.setName(name);
//            person.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
//            person.setColor255(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
//            String kurotori = cursor.getString(cursor.getColumnIndex("kurotori"));
//            person.setKurotori(kurotori);
//            String shirotori = cursor.getString(cursor.getColumnIndex("shirotori"));
//            person.setShirotori(shirotori);
//            int monaId = cursor.getInt(cursor.getColumnIndex("mona_id"));
//            person.setMonaId(monaId);
//            arrayList.add(person);
//            next = cursor.moveToNext();
//        }
//        cursor.close();
//        return arrayList;
//    }

    public ArrayList<Person> getComment(Context applicationContext) {
        String SQL = "SELECT person.name AS name,comment.comment AS comment,person.ignore_flag AS ignore_flag,person.name_ellipsis AS name_ellipsis," +
                "person.kurotori AS kurotori,person.shirotori AS shirotori,person.stat AS stat," +
                "person.cx2 AS cx2,person.cy2 AS cy2,person.aa_color_255 AS aa_color_255 " +
                "FROM comment " +
                "LEFT JOIN person ON comment.person_id = person.id " +
                "WHERE comment.room_id = (SELECT MAX(id) FROM room)";
//        Cursor cursor = null;
////        try {
////            cursor = UtilInfo.m_db.rawQuery(SQL, null);
////        } catch (DatabaseObjectNotClosedException e) {
//        Log.d("DatabaseObjectNotClosed", "通った");
//        SQLiteDatabase.loadLibs(applicationContext);
////        File databaseFile = getDatabasePath("db3.db");
////        databaseFile.mkdirs();
////        databaseFile.delete();
////        SQLiteDatabase.openOrCreateDatabase()
//
//        MyHelper myHelper = new MyHelper(applicationContext, "db2", null, Prop.DB_VERSION);
//        UtilInfo.m_db = myHelper.getWritableDatabase("testbeta");
//        cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        //       }
        ArrayList<Person> arrayList = new ArrayList();
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        while (next) {
            Person person = new Person();
            String comment = cursor.getString(cursor.getColumnIndex("comment"));
            person.setComment(comment);
            String name = cursor.getString(cursor.getColumnIndex("name"));
            person.setName(name);
            String nameEllipsis = cursor.getString(cursor.getColumnIndex("name_ellipsis"));
            person.setNameEllipsis(nameEllipsis);
            String kurotori = cursor.getString(cursor.getColumnIndex("kurotori"));
            person.setKurotori(kurotori);
            String shirotori = cursor.getString(cursor.getColumnIndex("shirotori"));
            person.setShirotori(shirotori);
            String stat = cursor.getString(cursor.getColumnIndex("stat"));
            person.setStat(stat);
            person.setcX2(cursor.getInt(cursor.getColumnIndex("cx2")));
            person.setcY2(cursor.getInt(cursor.getColumnIndex("cy2")));
            int color255 = cursor.getInt(cursor.getColumnIndex("aa_color_255"));
            int intIgnore = cursor.getInt(cursor.getColumnIndex("ignore_flag"));
            if (intIgnore == 0) {
                person.setIgnoreFlag(false);
            } else {
                person.setIgnoreFlag(true);
            }
            person.setColor255(color255);
//            person.setMonaId(monaId);
            arrayList.add(person);
            next = cursor.moveToNext();
        }
        cursor.close();
        return arrayList;
    }

    public Person getPersonByMonaId(int monaId) {
        String SQL = "SELECT * FROM `person` WHERE mona_id = ? AND room_id = (SELECT MAX(id) FROM room) " +
                "GROUP BY `shirotori` HAVING MAX(id)";
//        String SQL = "SELECT id FROM person WHERE id = (SELECT MAX(id) FROM person WHERE mona_id = ?)";
        String[] S = new String[]{
                String.valueOf(monaId)
        };
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, S);
        Person person = new Person();
        boolean next = cursor.moveToFirst();
        if (next == false) {
            cursor.close();
            return null;
        }
        while (next) {
            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            person.setName(name);
            String nameEllipsis = cursor.getString(cursor.getColumnIndex("name_ellipsis"));
            person.setNameEllipsis(nameEllipsis);
            String kurotori = cursor.getString(cursor.getColumnIndex("kurotori"));
            person.setKurotori(kurotori);
            String shirotori = cursor.getString(cursor.getColumnIndex("shirotori"));
            person.setShirotori(shirotori);
            String stat = cursor.getString(cursor.getColumnIndex("stat"));
            person.setStat(stat);
            person.setcX2(cursor.getInt(cursor.getColumnIndex("cx2")));
            person.setcY2(cursor.getInt(cursor.getColumnIndex("cy2")));
            int color255 = cursor.getInt(cursor.getColumnIndex("aa_color_255"));
            int intIgnore = cursor.getInt(cursor.getColumnIndex("ignore_flag"));
            if (intIgnore == 0) {
                person.setIgnoreFlag(false);
            } else {
                person.setIgnoreFlag(true);
            }
            person.setColor255(color255);
            person.setMonaId(monaId);
            next = cursor.moveToNext();
        }
        cursor.close();
        return person;
    }

    public HashMap<String, Person> getMember() {
        String sql = "SELECT * FROM `person` WHERE room_id = (SELECT MAX(id) FROM room) AND `exit_flag` = 0";
        Cursor cursor = UtilInfo.m_db.rawQuery(sql, null);
        boolean next = cursor.moveToFirst();
        HashMap<String, Person> hashMap = new HashMap<>();
        while (next) {
            Person person = new Person();
            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
            person.setName(cursor.getString(cursor.getColumnIndex("name")));
            person.setNameEllipsis(cursor.getString(cursor.getColumnIndex("name_ellipsis")));
            person.setColor255(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
            person.setRed(cursor.getInt(cursor.getColumnIndex("red")));
            person.setGreen(cursor.getInt(cursor.getColumnIndex("green")));
            person.setBlue(cursor.getInt(cursor.getColumnIndex("blue")));
            person.setAaresno(cursor.getInt(cursor.getColumnIndex("aa_res_no")));
            person.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
            person.setStat(cursor.getString(cursor.getColumnIndex("stat")));
            person.setcX(cursor.getInt(cursor.getColumnIndex("cx")));
            person.setShirotori(cursor.getString(cursor.getColumnIndex("shirotori")));
            person.setcY(cursor.getInt(cursor.getColumnIndex("cy")));
            person.setcX2(cursor.getInt(cursor.getColumnIndex("cx2")));
            person.setcY2(cursor.getInt(cursor.getColumnIndex("cy2")));
            person.setMonaId(cursor.getInt(cursor.getColumnIndex("mona_id")));
            boolean flag = (cursor.getInt(cursor.getColumnIndex("ignore_flag")) == 0) ? false : true;
            person.setIgnoreFlag(flag);
            hashMap.put(person.getShirotori(), person);
            next = cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }

    /**
     * returns mainsetting
     *
     * @return
     */
    public MainSetting getSetting() {
        String SQL = "SELECT * FROM `setting` WHERE id = (SELECT MAX(id) FROM `setting`)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        MainSetting mainSetting = new MainSetting();
        boolean next = cursor.moveToFirst();
        if (next == false) {
            mainSetting.setBlack(255);
            cursor.close();
            return mainSetting;
        }
        while (next) {
//            String alpha01 = cursor.getString(cursor.getColumnIndex("alpha01"));
//            mainSetting.setAlpha01(Float.parseFloat(alpha01));
//            mainSetting.setAlpha(cursor.getInt(cursor.getColumnIndex("alpha")));
            mainSetting.setBlack(cursor.getInt(cursor.getColumnIndex("black")));
//            if (cursor.getInt(cursor.getColumnIndex("touch_ok")) == 1) {
//                boolean touchOk = true;
//                mainSetting.setTouchOk(touchOk);
//            } else {
//                boolean touchOk = false;
//                mainSetting.setTouchOk(touchOk);
//            }

            next = cursor.moveToNext();
        }
        cursor.close();
        return mainSetting;
    }

    public SizeTate getSizeTate() {
        return null;
    }

    public void updateDBStatus() {

    }

    public void updateSetting(MainSetting tempSetting) {
        String SQL = "UPDATE `setting` SET alpha = ?, black = ?, touch_ok = ? WHERE id = (SELECT MAX(id) FROM `setting`)";

        int touchOk = -1;
        if (tempSetting.getTouchOk() == true) {
            touchOk = 1;
        } else {
            touchOk = 0;
        }

        String[] S = new String[]{
                String.valueOf(tempSetting.getAlpha()),
                String.valueOf(tempSetting.getBlack()),
                String.valueOf(touchOk)
        };
        UtilInfo.m_db.execSQL(SQL, S);
    }


    public void insertComment(String strCmt) {
//        int personId = getPersonIdBymonaId(MeMonaId.getMe().getMonaId());
//
//        String SQL = "INSERT INTO comment (start_id, login_id, room_id, person_id, comment, show_flag, date,send_flag)";
//        String VALUES = "VALUES((SELECT MAX(id) FROM `start`), (SELECT MAX(id) FROM `login`),(SELECT MAX(id) FROM room),?,?,0,?,0)";
//        String[] S = new String[]{
//                String.valueOf(personId),
//                strCmt,
//                String.valueOf(UtilInfo.getNowDate())
//        };
//        UtilInfo.m_db.execSQL(SQL + VALUES, S);
    }

    private int getPersonIdBymonaId(int monaId) {
        String SQL = "SELECT `id` FROM `person` WHERE `mona_id` = ?";
        String[] S = new String[]{
                String.valueOf(monaId)
        };

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, S);
        boolean next = cursor.moveToFirst();
        int personId = -1;
        while (next) {
            personId = cursor.getInt(cursor.getColumnIndex("id"));
            next = cursor.moveToNext();
        }
        cursor.close();
        return personId;
    }

    public Room getRoomOfNameCount() {
        String SQL = "SELECT * FROM `room` WHERE id = (SELECT MAX(id) FROM `room`)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        Room room = new Room();
        while (next) {
            String roomName = cursor.getString(cursor.getColumnIndex("room_name"));
            room.setRoomName(roomName);
            int roomCount = cursor.getInt(cursor.getColumnIndex("room_count"));
            room.setRoomCount(roomCount);
            next = cursor.moveToNext();
        }
        cursor.close();
        return room;
    }

    public Person getMemberById(int id) {
        String SQL = "SELECT * FROM `person` WHERE `room_id` = (SELECT MAX(id) FROM room) AND `mona_id` = ? " +
                "GROUP BY `shirotori` HAVING MAX(id)";
        String[] S = new String[]{
                String.valueOf(id)
        };

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, S);
        boolean next = cursor.moveToFirst();
        Person person = new Person();
        while (next) {
            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
            person.setName(cursor.getString(cursor.getColumnIndex("name")));
            person.setColor255(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
            person.setRed(cursor.getInt(cursor.getColumnIndex("red")));
            person.setGreen(cursor.getInt(cursor.getColumnIndex("green")));
            person.setBlue(cursor.getInt(cursor.getColumnIndex("blue")));
            person.setAaresno(cursor.getInt(cursor.getColumnIndex("aa_res_no")));
            person.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
            person.setStat(cursor.getString(cursor.getColumnIndex("stat")));
            person.setcX(cursor.getInt(cursor.getColumnIndex("cx")));
            person.setcY(cursor.getInt(cursor.getColumnIndex("cy")));
            person.setcX2(cursor.getInt(cursor.getColumnIndex("cx2")));
            person.setcY2(cursor.getInt(cursor.getColumnIndex("cy2")));
            person.setMonaId(cursor.getInt(cursor.getColumnIndex("mona_id")));
            person.setShirotori(cursor.getString(cursor.getColumnIndex("shirotori")));
            int intIgnoreFlag = cursor.getInt(cursor.getColumnIndex("ignore_flag"));
            if (intIgnoreFlag == 0) {
                person.setIgnoreFlag(false);
            } else {
                person.setIgnoreFlag(true);
            }
            next = cursor.moveToNext();
        }
        cursor.close();
        return person;
    }

    //    /**
//     * update currentStatus table
//     */
//    public void updateCurrentStatus() {
//        String SQL = "UPDATE `current_status` SET `room_id` = null WHERE `start_id` = (SELECT start_id FROM current_status)";
//        UtilInfo.m_db.execSQL(SQL);
//    }
//
    public void updatePerson(int monaId) {
        String SQL = "UPDATE `person` SET `exit_flag` = 1 WHERE mona_id = ? AND `login_id` = (SELECT MAX(id) FROM login) AND `room_id` = (SELECT MAX(id) FROM room)";
        String[] S = new String[]{
                String.valueOf(monaId)
        };


        UtilInfo.m_db.execSQL(SQL, S);
    }

    public HashMap<Integer, Person> getPerson() {
        return null;
    }

    /**
     * sets ignore
     *
     * @param personMap
     */
    public void updateIgnorePerson(HashMap<String, Person> personMap) {
        for (String key : personMap.keySet()) {
            Person person = personMap.get(key);
            int i = 0;
            if (person.isIgnoreFlag()) {
                i = 1;
            } else {
                i = 0;
            }
            String SQL = "UPDATE `person` SET `ignore_flag` = ? WHERE `shirotori` = ? AND `start_id` = (SELECT MAX(id) FROM start) AND `login_id` = (SELECT MAX(id) FROM login)";
            String[] S = new String[]{
                    String.valueOf(i),
                    key
            };
            UtilInfo.m_db.execSQL(SQL, S);
        }
    }

    public String getShirotoriByMonaId(int monaId) {
        String SQL = "SELECT `shirotori` FROM `person` WHERE `room_id` = (SELECT MAX(id) FROM room) AND `mona_id` = ? " +
                "GROUP BY `shirotori` HAVING MAX(`id`)";
        String[] S = new String[]{
                String.valueOf(monaId)
        };
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, S);
        String shirotori = null;
        boolean next = cursor.moveToFirst();
        while (next) {
            shirotori = cursor.getString(cursor.getColumnIndex("shirotori"));
            next = cursor.moveToNext();
        }
        cursor.close();
        return shirotori;
    }

    public void updateMeBystate(String stat) {
//        String SQL = "UPDATE `person` SET `stat` = ? WHERE `room_id` = (SELECT `room_id` FROM `current_status`) " +
//                "AND `is_me` = 1  AND `id` = (SELECT `` FROM `person` WHERE `is_me` = 1 GROUP BY `shirotori` HAVING MAX(`id`)) ";

        String SQL = "UPDATE `person` SET `stat` = ? WHERE `id` = (SELECT `id` FROM `person` WHERE `is_me` = 1 AND `room_id` = (SELECT MAX(id) FROM room) GROUP BY `shirotori` HAVING MAX(id))";
        String[] S = new String[]{
                stat
        };
        UtilInfo.m_db.execSQL(SQL, S);
    }

    public void updateState(int id2, String strStat) {
        String SQL = "UPDATE `person` SET `stat` = ? WHERE `room_id` = (SELECT MAX(id) FROM `room`) AND `id` = (SELECT MAX(`id`) FROM `person` WHERE `mona_id` = ?)";
        String[] S = new String[]{
                String.valueOf(id2),
                strStat
        };
        UtilInfo.m_db.execSQL(SQL, S);
    }

    public void updateRoom() {
        String SQL = "UPDATE `room` SET `exit_date` = ? WHERE `login_id` = (SELECT MAX(id) FROM login) AND `id` = (SELECT MAX(id) FROM `room`)";
        String[] S = new String[]{
                UtilInfo.getNowDate(),
        };
        UtilInfo.m_db.execSQL(SQL, S);
    }

    /**
     * get size-tate2
     *
     * @return
     */
    public SizeTate2 getSizeTate2() {
        SizeTate2 sizeTate2 = new SizeTate2();
        String SQL = "SELECT * FROM `size_tate2` WHERE `id` = (SELECT MAX(id) FROM `size_tate2`)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
//        while (next) {
//            sizeTate2.setWidth(cursor.getInt(cursor.getColumnIndex("width")));
//            sizeTate2.setH08HikuPic(cursor.getInt(cursor.getColumnIndex("h08_hiku_pic")));
//            sizeTate2.setH08HikuPicHalf(cursor.getInt(cursor.getColumnIndex("h08_hiku_pic_half")));
//            sizeTate2.setW08PlusPicHalf(cursor.getInt(cursor.getColumnIndex("w08_plus_pic_half")));
//            sizeTate2.setWitdh08(cursor.getInt(cursor.getColumnIndex("width08")));
//            next = cursor.moveToNext();
//        }
        while (next) {
            sizeTate2.setWidth(cursor.getInt(cursor.getColumnIndex("width")));
            sizeTate2.setHeight(cursor.getInt(cursor.getColumnIndex("height")));
            sizeTate2.setWitdh08(cursor.getInt(cursor.getColumnIndex("width08")));
            sizeTate2.setHeight08(cursor.getInt(cursor.getColumnIndex("height08")));
            sizeTate2.setH08HikuPicHalf(cursor.getInt(cursor.getColumnIndex("h08_hiku_pic_half")));
            sizeTate2.setH08HikuPic(cursor.getInt(cursor.getColumnIndex("h08_hiku_pic")));
            sizeTate2.setW08PlusPicHalf(cursor.getInt(cursor.getColumnIndex("w08_plus_pic_half")));
            next = cursor.moveToNext();
        }
        cursor.close();
        return sizeTate2;
    }

    /**
     * アップデート位置
     *
     * @param x
     * @param y
     * @param cx2
     * @param cy2
     */
    public void updateXYCXCY(int x, int y, int cx2, int cy2) {
        String SQL = "UPDATE `person` SET `x` = ?, `y` = ?, `cx2` = ?, `cy2` = ? WHERE `id` = (SELECT MAX(id) FROM `person` WHERE `is_me` = 1)";
        String[] S = new String[]{
                String.valueOf(x),
                String.valueOf(y),
                String.valueOf(cx2),
                String.valueOf(cy2)
        };
        UtilInfo.m_db.execSQL(SQL, S);
    }

    public int[] getPicSize() {
        int[] picSizeWH = new int[2];
        String SQL = "SELECT * FROM `pic_size` WHERE id = (SELECT MAX(id) FROM `pic_size`)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        while (next) {
            picSizeWH[0] = cursor.getInt(cursor.getColumnIndex("pic_width"));
            picSizeWH[1] = cursor.getInt(cursor.getColumnIndex("pic_height"));
            next = cursor.moveToNext();
        }
        cursor.close();
        return picSizeWH;

    }

    public void updateSettingBlack(int tempProgress) {
        String SQL = "UPDATE `setting` SET `black` = ? WHERE `id` = (SELECT MAX(id) FROM `setting`)";
        String[] S = new String[]{
                String.valueOf(tempProgress)
        };

        try {
            UtilInfo.m_db.execSQL(SQL, S);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 現在のルームを得る(今のところroom_noを得る)
     *
     * @return room
     */
    public Room getRoom() {
        String SQL = "SELECT * FROM room WHERE id = (SELECT MAX(id) FROM room)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        Room room = new Room();
        boolean next = cursor.moveToFirst();
        while (next) {
            int roomNo = cursor.getInt(cursor.getColumnIndex("room_no"));
            room.setRoomNumber(roomNo);
            next = cursor.moveToNext();
        }
        cursor.close();
        return room;
    }

    public HashMap<Integer, String> getNotification() {
        String SQL = "SELECT * FROM notify_setting WHERE id = 1";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        HashMap<Integer, String> hashMap = new HashMap<>();
        boolean next = cursor.moveToFirst();
        while (next) {
            String n1 = cursor.getString(cursor.getColumnIndex("notify1"));
            if (n1 != null) {
                hashMap.put(1, n1);
            }
            String n2 = cursor.getString(cursor.getColumnIndex("notify2"));
            if (n2 != null) {
                hashMap.put(2, n2);
            }
            String n3 = cursor.getString(cursor.getColumnIndex("notify3"));
            if (n3 != null) {
                hashMap.put(3, n3);
            }
            String n4 = cursor.getString(cursor.getColumnIndex("notify4"));
            if (n4 != null) {
                hashMap.put(4, n4);
            }
            String n5 = cursor.getString(cursor.getColumnIndex("notify5"));
            if (n5 != null) {
                hashMap.put(1, n5);
            }
            String n6 = cursor.getString(cursor.getColumnIndex("notify6"));
            if (n6 != null) {
                hashMap.put(6, n6);
            }
            String n7 = cursor.getString(cursor.getColumnIndex("notify7"));
            if (n7 != null) {
                hashMap.put(7, n7);
            }
            String n8 = cursor.getString(cursor.getColumnIndex("notify8"));
            if (n8 != null) {
                hashMap.put(8, n8);
            }
            String n9 = cursor.getString(cursor.getColumnIndex("notify9"));
            if (n9 != null) {
                hashMap.put(9, n9);
            }
            String n10 = cursor.getString(cursor.getColumnIndex("notify10"));
            if (n10 != null) {
                hashMap.put(10, n10);
            }
            next = cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }

    public void updateNotification(HashMap<Integer, String> hashMap) {
        String SQL = "UPDATE notify_setting SET notify1 = ? ,notify2 = ? ,notify3 = ? ,notify4 = ? ,notify5 = ? ,notify6 = ? ,notify7 = ? ,notify8 = ? ,notify9 = ? ,notify10 = ?  WHERE id = 1";

        String[] S = new String[]{


                hashMap.get(1),
                hashMap.get(2),
                hashMap.get(3),
                hashMap.get(4),
                hashMap.get(5),
                hashMap.get(6),
                hashMap.get(7),
                hashMap.get(8),
                hashMap.get(9),
                hashMap.get(10)
        };
        try {
            UtilInfo.m_db.execSQL(SQL, S);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }


//    public boolean isSetCommentTable() {
//        String SQL = "SELECT count(*) FROM comment";
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL,null);
//        int count = 0;
//
//        if (cursor.moveToNext()) {
//            count = cursor.getInt(0);
//        }
//        if (count > 0) {
//            return true;
//        } else {
//            return false;
//        }
//    }

//    /**
//     * イニットテーブルから
//     * @param com
//     */
//    public void insertCommentFromInit(String com) {
//        int personId = getPersonIdBymonaId(MeMonaId.monaId);
//
//        String SQL = "INSERT INTO comment (id,start_id, login_id, room_id, person_id, comment, show_flag, date,send_flag)";
//        String VALUES = "VALUES((SELECT comment_id_init FROM current_status_init),(SELECT `start_id` FROM `current_status`), (SELECT `login_id` FROM `current_status`),(SELECT `room_id` FROM `current_status`),?,?,0,?,0)";
//        String[] S = new String[]{
//                String.valueOf(personId),
//                com,
//                String.valueOf(UtilInfo.getNowDate())
//        };
//        UtilInfo.m_db.execSQL(SQL + VALUES, S);
//    }

//    public void updateCurrentStatusByComment() {
//        String SQL = "UPDATE current_status SET comment_id = (SELECT id FROM comment)";
//
//        UtilInfo.m_db.execSQL(SQL);
//    }

//    public void insertRoom(Room room) {
//        String SQL = "INSERT INTO room(start_id,login_id,room_no,room_name,room_count,time) ";
//        String VALUES = "VALUES((SELECT MAX(id) FROM start),(SELECT MAX(id) FROM login),?,?,?,?)";
//
//        String[] S = new String[]{
//                String.valueOf(room.getRoomNumber()),
//                String.valueOf(room.getRoomName()),
//                String.valueOf(room.getRoomCount()),
//                UtilInfo.getNowDate()
//        };
//
//        UtilInfo.m_db.execSQL(SQL + VALUES, S);
//    }
}
