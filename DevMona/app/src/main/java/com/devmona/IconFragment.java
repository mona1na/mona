package com.devmona;


import android.animation.ValueAnimator;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.app.LoaderManager;

import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.devmona.extendsviews.MyDragShadowBuilder;
import com.devmona.loadercallbacks.LoaderCallbacksSendScl;

import java.util.LinkedHashMap;

import static android.content.Context.VIBRATOR_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IconFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IconFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ImageView btnArrow;
    private ToggleButton btnKeyboard;
    private ImageView home;
    private View rootView;
    public static final String TAG = "IconFragment";
    private ToggleButton paging;
    private Vibrator vib;
    private FrameLayout frameLayout;
    private ImageView btnPeople;
    private ImageView mBtnLog2;

    public IconFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IconFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IconFragment newInstance(String param1, String param2) {
        IconFragment fragment = new IconFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_icon, container, false);
        frameLayout = (FrameLayout) rootView.findViewById(R.id.fragment_icon_frame);
        vib = (Vibrator) getContext().getSystemService(VIBRATOR_SERVICE);
        btnPeople = rootView.findViewById(R.id.tate2_people);
        mBtnLog2 = rootView.findViewById(R.id.tate2_log2);
        home = (ImageView) rootView.findViewById(R.id.tate2_home);
        btnArrow = (ImageView) rootView.findViewById(R.id.tate2_mukikae);
        btnArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vib.vibrate(10);
                Bundle bundle = new Bundle();
                hanten();
                Base2Activity nameActivity = (Base2Activity) getContext();
                LoaderManager.getInstance(getActivity()).initLoader(13, bundle, new LoaderCallbacksSendScl(nameActivity));
//                nameActivity.getSupportLoaderManager().initLoader(13, bundle, new LoaderCallbacksSendScl(nameActivity));
            }
        });
        keySet4();
        homeSet5();
        peopleSet6();
        dragSet();
        log2Set();
        TestShared testShared = TestShared.getInstance(getContext());
        int[] i1 = testShared.getHomeXY();
        if (i1[0] != -1) {
            home.setX(i1[0]);
            home.setY(i1[1]);
        } else {
            home.setX(80);
            home.setY(0);
        }

        int[] i2 = testShared.getKeyXY();
        if (i2[0] != -1) {
            btnKeyboard.setX(i2[0]);
            btnKeyboard.setY(i2[1]);
        } else {
            btnKeyboard.setX(160);
            btnKeyboard.setY(0);
        }

        int[] i3= testShared.getArrowXY();
        if (i3[0] != -1) {
            btnArrow.setX(i3[0]);
            btnArrow.setY(i3[1]);
        } else {
            btnArrow.setX(240);
            btnArrow.setY(0);
        }

        int[] i4= testShared.getPeopleXY();
        if (i4[0] != -1) {
            btnPeople.setX(i4[0]);
            btnPeople.setY(i4[1]);
        } else {
            btnPeople.setX(0);
            btnPeople.setY(60);
        }

        int[] i5= testShared.getLog2XY();
        if (i5[0] != -1) {
            mBtnLog2.setX(i5[0]);
            mBtnLog2.setY(i5[1]);
        } else {
            mBtnLog2.setX(0);
            mBtnLog2.setY(120);
        }

        return rootView;
    }

    private void log2Set() {
        mBtnLog2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log2Fragment log2Fragment = UtilInfo.getLog2Fragment(getContext());
                if (log2Fragment != null) {
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    if (log2Fragment.isHidden()) {
                        fragmentTransaction.show(log2Fragment);
                    } else {
                        fragmentTransaction.hide(log2Fragment);
                    }
                    fragmentTransaction.commit();
                }
            }
        });
    }

    private void peopleSet6() {
        btnPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(getContext());
                tate2Fragment.peopleMove();
            }
        });
    }

    public void setIconPos() {
        View view = getView();
        TestShared testShared = TestShared.getInstance(getContext());
        View v1 = view.findViewById(R.id.tate2_home);
        View v2 = view.findViewById(R.id.tate2_keyboard);
        View v3 = view.findViewById(R.id.tate2_mukikae);
        View v4 = view.findViewById(R.id.tate2_people);
        View v5 = view.findViewById(R.id.tate2_log2);
        testShared.setHomeXY((int)v1.getX(),
                (int)v1.getY()
        );
        testShared.setKeyXY((int)v2.getX(),
                (int)v2.getY()
        );

        testShared.setArrowXY((int)v3.getX(),
                (int)v3.getY()
        );
        testShared.setPeopleXY((int)v4.getX(),
                (int)v4.getY()
        );

        testShared.setLog2XY((int)v5.getX(),
                (int)v5.getY()
        );

    }

    class MyTouchListener implements View.OnTouchListener {

        private int iconNo;

        public MyTouchListener(int iconNo) {
            this.iconNo = iconNo;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.d("test555",event.getX() + "onTouch");
            Intent intent = new Intent();
            intent.putExtra("offsetX", (int) event.getX());
            intent.putExtra("offsetY", (int) event.getY());
            intent.putExtra("offsetFloatX", event.getX());
            intent.putExtra("offsetFloatY", event.getY());
            intent.putExtra("iconNo", this.iconNo);
            intent.putExtra("flag",ICON);
            v.setTag(intent);
            return false;
        }
    }


    class MyLongClickListener implements View.OnLongClickListener {

        private int iconNo;

        MyLongClickListener(int iconNo) {
            this.iconNo = iconNo;
        }


        @Override
        public boolean onLongClick(View v) {
            boolean b;
            Log.d("test555", "onLongClick");
            vib.vibrate(10);
//            Intent intent = new Intent();
//            intent.putExtra("offsetFloatX", v.getX());
//            intent.putExtra("offsetFloatY", v.getY());
//            intent.putExtra("iconNo", this.iconNo);
            Intent intent =(Intent) v.getTag();

            MyDragShadowBuilder myDragShadowBuilder = new MyDragShadowBuilder(v);
            myDragShadowBuilder.setIntent(intent);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                b = v.startDragAndDrop(ClipData.newIntent("offset", intent), myDragShadowBuilder, v, 0);
            } else {
                b = v.startDrag(ClipData.newIntent("offset", intent), myDragShadowBuilder, v, 0);
            }


            return true;
        }
    }

    public static final int ICON_HOME = 1;
    public static final int ICON_KEYBOARD = 2;
    public static final int ICON_ARROW = 3;
    public static final int ICON_PEOPLE = 4;
    public static final int ICON_LOG2 = 5;
    public static final int ICON = 2;

    private void dragSet() {

        home.setOnLongClickListener(new MyLongClickListener(ICON_HOME));
        home.setOnTouchListener(new MyTouchListener(ICON_HOME));
        btnKeyboard.setOnLongClickListener(new MyLongClickListener(ICON_KEYBOARD));
        btnKeyboard.setOnTouchListener(new MyTouchListener(ICON_KEYBOARD));
        btnArrow.setOnLongClickListener(new MyLongClickListener(ICON_ARROW));
        btnArrow.setOnTouchListener(new MyTouchListener(ICON_ARROW));
        btnPeople.setOnLongClickListener(new MyLongClickListener(ICON_PEOPLE));
        btnPeople.setOnTouchListener(new MyTouchListener(ICON_PEOPLE));
        mBtnLog2.setOnLongClickListener(new MyLongClickListener(ICON_LOG2));
        mBtnLog2.setOnTouchListener(new MyTouchListener(ICON_LOG2));


        frameLayout.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int action = event.getAction();
                switch (action) {
                    case DragEvent.ACTION_DROP:
                        ClipData clipData = event.getClipData();
                        ClipData.Item item = clipData.getItemAt(0);
                        Intent intent = item.getIntent();
                        int flag = intent.getIntExtra("flag",-1);
                        if (flag != ICON) {
                            Log.d("ACTION_DROP","not icon");
                            return true;
                        }

                        float offsetFloatX = intent.getFloatExtra("offsetFloatX", -1);
                        float offsetFloatY = intent.getFloatExtra("offsetFloatY", -1);
                        int iconNo = intent.getIntExtra("iconNo", -1);

                        float tempX = event.getX() - offsetFloatX;
                        float tempY = event.getY() - offsetFloatY;


                        if (0 > tempX) {
                            tempX = 0;
                        }

                        if (0 > tempY) {
                            tempY = 0;
                        }

//                        float tempX = event.getX();
//                        float tempY = event.getY();

                        if (ICON_HOME == iconNo) {
//                            home.getLayoutParams();
                            home.setX(tempX);
                            home.setY(tempY);
                        } else if(ICON_KEYBOARD == iconNo) {
                            btnKeyboard.setX(tempX);
                            btnKeyboard.setY(tempY);
                        } else if(ICON_ARROW == iconNo) {
                            btnArrow.setX(tempX);
                            btnArrow.setY(tempY);
                        } else if (ICON_PEOPLE == iconNo) {
                            btnPeople.setX(tempX);
                            btnPeople.setY(tempY);
                        } else if (ICON_LOG2 == iconNo) {
                            mBtnLog2.setX(tempX);
                            mBtnLog2.setY(tempY);
                        }
                        break;
                }


                return true;
            }
        });

    }


    private void hanten() {
        Log.d("testScl hanten", MeMonaId.getMe().getScl() + "");
        if (MeMonaId.getMe().getScl() == 1) {
            TestShared.getInstance(getContext()).setScl(-1);
        } else {
            TestShared.getInstance(getContext()).setScl(1);
        }

    }

    private void keySet4() {
        btnKeyboard = (ToggleButton) rootView.findViewById(R.id.tate2_keyboard);
        btnKeyboard.setText("");
        btnKeyboard.setTextOff("");
        btnKeyboard.setTextOn("");
        btnKeyboard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                } else {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            }
        });
    }

    private void homeSet5() {
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "home click");
                UtilInfo.fromMain4toRoom(getContext());


            }
        });
    }

}
