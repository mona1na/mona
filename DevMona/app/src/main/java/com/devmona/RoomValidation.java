package com.devmona;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sake on 2018/01/26.
 */

public class RoomValidation {

    public String m_ptn = "^[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]|6[0-9]|7[0-9]|8[0-9]|9[0-9]|10[0]$";

    public void roomCheck(String strRoomNo, String strRoomCount) throws MyException, NumberFormatException {
        Pattern pattern = Pattern.compile(m_ptn);
        Matcher matcher = pattern.matcher(strRoomNo);
        boolean checkHankaku = matcher.matches();

        if (checkHankaku == true) {
            //処理しない
        } else {
            throw new MyException("RoomNo invalid 1");
        }

        try {
            int i = Integer.parseInt(strRoomNo);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("RoomNo invalid 2");
        }

        try {
            int i = Integer.parseInt(strRoomCount);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("RoomNo invalid 3");
        }

        int roomNo = Integer.parseInt(strRoomNo);

        if (roomNo < 1) {
            throw new MyException("setRoomInfo");
        }

        if (roomNo > 100) {
            throw new MyException("setRoomInfo");
        }
    }
}
