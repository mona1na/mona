package com.devmona;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;

public class TestShared {


    public static final String TAG = "TestShared";
    private Context mC;
    private SharedPreferences mSp;
    public static TestShared testShared;
    public HashMap<String, Integer> aaNameaaResNoMap = new HashMap<>();


    private TestShared(Context context, SharedPreferences sp) {
        this.mC = context;
        this.mSp = sp;
    }

    public static TestShared getInstance(Context context) {

        if (testShared == null) {
            testShared = new TestShared(context, PreferenceManager.getDefaultSharedPreferences(context));
            return testShared;
        }
        return testShared;
    }

    public TestShared test() {
        return this;
    }

    public LinkedHashMap<Integer, IdAanameResno> getPage1() {
        LinkedHashMap<Integer, IdAanameResno> linkedHashMap = new LinkedHashMap();


        return linkedHashMap;
    }



    public HashMap<String, Integer> getaaNameaaResNoMap() {
        return aaNameaaResNoMap;
    }

    public void setMe(Person me) {
        mSp.edit().putInt(mC.getString(R.string.shared_me_color255), me.getColor255()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_red), me.getRed()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_green), me.getGreen()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_blue), me.getBlue()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_y), me.getY()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_x), me.getX()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_scl), me.getScl()).commit();
        mSp.edit().putString(mC.getString(R.string.shared_me_name), me.getName()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_mona_id), me.getMonaId()).commit();
        mSp.edit().putString(mC.getString(R.string.shared_me_shirotori), me.getShirotori()).commit();
        mSp.edit().putString(mC.getString(R.string.shared_me_stat), me.getStat()).commit();
        mSp.edit().putString(mC.getString(R.string.shared_me_trip), me.getTrip()).commit();
        mSp.edit().putString(mC.getString(R.string.shared_me_aa_name), me.getAaname()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_res_no), me.getAaresno()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_me_Hukidashi), Util.change100toHukidashi(me.getRed(), me.getGreen(), me.getBlue())).commit();
        MeMonaId.me = me;

    }


    public Person getMe() {
        Person me = new Person();
        me.setColor255(mSp.getInt(mC.getString(R.string.shared_me_color255), mC.getResources().getColor(R.color.commonWhite)));
        me.setRed(mSp.getInt(mC.getString(R.string.shared_me_red), 100));
        me.setGreen(mSp.getInt(mC.getString(R.string.shared_me_green), 100));
        me.setBlue(mSp.getInt(mC.getString(R.string.shared_me_blue), 100));
        Random random = new Random();
        me.setX(mSp.getInt(mC.getString(R.string.shared_me_x),random.nextInt(660) + 30));
        me.setY(mSp.getInt(mC.getString(R.string.shared_me_y),random.nextInt(80) + 240));
        me.setScl(mSp.getInt(mC.getString(R.string.shared_me_scl), 100));
        me.setScl(mSp.getInt(mC.getString(R.string.shared_me_scl), 100));
        me.setName(mSp.getString(mC.getString(R.string.shared_me_name), "名無しさん"));
        me.setMonaId(mSp.getInt(mC.getString(R.string.shared_me_mona_id), -1));

        me.setShirotori(mSp.getString(mC.getString(R.string.shared_me_shirotori), ""));
        me.setStat(mSp.getString(mC.getString(R.string.shared_me_stat), "通常"));
        me.setAaname(mSp.getString(mC.getString(R.string.shared_me_aa_name), "mona"));
        me.setAaresno(mSp.getInt(mC.getString(R.string.shared_me_res_no), -1));
        me.setColorHukidashi(mSp.getInt(mC.getString(R.string.shared_me_Hukidashi), -1));
//        mSp.edit().putString(mC.getString(R.string.shared_me_shirotori), me.getShirotori()).commit();
//        mSp.edit().putString(mC.getString(R.string.shared_me_stat), me.getStat()).commit();
//        mSp.edit().putString(mC.getString(R.string.shared_me_aa_name), me.getAaname()).commit();
//        mSp.edit().putInt(mC.getString(R.string.shared_me_res_no), me.getAaresno()).commit();

        me.setTrip(mSp.getString(mC.getString(R.string.shared_me_trip), null));


//        me.setAaresno(Integer.parseInt(UtilInfo.aaNameaaResNoMap.get("mona")));
        String aaName = mSp.getString(mC.getString(R.string.shared_me_aa_name), "mona");
        me.setAaresno(UtilInfo.aaNameaaResNoMap.get(aaName));
        me.setAaname(aaName);
        me.setStat(mSp.getString(mC.getString(R.string.shared_me_stat), "通常"));
//        me.setColor255(mSp.getInt(mC.get)  cursor.getInt(cursor.getColumnIndex("aa_color_255")));
//        me.setId(cursor.getInt(cursor.getColumnIndex("id")));
//        me.setName(cursor.getString(cursor.getColumnIndex("name")));
//        me.setTrip(cursor.getString(cursor.getColumnIndex("trip")));
//        me.setColor255(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
//        me.setRed(cursor.getInt(cursor.getColumnIndex("red")));
//        me.setGreen(cursor.getInt(cursor.getColumnIndex("green")));
//        me.setBlue(cursor.getInt(cursor.getColumnIndex("blue")));
//        me.setScl(cursor.getInt(cursor.getColumnIndex("scl")));
//            person.setX(cursor.getInt(cursor.getColumnIndex("x")));
//            person.setX(cursor.getInt(cursor.getColumnIndex("y")));
//        Random random = new Random();
//        me.setX(random.nextInt(660) + 30);
//        me.setToY(random.nextInt(80) + 240);
//        /**
//         * 初期処理でaanameにaaresnoをマッピングした変数を使用する。
//         * because of changing aaresno every time
//         */
//        if (cursor.getString(cursor.getColumnIndex("aa_name")) == null) {
//            me.setAaresno(Integer.parseInt(UtilInfo.aaNameaaResNoMap.get("mona")));
//            me.setAaname("mona");
//        } else {
//            me.setAaresno(Integer.parseInt(UtilInfo.aaNameaaResNoMap.get(cursor.getString(cursor.getColumnIndex("aa_name")))));
//            me.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
//        }
//        me.setStat("通常");


        return me;
    }

    public String getUuid() {
        return this.mSp.getString(this.mC.getString(R.string.shared_uuid), null);
    }

    public String getTrip() {
        return this.mSp.getString(this.mC.getString(R.string.shared_me_trip), null);
    }

    public Room getRoom() {
        Room room = new Room();
        room.setRoomName(mSp.getString(mC.getString(R.string.shared_room_name), "エラー"));
        room.setRoomNumber(mSp.getInt(mC.getString(R.string.shared_room_num), -1));

        return room;
    }

    public void setRoom(Room room) {

        /**
         *                 String.valueOf(room.getRoomCount()),
         *                 UtilInfo.getNowDate()
         */
        mSp.edit().putString(mC.getString(R.string.shared_room_name), room.getRoomName()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_room_num), room.getRoomNumber()).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_room_count), room.getRoomCount()).commit();
        mSp.edit().putString(mC.getString(R.string.shared_room_date), UtilInfo.getNowDate()).commit();
    }

    public void getRooms() {
//        Room room = new Room();
//        int roomNo = cursor.getInt(cursor.getColumnIndex("room_no"));
//        room.setRoomNumber(roomNo);
//        String roomName = cursor.getString(cursor.getColumnIndex("room_name"));
//        room.setRoomName(roomName);
//        int roomCount = cursor.getInt(cursor.getColumnIndex("room_count"));
//        room.setRoomCount(roomCount);
//        int buttonId = cursor.getInt(cursor.getColumnIndex("button_id"));
//        room.setButtonId(buttonId);
//        int updateFlag = cursor.getInt(cursor.getColumnIndex("update_flag"));
//        room.setUpdateFlag(updateFlag);
//        linkedHashMap.put(roomNo,room);
    }

    public boolean getRapidCheck() {
        return mSp.getBoolean(mC.getString(R.string.shared_rapid_check), false);
    }

    private boolean getChangeColorComment() {
        return mSp.getBoolean(mC.getString(R.string.shared_comment_change_check), false);
    }

    public void setChangeColorComment(boolean check) {
        mSp.edit().putBoolean(mC.getString(R.string.shared_comment_change_check), check).commit();
        Stub.commentChange = check;
    }

    private boolean getBackgroundColor() {
        return mSp.getBoolean(mC.getString(R.string.shared_background_color_check), false);
    }

    public void setBackgroundColor(boolean check) {
        mSp.edit().putBoolean(mC.getString(R.string.shared_background_color_check), check).commit();
        Stub.backgroundChange = check;
    }

    public int getReconectCount() {
        return mSp.getInt(mC.getString(R.string.shared_reconnect_value), 3);
    }

    public void setReconectCount(int count) {
        mSp.edit().putInt(mC.getString(R.string.shared_reconnect_value), count).commit();
    }

    public void setRapidRoomValue(int value) {
        mSp.edit().putInt(mC.getString(R.string.shared_rapid_room_value), value).commit();
    }

    public Room getRapidRoom() {
        Room room = new Room();
        room.setRoomNumber(mSp.getInt(mC.getString(R.string.shared_rapid_room_value), -1));
        room.setRoomName("kobetu");
        return room;
    }

    public int getRapidRoomValue() {
        return mSp.getInt(mC.getString(R.string.shared_rapid_room_value), -1);
    }

    public void setRapidCheck(boolean check) {
        mSp.edit().putBoolean(mC.getString(R.string.shared_rapid_check), check).commit();
    }

    public void setMeMonaId(int monaId) {
        mSp.edit().putInt(mC.getString(R.string.shared_me_mona_id), monaId).commit();
        MeMonaId.getMe().setMonaId(monaId);
//        MeMonaId.monaId = monaId;
    }

    public int getMonaId() {
        return mSp.getInt(mC.getString(R.string.shared_me_mona_id), -1);
    }

    public void setLog(boolean b) {
        mSp.edit().putBoolean(mC.getString(R.string.shared_log), b).commit();
    }

    public void setDevelop(boolean b) {
        mSp.edit().putBoolean(mC.getString(R.string.shared_develop), b).commit();
    }

    public void setShowDevelop(boolean b) {
        mSp.edit().putBoolean(mC.getString(R.string.shared_show_develop), b).commit();
    }

    public boolean getShowDevelop() {
        return mSp.getBoolean(mC.getString(R.string.shared_show_develop), false);
    }

    public boolean getDevelop() {
        return mSp.getBoolean(mC.getString(R.string.shared_develop), false);
    }

    public boolean getLog() {
        return mSp.getBoolean(mC.getString(R.string.shared_log), false);
    }

    public void setNotify(ArrayList<String> arrayList) {

        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) == null) {
                mSp.edit().remove("notify" + i).commit();
                continue;
            }
            mSp.edit().putString("notify" + i, arrayList.get(i)).commit();

        }
        MeMonaId.notifyArray = arrayList;
    }

    public ArrayList<String> getNotify() {
        ArrayList<String> arrayList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            String result = mSp.getString("notify" + i, null);
            if (result == null) {
                continue;
            }
            arrayList.add(result);
        }
        return arrayList;
    }

    public void setRotate(boolean check) {
        mSp.edit().putBoolean(mC.getString(R.string.shared_rotate), check).commit();
    }

    public boolean getRotate() {
        return this.mSp.getBoolean(this.mC.getString(R.string.shared_rotate), false);
    }

    public void setTateRegu(int tateRegu) {
        mSp.edit().putInt(mC.getString(R.string.shared_tate_regu), tateRegu).commit();
    }

    public int getTateRegu() {
        return this.mSp.getInt(this.mC.getString(R.string.shared_tate_regu), 0);
    }


    public void getNameaaResNoMap() {

    }

    public LinkedHashMap<String, Integer> getPage(int mParam) {
        LinkedHashMap<String, Integer> page = null;
        switch (mParam) {
            case 1:
                page = new LinkedHashMap<>();
                page.put("abogado", R.drawable.ic_svg_abogado);
                page.put("agemona", R.drawable.ic_svg_agemona);
                page.put("alice", R.drawable.ic_svg_alice);
                page.put("anamona", R.drawable.ic_svg_anamona);
                page.put("aramaki", R.drawable.ic_svg_aramaki);
                page.put("asou", R.drawable.ic_svg_asou);
                page.put("bana", R.drawable.ic_svg_bana);
                page.put("batu", R.drawable.ic_svg_batu);
                page.put("boljoa", R.drawable.ic_svg_boljoa);
                break;
            case 2:
                page = new LinkedHashMap<>();
                page.put("boljoa3", R.drawable.ic_svg_boljoa3);
                page.put("boljoa4", R.drawable.ic_svg_boljoa4);
                page.put("charhan", R.drawable.ic_svg_charhan);
                page.put("chichon", R.drawable.ic_svg_chichon);
                page.put("chotto1", R.drawable.ic_svg_chotto1);
                page.put("chotto2", R.drawable.ic_svg_chotto2);
                page.put("chotto3", R.drawable.ic_svg_chotto3);
                page.put("coc2", R.drawable.ic_svg_coc2);
                page.put("cock", R.drawable.ic_svg_cock);
                break;
            case 3:
                page = new LinkedHashMap<>();
                page.put("dokuo", R.drawable.ic_svg_dokuo);
                page.put("dokuo2", R.drawable.ic_svg_dokuo2);
                page.put("foppa", R.drawable.ic_svg_foppa);
                page.put("fusa", R.drawable.ic_svg_fusa);
                page.put("fuun", R.drawable.ic_svg_fuun);
                page.put("gaku", R.drawable.ic_svg_gaku);
                page.put("gakuri", R.drawable.ic_svg_gakuri);
                page.put("gari", R.drawable.ic_svg_gari);
                page.put("gerara", R.drawable.ic_svg_gerara);
                break;
            case 4:
                page = new LinkedHashMap<>();
                page.put("giko", R.drawable.ic_svg_giko);
                page.put("ging", R.drawable.ic_svg_ging);
                page.put("ginu", R.drawable.ic_svg_ginu);
                page.put("gyouza", R.drawable.ic_svg_gyouza);
                page.put("haa", R.drawable.ic_svg_haa);
                page.put("haka", R.drawable.ic_svg_haka);
                page.put("hat2", R.drawable.ic_svg_hat2);
                page.put("hati", R.drawable.ic_svg_hati);
                page.put("hati3", R.drawable.ic_svg_hati3);
                break;
            case 5:
                page = new LinkedHashMap<>();
                page.put("hati4", R.drawable.ic_svg_hati4);
                page.put("hikk", R.drawable.ic_svg_hikk);
                page.put("hiyoko", R.drawable.ic_svg_hiyoko);
                page.put("hokkyoku6", R.drawable.ic_svg_hokkyoku6);
                page.put("hosh", R.drawable.ic_svg_hosh);
                page.put("ichi", R.drawable.ic_svg_ichi);
                page.put("ichi2", R.drawable.ic_svg_ichi2);
                page.put("ichineko", R.drawable.ic_svg_ichineko);
                page.put("iiajan", R.drawable.ic_svg_iiajan);
                break;
            case 6:
                page = new LinkedHashMap<>();
                page.put("iyou", R.drawable.ic_svg_iyou);
                page.put("jien", R.drawable.ic_svg_jien);
                page.put("joruju", R.drawable.ic_svg_joruju);
                page.put("kabin", R.drawable.ic_svg_kabin);
                page.put("kagami", R.drawable.ic_svg_kagami);
                page.put("kamemona", R.drawable.ic_svg_kamemona);
                page.put("kappappa", R.drawable.ic_svg_kappappa);
                page.put("kasiwa", R.drawable.ic_svg_kasiwa);
                page.put("kato", R.drawable.ic_svg_kato);
                break;
            case 7:
                page = new LinkedHashMap<>();
                page.put("kikko2", R.drawable.ic_svg_kikko2);
                page.put("kita", R.drawable.ic_svg_kita);
                page.put("koit", R.drawable.ic_svg_koit);
                page.put("koya", R.drawable.ic_svg_koya);
                page.put("kunoichi", R.drawable.ic_svg_kunoichi);
                page.put("kuromimi", R.drawable.ic_svg_kuromimi);
                page.put("kyaku", R.drawable.ic_svg_kyaku);
                page.put("maji", R.drawable.ic_svg_maji);
                page.put("marumimi", R.drawable.ic_svg_marumimi);
                break;
            case 8:
                page = new LinkedHashMap<>();
                page.put("maturi", R.drawable.ic_svg_maturi);
                page.put("mina", R.drawable.ic_svg_mina);
                page.put("miwa", R.drawable.ic_svg_miwa);
                page.put("mona", R.drawable.ic_svg_mona);
                page.put("monaka", R.drawable.ic_svg_monaka);
                page.put("mora", R.drawable.ic_svg_mora);
                page.put("mosamosa1", R.drawable.ic_svg_mosamosa1);
                page.put("mosamosa2", R.drawable.ic_svg_mosamosa2);
                page.put("mosamosa3", R.drawable.ic_svg_mosamosa3);
                break;
            case 9:
                page = new LinkedHashMap<>();
                page.put("mosamosa4", R.drawable.ic_svg_mosamosa4);
                page.put("mossari", R.drawable.ic_svg_mossari);
                page.put("moudamepo", R.drawable.ic_svg_moudamepo);
                page.put("mouk", R.drawable.ic_svg_mouk);
                page.put("mouk1", R.drawable.ic_svg_mouk1);
                page.put("mouk2", R.drawable.ic_svg_mouk2);
                page.put("nanyo", R.drawable.ic_svg_nanyo);
                page.put("nazoko", R.drawable.ic_svg_nazoko);
                page.put("nezumi", R.drawable.ic_svg_nezumi);
                break;
            case 10:
                page = new LinkedHashMap<>();
                page.put("nida", R.drawable.ic_svg_nida);
                page.put("niku", R.drawable.ic_svg_niku);
                page.put("nin3", R.drawable.ic_svg_nin3);
                page.put("niraime", R.drawable.ic_svg_niraime);
                page.put("niraime2", R.drawable.ic_svg_niraime2);
                page.put("niramusume", R.drawable.ic_svg_niramusume);
                page.put("niraneko", R.drawable.ic_svg_niraneko);
                page.put("nyog", R.drawable.ic_svg_nyog);
                page.put("oni", R.drawable.ic_svg_oni);
                break;
            case 11:
                page = new LinkedHashMap<>();
                page.put("oniini", R.drawable.ic_svg_oniini);
                page.put("oomimi", R.drawable.ic_svg_oomimi);
                page.put("osa", R.drawable.ic_svg_osa);
                page.put("papi", R.drawable.ic_svg_papi);
                page.put("polygon", R.drawable.ic_svg_polygon);
                page.put("ppa2", R.drawable.ic_svg_ppa2);
                page.put("puru", R.drawable.ic_svg_puru);
                page.put("ranta", R.drawable.ic_svg_ranta);
                page.put("remona", R.drawable.ic_svg_remona);
                break;
            case 12:
                page = new LinkedHashMap<>();
                page.put("ri_man", R.drawable.ic_svg_ri_man);
                page.put("riru", R.drawable.ic_svg_riru);
                page.put("sai", R.drawable.ic_svg_sai);
                page.put("sens", R.drawable.ic_svg_sens);
                page.put("shaitama", R.drawable.ic_svg_shaitama);
                page.put("shak", R.drawable.ic_svg_shak);
                page.put("shob", R.drawable.ic_svg_shob);
                page.put("shodai", R.drawable.ic_svg_shodai);
                page.put("sii2", R.drawable.ic_svg_sii2);
                break;
            case 13:
                page = new LinkedHashMap<>();
                page.put("sika", R.drawable.ic_svg_sika);
                page.put("sira", R.drawable.ic_svg_sira);
                page.put("siranaiwa", R.drawable.ic_svg_siranaiwa);
                page.put("sugoi3", R.drawable.ic_svg_sugoi3);
                page.put("sumaso2", R.drawable.ic_svg_sumaso2);
                page.put("suwarifusa", R.drawable.ic_svg_suwarifusa);
                page.put("tahara", R.drawable.ic_svg_tahara);
                page.put("tatigiko", R.drawable.ic_svg_tatigiko);
                page.put("taxi", R.drawable.ic_svg_taxi);
                break;
            case 14:
                page = new LinkedHashMap<>();
                page.put("tibifusa", R.drawable.ic_svg_tibifusa);
                page.put("tibigiko", R.drawable.ic_svg_tibigiko);
                page.put("tibisii", R.drawable.ic_svg_tibisii);
                page.put("tiraneyo", R.drawable.ic_svg_tiraneyo);
                page.put("tofu", R.drawable.ic_svg_tofu);
                page.put("tokei", R.drawable.ic_svg_tokei);
                page.put("tuu", R.drawable.ic_svg_tuu);
                page.put("uma", R.drawable.ic_svg_uma);
                page.put("unknown2", R.drawable.ic_svg_unknown2);
                break;
            case 15:
                page = new LinkedHashMap<>();
                page.put("unko", R.drawable.ic_svg_unko);
                page.put("urara", R.drawable.ic_svg_urara);
                page.put("usi", R.drawable.ic_svg_usi);
                page.put("wachoi", R.drawable.ic_svg_wachoi);
                page.put("welneco2", R.drawable.ic_svg_welneco2);
                page.put("yamazaki1", R.drawable.ic_svg_yamazaki1);
                page.put("yamazaki2", R.drawable.ic_svg_yamazaki2);
                page.put("yokan", R.drawable.ic_svg_yokan);
                page.put("zonu", R.drawable.ic_svg_zonu);
                break;
            case 16:
                page = new LinkedHashMap<>();
                page.put("zuza", R.drawable.ic_svg_zuza);
                break;
            default:
                Log.d(TAG, "getPage");
                break;
        }
        return page;
    }

//    public LinkedHashMap<String, String> getPage(int mParam) {
//        LinkedHashMap<String, String> page = null;
//        switch (mParam) {
//            case 1:
//                page = new LinkedHashMap<>();
//                page.put("abogado", R.drawable.ic_svg_abogado + "");
//                page.put("agemona", R.drawable.ic_svg_agemona + "");
//                page.put("alice", R.drawable.ic_svg_alice + "");
//                page.put("anamona", R.drawable.ic_svg_anamona + "");
//                page.put("aramaki", R.drawable.ic_svg_aramaki + "");
//                page.put("asou", R.drawable.ic_svg_asou + "");
//                page.put("bana", R.drawable.ic_svg_bana + "");
//                page.put("batu", R.drawable.ic_svg_batu + "");
//                page.put("boljoa", R.drawable.ic_svg_boljoa + "");
//                break;
//            case 2:
//                page = new LinkedHashMap<>();
//                page.put("boljoa3", R.drawable.ic_svg_boljoa3 + "");
//                page.put("boljoa4", R.drawable.ic_svg_boljoa4 + "");
//                page.put("charhan", R.drawable.ic_svg_charhan + "");
//                page.put("chichon", R.drawable.ic_svg_chichon + "");
//                page.put("chotto1", R.drawable.ic_svg_chotto1 + "");
//                page.put("chotto2", R.drawable.ic_svg_chotto2 + "");
//                page.put("chotto3", R.drawable.ic_svg_chotto3 + "");
//                page.put("coc2", R.drawable.ic_svg_coc2 + "");
//                page.put("cock", R.drawable.ic_svg_cock + "");
//                break;
//            case 3:
//                page = new LinkedHashMap<>();
//                page.put("dokuo", R.drawable.ic_svg_dokuo + "");
//                page.put("dokuo2", R.drawable.ic_svg_dokuo2 + "");
//                page.put("foppa", R.drawable.ic_svg_foppa + "");
//                page.put("fusa", R.drawable.ic_svg_fusa + "");
//                page.put("fuun", R.drawable.ic_svg_fuun + "");
//                page.put("gaku", R.drawable.ic_svg_gaku + "");
//                page.put("gakuri", R.drawable.ic_svg_gakuri + "");
//                page.put("gari", R.drawable.ic_svg_gari + "");
//                page.put("gerara", R.drawable.ic_svg_gerara + "");
//                break;
//            case 4:
//                page = new LinkedHashMap<>();
//                page.put("giko", R.drawable.ic_svg_giko + "");
//                page.put("ging", R.drawable.ic_svg_ging + "");
//                page.put("ginu", R.drawable.ic_svg_ginu + "");
//                page.put("gyouza", R.drawable.ic_svg_gyouza + "");
//                page.put("haa", R.drawable.ic_svg_haa + "");
//                page.put("haka", R.drawable.ic_svg_haka + "");
//                page.put("hat2", R.drawable.ic_svg_hat2 + "");
//                page.put("hati", R.drawable.ic_svg_hati + "");
//                page.put("hati3", R.drawable.ic_svg_hati3 + "");
//                break;
//            case 5:
//                page = new LinkedHashMap<>();
//                page.put("hati4", R.drawable.ic_svg_hati4 + "");
//                page.put("hikk", R.drawable.ic_svg_hikk + "");
//                page.put("hiyoko", R.drawable.ic_svg_hiyoko + "");
//                page.put("hokkyoku6", R.drawable.ic_svg_hokkyoku6 + "");
//                page.put("hosh", R.drawable.ic_svg_hosh + "");
//                page.put("ichi", R.drawable.ic_svg_ichi + "");
//                page.put("ichi2", R.drawable.ic_svg_ichi2 + "");
//                page.put("ichineko", R.drawable.ic_svg_ichineko + "");
//                page.put("iiajan", R.drawable.ic_svg_iiajan + "");
//                break;
//            case 6:
//                page = new LinkedHashMap<>();
//                page.put("iyou", R.drawable.ic_svg_iyou + "");
//                page.put("jien", R.drawable.ic_svg_jien + "");
//                page.put("joruju", R.drawable.ic_svg_joruju + "");
//                page.put("kabin", R.drawable.ic_svg_kabin + "");
//                page.put("kagami", R.drawable.ic_svg_kagami + "");
//                page.put("kamemona", R.drawable.ic_svg_kamemona + "");
//                page.put("kappappa", R.drawable.ic_svg_kappappa + "");
//                page.put("kasiwa", R.drawable.ic_svg_kasiwa + "");
//                page.put("kato", R.drawable.ic_svg_kato + "");
//                break;
//            case 7:
//                page = new LinkedHashMap<>();
//                page.put("kikko2", R.drawable.ic_svg_kikko2 + "");
//                page.put("kita", R.drawable.ic_svg_kita + "");
//                page.put("koit", R.drawable.ic_svg_koit + "");
//                page.put("koya", R.drawable.ic_svg_koya + "");
//                page.put("kunoichi", R.drawable.ic_svg_kunoichi + "");
//                page.put("kuromimi", R.drawable.ic_svg_kuromimi + "");
//                page.put("kyaku", R.drawable.ic_svg_kyaku + "");
//                page.put("maji", R.drawable.ic_svg_maji + "");
//                page.put("marumimi", R.drawable.ic_svg_marumimi + "");
//                break;
//            case 8:
//                page = new LinkedHashMap<>();
//                page.put("maturi", R.drawable.ic_svg_maturi + "");
//                page.put("mina", R.drawable.ic_svg_mina + "");
//                page.put("miwa", R.drawable.ic_svg_miwa + "");
//                page.put("mona", R.drawable.ic_svg_mona + "");
//                page.put("monaka", R.drawable.ic_svg_monaka + "");
//                page.put("mora", R.drawable.ic_svg_mora + "");
//                page.put("mosamosa1", R.drawable.ic_svg_mosamosa1 + "");
//                page.put("mosamosa2", R.drawable.ic_svg_mosamosa2 + "");
//                page.put("mosamosa3", R.drawable.ic_svg_mosamosa3 + "");
//                break;
//            case 9:
//                page = new LinkedHashMap<>();
//                page.put("mosamosa4", R.drawable.ic_svg_mosamosa4 + "");
//                page.put("mossari", R.drawable.ic_svg_mossari + "");
//                page.put("moudamepo", R.drawable.ic_svg_moudamepo + "");
//                page.put("mouk", R.drawable.ic_svg_mouk + "");
//                page.put("mouk1", R.drawable.ic_svg_mouk1 + "");
//                page.put("mouk2", R.drawable.ic_svg_mouk2 + "");
//                page.put("nanyo", R.drawable.ic_svg_nanyo + "");
//                page.put("nazoko", R.drawable.ic_svg_nazoko + "");
//                page.put("nezumi", R.drawable.ic_svg_nezumi + "");
//                break;
//            case 10:
//                page = new LinkedHashMap<>();
//                page.put("nida", R.drawable.ic_svg_nida + "");
//                page.put("niku", R.drawable.ic_svg_niku + "");
//                page.put("nin3", R.drawable.ic_svg_nin3 + "");
//                page.put("niraime", R.drawable.ic_svg_niraime + "");
//                page.put("niraime2", R.drawable.ic_svg_niraime2 + "");
//                page.put("niramusume", R.drawable.ic_svg_niramusume + "");
//                page.put("niraneko", R.drawable.ic_svg_niraneko + "");
//                page.put("nyog", R.drawable.ic_svg_nyog + "");
//                page.put("oni", R.drawable.ic_svg_oni + "");
//                break;
//            case 11:
//                page = new LinkedHashMap<>();
//                page.put("oniini", R.drawable.ic_svg_oniini + "");
//                page.put("oomimi", R.drawable.ic_svg_oomimi + "");
//                page.put("osa", R.drawable.ic_svg_osa + "");
//                page.put("papi", R.drawable.ic_svg_papi + "");
//                page.put("polygon", R.drawable.ic_svg_polygon + "");
//                page.put("ppa2", R.drawable.ic_svg_ppa2 + "");
//                page.put("puru", R.drawable.ic_svg_puru + "");
//                page.put("ranta", R.drawable.ic_svg_ranta + "");
//                page.put("remona", R.drawable.ic_svg_remona + "");
//                break;
//            case 12:
//                page = new LinkedHashMap<>();
//                page.put("ri_man", R.drawable.ic_svg_ri_man + "");
//                page.put("riru", R.drawable.ic_svg_riru + "");
//                page.put("sai", R.drawable.ic_svg_sai + "");
//                page.put("sens", R.drawable.ic_svg_sens + "");
//                page.put("shaitama", R.drawable.ic_svg_shaitama + "");
//                page.put("shak", R.drawable.ic_svg_shak + "");
//                page.put("shob", R.drawable.ic_svg_shob + "");
//                page.put("shodai", R.drawable.ic_svg_shodai + "");
//                page.put("sii2", R.drawable.ic_svg_sii2 + "");
//                break;
//            case 13:
//                page = new LinkedHashMap<>();
//                page.put("sika", R.drawable.ic_svg_sika + "");
//                page.put("sira", R.drawable.ic_svg_sira + "");
//                page.put("siranaiwa", R.drawable.ic_svg_siranaiwa + "");
//                page.put("sugoi3", R.drawable.ic_svg_sugoi3 + "");
//                page.put("sumaso2", R.drawable.ic_svg_sumaso2 + "");
//                page.put("suwarifusa", R.drawable.ic_svg_suwarifusa + "");
//                page.put("tahara", R.drawable.ic_svg_tahara + "");
//                page.put("tatigiko", R.drawable.ic_svg_tatigiko + "");
//                page.put("taxi", R.drawable.ic_svg_taxi + "");
//                break;
//            case 14:
//                page = new LinkedHashMap<>();
//                page.put("tibifusa", R.drawable.ic_svg_tibifusa + "");
//                page.put("tibigiko", R.drawable.ic_svg_tibigiko + "");
//                page.put("tibisii", R.drawable.ic_svg_tibisii + "");
//                page.put("tiraneyo", R.drawable.ic_svg_tiraneyo + "");
//                page.put("tofu", R.drawable.ic_svg_tofu + "");
//                page.put("tokei", R.drawable.ic_svg_tokei + "");
//                page.put("tuu", R.drawable.ic_svg_tuu + "");
//                page.put("uma", R.drawable.ic_svg_uma + "");
//                page.put("unknown2", R.drawable.ic_svg_unknown2 + "");
//                break;
//            case 15:
//                page = new LinkedHashMap<>();
//                page.put("unko", R.drawable.ic_svg_unko + "");
//                page.put("urara", R.drawable.ic_svg_urara + "");
//                page.put("usi", R.drawable.ic_svg_usi + "");
//                page.put("wachoi", R.drawable.ic_svg_wachoi + "");
//                page.put("welneco2", R.drawable.ic_svg_welneco2 + "");
//                page.put("yamazaki1", R.drawable.ic_svg_yamazaki1 + "");
//                page.put("yamazaki2", R.drawable.ic_svg_yamazaki2 + "");
//                page.put("yokan", R.drawable.ic_svg_yokan + "");
//                page.put("zonu", R.drawable.ic_svg_zonu + "");
//                break;
//            case 16:
//                page = new LinkedHashMap<>();
//                page.put("zuza", R.drawable.ic_svg_zuza + "");
//                break;
//            default:
//                Log.d(TAG, "getPage");
//                break;
//        }
//        return page;
//    }


    public void init() {
//        mSp.edit().putInt("abogado", R.drawable.ic_svg_abogado).commit();
//        mSp.edit().putInt("agemona", R.drawable.ic_svg_agemona).commit();
//        mSp.edit().putInt("alice", R.drawable.ic_svg_alice).commit();
//        mSp.edit().putInt("anamona", R.drawable.ic_svg_anamona).commit();
//        mSp.edit().putInt("aramaki", R.drawable.ic_svg_aramaki).commit();
//        mSp.edit().putInt("asou", R.drawable.ic_svg_asou).commit();
//        mSp.edit().putInt("bana", R.drawable.ic_svg_bana).commit();
//        mSp.edit().putInt("batu", R.drawable.ic_svg_batu).commit();
//        mSp.edit().putInt("boljoa", R.drawable.ic_svg_boljoa).commit();
//        mSp.edit().putInt("boljoa3", R.drawable.ic_svg_boljoa3).commit();
//        mSp.edit().putInt("boljoa4", R.drawable.ic_svg_boljoa4).commit();
//        mSp.edit().putInt("charhan", R.drawable.ic_svg_charhan).commit();
//        mSp.edit().putInt("chichon", R.drawable.ic_svg_chichon).commit();
//        mSp.edit().putInt("chotto1", R.drawable.ic_svg_chotto1).commit();
//        mSp.edit().putInt("chotto2", R.drawable.ic_svg_chotto2).commit();
//        mSp.edit().putInt("chotto3", R.drawable.ic_svg_chotto3).commit();
//        mSp.edit().putInt("coc2", R.drawable.ic_svg_coc2).commit();
//        mSp.edit().putInt("cock", R.drawable.ic_svg_cock).commit();
//        mSp.edit().putInt("dokuo", R.drawable.ic_svg_dokuo).commit();
//        mSp.edit().putInt("dokuo2", R.drawable.ic_svg_dokuo2).commit();
//        mSp.edit().putInt("foppa", R.drawable.ic_svg_foppa).commit();
//        mSp.edit().putInt("fusa", R.drawable.ic_svg_fusa).commit();
//        mSp.edit().putInt("fuun", R.drawable.ic_svg_fuun).commit();
//        mSp.edit().putInt("gaku", R.drawable.ic_svg_gaku).commit();
//        mSp.edit().putInt("gakuri", R.drawable.ic_svg_gakuri).commit();
//        mSp.edit().putInt("gari", R.drawable.ic_svg_gari).commit();
//        mSp.edit().putInt("gerara", R.drawable.ic_svg_gerara).commit();
//        mSp.edit().putInt("giko", R.drawable.ic_svg_giko).commit();
//        mSp.edit().putInt("ging", R.drawable.ic_svg_ging).commit();
//        mSp.edit().putInt("ginu", R.drawable.ic_svg_ginu).commit();
//        mSp.edit().putInt("gyouza", R.drawable.ic_svg_gyouza).commit();
//        mSp.edit().putInt("haa", R.drawable.ic_svg_haa).commit();
//        mSp.edit().putInt("haka", R.drawable.ic_svg_haka).commit();
//        mSp.edit().putInt("hat2", R.drawable.ic_svg_hat2).commit();
//        mSp.edit().putInt("hati", R.drawable.ic_svg_hati).commit();
//        mSp.edit().putInt("hati3", R.drawable.ic_svg_hati3).commit();
//        mSp.edit().putInt("hati4", R.drawable.ic_svg_hati4).commit();
//        mSp.edit().putInt("hikk", R.drawable.ic_svg_hikk).commit();
//        mSp.edit().putInt("hiyoko", R.drawable.ic_svg_hiyoko).commit();
//        mSp.edit().putInt("hokkyoku6", R.drawable.ic_svg_hokkyoku6).commit();
//        mSp.edit().putInt("hosh", R.drawable.ic_svg_hosh).commit();
//        mSp.edit().putInt("ichi", R.drawable.ic_svg_ichi).commit();
//        mSp.edit().putInt("ichi2", R.drawable.ic_svg_ichi2).commit();
//        mSp.edit().putInt("ichineko", R.drawable.ic_svg_ichineko).commit();
//        mSp.edit().putInt("iiajan", R.drawable.ic_svg_iiajan).commit();
//        mSp.edit().putInt("iyou", R.drawable.ic_svg_iyou).commit();
//        mSp.edit().putInt("jien", R.drawable.ic_svg_jien).commit();
//        mSp.edit().putInt("joruju", R.drawable.ic_svg_joruju).commit();
//        mSp.edit().putInt("kabin", R.drawable.ic_svg_kabin).commit();
//        mSp.edit().putInt("kagami", R.drawable.ic_svg_kagami).commit();
//        mSp.edit().putInt("kamemona", R.drawable.ic_svg_kamemona).commit();
//        mSp.edit().putInt("kappappa", R.drawable.ic_svg_kappappa).commit();
//        mSp.edit().putInt("kasiwa", R.drawable.ic_svg_kasiwa).commit();
//        mSp.edit().putInt("kato", R.drawable.ic_svg_kato).commit();
//        mSp.edit().putInt("kikko2", R.drawable.ic_svg_kikko2).commit();
//        mSp.edit().putInt("kita", R.drawable.ic_svg_kita).commit();
//        mSp.edit().putInt("koit", R.drawable.ic_svg_koit).commit();
//        mSp.edit().putInt("koya", R.drawable.ic_svg_koya).commit();
//        mSp.edit().putInt("kunoichi", R.drawable.ic_svg_kunoichi).commit();
//        mSp.edit().putInt("kuromimi", R.drawable.ic_svg_kuromimi).commit();
//        mSp.edit().putInt("kyaku", R.drawable.ic_svg_kyaku).commit();
//        mSp.edit().putInt("maji", R.drawable.ic_svg_maji).commit();
//        mSp.edit().putInt("marumimi", R.drawable.ic_svg_marumimi).commit();
//        mSp.edit().putInt("maturi", R.drawable.ic_svg_maturi).commit();
//        mSp.edit().putInt("mina", R.drawable.ic_svg_mina).commit();
//        mSp.edit().putInt("miwa", R.drawable.ic_svg_miwa).commit();
//        mSp.edit().putInt("mona", R.drawable.ic_svg_mona).commit();
//        mSp.edit().putInt("monaka", R.drawable.ic_svg_monaka).commit();
//        mSp.edit().putInt("mora", R.drawable.ic_svg_mora).commit();
//        mSp.edit().putInt("mosamosa1", R.drawable.ic_svg_mosamosa1).commit();
//        mSp.edit().putInt("mosamosa2", R.drawable.ic_svg_mosamosa2).commit();
//        mSp.edit().putInt("mosamosa3", R.drawable.ic_svg_mosamosa3).commit();
//        mSp.edit().putInt("mosamosa4", R.drawable.ic_svg_mosamosa4).commit();
//        mSp.edit().putInt("mossari", R.drawable.ic_svg_mossari).commit();
//        mSp.edit().putInt("moudamepo", R.drawable.ic_svg_moudamepo).commit();
//        mSp.edit().putInt("mouk", R.drawable.ic_svg_mouk).commit();
//        mSp.edit().putInt("mouk1", R.drawable.ic_svg_mouk1).commit();
//        mSp.edit().putInt("mouk2", R.drawable.ic_svg_mouk2).commit();
//        mSp.edit().putInt("nanyo", R.drawable.ic_svg_nanyo).commit();
//        mSp.edit().putInt("nazoko", R.drawable.ic_svg_nazoko).commit();
//        mSp.edit().putInt("nezumi", R.drawable.ic_svg_nezumi).commit();
//        mSp.edit().putInt("nida", R.drawable.ic_svg_nida).commit();
//        mSp.edit().putInt("niku", R.drawable.ic_svg_niku).commit();
//        mSp.edit().putInt("nin3", R.drawable.ic_svg_nin3).commit();
//        mSp.edit().putInt("niraime", R.drawable.ic_svg_niraime).commit();
//        mSp.edit().putInt("niraime2", R.drawable.ic_svg_niraime2).commit();
//        mSp.edit().putInt("niramusume", R.drawable.ic_svg_niramusume).commit();
//        mSp.edit().putInt("niraneko", R.drawable.ic_svg_niraneko).commit();
//        mSp.edit().putInt("nyog", R.drawable.ic_svg_nyog).commit();
//        mSp.edit().putInt("oni", R.drawable.ic_svg_oni).commit();
//        mSp.edit().putInt("oniini", R.drawable.ic_svg_oniini).commit();
//        mSp.edit().putInt("oomimi", R.drawable.ic_svg_oomimi).commit();
//        mSp.edit().putInt("osa", R.drawable.ic_svg_osa).commit();
//        mSp.edit().putInt("papi", R.drawable.ic_svg_papi).commit();
//        mSp.edit().putInt("polygon", R.drawable.ic_svg_polygon).commit();
//        mSp.edit().putInt("ppa2", R.drawable.ic_svg_ppa2).commit();
//        mSp.edit().putInt("puru", R.drawable.ic_svg_puru).commit();
//        mSp.edit().putInt("ranta", R.drawable.ic_svg_ranta).commit();
//        mSp.edit().putInt("remona", R.drawable.ic_svg_remona).commit();
//        mSp.edit().putInt("ri_man", R.drawable.ic_svg_ri_man).commit();
//        mSp.edit().putInt("riru", R.drawable.ic_svg_riru).commit();
//        mSp.edit().putInt("sai", R.drawable.ic_svg_sai).commit();
//        mSp.edit().putInt("sens", R.drawable.ic_svg_sens).commit();
//        mSp.edit().putInt("shaitama", R.drawable.ic_svg_shaitama).commit();
//        mSp.edit().putInt("shak", R.drawable.ic_svg_shak).commit();
//        mSp.edit().putInt("shob", R.drawable.ic_svg_shob).commit();
//        mSp.edit().putInt("shodai", R.drawable.ic_svg_shodai).commit();
//        mSp.edit().putInt("sii2", R.drawable.ic_svg_sii2).commit();
//        mSp.edit().putInt("sika", R.drawable.ic_svg_sika).commit();
//        mSp.edit().putInt("sira", R.drawable.ic_svg_sira).commit();
//        mSp.edit().putInt("siranaiwa", R.drawable.ic_svg_siranaiwa).commit();
//        mSp.edit().putInt("sugoi3", R.drawable.ic_svg_sugoi3).commit();
//        mSp.edit().putInt("sumaso2", R.drawable.ic_svg_sumaso2).commit();
//        mSp.edit().putInt("suwarifusa", R.drawable.ic_svg_suwarifusa).commit();
//        mSp.edit().putInt("tahara", R.drawable.ic_svg_tahara).commit();
//        mSp.edit().putInt("tatigiko", R.drawable.ic_svg_tatigiko).commit();
//        mSp.edit().putInt("taxi", R.drawable.ic_svg_taxi).commit();
//        mSp.edit().putInt("tibifusa", R.drawable.ic_svg_tibifusa).commit();
//        mSp.edit().putInt("tibigiko", R.drawable.ic_svg_tibigiko).commit();
//        mSp.edit().putInt("tibisii", R.drawable.ic_svg_tibisii).commit();
//        mSp.edit().putInt("tiraneyo", R.drawable.ic_svg_tiraneyo).commit();
//        mSp.edit().putInt("tofu", R.drawable.ic_svg_tofu).commit();
//        mSp.edit().putInt("tokei", R.drawable.ic_svg_tokei).commit();
//        mSp.edit().putInt("tuu", R.drawable.ic_svg_tuu).commit();
//        mSp.edit().putInt("uma", R.drawable.ic_svg_uma).commit();
//        mSp.edit().putInt("unknown2", R.drawable.ic_svg_unknown2).commit();
//        mSp.edit().putInt("unko", R.drawable.ic_svg_unko).commit();
//        mSp.edit().putInt("urara", R.drawable.ic_svg_urara).commit();
//        mSp.edit().putInt("usi", R.drawable.ic_svg_usi).commit();
//        mSp.edit().putInt("wachoi", R.drawable.ic_svg_wachoi).commit();
//        mSp.edit().putInt("welneco2", R.drawable.ic_svg_welneco2).commit();
//        mSp.edit().putInt("yamazaki1", R.drawable.ic_svg_yamazaki1).commit();
//        mSp.edit().putInt("yamazaki2", R.drawable.ic_svg_yamazaki2).commit();
//        mSp.edit().putInt("yokan", R.drawable.ic_svg_yokan).commit();
//        mSp.edit().putInt("zonu", R.drawable.ic_svg_zonu).commit();
//        mSp.edit().putInt("zuza", R.drawable.ic_svg_zuza).commit();

        this.aaNameaaResNoMap.put("abogado", R.drawable.ic_svg_abogado );
        this.aaNameaaResNoMap.put("agemona", R.drawable.ic_svg_agemona );
        this.aaNameaaResNoMap.put("alice", R.drawable.ic_svg_alice );
        this.aaNameaaResNoMap.put("anamona", R.drawable.ic_svg_anamona );
        this.aaNameaaResNoMap.put("aramaki", R.drawable.ic_svg_aramaki );
        this.aaNameaaResNoMap.put("asou", R.drawable.ic_svg_asou );
        this.aaNameaaResNoMap.put("bana", R.drawable.ic_svg_bana );
        this.aaNameaaResNoMap.put("batu", R.drawable.ic_svg_batu );
        this.aaNameaaResNoMap.put("boljoa", R.drawable.ic_svg_boljoa );
        this.aaNameaaResNoMap.put("boljoa3", R.drawable.ic_svg_boljoa3 );
        this.aaNameaaResNoMap.put("boljoa4", R.drawable.ic_svg_boljoa4 );
        this.aaNameaaResNoMap.put("charhan", R.drawable.ic_svg_charhan );
        this.aaNameaaResNoMap.put("chichon", R.drawable.ic_svg_chichon );
        this.aaNameaaResNoMap.put("chotto1", R.drawable.ic_svg_chotto1 );
        this.aaNameaaResNoMap.put("chotto2", R.drawable.ic_svg_chotto2 );
        this.aaNameaaResNoMap.put("chotto3", R.drawable.ic_svg_chotto3 );
        this.aaNameaaResNoMap.put("coc2", R.drawable.ic_svg_coc2 );
        this.aaNameaaResNoMap.put("cock", R.drawable.ic_svg_cock );
        this.aaNameaaResNoMap.put("dokuo", R.drawable.ic_svg_dokuo );
        this.aaNameaaResNoMap.put("dokuo2", R.drawable.ic_svg_dokuo2 );
        this.aaNameaaResNoMap.put("foppa", R.drawable.ic_svg_foppa );
        this.aaNameaaResNoMap.put("fusa", R.drawable.ic_svg_fusa );
        this.aaNameaaResNoMap.put("fuun", R.drawable.ic_svg_fuun );
        this.aaNameaaResNoMap.put("gaku", R.drawable.ic_svg_gaku );
        this.aaNameaaResNoMap.put("gakuri", R.drawable.ic_svg_gakuri );
        this.aaNameaaResNoMap.put("gari", R.drawable.ic_svg_gari );
        this.aaNameaaResNoMap.put("gerara", R.drawable.ic_svg_gerara );
        this.aaNameaaResNoMap.put("giko", R.drawable.ic_svg_giko );
        this.aaNameaaResNoMap.put("ging", R.drawable.ic_svg_ging );
        this.aaNameaaResNoMap.put("ginu", R.drawable.ic_svg_ginu );
        this.aaNameaaResNoMap.put("gyouza", R.drawable.ic_svg_gyouza );
        this.aaNameaaResNoMap.put("haa", R.drawable.ic_svg_haa );
        this.aaNameaaResNoMap.put("haka", R.drawable.ic_svg_haka );
        this.aaNameaaResNoMap.put("hat2", R.drawable.ic_svg_hat2 );
        this.aaNameaaResNoMap.put("hati", R.drawable.ic_svg_hati );
        this.aaNameaaResNoMap.put("hati3", R.drawable.ic_svg_hati3 );
        this.aaNameaaResNoMap.put("hati4", R.drawable.ic_svg_hati4 );
        this.aaNameaaResNoMap.put("hikk", R.drawable.ic_svg_hikk );
        this.aaNameaaResNoMap.put("hiyoko", R.drawable.ic_svg_hiyoko );
        this.aaNameaaResNoMap.put("hokkyoku6", R.drawable.ic_svg_hokkyoku6 );
        this.aaNameaaResNoMap.put("hosh", R.drawable.ic_svg_hosh );
        this.aaNameaaResNoMap.put("ichi", R.drawable.ic_svg_ichi );
        this.aaNameaaResNoMap.put("ichi2", R.drawable.ic_svg_ichi2 );
        this.aaNameaaResNoMap.put("ichineko", R.drawable.ic_svg_ichineko );
        this.aaNameaaResNoMap.put("iiajan", R.drawable.ic_svg_iiajan );
        this.aaNameaaResNoMap.put("iyou", R.drawable.ic_svg_iyou );
        this.aaNameaaResNoMap.put("jien", R.drawable.ic_svg_jien );
        this.aaNameaaResNoMap.put("joruju", R.drawable.ic_svg_joruju );
        this.aaNameaaResNoMap.put("kabin", R.drawable.ic_svg_kabin );
        this.aaNameaaResNoMap.put("kagami", R.drawable.ic_svg_kagami );
        this.aaNameaaResNoMap.put("kamemona", R.drawable.ic_svg_kamemona );
        this.aaNameaaResNoMap.put("kappappa", R.drawable.ic_svg_kappappa );
        this.aaNameaaResNoMap.put("kasiwa", R.drawable.ic_svg_kasiwa );
        this.aaNameaaResNoMap.put("kato", R.drawable.ic_svg_kato );
        this.aaNameaaResNoMap.put("kikko2", R.drawable.ic_svg_kikko2 );
        this.aaNameaaResNoMap.put("kita", R.drawable.ic_svg_kita );
        this.aaNameaaResNoMap.put("koit", R.drawable.ic_svg_koit );
        this.aaNameaaResNoMap.put("koya", R.drawable.ic_svg_koya );
        this.aaNameaaResNoMap.put("kunoichi", R.drawable.ic_svg_kunoichi );
        this.aaNameaaResNoMap.put("kuromimi", R.drawable.ic_svg_kuromimi );
        this.aaNameaaResNoMap.put("kyaku", R.drawable.ic_svg_kyaku );
        this.aaNameaaResNoMap.put("maji", R.drawable.ic_svg_maji );
        this.aaNameaaResNoMap.put("marumimi", R.drawable.ic_svg_marumimi );
        this.aaNameaaResNoMap.put("maturi", R.drawable.ic_svg_maturi );
        this.aaNameaaResNoMap.put("mina", R.drawable.ic_svg_mina );
        this.aaNameaaResNoMap.put("miwa", R.drawable.ic_svg_miwa );
        this.aaNameaaResNoMap.put("mona", R.drawable.ic_svg_mona );
        this.aaNameaaResNoMap.put("monaka", R.drawable.ic_svg_monaka );
        this.aaNameaaResNoMap.put("mora", R.drawable.ic_svg_mora );
        this.aaNameaaResNoMap.put("mosamosa1", R.drawable.ic_svg_mosamosa1 );
        this.aaNameaaResNoMap.put("mosamosa2", R.drawable.ic_svg_mosamosa2 );
        this.aaNameaaResNoMap.put("mosamosa3", R.drawable.ic_svg_mosamosa3 );
        this.aaNameaaResNoMap.put("mosamosa4", R.drawable.ic_svg_mosamosa4 );
        this.aaNameaaResNoMap.put("mossari", R.drawable.ic_svg_mossari );
        this.aaNameaaResNoMap.put("moudamepo", R.drawable.ic_svg_moudamepo );
        this.aaNameaaResNoMap.put("mouk", R.drawable.ic_svg_mouk );
        this.aaNameaaResNoMap.put("mouk1", R.drawable.ic_svg_mouk1 );
        this.aaNameaaResNoMap.put("mouk2", R.drawable.ic_svg_mouk2 );
        this.aaNameaaResNoMap.put("nanyo", R.drawable.ic_svg_nanyo );
        this.aaNameaaResNoMap.put("nazoko", R.drawable.ic_svg_nazoko );
        this.aaNameaaResNoMap.put("nezumi", R.drawable.ic_svg_nezumi );
        this.aaNameaaResNoMap.put("nida", R.drawable.ic_svg_nida );
        this.aaNameaaResNoMap.put("niku", R.drawable.ic_svg_niku );
        this.aaNameaaResNoMap.put("nin3", R.drawable.ic_svg_nin3 );
        this.aaNameaaResNoMap.put("niraime", R.drawable.ic_svg_niraime );
        this.aaNameaaResNoMap.put("niraime2", R.drawable.ic_svg_niraime2 );
        this.aaNameaaResNoMap.put("niramusume", R.drawable.ic_svg_niramusume );
        this.aaNameaaResNoMap.put("niraneko", R.drawable.ic_svg_niraneko );
        this.aaNameaaResNoMap.put("nyog", R.drawable.ic_svg_nyog );
        this.aaNameaaResNoMap.put("oni", R.drawable.ic_svg_oni );
        this.aaNameaaResNoMap.put("oniini", R.drawable.ic_svg_oniini );
        this.aaNameaaResNoMap.put("oomimi", R.drawable.ic_svg_oomimi );
        this.aaNameaaResNoMap.put("osa", R.drawable.ic_svg_osa );
        this.aaNameaaResNoMap.put("papi", R.drawable.ic_svg_papi );
        this.aaNameaaResNoMap.put("polygon", R.drawable.ic_svg_polygon );
        this.aaNameaaResNoMap.put("ppa2", R.drawable.ic_svg_ppa2 );
        this.aaNameaaResNoMap.put("puru", R.drawable.ic_svg_puru );
        this.aaNameaaResNoMap.put("ranta", R.drawable.ic_svg_ranta );
        this.aaNameaaResNoMap.put("remona", R.drawable.ic_svg_remona );
        this.aaNameaaResNoMap.put("ri_man", R.drawable.ic_svg_ri_man );
        this.aaNameaaResNoMap.put("riru", R.drawable.ic_svg_riru );
        this.aaNameaaResNoMap.put("sai", R.drawable.ic_svg_sai );
        this.aaNameaaResNoMap.put("sens", R.drawable.ic_svg_sens );
        this.aaNameaaResNoMap.put("shaitama", R.drawable.ic_svg_shaitama );
        this.aaNameaaResNoMap.put("shak", R.drawable.ic_svg_shak );
        this.aaNameaaResNoMap.put("shob", R.drawable.ic_svg_shob );
        this.aaNameaaResNoMap.put("shodai", R.drawable.ic_svg_shodai );
        this.aaNameaaResNoMap.put("sii2", R.drawable.ic_svg_sii2 );
        this.aaNameaaResNoMap.put("sika", R.drawable.ic_svg_sika );
        this.aaNameaaResNoMap.put("sira", R.drawable.ic_svg_sira );
        this.aaNameaaResNoMap.put("siranaiwa", R.drawable.ic_svg_siranaiwa );
        this.aaNameaaResNoMap.put("sugoi3", R.drawable.ic_svg_sugoi3 );
        this.aaNameaaResNoMap.put("sumaso2", R.drawable.ic_svg_sumaso2 );
        this.aaNameaaResNoMap.put("suwarifusa", R.drawable.ic_svg_suwarifusa );
        this.aaNameaaResNoMap.put("tahara", R.drawable.ic_svg_tahara );
        this.aaNameaaResNoMap.put("tatigiko", R.drawable.ic_svg_tatigiko );
        this.aaNameaaResNoMap.put("taxi", R.drawable.ic_svg_taxi );
        this.aaNameaaResNoMap.put("tibifusa", R.drawable.ic_svg_tibifusa );
        this.aaNameaaResNoMap.put("tibigiko", R.drawable.ic_svg_tibigiko );
        this.aaNameaaResNoMap.put("tibisii", R.drawable.ic_svg_tibisii );
        this.aaNameaaResNoMap.put("tiraneyo", R.drawable.ic_svg_tiraneyo );
        this.aaNameaaResNoMap.put("tofu", R.drawable.ic_svg_tofu );
        this.aaNameaaResNoMap.put("tokei", R.drawable.ic_svg_tokei );
        this.aaNameaaResNoMap.put("tuu", R.drawable.ic_svg_tuu );
        this.aaNameaaResNoMap.put("uma", R.drawable.ic_svg_uma );
        this.aaNameaaResNoMap.put("unknown2", R.drawable.ic_svg_unknown2 );
        this.aaNameaaResNoMap.put("unko", R.drawable.ic_svg_unko );
        this.aaNameaaResNoMap.put("urara", R.drawable.ic_svg_urara );
        this.aaNameaaResNoMap.put("usi", R.drawable.ic_svg_usi );
        this.aaNameaaResNoMap.put("wachoi", R.drawable.ic_svg_wachoi );
        this.aaNameaaResNoMap.put("welneco2", R.drawable.ic_svg_welneco2 );
        this.aaNameaaResNoMap.put("yamazaki1", R.drawable.ic_svg_yamazaki1 );
        this.aaNameaaResNoMap.put("yamazaki2", R.drawable.ic_svg_yamazaki2 );
        this.aaNameaaResNoMap.put("yokan", R.drawable.ic_svg_yokan );
        this.aaNameaaResNoMap.put("zonu", R.drawable.ic_svg_zonu );
        this.aaNameaaResNoMap.put("zuza", R.drawable.ic_svg_zuza );
    }

    public void setStat(String strStat) {
        mSp.edit().putString(mC.getString(R.string.shared_me_stat), strStat).commit();
        MeMonaId.getMe().setStat(strStat);
    }

    public void setX(int x) {
        mSp.edit().putInt(mC.getString(R.string.shared_me_x), x).commit();
        MeMonaId.getMe().setX(x);
    }

    public void setY(int y) {
        mSp.edit().putInt(mC.getString(R.string.shared_me_y), y).commit();
        MeMonaId.getMe().setY(y);
    }

    public void setScl(int scl) {
        mSp.edit().putInt(mC.getString(R.string.shared_me_scl), scl).commit();
        MeMonaId.getMe().setScl(scl);
    }

    public void setTrip(String trip) {
        mSp.edit().putString(mC.getString(R.string.shared_me_trip), trip).commit();
        MeMonaId.getMe().setTrip(trip);
    }

    public void init2() {
        Stub.commentChange = getChangeColorComment();
        Stub.backgroundChange = getBackgroundColor();
    }

    public void setToken(String token) {
        mSp.edit().putString(mC.getString(R.string.shared_token), token).commit();
    }
    public void setHomeXY(int x,int y) {
        mSp.edit().putInt(mC.getString(R.string.shared_home_x), x).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_home_y), y).commit();
    }

    public void setKeyXY(int x,int y) {
        mSp.edit().putInt(mC.getString(R.string.shared_key_x), x).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_key_y), y).commit();
    }

    public void setArrowXY(int x,int y) {
        mSp.edit().putInt(mC.getString(R.string.shared_arrow_x), x).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_arrow_y), y).commit();
    }

    public void setPeopleXY(int x,int y) {
        mSp.edit().putInt(mC.getString(R.string.shared_people_x), x).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_people_y), y).commit();
    }


    public int[] getPeopleXY() {
        int[] i = new int[2];
        i[0] = mSp.getInt(mC.getString(R.string.shared_people_x), -1);
        i[1] = mSp.getInt(mC.getString(R.string.shared_people_y), -1);
        return i;
    }

    public void setLog2XY(int x,int y) {
        mSp.edit().putInt(mC.getString(R.string.shared_log2_x), x).commit();
        mSp.edit().putInt(mC.getString(R.string.shared_log2_y), y).commit();
    }


    public int[] getLog2XY() {
        int[] i = new int[2];
        i[0] = mSp.getInt(mC.getString(R.string.shared_log2_x), -1);
        i[1] = mSp.getInt(mC.getString(R.string.shared_log2_y), -1);
        return i;
    }


    public int[] getHomeXY() {
        int[] i = new int[2];
        i[0] = mSp.getInt(mC.getString(R.string.shared_home_x), -1);
        i[1] = mSp.getInt(mC.getString(R.string.shared_home_y), -1);
        return i;
    }

    public int[] getKeyXY() {
        int[] i = new int[2];
        i[0] = mSp.getInt(mC.getString(R.string.shared_key_x), -1);
        i[1] = mSp.getInt(mC.getString(R.string.shared_key_y), -1);
        return i;
    }

    public int[] getArrowXY() {
        int[] i = new int[2];
        i[0] = mSp.getInt(mC.getString(R.string.shared_arrow_x), -1);
        i[1] = mSp.getInt(mC.getString(R.string.shared_arrow_y), -1);
        return i;
    }

    public void setCharWholePixel(int pixel) {
        mSp.edit().putInt(mC.getString(R.string.char_whole_pixel), pixel).commit();
    }

    public int getCharWholePixel() {
        return mSp.getInt(mC.getString(R.string.char_whole_pixel), -1);
    }

    public void setAasizePixel(int pixel) {
        mSp.edit().putInt(mC.getString(R.string.aa_size_pixel), pixel).commit();
    }

    public int getAasizePixel() {
        return mSp.getInt(mC.getString(R.string.aa_size_pixel), -1);
    }

    public String getToken() {
        return mSp.getString(mC.getString(R.string.shared_token), null);
    }

    public void setRootViewSize(int pixel) {
        mSp.edit().putInt(mC.getString(R.string.root_view_size), pixel).commit();
    }

    public void setRootViewSizeInnerSoftKeyYosoku(int pixel) {
        mSp.edit().putInt(mC.getString(R.string.root_view_size_inner_soft_yosoku), pixel).commit();
    }

    public int getRootViewSizeInnerSoftKeyYosoku(){
        return mSp.getInt(mC.getString(R.string.root_view_size_inner_soft_yosoku), -1);
    }

    public void setRootViewSizeInnerSoftKey(int pixel) {
        mSp.edit().putInt(mC.getString(R.string.root_view_size_inner_soft), pixel).commit();
    }

    public int getRootViewSize(){
        return mSp.getInt(mC.getString(R.string.root_view_size), -1);
    }

    public int getRootViewSizeInnerSoftKey() {
        return mSp.getInt(mC.getString(R.string.root_view_size_inner_soft), -1);
    }

    public void setIgnores(String shirotori) {
        mSp.edit().putString(mC.getString(R.string.ignores), shirotori).commit();
    }

    public String getIgnores() {
        return mSp.getString(mC.getString(R.string.ignores), "");
    }
}
