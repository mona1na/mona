package com.devmona;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sake on 2017/12/31.
 */

public class Validation {

    private Context context;


    public Validation(Context context) {
        this.context = context;

    }

    // 共通したバリデーション
    public void checkJsonSentaku(String result) throws JSONException {

        JSONObject json;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjsen0x01));
            throw new JSONException(jsonObject.toString());
        }
    }


    public void checkPseqStartseq(String result) throws JSONException, NumberFormatException {
        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjsen0x02));
            throw new JSONException(jsonObject.toString());
        }

        try {
//            String pId = json.getString("p_id");
            String pId = json.getString(context.getResources().getString(R.string.sdb_p_id));
            int i = Integer.parseInt(pId);
        } catch (JSONException e) {
            // 出力完了
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjsen0x03));
            throw new JSONException(jsonObject.toString());
        } catch (NumberFormatException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjsen0x04));
            throw new NumberFormatException(jsonObject.toString());
        }

        try {
//            String startSeq = json.getString("start_id");
            String startSeq = json.getString(context.getResources().getString(R.string.sdb_start_id));
            int i = Integer.parseInt(startSeq);
        } catch (JSONException e) {
            // 出力完了
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjsen0x05));
            throw new JSONException(jsonObject.toString());
        } catch (NumberFormatException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjsen0x06));
            throw new NumberFormatException(jsonObject.toString());
        }
    }

    /**
     * checks whether result is json
     *
     * @param result
     * @throws JSONException
     */
    public void checkJsonSo(String result) throws JSONException {

        JSONObject json;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjs0x01));
            throw new JSONException(jsonObject.toString());
        }
    }

    /**
     * @param result
     * @throws JSONException
     * @throws NumberFormatException
     */
    public void checkJsonStatus(String result) throws JSONException {

        JSONObject jsonObject = new JSONObject(result);
        if (jsonObject.isNull(context.getResources().getString(R.string.sdb_gc))) {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("error", context.getString(R.string.cjs0x02));
            throw new JSONException(jsonObject1.toString());
        }
    }

    /**
     * throw JSONException if there is no loginseq
     *
     * @param result
     * @throws JSONException
     */
    public void checkLoginIdSo3(String result) throws JSONException {
        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjs30x02));
            throw new JSONException(jsonObject.toString());
        }

        try {
            String loginId = json.getString(context.getResources().getString(R.string.sdb_login_id));
            int i = Integer.parseInt(loginId);
        } catch (JSONException e) {
            // 出力完了
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjs30x03));
            throw new JSONException(jsonObject.toString());
        } catch (NumberFormatException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjs30x04));
            throw new NumberFormatException(jsonObject.toString());
        }
    }

    public void validateDefaultFromNopBym(String result) throws JSONException {

        try {
            JSONObject json = new JSONObject(result);
        } catch (JSONException e) {
            throw new JSONException("ERROR: no0x01");
        }
    }


    public void validateFromNopBym(String result) throws JSONException, NumberFormatException {
        JSONObject json;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            // 呼ばれないけど一応作ってる
            throw new JSONException("ERROR: no0x02");
        }
        String r;
        try {
            r = json.getString("gc");
        } catch (JSONException e) {
            throw new JSONException("ERROR: no0x03");
        }
    }


    public void serverValidation(String strServer) throws JSONException {
        JSONObject json = null;
        try {
            json = new JSONObject(strServer);
        } catch (JSONException e) {

            throw new JSONException("ERROR: no0x04");
        }

        String str;
        try {
            str = json.getString("server");
        } catch (JSONException e) {
            throw new JSONException("ERROR: nop0x02");
        } catch (NumberFormatException e) {
            throw new NumberFormatException("ERROR: nop0x02");
        }

        if (str.equals("OK")) {

        } else {
            throw new MyException(str);
        }
    }

    /**
     * returns {@link JSONException} if there is error mapped by name
     *
     * @param s
     * @throws JSONException
     */
    public void checkErrorSo(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }
    }

    /**
     * returns {@link JSONException} if there is error mapped by name
     *
     * @param s
     * @throws JSONException
     */
    public void checkErrorSo3(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkJsonR(String s) throws JSONException {

        try {
            JSONObject jsonObject = new JSONObject(s);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjr0x01));
            throw new JSONException(jsonObject.toString());
        }
//        if (!jsonObject.isNull("error")) {
//            throw new JSONException(jsonObject.toString());
//        }


    }

    /**
     * returns {@link JSONException} if there is no roomId
     *
     * @param s
     * @throws JSONException
     */
    public void checkRoomId(String s) throws JSONException {

        JSONObject jsonObject = new JSONObject(s);
        if (jsonObject.isNull(context.getResources().getString(R.string.sdb_room_id))) {
            jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjr0x02));
            throw new JSONException(jsonObject.toString());
        }
    }

    /**
     * returns {@link JSONException} if there is no error
     *
     * @param s
     * @throws JSONException
     */
    public void checkErrorR(String s) throws JSONException {

        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkErrorSentaku(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkJsonSo3(String result) throws JSONException {
        JSONObject json;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjs30x01));
            throw new JSONException(jsonObject.toString());
        }

    }

    public void checkJsonN(String strServer) throws JSONException {
        JSONObject json;
        try {
            json = new JSONObject(strServer);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjn0x01));
            throw new JSONException(jsonObject.toString());
        }

    }

    public void checkErrorN(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkServerN(String s) throws JSONException {
        JSONObject json = null;
        try {
            json = new JSONObject(s);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjn0x02));
            throw new JSONException(jsonObject.toString());
        }

        try {
            String server = json.getString("server");
        } catch (JSONException e) {
            // 出力完了
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjn0x03));
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkJsonG(String s) throws JSONException {
        JSONObject json;
        try {
            json = new JSONObject(s);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjg0x01));
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkErrorG(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }

    }

    public void checkG(String s) throws JSONException {
        JSONObject json = null;
        try {
            json = new JSONObject(s);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjg0x02));
            throw new JSONException(jsonObject.toString());
        }

        try {
            int gc = json.getInt(context.getString(R.string.sdb_gc));
        } catch (JSONException e) {
            // 出力完了
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjg0x03));
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkJsonER(String s) throws JSONException {

        try {
            JSONObject jsonObject = new JSONObject(s);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjer0x01));
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkErrorER(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkRoomId2(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (jsonObject.isNull(context.getResources().getString(R.string.sdb_status))) {
            jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjer0x02));
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkJsonC(String s) throws JSONException {
        try {
            JSONObject jsonObject = new JSONObject(s);
        } catch (JSONException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjc0x01));
            throw new JSONException(jsonObject.toString());
        }

    }

    public void checkErrorC(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (!jsonObject.isNull("error")) {
            throw new JSONException(jsonObject.toString());
        }
    }

    public void checkCmtId(String s) throws JSONException {
        JSONObject jsonObject = new JSONObject(s);
        if (jsonObject.isNull(context.getResources().getString(R.string.sdb_cmt_id))) {
            jsonObject = new JSONObject();
            jsonObject.put("error", context.getString(R.string.cjc0x02));
            throw new JSONException(jsonObject.toString());
        }
    }

    /**
     * 文字チェック
     *
     * @param s
     * @return true is error
     */
    public static boolean mySendComCheck(String s) {
        if (s.length() >= 33) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean myEmptyCheck(String s) {
        if (s.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public static String myStrConvert(String s) {
        return s.replace("<", "#l")
                .replace(">", "#g")
                .replace("\"", "#q")
                .replace("\'", "#d");
    }
}
