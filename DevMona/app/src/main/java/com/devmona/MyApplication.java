package com.devmona;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

//import com.appsflyer.AppsFlyerConversionListener;
//import com.appsflyer.AppsFlyerLib;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MyApplication extends Application implements LifecycleObserver{

    public Base2Activity nameActivity;
    public static Status base2sStatus;
    public static boolean back = false;
    public HashMap<Integer, String> hashMap;
    public static int id = 1;
    public static String AF_DEV_KEY = "eq2Z4QmuGWEXBbbuCdC3zZ";
    private Socket socket;
    private BufferedInputStream in;
    private BufferedWriter writer;
    private NopThread nopThread;
    public static LinkedHashMap<Integer, Room> roomsMap;
    //    public TestConnect testConnect;
    public static TestShared testShared;
    public int reconnectCount;
    public int reconnectIncre;
    private boolean clickable;

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();

        Log.i("processtest", "a onCreate");
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    private void initialize() {
        socket = null;
        in = null;
        reconnectCount = TestShared.getInstance(this).getReconectCount();
        reconnectIncre = 0;
        clickable = false;
        roomsMap = new LinkedHashMap<>();
        Room room1 = new Room();
        Room room2 = new Room();
        Room room3 = new Room();
        Room room4 = new Room();
        Room room5 = new Room();
        Room room6 = new Room();
        Room room7 = new Room();
        Room room8 = new Room();
        Room room9 = new Room();
        Room room10 = new Room();
        Room room11 = new Room();
        Room room12 = new Room();
        Room room13 = new Room();
        Room room14 = new Room();
        Room room15 = new Room();
        Room room16 = new Room();
        Room room17 = new Room();
        Room room18 = new Room();
        Room room19 = new Room();
        Room room20 = new Room();
        Room room21 = new Room();
        Room room22 = new Room();
        Room room23 = new Room();
        Room room24 = new Room();
        Room room25 = new Room();
        Room room26 = new Room();
        Room room27 = new Room();
        Room room28 = new Room();
        Room room29 = new Room();
        Room room30 = new Room();
        Room room31 = new Room();
        Room room32 = new Room();
        Room room33 = new Room();
        Room room34 = new Room();
        Room room35 = new Room();
        Room room36 = new Room();
        Room room37 = new Room();
        Room room38 = new Room();
        Room room39 = new Room();
        Room room40 = new Room();
        Room room41 = new Room();
        Room room42 = new Room();
        Room room43 = new Room();
        Room room44 = new Room();
        Room room45 = new Room();
        Room room46 = new Room();
        Room room47 = new Room();
        Room room48 = new Room();
        Room room49 = new Room();
        Room room50 = new Room();
        Room room51 = new Room();
        Room room52 = new Room();
        Room room53 = new Room();
        Room room54 = new Room();
        Room room55 = new Room();
        Room room56 = new Room();
        Room room57 = new Room();
        Room room58 = new Room();
        Room room59 = new Room();
        Room room60 = new Room();
        Room room61 = new Room();
        Room room62 = new Room();
        Room room63 = new Room();
        Room room64 = new Room();
        Room room65 = new Room();
        Room room66 = new Room();
        Room room67 = new Room();
        Room room68 = new Room();
        Room room69 = new Room();
        Room room70 = new Room();
        Room room71 = new Room();
        Room room72 = new Room();
        Room room73 = new Room();
        Room room74 = new Room();
        Room room75 = new Room();
        Room room76 = new Room();
        Room room77 = new Room();
        Room room78 = new Room();
        Room room79 = new Room();
        Room room80 = new Room();
        Room room81 = new Room();
        Room room82 = new Room();
        Room room83 = new Room();
        Room room84 = new Room();
        Room room85 = new Room();
        Room room86 = new Room();
        Room room87 = new Room();
        Room room88 = new Room();
        Room room89 = new Room();
        Room room90 = new Room();
        Room room91 = new Room();
        Room room92 = new Room();
        Room room93 = new Room();
        Room room94 = new Room();
        Room room95 = new Room();
        Room room96 = new Room();
        Room room97 = new Room();
        Room room98 = new Room();
        Room room99 = new Room();
        Room room100 = new Room();


        room1.setRoomNumber(1);

        room1.setRoomName("もなちゃと");
        room2.setRoomName("祭");
        room3.setRoomName("貞子");
        room4.setRoomName("樹海");
        room5.setRoomName("廃墟");
        room6.setRoomName("ハァ？");
        room7.setRoomName("キラキラ");
        room8.setRoomName("サッ");
        room9.setRoomName("食い逃げ");
        room10.setRoomName("学校");
        room11.setRoomName("喫茶");
        room12.setRoomName("Flash");
        room13.setRoomName("東京精神病院");
        room14.setRoomName("ヲタ");
        room15.setRoomName("さいたま");
        room16.setRoomName("BARギコ");
        room17.setRoomName("ワショーイ堂");
        room18.setRoomName("神社");
        room19.setRoomName("やまなし");
        room20.setRoomName("さすが兄弟");
        room21.setRoomName("もなちゃと21");
        room22.setRoomName("もなちゃと22");
        room23.setRoomName("もなちゃと23");
        room24.setRoomName("もなちゃと24");
        room25.setRoomName("もなちゃと25");
        room26.setRoomName("もなちゃと26");
        room27.setRoomName("もなちゃと27");
        room28.setRoomName("もなちゃと28");
        room29.setRoomName("もなちゃと29");
        room30.setRoomName("もなちゃと30");
        room31.setRoomName("もなちゃと31");
        room32.setRoomName("もなちゃと32");
        room33.setRoomName("もなちゃと33");
        room34.setRoomName("もなちゃと34");
        room35.setRoomName("もなちゃと35");
        room36.setRoomName("もなちゃと36");
        room37.setRoomName("もなちゃと37");
        room38.setRoomName("もなちゃと38");
        room39.setRoomName("もなちゃと39");
        room40.setRoomName("もなちゃと40");
        room41.setRoomName("もなちゃと41");
        room42.setRoomName("もなちゃと42");
        room43.setRoomName("もなちゃと43");
        room44.setRoomName("もなちゃと44");
        room45.setRoomName("もなちゃと45");
        room46.setRoomName("もなちゃと46");
        room47.setRoomName("もなちゃと47");
        room48.setRoomName("もなちゃと48");
        room49.setRoomName("もなちゃと49");
        room50.setRoomName("もなちゃと50");
        room51.setRoomName("もなちゃと51");
        room52.setRoomName("もなちゃと52");
        room53.setRoomName("もなちゃと53");
        room54.setRoomName("もなちゃと54");
        room55.setRoomName("もなちゃと55");
        room56.setRoomName("もなちゃと56");
        room57.setRoomName("もなちゃと57");
        room58.setRoomName("もなちゃと58");
        room59.setRoomName("もなちゃと59");
        room60.setRoomName("もなちゃと60");
        room61.setRoomName("もなちゃと61");
        room62.setRoomName("もなちゃと62");
        room63.setRoomName("もなちゃと63");
        room64.setRoomName("もなちゃと64");
        room65.setRoomName("もなちゃと65");
        room66.setRoomName("もなちゃと66");
        room67.setRoomName("もなちゃと67");
        room68.setRoomName("もなちゃと68");
        room69.setRoomName("もなちゃと69");
        room70.setRoomName("もなちゃと70");
        room71.setRoomName("もなちゃと71");
        room72.setRoomName("もなちゃと72");
        room73.setRoomName("もなちゃと73");
        room74.setRoomName("もなちゃと74");
        room75.setRoomName("もなちゃと75");
        room76.setRoomName("もなちゃと76");
        room77.setRoomName("もなちゃと77");
        room78.setRoomName("もなちゃと78");
        room79.setRoomName("もなちゃと79");
        room80.setRoomName("もなちゃと80");
        room81.setRoomName("もなちゃと81");
        room82.setRoomName("もなちゃと82");
        room83.setRoomName("もなちゃと83");
        room84.setRoomName("もなちゃと84");
        room85.setRoomName("もなちゃと85");
        room86.setRoomName("もなちゃと86");
        room87.setRoomName("もなちゃと87");
        room88.setRoomName("もなちゃと88");
        room89.setRoomName("もなちゃと89");
        room90.setRoomName("もなちゃと90");
        room91.setRoomName("もなちゃと91");
        room92.setRoomName("もなちゃと92");
        room93.setRoomName("もなちゃと93");
        room94.setRoomName("もなちゃと94");
        room95.setRoomName("もなちゃと95");
        room96.setRoomName("もなちゃと96");
        room97.setRoomName("もなちゃと97");
        room98.setRoomName("もなちゃと98");
        room99.setRoomName("もなちゃと99");
        room100.setRoomName("もなちゃと100");

        room1.setRoomNumber(1);
        room2.setRoomNumber(2);
        room3.setRoomNumber(3);
        room4.setRoomNumber(4);
        room5.setRoomNumber(5);
        room6.setRoomNumber(6);
        room7.setRoomNumber(7);
        room8.setRoomNumber(8);
        room9.setRoomNumber(9);
        room10.setRoomNumber(10);
        room11.setRoomNumber(11);
        room12.setRoomNumber(12);
        room13.setRoomNumber(13);
        room14.setRoomNumber(14);
        room15.setRoomNumber(15);
        room16.setRoomNumber(16);
        room17.setRoomNumber(17);
        room18.setRoomNumber(18);
        room19.setRoomNumber(19);
        room20.setRoomNumber(20);
        room21.setRoomNumber(21);
        room22.setRoomNumber(22);
        room23.setRoomNumber(23);
        room24.setRoomNumber(24);
        room25.setRoomNumber(25);
        room26.setRoomNumber(26);
        room27.setRoomNumber(27);
        room28.setRoomNumber(28);
        room29.setRoomNumber(29);
        room30.setRoomNumber(30);
        room31.setRoomNumber(31);
        room32.setRoomNumber(32);
        room33.setRoomNumber(33);
        room34.setRoomNumber(34);
        room35.setRoomNumber(35);
        room36.setRoomNumber(36);
        room37.setRoomNumber(37);
        room38.setRoomNumber(38);
        room39.setRoomNumber(39);
        room40.setRoomNumber(40);
        room41.setRoomNumber(41);
        room42.setRoomNumber(42);
        room43.setRoomNumber(43);
        room44.setRoomNumber(44);
        room45.setRoomNumber(45);
        room46.setRoomNumber(46);
        room47.setRoomNumber(47);
        room48.setRoomNumber(48);
        room49.setRoomNumber(49);
        room50.setRoomNumber(50);
        room51.setRoomNumber(51);
        room52.setRoomNumber(52);
        room53.setRoomNumber(53);
        room54.setRoomNumber(54);
        room55.setRoomNumber(55);
        room56.setRoomNumber(56);
        room57.setRoomNumber(57);
        room58.setRoomNumber(58);
        room59.setRoomNumber(59);
        room60.setRoomNumber(60);
        room61.setRoomNumber(61);
        room62.setRoomNumber(62);
        room63.setRoomNumber(63);
        room64.setRoomNumber(64);
        room65.setRoomNumber(65);
        room66.setRoomNumber(66);
        room67.setRoomNumber(67);
        room68.setRoomNumber(68);
        room69.setRoomNumber(69);
        room70.setRoomNumber(70);
        room71.setRoomNumber(71);
        room72.setRoomNumber(72);
        room73.setRoomNumber(73);
        room74.setRoomNumber(74);
        room75.setRoomNumber(75);
        room76.setRoomNumber(76);
        room77.setRoomNumber(77);
        room78.setRoomNumber(78);
        room79.setRoomNumber(79);
        room80.setRoomNumber(80);
        room81.setRoomNumber(81);
        room82.setRoomNumber(82);
        room83.setRoomNumber(83);
        room84.setRoomNumber(84);
        room85.setRoomNumber(85);
        room86.setRoomNumber(86);
        room87.setRoomNumber(87);
        room88.setRoomNumber(88);
        room89.setRoomNumber(89);
        room90.setRoomNumber(90);
        room91.setRoomNumber(91);
        room92.setRoomNumber(92);
        room93.setRoomNumber(93);
        room94.setRoomNumber(94);
        room95.setRoomNumber(95);
        room96.setRoomNumber(96);
        room97.setRoomNumber(97);
        room98.setRoomNumber(98);
        room99.setRoomNumber(99);
        room100.setRoomNumber(100);

        roomsMap.put(1, room1);
        roomsMap.put(2, room2);
        roomsMap.put(3, room3);
        roomsMap.put(4, room4);
        roomsMap.put(5, room5);
        roomsMap.put(6, room6);
        roomsMap.put(7, room7);
        roomsMap.put(8, room8);
        roomsMap.put(9, room9);
        roomsMap.put(10, room10);
        roomsMap.put(11, room11);
        roomsMap.put(12, room12);
        roomsMap.put(13, room13);
        roomsMap.put(14, room14);
        roomsMap.put(15, room15);
        roomsMap.put(16, room16);
        roomsMap.put(17, room17);
        roomsMap.put(18, room18);
        roomsMap.put(19, room19);
        roomsMap.put(20, room20);
        roomsMap.put(21, room21);
        roomsMap.put(22, room22);
        roomsMap.put(23, room23);
        roomsMap.put(24, room24);
        roomsMap.put(25, room25);
        roomsMap.put(26, room26);
        roomsMap.put(27, room27);
        roomsMap.put(28, room28);
        roomsMap.put(29, room29);
        roomsMap.put(30, room30);
        roomsMap.put(31, room31);
        roomsMap.put(32, room32);
        roomsMap.put(33, room33);
        roomsMap.put(34, room34);
        roomsMap.put(35, room35);
        roomsMap.put(36, room36);
        roomsMap.put(37, room37);
        roomsMap.put(38, room38);
        roomsMap.put(39, room39);
        roomsMap.put(40, room40);
        roomsMap.put(41, room41);
        roomsMap.put(42, room42);
        roomsMap.put(43, room43);
        roomsMap.put(44, room44);
        roomsMap.put(45, room45);
        roomsMap.put(46, room46);
        roomsMap.put(47, room47);
        roomsMap.put(48, room48);
        roomsMap.put(49, room49);
        roomsMap.put(50, room50);
        roomsMap.put(51, room51);
        roomsMap.put(52, room52);
        roomsMap.put(53, room53);
        roomsMap.put(54, room54);
        roomsMap.put(55, room55);
        roomsMap.put(56, room56);
        roomsMap.put(57, room57);
        roomsMap.put(58, room58);
        roomsMap.put(59, room59);
        roomsMap.put(60, room60);
        roomsMap.put(61, room61);
        roomsMap.put(62, room62);
        roomsMap.put(63, room63);
        roomsMap.put(64, room64);
        roomsMap.put(65, room65);
        roomsMap.put(66, room66);
        roomsMap.put(67, room67);
        roomsMap.put(68, room68);
        roomsMap.put(69, room69);
        roomsMap.put(70, room70);
        roomsMap.put(71, room71);
        roomsMap.put(72, room72);
        roomsMap.put(73, room73);
        roomsMap.put(74, room74);
        roomsMap.put(75, room75);
        roomsMap.put(76, room76);
        roomsMap.put(77, room77);
        roomsMap.put(78, room78);
        roomsMap.put(79, room79);
        roomsMap.put(80, room80);
        roomsMap.put(81, room81);
        roomsMap.put(82, room82);
        roomsMap.put(83, room83);
        roomsMap.put(84, room84);
        roomsMap.put(85, room85);
        roomsMap.put(86, room86);
        roomsMap.put(87, room87);
        roomsMap.put(88, room88);
        roomsMap.put(89, room89);
        roomsMap.put(90, room90);
        roomsMap.put(91, room91);
        roomsMap.put(92, room92);
        roomsMap.put(93, room93);
        roomsMap.put(94, room94);
        roomsMap.put(95, room95);
        roomsMap.put(96, room96);
        roomsMap.put(97, room97);
        roomsMap.put(98, room98);
        roomsMap.put(99, room99);
        roomsMap.put(100, room100);
    }

    public String getRoomName(int num) {
        return roomsMap.get(num).getRoomName();
    }

    public void notifyCOM2(Person person, String strCmt) {
        for (int i = 0; i < MeMonaId.notifyArray.size(); i++) {
            String result = MeMonaId.notifyArray.get(i);
            if (result == null) {
                break;
            }
            if (strCmt.indexOf(result) == 0) {
                notifyCOM(person, strCmt);
            }
        }


    }

    public void notifyCOM(Person person, String strCmt) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            int type = ReadMainThread.resNoFromStrType(person.getAaname());
            String channelId = "updates"; // 通知チャンネルのIDにする任意の文字列
            String name = "更新情報"; // 通知チャンネル名
            int importance = NotificationManager.IMPORTANCE_HIGH; // デフォルトの重要度
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription("通知チャンネルの説明"); // 必須ではない

// 通知チャンネルの設定のデフォルト値。設定必須ではなく、ユーザーが変更可能。
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableVibration(true);
            channel.enableLights(true);
            channel.setLightColor(Color.argb(255, 255, 255, 255));
//            channel.setSound(uri, audioAttributes);
            channel.setShowBadge(false); // ランチャー上でアイコンバッジを表示するかどうか


            NotificationManager nm = getSystemService(NotificationManager.class);
            nm.createNotificationChannel(channel);
            NotificationCompat.Builder builder
                    = new NotificationCompat.Builder(getApplicationContext(), channelId)
                    .setContentText(strCmt)
                    .setSmallIcon(R.mipmap.mona_icon_kadomaru2);
            if (!person.getKurotori().equals("")) {
                builder.setContentTitle(person.getName() + person.getShikakuRealKurotori());
            } else {
                builder.setContentTitle(person.getName() + person.getShikakuRealShirotori());
            }


            NotificationManagerCompat.from(getApplicationContext()).notify(id, builder.build());
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            builder.setSmallIcon(R.mipmap.mona_icon_kadomaru2);
            if (!person.getKurotori().equals("")) {
                builder.setContentTitle(person.getName() + person.getShikakuRealKurotori());
            } else {
                builder.setContentTitle(person.getName() + person.getShikakuRealShirotori());
            }
            builder.setContentText(strCmt);
            NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
            manager.notify(id, builder.build());
        }
    }



    protected enum Status {
        onResumed,
        onPaused,
        onDestroyed,
        onCreated,
        onStarted,
        onStopped
    }



}
