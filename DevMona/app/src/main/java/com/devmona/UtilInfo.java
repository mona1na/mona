package com.devmona;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.devmona.loadercallbacks.LoaderCallbacksSendComment6;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import io.fabric.sdk.android.Fabric;


/**
 * Created by sake on 2017/06/18.
 */

public class UtilInfo {


    final public static String testRoom = "99";
    //    public static BufferedInputStream in;
    private static String strGrobal = "";

    public static final String TAG = "UtilInfo";
    public static SQLiteDatabase m_db;
    public static HashMap<String, Integer> aaNameaaResNoMap;

    public static final String ENTER = "ENTER";
    public static final String USER = "USER";
    public static final String COM = "COM";
    public static final String STAT = "stat";
    public static final String XY = "xy";
    public static final String EXIT = "EXIT";
    public static Person person;
    private static boolean mClick = true;

    /**
     * Myselfオブジェクトから入室処理を作成する
     *
     * @param me
     */
    public static String createEnter1(Person me) {

        String enter = "<ENTER room='/MONA8094' " + "name='" + me.getName() + "' "
                + "attrib='no'/>\0";

        return enter;
    }

    public static boolean isClick() {
        return mClick;
    }

    public static boolean clickable() {
        if (mClick) {
            clickOff();
            return true;
        } else {
            return false;
        }
    }
    public  static void clickOff() {
        mClick = false;
    }

    public static void clickTimer(final View view) {
        view.setEnabled(false);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                view.setEnabled(true);
            }
        }, 4000L);
    }

    public static ArrayList<String> testRead(Context context, byte[] keyinbuf, int k) {
        ArrayList<String> arrayResult = new ArrayList<>();
        String temp10 = null;
        temp10 = new String(keyinbuf, 0, k);

//        int connect = temp10.indexOf("+connect id");
//        if (connect != -1) {
//            String connectstr = temp10.trim();
//            arrayResult.add(connectstr);
//            return arrayResult;
//        }


        int zerokara = 0;
        while (true) {

            int kokomade = temp10.indexOf("\0", zerokara);

            if (kokomade != -1) {
                String result = temp10.substring(zerokara, kokomade);
                if (strGrobal.equals("")) {
                    arrayResult.add(result);
                    zerokara = kokomade + 1;
                } else if (!strGrobal.equals("")) {
                    arrayResult.add(strGrobal + result);
                    zerokara = kokomade + 1;
                    strGrobal = "";
                }
            } else {

                if (zerokara == temp10.length()) {

                    return arrayResult;
                } else {
                    String result = temp10.substring(zerokara, temp10.length());
                    strGrobal = result;
                    return arrayResult;
                }
            }

        }
    }

    private static int test5(String color) {

        int result = 100;
        try {
            result = Integer.parseInt(color);
        } catch (NumberFormatException e) {
            Log.d("test5", "colorChangeError値 :" + color);
        }
        return result;
    }

    private static int test6(String position) {

        int result = 100;
        try {

            result = Integer.parseInt(position);
            Log.d("lineStartX", "XかY" + result);
        } catch (NumberFormatException e) {
            Log.d("1021test6", position + "");
            String position16 = position.replaceAll("0x", "");
            result = Integer.parseInt(position16, 16);
            Log.d("lineStartX", "positionChangeError値 :" + position);
        }

        return result;
    }

    private static int test7(String strScl) {

        int result = 100;

        try {
            result = Integer.parseInt(strScl);
        } catch (NumberFormatException e) {
            Log.d("lineStopX", "sclChangeError値 :" + strScl);
        }

        return result;
    }

    private static int test8(String strId) {

        int result = -1;
        try {
            result = Integer.parseInt(strId);
        } catch (NumberFormatException e) {
            Log.d("lineStopX", "idChangeError値 :" + strId);
        }

        return result;
    }

    public static void memory() {
        long total = Runtime.getRuntime().totalMemory() / 1024;
        long free = Runtime.getRuntime().freeMemory() / 1024;
        long use = (total - free) / 1024;
        long max = Runtime.getRuntime().maxMemory() / 1024;

        Log.d("ttt total", "" + total + "kb");

        Log.d("ttt free", "" + free + "kb");

        Log.d("ttt use", "" + use + "kb");

        Log.d("ttt max", "" + max + "kb");
    }

    public static String createSet(int[] xy, Person me) {

        String set = "<SET x='" + xy[0] + "' " +
                "y='" + xy[1] + "' " +
                "scl='100' />\0";

        return set;
    }

    public static JsonFlagList jsonpersonset2(Context context, Cursor cursor) {

        ArrayList<JSONObject> arrayObjects = new ArrayList<>();
        ArrayList<Integer> idlist = new ArrayList<>();
        JSONObject jsonObjectsoto = new JSONObject();
        int i = 0;

        while (cursor.moveToNext()) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("comid", cursor.getInt(cursor.getColumnIndex("id")));
                idlist.add(i, cursor.getInt(cursor.getColumnIndex("id")));
                jsonObject.put("cmt", cursor.getString(cursor.getColumnIndex("cmt")));
                jsonObject.put("name", cursor.getString(cursor.getColumnIndex("name")));
                jsonObject.put("time", cursor.getString(cursor.getColumnIndex("time")));
                jsonObject.put("roomno", cursor.getString(cursor.getColumnIndex("roomno")));
                jsonObject.put("shirotori", cursor.getString(cursor.getColumnIndex("shirotori")));
                jsonObject.put("shirotori6", cursor.getString(cursor.getColumnIndex("shirotori6")));
                jsonObject.put("androidid", cursor.getString(cursor.getColumnIndex("androidid")));
                jsonObject.put("deviceid", cursor.getString(cursor.getColumnIndex("deviceid")));
                jsonObject.put("simcountryiso", cursor.getString(cursor.getColumnIndex("simcountryiso")));
                jsonObject.put("simserialnumber", cursor.getString(cursor.getColumnIndex("simserialnumber")));
                jsonObjectsoto.put(i + "", jsonObject);
//                arrayObjects.add(i, jsonObjectsoto);
                i++;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d("jsonObjectsoto", jsonObjectsoto.toString());
        return new JsonFlagList(jsonObjectsoto, idlist);
    }

//    public static JSONObject jsonpersonset0(Context context) {
//
//        TelephonyManager manager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
//        String androidid = Settings.Secure.getString(context.getContentResolver(), Settings.System.ANDROID_ID);
//        String deviceid = manager.getDeviceId();
//        String simCountryIso = manager.getSimCountryIso();
//        String simSerialNumber = manager.getSimSerialNumber();
//
//        JSONObject jsonObject = new JSONObject();
//        String name = UtilInfo.me.getName();
//        String shirotori = UtilInfo.me.getShirotori();
//        String shirotori6 = UtilInfo.me.getShirotori6();
//        try {
//            jsonObject.put("name", name);
//            jsonObject.put("androidid", androidid);
//            jsonObject.put("deviceid", deviceid);
//            jsonObject.put("shirotori", shirotori);
//            jsonObject.put("shirotori6", shirotori6);
//            jsonObject.put("simiso", simCountryIso);
//            jsonObject.put("simnum", simSerialNumber);
//            jsonObject.put("testtime", getNowDate());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return jsonObject;
//    }

    //    public static int AlphaFrom0To1(int m_alphaCurrent2) {
//        int y = -m_alphaCurrent2 + 255;
//
//        return y;
//    }
    public static String getNowDate() {
        final DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final Date date = new Date(System.currentTimeMillis());
        return df.format(date);
    }

    /**
     * convert bitmap to byte array
     *
     * @param bitmapline
     * @return byte array
     */
    public static byte[] bitmapTobyteArr(Bitmap bitmapline) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmapline.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    public static void commonSend(Context context, String com) {

        /**
         * コメントテーブルにあった場合そのまま登録する
         */

        Bundle bundle = new Bundle();
        bundle.putString("cmt", com);
        Base2Activity nameActivity = (Base2Activity) context;
        nameActivity.getSupportLoaderManager().initLoader(6, bundle, new LoaderCallbacksSendComment6(nameActivity));


    }

    public static int[] testCal(int monaId, int x, int y, int scl) {
        int androidY2 = CalPositionUtil.test1(Calc.heightDp - Calc.aasizeDp, x);
        int androidX2 = CalPositionUtil.test2((int) Calc.widthDp, y);

        /**
         * picをx座標0に置いたときのはみ出し分も考慮して画像サイズを開けた分の関数を出力した
         *
         * 次は限界値で、
         *
         *
         *
         *
         *
         */

        int cX2 = testX(androidX2, (int) Calc.widthDp, Calc.aasizeDp);
        int cY2 = testY(androidY2, (int) (Calc.heightDp - Calc.aasizeDp));

        int[] cx2cy2scl = new int[]{
                monaId,
                cX2,
                cY2,
                scl
        };
        return cx2cy2scl;
    }

    public static int testX(int x, int width08, float aasizeDp) {

        if (x <= 0) {
            x = 0;
        } else if (x > width08) {
            x = width08;
        }

        return x;
    }

    public static int testY(int y, int height08) {

        if (y <= 0) {
            y = 0;
        } else if (y > height08) {
            y = height08;
        }
        return y;
    }

    public static void testError2(final Activity activity, String string) {
        Fabric.with((Context) activity, new Crashlytics());
    }

    /**
     * サーバに返すxを戻す
     *
     * @param heightDp
     * @param aasizeDp
     * @param cy2
     */
    public static int convertTestX(int heightDp, int aasizeDp, int cy2) {

        int y = 660 * cy2 / (heightDp - 2 * aasizeDp) + -660 * aasizeDp / (heightDp - 2 * aasizeDp) + 30;
        return y;
    }

    public static int convertTestY(int widthDp, int aasizeDpwaru2, int cx2) {
        int y = -80 * cx2 / widthDp + 80 * aasizeDpwaru2 / widthDp + 320;
        return y;
    }

    /**
     * 現行
     *
     * @param widthDp
     * @param cx2
     * @return
     */
    public static int convertTestX2(int widthDp, int cx2) {

        return -80 * cx2 / widthDp + 320;
    }

    /**
     * @param context
     * @return RoomsFragment
     */
    public static RoomsFragment getRoomsFragment(Context context) {
        try {
            Base2Activity nameActivity = (Base2Activity) context;
            RoomsFragment roomsFragment = (RoomsFragment) nameActivity.getSupportFragmentManager().findFragmentByTag(context.getString(R.string.fragment_rooms));
            return roomsFragment;
        } catch (NullPointerException e) {
            Log.d("test", "test");
            return null;
        }
    }

    /**
     * @param context
     * @return RoomsFragment
     */
    public static SettingFragment getSettingFragment(Context context) {
        try {
            Base2Activity nameActivity = (Base2Activity) context;
            SettingFragment settingFragment = (SettingFragment) nameActivity.getSupportFragmentManager().findFragmentByTag(context.getString(R.string.fragment_setting));
            return settingFragment;
        } catch (NullPointerException e) {
            Log.d(TAG, "getSettingFragment null");
            return null;
        }
    }

    /**
     * @param context
     * @return Tate2Fragment
     */
    public static Tate2Fragment getTate2Fragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        Tate2Fragment tate2Fragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof Tate2Fragment) {
                tate2Fragment = (Tate2Fragment) fragment;
                return tate2Fragment;
            }
        }
        return null;
    }

    public static Log2Fragment getLog2Fragment(Context context) {
//        Base2Activity nameActivity = (Base2Activity) context;
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        Log2Fragment log2Fragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof Log2Fragment) {
                log2Fragment = (Log2Fragment) fragment;
                return log2Fragment;
            }
        }
        return null;
    }

    public static Tate3Fragment getTate3Fragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        Tate3Fragment tate3Fragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof Tate3Fragment) {
                tate3Fragment = (Tate3Fragment) fragment;
                return tate3Fragment;
            }
        }
        return null;
    }

    public static ProgressFragment getProgressFragment(Context context) {
        try {
            Base2Activity nameActivity = (Base2Activity) context;
            ProgressFragment progressFragment = (ProgressFragment) nameActivity.getSupportFragmentManager().findFragmentByTag(context.getString(R.string.fragment_progress));
            return progressFragment;
        } catch (NullPointerException e) {

            return null;
        }

    }

    public static Main4Fragment getMain4Fragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        Main4Fragment main4Fragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof Main4Fragment) {
                main4Fragment = (Main4Fragment) fragment;
                return main4Fragment;
            }
        }
        return null;
    }

    public static Name2Fragment getName2Fragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        Name2Fragment name2Fragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof Name2Fragment) {
                name2Fragment = (Name2Fragment) fragment;
                return name2Fragment;
            }
        }
        return null;
    }

    public static MessageFragment getMessageFragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        MessageFragment messageFragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof MessageFragment) {
                messageFragment = (MessageFragment) fragment;
                return messageFragment;
            }
        }
        return null;
    }

    public static RandomFragment getRandomFragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        RandomFragment randomFragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof RandomFragment) {
                randomFragment = (RandomFragment) fragment;
                return randomFragment;
            }
        }
        return null;
    }

    public static int converScal(int scl) {
        scl = 1;
        if (scl == 1) {
            return 100;
        } else {
            return 100;
        }
    }

    public static void fromMain4toRoom(Context context) {

        IconFragment iconFragment = UtilInfo.getIconFragment(context);
        if (iconFragment != null) {
            iconFragment.setIconPos();
        }

        Base2Activity base2Activity = (Base2Activity) context;
        Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(base2Activity);
        Tate3Fragment tate3Fragment = UtilInfo.getTate3Fragment(base2Activity);
        Main4Fragment main4Fragment = UtilInfo.getMain4Fragment(base2Activity);
        base2Activity.getSupportFragmentManager().beginTransaction().remove(tate2Fragment).commit();
        base2Activity.getSupportFragmentManager().beginTransaction().remove(tate3Fragment).commit();
        base2Activity.getSupportFragmentManager().beginTransaction().remove(main4Fragment).commit();


        RoomsFragment roomsFragment = UtilInfo.getRoomsFragment(base2Activity);
        if (roomsFragment != null && roomsFragment.isHidden()) {
            base2Activity.getSupportFragmentManager().beginTransaction().show(roomsFragment).commit();
            roomsFragment.test();
        } else {
            roomsFragment = new RoomsFragment();
            FragmentTransaction fragmentTransaction = base2Activity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.test_fragment, roomsFragment, base2Activity.getString(R.string.fragment_rooms));
            fragmentTransaction.commit();
        }
    }

    private static IconFragment getIconFragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        IconFragment iconFragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof IconFragment) {
                iconFragment = (IconFragment) fragment;
                return iconFragment;
            }
        }
        return null;
    }

    public static void errorSend(String error) {
    }

    public static NewFutureFragment getNewFutureFragment(Context context) {
        Base2Activity nameActivity = (Base2Activity) context;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        NewFutureFragment newFutureFragment = null;
        for (Fragment fragment : fragments) {
            if (fragment instanceof NewFutureFragment) {
                newFutureFragment = (NewFutureFragment) fragment;
                return newFutureFragment;
            }
        }
        return null;

    }
}
