package com.devmona;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.devmona.extendsviews.MyCustomAnimation;
import com.devmona.settingfragment.IgnoreFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.devmona.extendsviews.MyDragShadowBuilder;
import com.devmona.extendsviews.MyFrameLayout;
import com.devmona.loadercallbacks.LoaderCallbacks2FromSocket;
import com.devmona.loadercallbacks.LoaderCallbacksSendXY;
import com.devmona.settingfragment.StateDialogFragment;
import com.devs.vectorchildfinder.VectorChildFinder;
import com.devs.vectorchildfinder.VectorDrawableCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static android.content.Context.VIBRATOR_SERVICE;
import static com.devmona.MeMonaId.getMe;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tate2Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tate2Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int commentCount = 0;
    private HashMap<Integer, CommentAndValueListener> commentMap;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private final static String TAG = "Tate2Fragment";
    private OnFragmentInteractionListener mListener;
    public MyFrameLayout frameTate2;
    private Tate2Fragment m_tate2;
    private Base2Activity nameActivity;
    private TextView txtRoomName;
    private NavigationView nav_view;
    private Handler mHandler = new Handler();
    private Runnable update;
    private ScrollView mScrollView;
    private ToggleButton btnPc;
    private LinearLayout linearIcons;
    private Animation in;
    private Animation out;
    private Animation rotateOn;
    private Animation rotateOff;
    private View rootView;
    private ToggleButton btnMember;
    public static boolean test3 = false;
    private TestConnect testConnect;
    private LinearLayout linearLayout;
    private ToggleButton btnRegular;
    private String state = "";

    public static final int STATE_TATE = 1;
    public static final int STATE_REGU = 2;
    public static final int MOVE_CHARACTER = 1;
    public static final int MOVE_COMMNET = 2;

    private int tateReguState;
    private Vibrator vib;
    private boolean ob = false;
    private FrameLayout frameLayoutWhole;
    private LinearLayout linearEtc;
    private TabLayout tabLayout;
    private LinearLayout etcLinearFragment;
    private ValueAnimator valueAnimator;

    public static final int TAB_ICON = 0;
    public static final int TAB_BLOCK = 1;
    public static final int TAB_STAT = 2;
    public static final int TAB_NOTIFICATION = 3;
    public static final int TAB_SETTING = 4;
    public static final int TAB_WEB = 5;
    private int mCharWholePixcel;
    private int mAasizePixel;
    private int mWholwHeight;
    private int mRegularTopMargin;
    private int mEtcHeight;
    private int mPutY;
    private int mWholeWidth;
    private boolean mPeopleMoveFlag;
    private int mMoveDelay = 50;
    private int mLog2HeightEnd;
    private Log2Fragment mLog2Fragment;
    private Context mContext;
    private Base2Activity mActivity;

    public void peopleMove() {
//        Collections.sort((List<Person>) testConnect.readMainThread.people,new MyComparator());
        if (mPeopleMoveFlag) {
            testMove3();
            mPeopleMoveFlag = false;
        } else {
//            testMove1();
            testMove2();
            mPeopleMoveFlag = true;
        }
    }

    private void testMove3() {
        LinkedHashMap<Integer, Person> people = testConnect.readMainThread.people;
        int i = 0;
        int toY = 0;
//        int totalY = m - mAasizePixel;
        int yDead = mRegularTopMargin - mAasizePixel;
        int toX = 0;
        int delay = 300;
        for (int uniqueId : people.keySet()) {
//            MyRunnable runnable = new MyRunnable();
            Person tempPerson = people.get(uniqueId);
            PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(tempPerson.getUniqueId());
            RelativeLayout relative = personLayoutDrawable.getRelativeLayout2();

            int[] ys = new int[2];
            ys[0] = tempPerson.getcYR();
            ys[1] = mPutY;
            int[] xs = new int[2];
            xs[0] = 1;
            xs[1] = tempPerson.getcXR();
            ObjectAnimator objectAnimator = getAnimator(relative, xs, ys, delay);
            MyRunnable2 myRunnable2 = new MyRunnable2();
            myRunnable2.setObjectAnim(objectAnimator);
            new Handler().postDelayed(myRunnable2, i);
            i = i + mMoveDelay;
        }

    }

    public static ObjectAnimator getAnimator(View target, int[] x, int[] y, int delay) {
        PropertyValuesHolder holderX = PropertyValuesHolder.ofFloat("translationX", x[0], x[1]);
        PropertyValuesHolder holderY = PropertyValuesHolder.ofFloat("translationY", y[0], y[1]);
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(
                target, holderX, holderY);
        objectAnimator.setDuration(delay);
        return objectAnimator;
    }

    class MyRunnable2 implements Runnable {

        private ObjectAnimator objectAnimator;

        @Override
        public void run() {
            objectAnimator.start();
        }

        public void setObjectAnim(ObjectAnimator objectAnimator) {
            this.objectAnimator = objectAnimator;
        }
    }

    private void testMove1() {
        LinkedHashMap<Integer, Person> people = testConnect.readMainThread.people;
        int i = 0;
        int y = 0;
        for (int uniqueId : people.keySet()) {
            MyRunnable runnable = new MyRunnable();
            Person tempPerson = people.get(uniqueId);
            PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(tempPerson.getUniqueId());
            RelativeLayout relative = personLayoutDrawable.getRelativeLayout2();
            runnable.setView(relative);
            runnable.setFromY(mPutY);
            runnable.setToY(y);
            y = y + 40;
            new Handler().postDelayed(runnable, i);
            i = i + mMoveDelay;
        }
    }

    private void testMove2() {
        LinkedHashMap<Integer, Person> people = testConnect.readMainThread.people;
        int i = 0;
        int toY = 0;
//        int totalY = m - mAasizePixel;
        int yDead = mRegularTopMargin - mAasizePixel;
        int toX = 0;
        int delay = 300;
        for (int uniqueId : people.keySet()) {
//            MyRunnable runnable = new MyRunnable();
            Person tempPerson = people.get(uniqueId);
            PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(tempPerson.getUniqueId());
            RelativeLayout relative = personLayoutDrawable.getRelativeLayout2();
            if (yDead > toY) {
                int[] ys = new int[2];
                ys[0] = mPutY;
                ys[1] = toY;
                int[] xs = new int[2];
                xs[0] = tempPerson.getcXR();
                xs[1] = toX;
                ObjectAnimator objectAnimator = getAnimator(relative, xs, ys, delay);
                MyRunnable2 myRunnable2 = new MyRunnable2();
                myRunnable2.setObjectAnim(objectAnimator);
                new Handler().postDelayed(myRunnable2, i);
                i = i + mMoveDelay;
                toY = toY + mAasizePixel;
            } else {
                toY = 0;
                toX = toX + mAasizePixel;

                int[] ys = new int[2];
                ys[0] = mPutY;
                ys[1] = toY;
                int[] xs = new int[2];
                xs[0] = tempPerson.getcXR();
                xs[1] = toX;
                ObjectAnimator objectAnimator = getAnimator(relative, xs, ys, delay);
                MyRunnable2 myRunnable2 = new MyRunnable2();
                myRunnable2.setObjectAnim(objectAnimator);
                new Handler().postDelayed(myRunnable2, i);
                i = i + mMoveDelay;
                toY = toY + mAasizePixel;
            }
        }
    }

    class MyRunnable implements Runnable {

        private View view;
        private int mToY;
        private int mFromY;

        @Override
        public void run() {
            MyPeopleListener myPeopleListener = new MyPeopleListener(view);
//            valueAnimator.setInterpolator(new LinearInterpolator());
            ValueAnimator valueAnimator = ValueAnimator.ofInt(mFromY, mToY);
            valueAnimator.setDuration(500);
            valueAnimator.addUpdateListener(myPeopleListener);
            valueAnimator.start();
        }

        public void setView(View view) {
            this.view = view;
        }

        public void setToY(int y) {
            this.mToY = y;
        }

        public void setFromY(int mPutY) {
            this.mFromY = mPutY;
        }
    }

    public int getmPutY() {
        return mPutY;
    }

    public Tate2Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tate2Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Tate2Fragment newInstance(String param1, String param2) {
        Tate2Fragment fragment = new Tate2Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private final LoaderManager.LoaderCallbacks<Object> mCallback =
            new LoaderManager.LoaderCallbacks<Object>() {


                @Override
                public Loader<Object> onCreateLoader(int id, Bundle bundle) {
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("key", 1);
                    MyLoader myLoader = new MyLoader(getContext(), bundle1);
                    return myLoader;
                }

                @Override
                public void onLoadFinished(Loader<Object> loader, Object data) {
                    getActivity().getSupportLoaderManager().destroyLoader(1);

                    if (Prop.DEBUG_MODE == 1) {
                        data = "OK";
                    }

                    if (data.equals(Prop.SERVE_MESSAGE)) {
                        notErrorSetBtnEnter(data.toString());
                        return;
                    } else {
//                        //todo 精査する必要がある
//                        UtilInfo.testError2(getActivity(), data.toString());
//                        errorSetBtnEnter();
                        TestConnect testConnect = TestConnect.getInstance(getContext());
                        final String methodName = new Object() {
                        }.getClass().getEnclosingMethod().getName();
                        testConnect.error(getContext(), methodName);
                        return;
                    }
                }


                private void showErrorDialog(final String string) {
                    Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ErrorDialogFragment errorDialogFragment =
                                    new ErrorDialogFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("error", string);
                            errorDialogFragment.setArguments(bundle);
                            errorDialogFragment.show(getActivity().getFragmentManager(), "errorDialog");
                        }
                    });
                }

                private void notErrorSetBtnEnter(String result) {
//                    SentakuDB sentakuDB = new SentakuDB();
////                    DBStatus dbStatus = sentakuDB.getFromTo();
//                    sentakuDB.updateGU();

//                    btnEnterSet();

                }

                private void errorSetBtnEnter() {
                    Toast.makeText(getActivity(), "サーバーエラー", Toast.LENGTH_LONG);
                }

                @Override
                public void onLoaderReset(Loader<Object> loader) {

                }
            };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("20180503testjjj", "onCreate Tate2");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        this.nameActivity = (Base2Activity) getActivity();
        this.m_tate2 = this;
        testConnect = TestConnect.getInstance(mContext);
    }

    public void seishinSet(ImageView seishinImage) {
        ViewTreeObserver imageObserver = seishinImage.getViewTreeObserver();
        imageObserver.addOnGlobalLayoutListener(new ImageObserver(seishinImage));
        seishinImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                valueAnimator.start();
                return false;
            }
        });
    }

    class ImageObserver implements ViewTreeObserver.OnGlobalLayoutListener {

        private boolean first;
        private View imageView;

        ImageObserver(ImageView imageView) {
            this.imageView = imageView;
            this.first = false;
        }

        @Override
        public void onGlobalLayout() {
            if (first) {
                return;
            }
            Log.d("ImageObserver", "onGlobalLayout");
            int x = (int) imageView.getX();
            valueAnimator = ValueAnimator.ofInt(x, -imageView.getWidth() * 2);
            MyImageListener myImageListener = new MyImageListener(imageView);
//            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.setDuration(7000);
            valueAnimator.addUpdateListener(myImageListener);
            first = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        Log.d("killTest", "onCreateView Tate2");

        if (savedInstanceState != null) {
            mPeopleMoveFlag = savedInstanceState.getBoolean(getString(R.string.tate2_people));
        }
        mActivity = (Base2Activity) getActivity();
        mCharWholePixcel = TestShared.getInstance(getContext()).getCharWholePixel();
        mAasizePixel = TestShared.getInstance(getContext()).getAasizePixel();
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tate2, container, false);
        linearEtc = (LinearLayout) rootView.findViewById(R.id.test_etc);

        rootView.post(new Runnable() {
            @Override
            public void run() {
                Log.d("onGlobalLayout:handle", "getMeasuredHeight" + rootView.getMeasuredHeight() + "");
                mWholwHeight = rootView.getMeasuredHeight();
                mWholeWidth = rootView.getMeasuredWidth();
                mRegularTopMargin = (int) (mWholwHeight / 1.6);
                mEtcHeight = mWholwHeight - mRegularTopMargin;
                Calc.putY = Calc.regularTopMargin - mCharWholePixcel;
                mPutY = mRegularTopMargin - mCharWholePixcel;

                mPutY = mPutY - (mAasizePixel / 3);

                LinearLayout testDeLine = rootView.findViewById(R.id.testDe_line);
                FrameLayout.LayoutParams lineLayoutParams = (FrameLayout.LayoutParams) testDeLine.getLayoutParams();
                lineLayoutParams.height = mRegularTopMargin;
                testDeLine.setLayoutParams(lineLayoutParams);


                //
                FrameLayout.LayoutParams linearEtcLayoutParams = (FrameLayout.LayoutParams) linearEtc.getLayoutParams();
                linearEtcLayoutParams.height = mEtcHeight;
                linearEtc.setLayoutParams(linearEtcLayoutParams);
                linearEtc.setY(mRegularTopMargin);

                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tabLayout.getLayoutParams();
                int tab = mEtcHeight / 5;
                layoutParams.height = tab;
                tabLayout.setLayoutParams(layoutParams);

                LinearLayout.LayoutParams etcFragmentLayout = (LinearLayout.LayoutParams) etcLinearFragment.getLayoutParams();
                etcFragmentLayout.height = mEtcHeight - tab;
                etcLinearFragment.setLayoutParams(etcFragmentLayout);
                Log.d("killTest", "onGlobalLayout");

                if (savedInstanceState == null) {
                    mLog2Fragment = new Log2Fragment();
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.tate2_log2, mLog2Fragment, getString(R.string.fragment_log2));
                    ft.commit();
                } else {
                    mLog2Fragment = UtilInfo.getLog2Fragment(getContext());
                }
                LinearLayout log2linearLayout = rootView.findViewById(R.id.tate2_log2);
                ViewGroup.LayoutParams log2linearLayoutLayoutParams = log2linearLayout.getLayoutParams();
                mLog2HeightEnd = mRegularTopMargin / 3;
                log2linearLayoutLayoutParams.height = mLog2HeightEnd;
                log2linearLayout.setLayoutParams(log2linearLayoutLayoutParams);


                getActivity().getSupportLoaderManager().initLoader(1, null, mCallback);
                getActivity().getSupportLoaderManager().initLoader(2, new Bundle(), new LoaderCallbacks2FromSocket(getContext()));

            }
        });

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabLayout2);
//        imageView = (ImageView) rootView.findViewById(R.id.tate2_image_seishin);


        etcLinearFragment = (LinearLayout) rootView.findViewById(R.id.tate2_etc_fragment);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2, R.animator.update_animator1, R.animator.chara_animator2);

        IconFragment iconFragment = new IconFragment();
        ft.replace(R.id.tate2_etc_fragment, iconFragment, getString(R.string.fragment_icon));
        ft.commit();

        LinearLayout linearLayout0 = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tab_item, null);
        ImageView imageView0 = (ImageView) linearLayout0.findViewById(R.id.tab_item_image);
        imageView0.setImageDrawable(getContext().getDrawable(R.drawable.ic_icon_50));
        TextView textView0 = (TextView) linearLayout0.findViewById(R.id.tab_item_text);
        textView0.setText("Icon");

        LinearLayout linearLayout1 = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tab_item, null);
        ImageView imageView1 = (ImageView) linearLayout1.findViewById(R.id.tab_item_image);
        imageView1.setImageDrawable(getContext().getDrawable(R.drawable.ic_block_50));
        TextView textView = (TextView) linearLayout1.findViewById(R.id.tab_item_text);
        textView.setText("Block");

        LinearLayout linearLayout2 = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tab_item, null);
        ImageView imageView2 = (ImageView) linearLayout2.findViewById(R.id.tab_item_image);
        imageView2.setImageDrawable(getContext().getDrawable(R.drawable.ic_face_50));
        TextView textView2 = (TextView) linearLayout2.findViewById(R.id.tab_item_text);
        textView2.setText("Stat");

        LinearLayout linearLayout3 = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tab_item, null);
        ImageView imageView3 = (ImageView) linearLayout3.findViewById(R.id.tab_item_image);
        imageView3.setImageDrawable(getContext().getDrawable(R.drawable.ic_notifications_none_black_24dp));
        TextView textView3 = (TextView) linearLayout3.findViewById(R.id.tab_item_text);
        textView3.setText("Notice");

        LinearLayout linearLayout4 = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tab_item, null);
        ImageView imageView4 = (ImageView) linearLayout4.findViewById(R.id.tab_item_image);
        imageView4.setImageDrawable(getContext().getDrawable(R.drawable.ic_settings_50));
        TextView textView4 = (TextView) linearLayout4.findViewById(R.id.tab_item_text);
        textView4.setText("Setting");

        LinearLayout linearLayout5 = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tab_item, null);
        ImageView imageView5 = (ImageView) linearLayout5.findViewById(R.id.tab_item_image);
        imageView5.setImageDrawable(getContext().getDrawable(R.drawable.ic_web_50));
        TextView textView5 = (TextView) linearLayout5.findViewById(R.id.tab_item_text);
        textView5.setText("Web");

        tabLayout.addTab(tabLayout.newTab().setCustomView(linearLayout0));
        tabLayout.addTab(tabLayout.newTab().setCustomView(linearLayout1));
        tabLayout.addTab(tabLayout.newTab().setCustomView(linearLayout2));
        tabLayout.addTab(tabLayout.newTab().setCustomView(linearLayout3));
        tabLayout.addTab(tabLayout.newTab().setCustomView(linearLayout4));
        tabLayout.addTab(tabLayout.newTab().setCustomView(linearLayout5));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2, R.animator.update_animator1, R.animator.chara_animator2);
                if (TAB_ICON == position) {
                    IconFragment iconFragment = new IconFragment();
                    fragmentTransaction.replace(R.id.tate2_etc_fragment, iconFragment, getString(R.string.fragment_icon));
                    fragmentTransaction.commit();
                } else if (TAB_BLOCK == position) {
                    IgnoreFragment ignoreFragment = new IgnoreFragment();
                    fragmentTransaction.replace(R.id.tate2_etc_fragment, ignoreFragment, getString(R.string.fragment_ignore));
                    fragmentTransaction.commit();
                } else if (TAB_STAT == position) {
                    StateDialogFragment stateDialogFragment = new StateDialogFragment();
                    fragmentTransaction.replace(R.id.tate2_etc_fragment, stateDialogFragment, getString(R.string.fragment_state));
                    fragmentTransaction.commit();
                } else if (TAB_NOTIFICATION == position) {
                    NotificationSettingFragment notificationSettingFragment = new NotificationSettingFragment();
                    fragmentTransaction.replace(R.id.tate2_etc_fragment, notificationSettingFragment, getString(R.string.fragment_notify));
                    fragmentTransaction.commit();
                } else if (TAB_SETTING == position) {
                    SettingFragment settingFragment = new SettingFragment();
                    fragmentTransaction.replace(R.id.tate2_etc_fragment, settingFragment, getString(R.string.fragment_setting));
                    fragmentTransaction.commit();
                } else if (TAB_WEB == position) {
                    MyWebViewFragment myWebViewFragment = new MyWebViewFragment();
                    fragmentTransaction.replace(R.id.tate2_etc_fragment, myWebViewFragment, getString(R.string.tab_layout2_web_view));
                    fragmentTransaction.commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        frameLayoutWhole = rootView.findViewById(R.id.tate2_frame);
        ViewTreeObserver viewTreeObserver = frameLayoutWhole.getViewTreeObserver();

        vib = (Vibrator) getContext().getSystemService(VIBRATOR_SERVICE);
        commentMap = new HashMap<>();

        frameTate2 = (MyFrameLayout) rootView.findViewById(R.id.frame_tate2);


//        viewTreeObserver.addOnGlobalLayoutListener(new MyO(etcLinearFragment, tabLayout, frameLayoutWhole, frameTate2, (LinearLayout) rootView.findViewById(R.id.testDe_line), linearEtc));
        linearLayout = (LinearLayout) rootView.findViewById(R.id.tate2_log);

        FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams) linearLayout.getLayoutParams();
        layoutParams1.leftMargin = (int) Calc.Dp2Px(Calc.aasizeDp + Calc.aasizeDp / 3, getContext());
        linearLayout.setLayoutParams(layoutParams1);

        TestShared testShared = TestShared.getInstance(getContext());

        Tate3Fragment tate3Fragment = new Tate3Fragment();
        tate3Fragment.setDimension(getResources().getDimension(R.dimen.cmn_size12));
        tate3Fragment.setBackColor(getResources().getColor(R.color.commonGray));
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();

        if (!testShared.getLog()) {
            fragmentTransaction.hide(tate3Fragment);
        }




        if (!testShared.getDevelop()) {

        } else {
            if (!testShared.getShowDevelop()) {

            } else {

            }
        }
        fragmentTransaction.replace(R.id.tate2_log, tate3Fragment);
        fragmentTransaction.commit();


        rotateOn = AnimationUtils.loadAnimation(getContext(), R.anim.char_rotate1);
        rotateOff = AnimationUtils.loadAnimation(getContext(), R.anim.char_rotate2);
        tateReguState = TestShared.getInstance(getContext()).getTateRegu();


        txtRoomName = (TextView) rootView.findViewById(R.id.txt_tate2_room_name);
        nav_view = rootView.findViewById(R.id.nav_view);
        mScrollView = (ScrollView) rootView.findViewById(R.id.tate2_scroll);
        ViewTreeObserver scrollViewViewTreeObserver = mScrollView.getViewTreeObserver();
        scrollViewViewTreeObserver.addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (mScrollView.getChildAt(0).getBottom()
                        <= (mScrollView.getHeight() + mScrollView.getScrollY())) {
                    //scroll view is at bottom
                } else {
                    //scroll view is not at bottom
//                    mScrollView.fullScroll(View.FOCUS_DOWN);
                }
            }
        });


        frameTate2.init();


        frameTate2.setOnDragListener(new View.OnDragListener() {

            @Override
            public boolean onDrag(View v, DragEvent event) {

                int action = event.getAction();

//                ClipData clipData = event.getClipData();
//                ClipData.Item item = clipData.getItemAt(0);
//                Intent intent = item.getIntent();
//                if (intent == null) {
//                    return true;
//                }

                switch (action) {

                    case DragEvent.ACTION_DRAG_STARTED:
//                        Log.d("ACTION_DRAG_STARTED", "x:" + event.getX() + " mToY" + event.getY());
                        break;
                    case DragEvent.ACTION_DRAG_ENDED:

                        break;

                    case DragEvent.ACTION_DRAG_LOCATION:

                        break;

                    case DragEvent.ACTION_DROP:
                        Log.d("ACTION_DROP get", "x:" + event.getX());

                        ClipData clipData = event.getClipData();
                        ClipData.Item item = clipData.getItemAt(0);
                        Intent intent = item.getIntent();
//                        if (intent == null) {
//                            return
//                        }
                        int flag = intent.getIntExtra("flag", -1);
                        if (flag == IconFragment.ICON) {
                            Log.d("ACTION_DROP", "is icon");
                            return true;
                        }

                        int offsetX = intent.getIntExtra("offsetX", -1);
                        int offsetY = intent.getIntExtra("offsetY", -1);
                        float offsetFloatX = intent.getFloatExtra("offsetFloatX", -1);
                        float offsetFloatY = intent.getFloatExtra("offsetFloatY", -1);
                        int uniqueId = intent.getIntExtra("uniqueId", -1);
                        int move = intent.getIntExtra("move", -1);
                        int commentCount = intent.getIntExtra("commentCount", -1);

                        Log.d("ACTION_DROP offset", "x:" + offsetFloatX);
                        Person person;
                        if (testConnect.readMainThread.people.get(uniqueId).getUniqueId() == MeMonaId.uniqueId) {
                            person = getMe();
                        } else {
                            person = testConnect.readMainThread.people.get(uniqueId);
                        }
                        PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(uniqueId);
                        if (move == MOVE_CHARACTER) {
//                        main2DB = new Main2DB();
                            int x;
                            int y;

                            int cx2;
                            int cy2;

                            /**
                             * regular用
                             */
                            float tempRegX = event.getX() - offsetFloatX;

                            if (tempRegX < 0) {
                                tempRegX = 0;
                            } else if (tempRegX > Calc.widthRegular) {
                                tempRegX = Calc.widthRegular;
                            }

                            int regX = Math.round(tempRegX);
                            int personRegMonaX = Calc.convertFromAndroidX(regX, Calc.widthRegular);
                            person.setX(personRegMonaX);
                            person.setcXR(regX);


                            FrameLayout.LayoutParams regularParams = personLayoutDrawable.getLayoutRegularParams();
                            regularParams.setMargins(person.getcXR(), 0, 0, 0);
                            personLayoutDrawable.getRelativeLayout2().setLayoutParams(regularParams);
                            frameTate2.invalidate();

                            Bundle bundle = new Bundle();
                            bundle.putInt("x", person.getX());
                            bundle.putInt("y", Stub.Y);
                            Base2Activity nameActivity = (Base2Activity) getContext();
                            nameActivity.getSupportLoaderManager().initLoader(10, bundle, new LoaderCallbacksSendXY(nameActivity));

                        } else if (move == MOVE_COMMNET) {

                            float resultX = event.getX() - offsetFloatX;
                            float resultY = event.getY() - offsetFloatY;
                            int regX = Math.round(resultX);
                            int regY = Math.round(resultY);
                            try {
                                CommentAndValueListener commentAndValueListener = (CommentAndValueListener) commentMap.get(commentCount);
                                MyRegularCommentListener myRegularCommentListener = commentAndValueListener.myRegularCommentListener;
                                LinearLayout linearLayout = commentAndValueListener.comment;

                                FrameLayout.LayoutParams linerParams = (FrameLayout.LayoutParams) linearLayout.getLayoutParams();
                                int posY = regY - linerParams.topMargin;
                                // これが問題かもしれない
//                                valueAnimator.setIntValues(Calc.regularTopMargin - Calc.aasizePixcel + posY, Stub.TENJYO + posY);
                                ValueAnimator valueAnimator = myRegularCommentListener.getAnimation();
                                myRegularCommentListener.setFirst(myRegularCommentListener.getFirst() + posY);
                                myRegularCommentListener.setTenjyo(myRegularCommentListener.getTenjyo() + posY);
                                valueAnimator.setIntValues(myRegularCommentListener.getFirst(), myRegularCommentListener.getTenjyo());
                                Log.d(TAG, "aieuo:" + (Stub.DURATION - myRegularCommentListener.getKeikaTime()));
                                valueAnimator.setCurrentPlayTime(myRegularCommentListener.getKeikaTime());
                                valueAnimator.setDuration(Stub.DURATION);
                                Log.d("kakarotto", regY + "");
                                linerParams.setMargins(regX, regY, 0, 0);
                                linearLayout.setLayoutParams(linerParams);

                            } catch (NullPointerException e) {
                                Log.d(TAG, "comment drop");
                            }
                        }
                        break;
                }

                return true;
            }

        });

        return rootView;
    }

    class MyO implements ViewTreeObserver.OnGlobalLayoutListener {

        private LinearLayout etcLinearFragment;
        private TabLayout tabLayout;
        private LinearLayout linearEtcMyo;
        private LinearLayout line;
        private FrameLayout addFrame;
        private FrameLayout frameLayout;

        MyO(LinearLayout etcLinearFragment, TabLayout tabLayout, FrameLayout frameLayout, FrameLayout addFrame, LinearLayout linearLayout, LinearLayout linearEtc) {
            this.etcLinearFragment = etcLinearFragment;
            this.tabLayout = tabLayout;
            // zentai
            this.frameLayout = frameLayout;
            this.addFrame = addFrame;
            this.line = linearLayout;
            this.linearEtcMyo = linearEtc;
        }

        @Override
        public void onGlobalLayout() {
//            Log.d("onGlobalLayout3", "" + frameLayout.getHeight());
            if (ob == false) {
                Log.d("onGlobalLayout", "" + frameLayout.getHeight());
                int test = frameLayout.getHeight() + Calc.actionBarHeight;
                Calc.regularTopMargin = (int) (test / 1.85f);
//                Calc.regularTopMargin = (int) (test / 2);
                Log.d("onGlobalLayout", "test:" + test);
                Log.d("onGlobalLayout", "regularTopMargin:" + Calc.regularTopMargin);
                Calc.etcHeight = test - Calc.regularTopMargin;
                Log.d("onGlobalLayout", "etcHeight:" + Calc.etcHeight);
                Calc.putY = Calc.regularTopMargin - mCharWholePixcel;
                Log.d("killTest", "onGlobalLayout_Calc.charWholePixcel:" + Calc.charWholePixcel);
                Log.d("killTest", "onGlobalLayout_Calc.regularTopMargin:" + Calc.regularTopMargin);
                Log.d("killTest", "onGlobalLayout_Calc.putY:" + Calc.putY);
                Log.d("onGlobalLayout", "Calc.charWholePixcel:" + Calc.charWholePixcel);
                Log.d("onGlobalLayout", "Calc.putY:" + Calc.putY);
                Log.d("onGlobalLayout", "Calc.actionBarHeight" + Calc.actionBarHeight);
//                space.setToY(Calc.regularTopMargin);
                FrameLayout.LayoutParams lineLayoutParams = (FrameLayout.LayoutParams) line.getLayoutParams();
                lineLayoutParams.height = Calc.regularTopMargin;
                line.setLayoutParams(lineLayoutParams);

//                FrameLayout.LayoutParams tabLayoutLayoutParams = (FrameLayout.LayoutParams) tabLayout.getLayoutParams();
//                tabLayoutLayoutParams.height = Calc.regularTopMargin;
//                tabLayout.setLayoutParams(lineLayoutParams);
//                tabLayout.setToY(Calc.regularTopMargin);


                FrameLayout.LayoutParams linearEtc = (FrameLayout.LayoutParams) linearEtcMyo.getLayoutParams();
                linearEtc.height = Calc.etcHeight;
                linearEtcMyo.setLayoutParams(linearEtc);
                linearEtcMyo.setY(Calc.regularTopMargin);

                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tabLayout.getLayoutParams();
                int tab = Calc.etcHeight / 5;
                layoutParams.height = tab;
                tabLayout.setLayoutParams(layoutParams);

                LinearLayout.LayoutParams etcFragmentLayout = (LinearLayout.LayoutParams) etcLinearFragment.getLayoutParams();
                etcFragmentLayout.height = Calc.etcHeight - tab;
                etcLinearFragment.setLayoutParams(etcFragmentLayout);
                Log.d("killTest", "onGlobalLayout");
                getActivity().getSupportLoaderManager().initLoader(1, null, mCallback);
                getActivity().getSupportLoaderManager().initLoader(2, new Bundle(), new LoaderCallbacks2FromSocket(getContext()));
                ob = true;
            }
        }
    }


    @Override
    public void onDetach() {
        Log.d("20180503test", "onDetach Tate2");
        super.onDetach();
        mListener = null;
    }

    private void commonColorChange(ImageView imageView, Person p) {
        try {
            int type = testConnect.readMainThread.resNoFromStrType(p.getAaname());
            VectorChildFinder vectorChildFinder = new VectorChildFinder(getContext(), type, imageView);
            VectorDrawableCompat.VFullPath path1 = vectorChildFinder.findPathByName("path1");
            path1.setFillColor(p.getColor255());
        } catch (NullPointerException e) {

        }
//        imageView.invalidate();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(getString(R.string.tate2_people), mPeopleMoveFlag);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void commentColorChange(ImageView imageView, Person p) {
        if (!Stub.commentChange) {
            return;
        }
        int type = testConnect.readMainThread.resNoFromStrType(p.getAaname());
        VectorChildFinder vectorChildFinder = new VectorChildFinder(getContext(), type, imageView);
        VectorDrawableCompat.VFullPath path1 = vectorChildFinder.findPathByName("path1");
        path1.setFillColor(p.getColorInverte255());
//        imageView.invalidate();
    }

    class ColorChangeThread extends Thread {

        private Person person;
        private ImageView imageView;

        ColorChangeThread(ImageView imageView, Person person) {
            this.imageView = imageView;
            this.person = person;
        }

        @Override
        public void run() {
            super.run();
            commonColorChange(imageView, person);
        }
    }


    public void putTest(TestIdXML testIdXML) {
        Person person;
        /**
         * 自分かそれ以外かで切り替え
         */
        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }
        Log.d("killTest", person.getName() + "");
        /**
         * キャラ画像などの読み出し
         */
        PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());


        /**
         * 縦かレギュラーか判定
         * レイアウトパラムズ
         */

        RelativeLayout relative = personLayoutDrawable.getRelativeLayout2();
        tateReguState = 2;

        /**
         * reguかtateのレイアウトパラムズをセット
         */

        ImageView imageView = relative.findViewById(R.id.char_image);
        imageView.setScaleX(person.getScl());
        relative.setX(person.getcXR());
        relative.setY(mPutY);
        Log.d("killTest", "putY:" + mPutY);
        relative.invalidate();
        commonColorChange(imageView, person);

        if (person.isIgnoreFlag()) {
            personLayoutDrawable.getRelativeLayout2().setVisibility(View.INVISIBLE);
        } else {
            personLayoutDrawable.getRelativeLayout2().setVisibility(View.VISIBLE);
        }

        personLayoutDrawable.setRelativeLayout2(relative);

        /**
         * enterの場合
         */
        if (testIdXML.getXml().equals(UtilInfo.ENTER)) {
            if (person.isIgnoreFlag()) {
                personLayoutDrawable.getRelativeLayout2().setVisibility(View.INVISIBLE);
            } else {
                cloudAnime(person);
            }
            if (MeMonaId.getMe().getUniqueId() == testIdXML.getUniqueId()) {
                Base2Activity base2Activity = (Base2Activity) getActivity();
                testConnect.readMainThread.hashLayoutByte.get(testIdXML.getUniqueId());
                imageView.setOnTouchListener(new MyTouchListener(testIdXML.getUniqueId(), MOVE_CHARACTER));

                ProgressFragment progressFragment = UtilInfo.getProgressFragment(getContext());
                if (progressFragment != null) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(progressFragment).commit();
                }
            }
        }
        frameTate2.addView(personLayoutDrawable.getRelativeLayout2());
        if (test3 == true) {
            personLayoutDrawable.getRelativeLayout2().setOnTouchListener(new MyTouchListener(person.getUniqueId(), MOVE_CHARACTER));
        }
        frameTate2.invalidate();
    }

    private void cloudAnime(Person person) {
        final ImageView imageView = new ImageView(mActivity);
        MyCustomAnimation cad = new MyCustomAnimation((AnimationDrawable) ContextCompat.getDrawable(getContext(), R.drawable.cloud_anime)) {
            @Override
            public void onAnimationFinish() {
                frameTate2.removeView(imageView);
            }

            @Override
            public void onAnimationStart() {

            }
        };

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(mAasizePixel, mAasizePixel);
        imageView.setLayoutParams(layoutParams);
        frameTate2.addView(imageView);
        imageView.setX(person.getcXR());
        imageView.setY(mPutY);
        imageView.setBackground(cad);
        cad.start();
    }

    class CommentAndValueListener {
        private MyRegularCommentListener myRegularCommentListener;
        private LinearLayout comment;

        public CommentAndValueListener(LinearLayout comment, MyRegularCommentListener myRegularCommentListener) {
            this.comment = comment;
            this.myRegularCommentListener = myRegularCommentListener;
        }

    }

    static class MyImageListener implements ValueAnimator.AnimatorUpdateListener {

        private final int initX;
        private View imageView;

        public MyImageListener(View imageView) {
            this.imageView = imageView;
            this.initX = (int) this.imageView.getX();
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            if (animation.getCurrentPlayTime() >= animation.getDuration()) {
                imageView.setX(initX);
                return;
            }
            int x = (int) animation.getAnimatedValue();
            imageView.setX(x);
        }
    }

    static class MyPeopleListener implements ValueAnimator.AnimatorUpdateListener {

        private int initY;
        private View view;

        public MyPeopleListener(View view) {
            this.view = view;
            this.initY = (int) this.view.getY();
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            if (animation.getCurrentPlayTime() >= animation.getDuration()) {
                int y = (int) animation.getAnimatedValue();
                view.setY(y);
                return;
            }
            int y = (int) animation.getAnimatedValue();
            view.setY(y);
        }
    }

    static class MyRegularCommentListener implements ValueAnimator.AnimatorUpdateListener {

        private int keikaTime;
        private int first;
        private int commentNumber;
        private HashMap<Integer, CommentAndValueListener> commentMap;
        private MyFrameLayout myFrameLayout;
        private LinearLayout linearLayout;
        private int tenjyo;
        private ValueAnimator animation;

        public MyRegularCommentListener(int commentNumber, MyFrameLayout myFrameLayout, LinearLayout linearLayout, int first, int tenjyo, int nokoriTime, HashMap<Integer, CommentAndValueListener> commentMap) {
            this.commentNumber = commentNumber;
            this.myFrameLayout = myFrameLayout;
            this.linearLayout = linearLayout;
            this.first = first;
            this.tenjyo = tenjyo;
            this.keikaTime = nokoriTime;
            this.commentMap = commentMap;
        }


        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            this.animation = animation;
//            Log.d(TAG, "animation.keika:" + this.keikaTime);
//            this.nokoriTime = (int) (this.nokoriTime - animation.getCurrentPlayTime());
            this.keikaTime = (int) animation.getCurrentPlayTime();
            int y = (int) animation.getAnimatedValue();
//            Log.d(TAG, "animation.getDuration():" + animation.getDuration());
//            Log.d(TAG, "animation.getCurrentPlayTime():" + animation.getCurrentPlayTime());
//            Log.d(TAG, "animation. nokori" + this.keikaTime);
            linearLayout.setVisibility(View.VISIBLE);

            if (animation.getCurrentPlayTime() >= animation.getDuration()) {
                commentMap.remove(commentNumber);
                myFrameLayout.removeView(linearLayout);
                return;
            }

            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) linearLayout.getLayoutParams();
            layoutParams.topMargin = y;
            linearLayout.setLayoutParams(layoutParams);

        }

        public ValueAnimator getAnimation() {
            return animation;
        }

        public void setTenjyo(int tenjyo) {
            this.tenjyo = tenjyo;
        }

        public void setFirst(int first) {
            this.first = first;
        }

        public int getFirst() {
            return first;
        }

        public int getTenjyo() {
            return tenjyo;
        }

        public int getKeikaTime() {
            return keikaTime;
        }
    }

    static class MyUpdateListener implements ValueAnimator.AnimatorUpdateListener {

        private TextView textView;
        int b = 0;
        int i;
        private float a;
        private int c;
        private float alpha;

        public MyUpdateListener(TextView textView) {
            this.textView = textView;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            a = (float) animation.getAnimatedValue();
            alpha = a;
            c = (int) (a * 5f);
            if (b - c == 1) {
                alpha = a;
                textView.setAlpha(alpha);
                Log.d("aplha", i++ + "");
            } else if (c == 0) {
                textView.setAlpha(0);
                animation.cancel();
                textView.setBackground(null);
                textView.setAnimation(null);
                textView.setText(null);
            }
            b = c;
            if (a == 0) {
                textView.setAlpha(0);
                animation.cancel();
                textView.setBackground(null);
                textView.setAnimation(null);
                textView.setText(null);
            }
        }
    }

    class MyTouchListener implements View.OnTouchListener {

        private int commentCount;
        private int check;
        protected int uniqueId;

        MyTouchListener(int uniqueId, int check) {
            this.uniqueId = uniqueId;
            this.check = check;
        }

        MyTouchListener(int commentCount, int check, String str) {
            this.commentCount = commentCount;
            this.check = check;
        }


        @Override
        public boolean onTouch(View v, MotionEvent event) {
            boolean b;
            if (check == MOVE_CHARACTER) {
                vib.vibrate(10);
            }

            Intent intent = new Intent();
            intent.putExtra("offsetX", (int) event.getX());
            Log.d("nana:X", event.getX() + "");
            Log.d("nana:Y", event.getY() + "");
            intent.putExtra("offsetY", (int) event.getY());
            intent.putExtra("offsetFloatX", event.getX());
            intent.putExtra("offsetFloatY", event.getY());

            intent.putExtra("uniqueId", this.uniqueId);
            intent.putExtra("move", check);
            intent.putExtra("commentCount", commentCount);
            MyDragShadowBuilder myDragShadowBuilder = new MyDragShadowBuilder(v);
            myDragShadowBuilder.setIntent(intent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                b = v.startDragAndDrop(ClipData.newIntent("offset", intent), myDragShadowBuilder, v, 0);
            } else {
                b = v.startDrag(ClipData.newIntent("offset", intent), myDragShadowBuilder, v, 0);
            }


            return true;
        }
    }


    public void showComment2(TestIdXML testIdXML) {

        final Person person;

        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }
        ArrayList<TestCom> testComs = person.getTestComs();
        String com = "";
        for (TestCom testCom : testComs) {
            if (!testCom.isTate2Disp()) {
                com = testCom.getCmt();
                testCom.setTate2Disp(true);
                break;
            }
        }

        if (person.isIgnoreFlag()) {
            /**
             * 無視している場合は処理しない
             */

        } else if (com.indexOf("モナモバ") != -1) {

            /**
             * 無視していなかった場合モナモバ
             */
//            int rand = new Random().nextInt(Calc.heightDp - Calc.aasizeDp);
//            final ImageView imageView = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.aa_gif, null);
//            ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(imageView, "translationX", Calc.Dp2Px(Calc.aasizeDp, getContext()), Calc.Dp2Px(Calc.widthDp, getContext()));
//            ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(imageView, "translationY", rand, rand);
//
//            int size = (int) Calc.Dp2Px(Calc.aasizeDp, getContext()) / 2;
//            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(size, size);
//            imageView.setLayoutParams(layoutParams);
////            GlideDrawableImageViewTarget target = new GlideDrawableImageViewTarget(imageView);
////            Glide.with(getContext()).load(R.mipmap.g_mona).into(target);
//            imageView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.g_mona_anim));
//            AnimationDrawable frameAnimation = (AnimationDrawable) imageView.getBackground();
//            frameGif.addView(imageView);
//            frameAnimation.start();
//            AnimatorSet set = new AnimatorSet();
//            set.setDuration(5000);
//            set.setInterpolator(new LinearInterpolator());
//            set.playTogether(objectAnimator1, objectAnimator2);
//            set.start();
//
//            set.addListener(new Animator.AnimatorListener() {
//                @Override
//                public void onAnimationStart(Animator animation) {
//
//                }
//
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    frameGif.removeView(imageView);
//                }
//
//                @Override
//                public void onAnimationCancel(Animator animation) {
//                    frameGif.removeView(imageView);
//                }
//
//                @Override
//                public void onAnimationRepeat(Animator animation) {
//
//                }
//            });
        }
        if (!person.isIgnoreFlag()) {

            PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());
            RelativeLayout relativeLayout2 = personLayoutDrawable.getRelativeLayout2();
            final ImageView imageView = relativeLayout2.findViewById(R.id.char_image);

            /**
             * 一瞬色変える
             */
            commentColorChange(imageView, person);
            final Handler handler = new Handler(Looper.getMainLooper());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.post(new ColorChangeThread(imageView, person));
                }
            }).start();


            TestCom2 testCom2 = new TestCom2();
//            testCom2.setValueAnimator(ValueAnimator.ofFloat(1f, 0));
//            testCom2.getValueAnimator().setDuration(20000);
//            testCom2.setMyUpdateListener(new MyUpdateListener());
            testCom2.setCom(com);
//            testCom2.setColor(person.getColor255());
            testCom2.setColor(person.getColorHukidashi());

            if (STATE_TATE == tateReguState) {
                tateShowComment(relativeLayout2, testCom2);
            } else if (STATE_REGU == tateReguState) {
                showReguComment2(relativeLayout2, testCom2, person);
            }


        }
    }


    private void showReguComment2(RelativeLayout relativeLayout2, TestCom2 testCom2, Person person) {
        if (mContext == null) {
            return;
        }
        LinearLayout linearComment = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.test_comment1, frameTate2, false);
        FrameLayout.LayoutParams fLayoutParams = (FrameLayout.LayoutParams) linearComment.getLayoutParams();

        linearComment.setOnTouchListener(new MyTouchListener(commentCount, MOVE_COMMNET, null));

        TextView textView = (TextView) linearComment.findViewById(R.id.test_comment1);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setStroke(3, getContext().getResources().getColor(R.color.commonBlack));
        drawable.setCornerRadius(10);
        drawable.setColor(testCom2.getColor());
        textView.setBackground(drawable);
        textView.setText(testCom2.getCom());
        frameTate2.addView(linearComment);


        ViewTreeObserver viewTreeObserver = linearComment.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new MyOnGlobalLayoutListener(person, linearComment));

        ValueAnimator valueAnimator = ValueAnimator.ofInt(mPutY, Stub.TENJYO);
        MyRegularCommentListener myRegularCommentListener = new MyRegularCommentListener(commentCount, frameTate2, linearComment, mPutY, Stub.TENJYO, Stub.DURATION, commentMap);
        CommentAndValueListener commentAndValueListener = new CommentAndValueListener(linearComment, myRegularCommentListener);
        commentMap.put(commentCount++, commentAndValueListener);

        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setDuration(Stub.DURATION);
        valueAnimator.addUpdateListener(myRegularCommentListener);
        valueAnimator.start();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    class MyOnGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {

        private Person person;
        private View view;

        MyOnGlobalLayoutListener(Person person, View view) {
            this.person = person;
            this.view = view;
        }

        @Override
        public void onGlobalLayout() {

            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
//            if (layoutParams.topMargin)


            Log.d(TAG, "onGlobalLayout:" + layoutParams.leftMargin);
            layoutParams.setMargins(person.getcXR() + (mAasizePixel / 2) - (view.getWidth() / 2), mRegularTopMargin - mAasizePixel, 0, 0);
            view.setLayoutParams(layoutParams);
            ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
            viewTreeObserver.removeOnGlobalLayoutListener(this);
        }
    }


    private void tateShowComment(RelativeLayout relativeLayout2, TestCom2 testCom2) {
        if (mContext == null) {
            return;
        }
        if (relativeLayout2.findViewById(R.id.test_comment1) == null) {

            TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.test_comment1, relativeLayout2, false);
            textView.setText(testCom2.getCom());
            textView.setTag(1);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setShape(GradientDrawable.RECTANGLE);
            drawable.setStroke(3, getContext().getResources().getColor(R.color.commonBlack));
            drawable.setColor(testCom2.getColor());
            textView.setBackground(drawable);
            relativeLayout2.addView(textView);
//                textView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.com_anim1));
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(1f, 0);
            valueAnimator.setDuration(20000);
            valueAnimator.addUpdateListener(new MyUpdateListener(textView));
            valueAnimator.start();
        } else {
            /**
             * 既存のテキストビューを取り出して
             * マージンを倍にする
             *
             * そのあと新規でテキストを作り追加する
             */
            TextView textView1 = null;
            for (int i = 0; i < relativeLayout2.getChildCount(); i++) {
                int tag;
                try {
                    tag = (int) relativeLayout2.getChildAt(i).getTag();
                } catch (NullPointerException e) {
                    continue;
                }
                if (tag == 1) {
                    textView1 = (TextView) relativeLayout2.getChildAt(i);
                    break;
                }
            }

            for (int i = 0; i < relativeLayout2.getChildCount(); i++) {
                int tag;
                try {
                    tag = (int) relativeLayout2.getChildAt(i).getTag();
                } catch (NullPointerException e) {
                    continue;
                }
                if (tag == 2) {
                    TextView textView2 = (TextView) relativeLayout2.getChildAt(i);
                    relativeLayout2.removeView(textView2);
                    break;
                }
            }
//                textView1.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.com_anim1));


            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView1.getLayoutParams();
            layoutParams.topMargin = layoutParams.topMargin * 2;
            textView1.setLayoutParams(layoutParams);
            textView1.setTag(2);

            TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.test_comment1, relativeLayout2, false);
            textView.setText(testCom2.getCom());
            textView.setTag(1);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setShape(GradientDrawable.RECTANGLE);
            drawable.setStroke(3, getContext().getResources().getColor(R.color.commonBlack));
            drawable.setColor(testCom2.getColor());
            textView.setBackground(drawable);
            relativeLayout2.addView(textView);
            textView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.com_anim1));
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(1f, 0);
            valueAnimator.setDuration(20000);
            valueAnimator.addUpdateListener(new MyUpdateListener(textView));
            valueAnimator.start();
        }
    }

    public void SetState2(TestIdXML testIdXML) {

        Person person;

        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }


        PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());
        if (person.getStat().equals("通常")) {
            personLayoutDrawable.getRelativeLayout2().findViewById(R.id.char_stat).setVisibility(View.INVISIBLE);
        } else {
            TextView textView = (TextView) personLayoutDrawable.getRelativeLayout2().findViewById(R.id.char_stat);
            textView.setText(person.getStat());
            textView.setVisibility(View.VISIBLE);
            textView.invalidate();
        }

    }

    public void movePersonSet2(TestIdXML testIdXML) {

        Person person;

        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }
        PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());
        ImageView imageView = personLayoutDrawable.getRelativeLayout2().findViewById(R.id.char_image);
        imageView.setScaleX(person.getScl());
        imageView.invalidate();
        /**
         * 縦用
         */
//        FrameLayout.LayoutParams layoutParams2 = personLayoutDrawable.getLayoutParams2();
//        // stub todo
//        layoutParams2.setMargins(person.getcX2(), person.getcY2(), 0, 0);
//        personLayoutDrawable.getRelativeLayout2().setLayoutParams(layoutParams2);

        /**
         * レギュラー用のレイアウトパラムズを設定
         */
        RelativeLayout relativeLayout = personLayoutDrawable.getRelativeLayout2();
        relativeLayout.setX(person.getcXR());
        relativeLayout.setY(mPutY);
//        relativeLayout.setToY(1);
        Log.d("movePerson", "Calc.putX:" + person.getcXR());
        Log.d("movePerson", "Calc.putY:" + Calc.putY);

//        FrameLayout.LayoutParams regularParams = personLayoutDrawable.getLayoutRegularParams();
        // stub todo
//        layoutParams2.setMargins(person.getcX2(), person.getcY2(), 0, 0);
//        regularParams.setMargins(person.getcXR(), Calc.regularTopMargin - Calc.charWholePixcel, 0, 0);
//        personLayoutDrawable.getRelativeLayout2().setLayoutParams(regularParams);

        frameTate2.invalidate();

    }

    public void removePersonOnCanvas2(TestIdXML testIdXML) {
        /**
         * remove view of LinearLayout ,after gets Name with monaId.
         * remove person from the person table.
         * monaIdからnameを取得した後、LinearLayoutを削除する
         */

        Person person;

        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }


        final PersonLayoutDrawable personLayoutByte = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());

        RelativeLayout relativeLayout2;
        try {
            relativeLayout2 = personLayoutByte.getRelativeLayout2();
        } catch (NullPointerException e) {
            Log.d("unknow why", "なぜかnull");
            return;
        }
//        frameTate2.removeView(relativeLayout2);

        if (!person.isIgnoreFlag()) {
//            displayKumo2(person, (ImageView) relativeLayout2.findViewById(R.id.char_image), personLayoutByte, true, relativeLayout2);
            cloudAnime(person);
            frameTate2.removeView(relativeLayout2);
            testConnect.readMainThread.hashLayoutByte.remove(person.getUniqueId());
            testConnect.readMainThread.people.remove(person.getUniqueId());

        } else {
//            frameTate2.removeView(relativeLayout2);
//            testConnect.readMainThread.hashLayoutByte.remove(person.getId());
            frameTate2.removeView(relativeLayout2);
            testConnect.readMainThread.hashLayoutByte.remove(person.getUniqueId());
            testConnect.readMainThread.people.remove(person.getUniqueId());
        }


    }


    public synchronized void doProcess(ArrayList<TestIdXML> testIdXMLS) {
        int i = 0;
        Log.d("doProcess start", i + "");

        for (TestIdXML testIdXML : testIdXMLS) {

            try {
                Log.d("doProcess process", testIdXML.getXml());
                // log2
                if (mLog2Fragment != null) {
                    if (testIdXML.getXml().equals(UtilInfo.ENTER)) {


                        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
                            mLog2Fragment.enterMe2(testIdXML);
                        } else {
                            mLog2Fragment.enterOther2(testIdXML);
                        }
                    } else if (testIdXML.getXml().equals(UtilInfo.USER)) {

                    } else if (testIdXML.getXml().equals(UtilInfo.COM)) {
                        mLog2Fragment.showComment(testIdXML);
                    } else if (testIdXML.getXml().equals(UtilInfo.STAT)) {

                    } else if (testIdXML.getXml().equals(UtilInfo.XY)) {

                    } else if (testIdXML.getXml().equals(UtilInfo.EXIT)) {

                        mLog2Fragment.showExitOther2(testIdXML);

                    }
                }


                if (testIdXML.getXml().equals(UtilInfo.ENTER) || testIdXML.getXml().equals(UtilInfo.USER)) {
                    putTest(testIdXML);
                } else if (testIdXML.getXml().equals(UtilInfo.COM)) {
                    showComment2(testIdXML);
                } else if (testIdXML.getXml().equals(UtilInfo.STAT)) {
                    SetState2(testIdXML);
                } else if (testIdXML.getXml().equals(UtilInfo.XY)) {
                    if (!test3) {
                        movePersonSet2(testIdXML);
                    } else if (test3) {
                        testNotMoveSet2(testIdXML);
                    }


                } else if (testIdXML.getXml().equals(UtilInfo.EXIT)) {
                    removePersonOnCanvas2(testIdXML);
                }


            } catch (IllegalStateException e) {
                Log.d("20180707", "aaaaa");
            }

        }
        Log.d("doProcess finish", ++i + "");
    }

    private void testNotMoveSet2(TestIdXML testIdXML) {
        Person person;

        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }
        PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());
        ImageView imageView = personLayoutDrawable.getRelativeLayout2().findViewById(R.id.char_image);
        imageView.setScaleX(person.getScl());
        imageView.invalidate();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        UtilInfo.queueThread.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("lifeTest", "Tate2Fragment onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("lifeTest", "Tate2Fragment onPause");
    }


}
