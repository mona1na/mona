package com.devmona;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
// * {@link Log2Fragment.OnFragmentInteractionListener} interface
// * to handle interaction events.
// * Use the {@link Log2Fragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class Log2Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TestConnect mTestConnect;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mRecyclerAdapter;

    public ArrayList<PersonEnum> mDataList;
    private ImageView mImageScroll;

//    private OnFragmentInteractionListener mListener;

    public Log2Fragment() {
        // Required empty public constructor
        mTestConnect = TestConnect.getInstance(getContext());
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Log2Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Log2Fragment newInstance(String param1, String param2) {
        Log2Fragment fragment = new Log2Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        MyLog2Data myLog2Data = new MyLog2Data();
        myLog2Data.setmLog2List(mDataList);
        outState.putParcelable(getString(R.string.log2_parcel), myLog2Data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_log2, container, false);
        mImageScroll = root.findViewById(R.id.image_scroll_log2);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.log2_recycler);


        mImageScroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickImgScroll();
                mImageScroll.setVisibility(View.INVISIBLE);
            }
        });


        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerAdapter = new RecyclerAdapter();
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mRecyclerAdapter);


        if (savedInstanceState == null) {
            mDataList = new ArrayList<>();
            mRecyclerAdapter.setData(mDataList);
        } else {
            MyLog2Data myLog2Data = (MyLog2Data) savedInstanceState.get(getString(R.string.log2_parcel));
            Log.d("killTest", "commentList:" + myLog2Data.getmLog2List());
            mDataList = myLog2Data.getmLog2List();
            PersonEnum personEnum = new PersonEnum();
            personEnum.setType(TYPE.Connect);
            Person person = new Person();
            person.setLog(Util.getDate() + "に再接続しました。");
            personEnum.setPerson(person);
            mDataList.add(personEnum);
            if (mDataList != null) {
                mRecyclerAdapter.setData(mDataList);
                mRecyclerAdapter.notifyDataSetChanged();
            }
        }

        return root;
    }

    private void clickImgScroll() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerView.getLayoutManager());
        layoutManager.scrollToPosition(mDataList.size() -1);
    }

    private void checkScroll() {
        if (mDataList.size() > 0) {
            LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerView.getLayoutManager());
            int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
            int last = layoutManager.findLastCompletelyVisibleItemPosition();
            Log.d("4gatsunoame",  "first" +firstVisiblePosition);
            Log.d("4gatsunoame", "last:"+ last + "");



            if (last + 2 != mDataList.size()) {
                mImageScroll.setVisibility(View.VISIBLE);
            } else {
                mImageScroll.setVisibility(View.INVISIBLE);
                layoutManager.scrollToPosition(mDataList.size() - 1);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void enterMe2(TestIdXML testIdXML) {
        PersonEnum personEnum = new PersonEnum();
        personEnum.setType(TYPE.Enter);
        personEnum.setPerson(personGenelate(MeMonaId.getMe()));
        mDataList.add(personEnum);
        mRecyclerAdapter.notifyItemInserted(mDataList.size() - 1);
        checkScroll();
    }

    public Person personGenelate(Person person) {
        Person genPerson = new Person();
        genPerson.setNameEllipsis(person.getNameEllipsis());
        genPerson.setShirotori(person.getShirotori());
        genPerson.setKurotori(person.getKurotori());
        return genPerson;
    }


    public void enterOther2(TestIdXML testIdXML) {
        Person person = mTestConnect.readMainThread.people.get(testIdXML.getUniqueId());
        PersonEnum personEnum = new PersonEnum();
        personEnum.setType(TYPE.Enter);
        personEnum.setPerson(personGenelate(person));
        mDataList.add(personEnum);
        mRecyclerAdapter.notifyItemInserted(mDataList.size() - 1);
        checkScroll();
    }

    public void showComment(TestIdXML testIdXML) {
        Person person;
        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = mTestConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }

        if (!person.isIgnoreFlag()) {
            PersonEnum personEnum = new PersonEnum();
            Person genPerson = personGenelate(person);
            genPerson.setComment2(person.getComment2());
            personEnum.setPerson(genPerson);
            personEnum.setType(TYPE.Comment);
            mDataList.add(personEnum);
            mRecyclerAdapter.notifyItemInserted(mDataList.size() - 1);
        }
        checkScroll();
    }

    public void showExitOther2(TestIdXML testIdXML) {
        Person person = mTestConnect.readMainThread.people.get(testIdXML.getUniqueId());
        PersonEnum personEnum = new PersonEnum();
        personEnum.setPerson(personGenelate(person));
        personEnum.setType(TYPE.Exit);
        mDataList.add(personEnum);
        mRecyclerAdapter.notifyItemInserted(mDataList.size() - 1);
        checkScroll();
    }

    static class PersonEnum implements Parcelable {
        private Person person;
        private TYPE type;

        public PersonEnum() {

        }

        protected PersonEnum(Parcel in) {
            this.person = in.readParcelable(Person.class.getClassLoader());
            this.type = TYPE.valueOf(in.readString());
        }

        public static final Creator<PersonEnum> CREATOR = new Creator<PersonEnum>() {
            @Override
            public PersonEnum createFromParcel(Parcel in) {
                return new PersonEnum(in);
            }

            @Override
            public PersonEnum[] newArray(int size) {
                return new PersonEnum[size];
            }
        };

        public Person getPerson() {
            return person;
        }

        public TYPE getType() {
            return type;
        }

        public void setType(TYPE type) {
            this.type = type;
        }

        public void setPerson(Person person) {
            this.person = person;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(person, flags);
            dest.writeString(type.toString());
        }
    }

    public enum TYPE {Comment, Enter, Exit, Log, DisConnect, Connect}

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }

    public class RecyclerAdapter extends RecyclerView.Adapter<CasarealViewHolder> {

        private ArrayList<PersonEnum> mData;


        public RecyclerAdapter() {

        }

        @NonNull
        @Override
        public CasarealViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.log2_low, parent, false);
            CasarealViewHolder vh = new CasarealViewHolder(inflate);
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull CasarealViewHolder holder, int position) {
            PersonEnum personEnum = mData.get(position);
            Person person = personEnum.getPerson();
            holder.mTitleView.setTextColor(getResources().getColor(R.color.log2Text));
            if (mData.size() - 1 == position) {
//                holder.mDivider.setVisibility(View.GONE);
            } else {
//                holder.mDivider.setVisibility(View.VISIBLE);
            }

            if (TYPE.Comment == personEnum.getType()) {
                if (!TextUtils.isEmpty(person.getKurotori())) {
                    holder.mTitleView.setText(genelateNameKurotori(person) + ":　" + person.getComment2());
                } else {
                    holder.mTitleView.setText(genelateNameShirotori(person) + ":　" + person.getComment2());
                }
            } else if (TYPE.Enter == personEnum.getType()) {
                if (!TextUtils.isEmpty(person.getKurotori())) {
                    holder.mTitleView.setText(genelateNameKurotori(person) + "が入室");
                } else {
                    holder.mTitleView.setText(genelateNameShirotori(person) + "が入室");
                }
            } else if (TYPE.Exit == personEnum.getType()) {
                if (!TextUtils.isEmpty(person.getKurotori())) {
                    holder.mTitleView.setText(genelateNameKurotori(person) + "が退室");
                } else {
                    holder.mTitleView.setText(genelateNameShirotori(person) + "が退室");
                }
            } else if (TYPE.DisConnect == personEnum.getType()) {
                holder.mTitleView.setText(personEnum.getPerson().getLog());
                holder.mTitleView.setTextColor(getResources().getColor(R.color.commonRed));
            } else if (TYPE.Connect == personEnum.getType()) {
                holder.mTitleView.setText(personEnum.getPerson().getLog());
                holder.mTitleView.setTextColor(getResources().getColor(R.color.commonGreen));
            }

//            holder.mTitleView.setText();
        }

        private String genelateNameShirotori(Person person) {
            return person.getNameEllipsis() + person.getShikakuRealShirotori();
        }

        private String genelateNameKurotori(Person person) {
            return person.getNameEllipsis() + person.getShikakuRealKurotori();
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }


        public void setData(ArrayList<PersonEnum> mDataList) {
            this.mData = mDataList;
        }
    }

    public class CasarealViewHolder extends RecyclerView.ViewHolder {
        private View mDivider;
        public TextView mTitleView;

        public CasarealViewHolder(View itemView) {
            super(itemView);
            mTitleView = (TextView) itemView.findViewById(R.id.log2_low_text);
            mDivider = itemView.findViewById(R.id.log2_divider);
        }
    }
}
