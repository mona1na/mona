package com.devmona;

/**
 * Created by sake on 2018/06/15.
 */

public class IdAanameResno {
    private String name;
    private int resNo;
    private int id;

    public IdAanameResno() {
        this.name = "";
        this.resNo = -1;
        this.id = -1;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getResNo() {
        return resNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setResNo(int resNo) {
        this.resNo = resNo;
    }


}
