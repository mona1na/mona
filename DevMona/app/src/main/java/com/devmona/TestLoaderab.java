package com.devmona;

import android.content.AsyncTaskLoader;
import android.content.Context;

/**
 * Created by sake on 2018/01/06.
 */

public abstract class TestLoaderab<D> extends AsyncTaskLoader<D> {

    private D mResult;
    private boolean mIsStarted = false;

    public TestLoaderab(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        if (mResult != null) {
            deliverResult(mResult);
            return;
        }
        if (!mIsStarted || takeContentChanged()) {
            forceLoad();
        }
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        mIsStarted = true;
    }

    @Override
    public void deliverResult(D data) {
        mResult = data;
        super.deliverResult(data);
    }
}