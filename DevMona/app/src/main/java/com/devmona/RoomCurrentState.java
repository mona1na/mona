package com.devmona;

import java.io.Serializable;

/**
 * Created by sake on 2018/01/25.
 */

public class RoomCurrentState implements Serializable{

    private int currentRoomNumber;
    private int currentRoomCount;
    public RoomCurrentState (){
        currentRoomNumber = 0;
        currentRoomCount = 0;
    }

    public void setCurrentRoomCount(int currentRoomCount) {
        this.currentRoomCount = currentRoomCount;
    }

    public void setCurrentRoomNumber(int currentRoomNumber) {
        this.currentRoomNumber = currentRoomNumber;
    }

    public int getCurrentRoomCount() {
        return currentRoomCount;
    }

    public int getCurrentRoomNumber() {
        return currentRoomNumber;
    }
}
