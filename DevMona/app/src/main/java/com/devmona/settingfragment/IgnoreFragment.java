package com.devmona.settingfragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.devmona.TestShared;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.devmona.Base2Activity;
import com.devmona.MeMonaId;
import com.devmona.Person;
import com.devmona.PersonLayoutDrawable;
import com.devmona.R;
import com.devmona.Tate2Fragment;
import com.devmona.TestConnect;
import com.devmona.UtilInfo;
import com.devmona.loadercallbacks.LoaderCallbacksSendIgnore;

/**
 * Created by sake on 2018/04/21.
 */

public class IgnoreFragment extends Fragment {

    private ListView listView;
    private IgnoreAdapter ignoreAdapter;
    private Button btnClose;
    public static final String TAG = "IgnoreFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_ignore_fragment_dialog, container, false);
        listView = rootView.findViewById(R.id.ignore_list);
        ignoreAdapter = new IgnoreAdapter(getContext(), R.layout.ignore_inner);
        listView.setAdapter(ignoreAdapter);
        TestConnect testConnect = TestConnect.getInstance(getContext());
        try {
            for (int uniqueId : testConnect.readMainThread.people.keySet()) {
                Person person = testConnect.readMainThread.people.get(uniqueId);
                if (person.getMonaId() == MeMonaId.getMe().getMonaId()) {
                    continue;
                }
                ignoreAdapter.add(person);
            }
        } catch (NullPointerException e) {
            Log.d(TAG, "null");
        }

        return rootView;
    }

    class IgnoreAdapter extends ArrayAdapter<Person> {

        public IgnoreAdapter(@NonNull Context context, int resource) {
            super(context, resource);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            convertView = getActivity().getLayoutInflater().inflate(R.layout.ignore_inner, null);
            final Person person = getItem(position);
            LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.ignore_linear);
            TextView textNameTrip = convertView.findViewById(R.id.ignore_inner_name_trip_id);

            linearLayout.setOnClickListener(new MyOnClickListener(person) {
                @Override
                public void onClick(View v) {

                    try {
                        Bundle bundle = new Bundle();

                        bundle.putString("ihash", this.person.getShirotori());
                        if (this.person.isIgnoreFlag() == false) {
                            bundle.putString("stat", "on");
                            this.person.setIgnoreFlag(true);
                            preferenceIgnoreSet(this.person);
                            TestConnect testConnect = TestConnect.getInstance(getContext());
                            PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());
                            personLayoutDrawable.getRelativeLayout2().setVisibility(View.INVISIBLE);
                        } else {
                            bundle.putString("stat", "off");
                            this.person.setIgnoreFlag(false);
                            preferenceIgnoreDelete(this.person);
                            TestConnect testConnect = TestConnect.getInstance(getContext());
                            PersonLayoutDrawable personLayoutDrawable = testConnect.readMainThread.hashLayoutByte.get(person.getUniqueId());
                            personLayoutDrawable.getRelativeLayout2().setVisibility(View.VISIBLE);
                        }


                        getActivity().getSupportLoaderManager().initLoader(14, bundle, new LoaderCallbacksSendIgnore((Base2Activity) getActivity()));
                    }catch (NullPointerException e) {

                    }
                    getActivity().getSupportFragmentManager().beginTransaction().remove(IgnoreFragment.this).commit();
                    Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(getContext());
                    TabLayout tabLayout = (TabLayout) tate2Fragment.getView().findViewById(R.id.tabLayout2);
                    TabLayout.Tab tab = tabLayout.getTabAt(0);
                    tab.select();
                }
            });

            if (person.isIgnoreFlag()) {
                linearLayout.setBackgroundColor(getResources().getColor(R.color.commonTyairo));
            }

            if (person.getKurotori().equals("")) {
                textNameTrip.setText(person.getNameEllipsis() + person.getShikakuRealShirotori() + "(ID:" + person.getMonaId() +")");
            } else {
                textNameTrip.setText(person.getNameEllipsis() + person.getShikakuRealKurotori() + "(ID:" + person.getMonaId() +")");
            }

            return convertView;
        }
    }

    private void preferenceIgnoreDelete(Person person) {
        String ignores = TestShared.getInstance(getContext()).getIgnores();
        if (ignores.contains("," + person.getShirotori())) {
            ignores = ignores.replace("," + person.getShirotori(), "");
            TestShared.getInstance(getContext()).setIgnores(ignores);
            Log.d(TAG, person.getShirotori());
        } else {
            Log.d(TAG, "failed");
        }
    }

    private void preferenceIgnoreSet(Person person) {
        String ignores = TestShared.getInstance(getContext()).getIgnores();
        ignores = ignores + "," + person.getShirotori();
        TestShared.getInstance(getContext()).setIgnores(ignores);
    }

    class MyOnClickListener implements View.OnClickListener {

        protected Person person;

        public MyOnClickListener(Person person) {
            this.person = person;
        }

        @Override
        public void onClick(View v) {

        }
    }
}
