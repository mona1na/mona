package com.devmona.settingfragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devmona.Base2Activity;
import com.devmona.MeMonaId;
import com.devmona.Person;
import com.devmona.PersonLayoutDrawable;
import com.devmona.R;
import com.devmona.ReadMainThread;
import com.devmona.Tate2Fragment;
import com.devmona.TestConnect;
import com.devmona.loadercallbacks.LoaderCallbacksSendStat;

import java.util.List;

/**
 * Created by sake on 2018/04/28.
 */

public class StateDialogFragment extends Fragment {

    private ReadMainThread readMainThread;
    private Base2Activity base2Activity;
    private Tate2Fragment tate2Fragment = null;


    public StateDialogFragment() {
        TestConnect testConnect = TestConnect.getInstance(getContext());
        readMainThread = testConnect.readMainThread;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_state_dialog, container, false);
        base2Activity =(Base2Activity) getActivity();
        TextView textTsujyo = rootView.findViewById(R.id.state_tsujyo);
        textTsujyo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textTaiseki = rootView.findViewById(R.id.state_taisekityu);
        textTaiseki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textTorikomityu = rootView.findViewById(R.id.state_torikomityu);
        textTorikomityu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textMatari = rootView.findViewById(R.id.state_matari);
        textMatari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textSatsubatsu = rootView.findViewById(R.id.state_satsubatsu);
        textSatsubatsu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textMatsuri = rootView.findViewById(R.id.state_matsuri);
        textMatsuri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textWashoi = rootView.findViewById(R.id.state_wasyoi);
        textWashoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textManse = rootView.findViewById(R.id.state_manse);
        textManse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textHalahala = rootView.findViewById(R.id.state_halahala);
        textHalahala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textHaraheta = rootView.findViewById(R.id.state_haraheta);
        textHaraheta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textHaraipai = rootView.findViewById(R.id.state_haraipai);
        textHaraipai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textUma = rootView.findViewById(R.id.state_uma);
        textUma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textMazu = rootView.findViewById(R.id.state_mazu);
        textMazu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textAtsu = rootView.findViewById(R.id.state_atsu);
        textAtsu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textSamu = rootView.findViewById(R.id.state_samu);
        textSamu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textNemu = rootView.findViewById(R.id.state_nemu);
        textNemu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });
        TextView textMoudamepo = rootView.findViewById(R.id.state_moudamepo);
        textMoudamepo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });

        TextView textMonamoba = rootView.findViewById(R.id.state_monamoba);
        textMonamoba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stat = ((TextView) v).getText().toString();
                SetState(stat);
            }
        });

        return rootView;
    }

    private void SetState(String stat) {
        Person me = MeMonaId.getMe();
        PersonLayoutDrawable personLayoutDrawable = readMainThread.hashLayoutByte.get(me.getUniqueId());
        TextView textView = (TextView) personLayoutDrawable.getRelativeLayout2().findViewById(R.id.char_stat);
        textView.setText(stat);
        Bundle bundle = new Bundle();
        bundle.putString("stat", stat);
        base2Activity.getSupportLoaderManager().initLoader(9, bundle, new LoaderCallbacksSendStat(base2Activity));
    }
}
