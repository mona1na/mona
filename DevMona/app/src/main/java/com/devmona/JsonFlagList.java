package com.devmona;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sake on 2017/09/15.
 */

public class JsonFlagList {
    JSONObject jsonObjects;
    ArrayList<Integer> integers;

    public JsonFlagList(JSONObject jsonObjects, ArrayList<Integer> integers) {
        this.jsonObjects = jsonObjects;
        this.integers = integers;
    }
}
