package com.devmona;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.devs.vectorchildfinder.VectorChildFinder;
import com.devs.vectorchildfinder.VectorDrawableCompat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;


///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
//// * {@link AAFragment1.OnFragmentInteractionListener} interface
// * to handle interaction events.
// * Use the {@link AAFragment1#newInstance} factory method to
// * create an instance of this fragment.
// */
@SuppressWarnings("FieldCanBeLocal")
public class AAFragment1 extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = "AAFragment1";
    private int mParam1;
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;
    private ImageView imageView6;
    private ImageView imageView7;
    private ImageView imageView8;
    private ImageView imageView9;
    private Person tempPerson;

//    private OnFragmentInteractionListener mListener;

    public AAFragment1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param tempPerson
     * @return A new instance of fragment AAFragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static AAFragment1 newInstance(int param1, Person tempPerson) {
        AAFragment1 fragment = new AAFragment1();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putSerializable("tempPerson",tempPerson);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aafragment1, container, false);
//        TestShared
        TestShared testShared = TestShared.getInstance(getContext());
        LinkedHashMap<String,Integer> page = testShared.getPage(mParam1);
//        final List<IdAanameResno> pageIdList = sentakuDB.selectPageAAResNo(mParam1);

//        SizeTate2 sizeTate2 = sentakuDB.getSizeTate2();
        int width = Calc.width;
        int height = Calc.height;

        int widthWaru4 = width / 4;
        int height4 = height / 4;
//         = getActivity().getSupportFragmentManager().getFragments();

//        List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//        for (Fragment fragment : fragments) {
//            if (fragment instanceof CharaSentakuFragment) {
//                CharaSentakuFragment charaSentakuFragment = (CharaSentakuFragment) fragment;
//                this.tempPerson = charaSentakuFragment.tempPerson;
//            }
//        }
        Person tempPerson =(Person) getArguments().getSerializable("tempPerson");
        this.tempPerson = tempPerson;

//        this.tempPerson = ((SentakuActivity) getActivity()).getMe();
//        ProgressBar progressBar1 = view.findViewById(R.id.aa_progress_1);
//        ProgressBar progressBar2 = view.findViewById(R.id.aa_progress_2);
//        ProgressBar progressBar3 = view.findViewById(R.id.aa_progress_3);
//        ProgressBar progressBar4 = view.findViewById(R.id.aa_progress_4);
//        ProgressBar progressBar5 = view.findViewById(R.id.aa_progress_5);
//        ProgressBar progressBar6 = view.findViewById(R.id.aa_progress_6);
//        ProgressBar progressBar7 = view.findViewById(R.id.aa_progress_7);
//        ProgressBar progressBar8 = view.findViewById(R.id.aa_progress_8);
//        ProgressBar progressBar9 = view.findViewById(R.id.aa_progress_9);

        imageView1 = view.findViewById(R.id.aa_img_1);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView2 = view.findViewById(R.id.aa_img_2);
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView3 = view.findViewById(R.id.aa_img_3);
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView4 = view.findViewById(R.id.aa_img_4);
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView5 = view.findViewById(R.id.aa_img_5);
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView6 = view.findViewById(R.id.aa_img_6);
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView7 = view.findViewById(R.id.aa_img_7);
        imageView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView8 = view.findViewById(R.id.aa_img_8);
        imageView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });
        imageView9 = view.findViewById(R.id.aa_img_9);
        imageView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = v.getTag().toString();
//                SentakuActivity sentakuActivity = (SentakuActivity) getActivity();
//                sentakuActivity.resultAA(name);
//                List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
//                DialogFragment fragment = (DialogFragment) fragments.get(0);
//                fragment.dismiss();
                aaSet(name);
            }
        });

        ViewGroup.LayoutParams layoutParams1 = imageView1.getLayoutParams();
        layoutParams1.width = widthWaru4;
        layoutParams1.height = height4;
        imageView1.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams2 = imageView2.getLayoutParams();
        layoutParams2.width = widthWaru4;
        layoutParams2.height = height4;
        imageView2.setLayoutParams(layoutParams2);
        ViewGroup.LayoutParams layoutParams3 = imageView3.getLayoutParams();
        layoutParams3.width = widthWaru4;
        layoutParams3.height = height4;
        imageView3.setLayoutParams(layoutParams3);
        ViewGroup.LayoutParams layoutParams4 = imageView4.getLayoutParams();
        layoutParams4.width = widthWaru4;
        layoutParams4.height = height4;
        imageView4.setLayoutParams(layoutParams4);
        ViewGroup.LayoutParams layoutParams5 = imageView5.getLayoutParams();
        layoutParams5.width = widthWaru4;
        layoutParams5.height = height4;
        imageView5.setLayoutParams(layoutParams5);
        ViewGroup.LayoutParams layoutParams6 = imageView6.getLayoutParams();
        layoutParams6.width = widthWaru4;
        layoutParams6.height = height4;
        imageView6.setLayoutParams(layoutParams6);
        ViewGroup.LayoutParams layoutParams7 = imageView7.getLayoutParams();
        layoutParams7.width = widthWaru4;
        layoutParams7.height = height4;
        imageView7.setLayoutParams(layoutParams7);
        ViewGroup.LayoutParams layoutParams8 = imageView8.getLayoutParams();
        layoutParams8.width = widthWaru4;
        layoutParams8.height = height4;
        imageView8.setLayoutParams(layoutParams8);
        ViewGroup.LayoutParams layoutParams9 = imageView9.getLayoutParams();
        layoutParams9.width = widthWaru4;
        layoutParams9.height = height4;
        imageView8.setLayoutParams(layoutParams9);

        ArrayList<ImageView> imageViews = new ArrayList<>();
        imageViews.add(imageView1);
        imageViews.add(imageView2);
        imageViews.add(imageView3);
        imageViews.add(imageView4);
        imageViews.add(imageView5);
        imageViews.add(imageView6);
        imageViews.add(imageView7);
        imageViews.add(imageView8);
        imageViews.add(imageView9);

        Iterator iterator = page.keySet().iterator();
        int num =0;
        Log.d(TAG, "iterator before");
        while(iterator.hasNext()) {
            String aaName =(String) iterator.next();
            int resNo = page.get(aaName);
            Log.d(TAG, "aaName:" + aaName);
            Log.d(TAG, "key:" + resNo);
            ImageView imageView = imageViews.get(num);
            readAndShow2(resNo,aaName,imageView);
            num++;
        }

//        if (pageIdList.get(0) != null) {
//            readAndShow(0, pageIdList, tempPerson, imageView1, progressBar1);
//        }
//        if (pageIdList.get(1) != null) {
//            readAndShow(1, pageIdList, tempPerson, imageView2, progressBar2);
////            imageView2.setImageDrawable(pageIdList.get(1).getDrawable());
////            imageView2.setTag(pageIdList.get(1).getName());
////            vectorChildFinder = new VectorChildFinder(imageView2.getContext(), pageIdList.get(1).getResNo(), imageView2);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
//        }
//        if (pageIdList.get(2) != null) {
//            readAndShow(2, pageIdList, tempPerson, imageView3, progressBar3);
////            imageView3.setImageDrawable(pageIdList.get(2).getDrawable());
////            vectorChildFinder = new VectorChildFinder(imageView3.getContext(), pageIdList.get(2).getResNo(), imageView3);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
////            imageView3.setTag(pageIdList.get(2).getName());
//        }
//        if (pageIdList.get(3) != null) {
//            readAndShow(3, pageIdList, tempPerson, imageView4, progressBar4);
////            imageView4.setImageDrawable(pageIdList.get(3).getDrawable());
////            vectorChildFinder = new VectorChildFinder(imageView4.getContext(), pageIdList.get(3).getResNo(), imageView4);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
////            imageView4.setTag(pageIdList.get(3).getName());
//        }
//        if (pageIdList.get(4) != null) {
//            readAndShow(4, pageIdList, tempPerson, imageView5, progressBar5);
////            imageView5.setImageDrawable(pageIdList.get(4).getDrawable());
////            vectorChildFinder = new VectorChildFinder(imageView5.getContext(), pageIdList.get(4).getResNo(), imageView5);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
////            imageView5.setTag(pageIdList.get(4).getName());
//        }
//        if (pageIdList.get(5) != null) {
//            readAndShow(5, pageIdList, tempPerson, imageView6, progressBar6);
////            imageView6.setImageDrawable(pageIdList.get(5).getDrawable());
////            vectorChildFinder = new VectorChildFinder(imageView6.getContext(), pageIdList.get(5).getResNo(), imageView6);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
////            imageView6.setTag(pageIdList.get(5).getName());
//        }
//        if (pageIdList.get(6) != null) {
//            readAndShow(6, pageIdList, tempPerson, imageView7, progressBar7);
////            imageView7.setImageDrawable(pageIdList.get(6).getDrawable());
////            vectorChildFinder = new VectorChildFinder(imageView7.getContext(), pageIdList.get(6).getResNo(), imageView7);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
////            imageView7.setTag(pageIdList.get(6).getName());
//        }
//        if (pageIdList.get(7) != null) {
//            readAndShow(7, pageIdList, tempPerson, imageView8, progressBar8);
////            imageView8.setImageDrawable(pageIdList.get(7).getDrawable());
////            vectorChildFinder = new VectorChildFinder(imageView8.getContext(), pageIdList.get(7).getResNo(), imageView8);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
////            imageView8.setTag(pageIdList.get(7).getName());
//        }
//        if (pageIdList.get(8) != null) {
//            readAndShow(8, pageIdList, tempPerson, imageView9, progressBar9);
////            imageView9.setImageDrawable(pageIdList.get(8).getDrawable());
////            vectorChildFinder = new VectorChildFinder(imageView9.getContext(), pageIdList.get(8).getResNo(), imageView9);
////            path = vectorChildFinder.findPathByName("path1");
////            path.setFillColor(tempPerson.getColor255());
////            imageView9.setTag(pageIdList.get(8).getName());
//        }

        return view;
    }

    private void aaSet(String name) {
        CharaSentakuFragment charaFragment = null;
        List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof CharaSentakuFragment) {
                charaFragment =(CharaSentakuFragment) fragment;
                charaFragment.resultAA(name);
//                getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
        for (Fragment fragment : fragments) {
            if (fragment instanceof AADialogFragment2) {
                AADialogFragment2 aaDialogFragment2 =(AADialogFragment2) fragment;
//                charaFragment.resultAA(name);
                getActivity().getSupportFragmentManager().beginTransaction().remove(aaDialogFragment2).commit();
            }
        }

    }

//    private void readAndShow(final int num, final List<IdAanameResno> pageIdList, final Person tempPerson, final ImageView imageView, final ProgressBar progressBar) {
//        imageView.setVisibility(View.INVISIBLE);
//        progressBar.setVisibility(View.VISIBLE);
//        ExecutorService executor = Executors.newCachedThreadPool();
//        executor.submit(new Runnable() {
//
//            @Override
//            public void run() {
//                final Drawable d = getDrawable(pageIdList.get(num).getResNo());
//                onLoadComp(d);
////                Handler handler = new Handler();
////                handler.post(new Runnable() {
////                    @Override
////                    public void run() {
////                        onLoadComplete(d);
////                    }
////
////                    private void onLoadComplete(final Drawable d) {
////                        getActivity().runOnUiThread(new Runnable() {
////                            @Override
////                            public void run() {
////                                imageView.setImageDrawable(d);
////                                imageView.setTag(pageIdList.get(num).getName());
////                                VectorChildFinder vectorChildFinder = new VectorChildFinder(imageView.getContext(), pageIdList.get(num).getResNo(), imageView);
////                                VectorDrawableCompat.VFullPath path = vectorChildFinder.findPathByName("path1");
////                                path.setFillColor(tempPerson.getColor255());
////                                imageView1.setTag(pageIdList.get(num).getName());
////                                imageView.setVisibility(View.VISIBLE);
////                                progressBar.setVisibility(View.INVISIBLE);
////                            }
////                        });
////                    }
////                });
//            }
//
//            private void onLoadComp(final Drawable d) {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        imageView.setImageDrawable(d);
//                        imageView.setTag(pageIdList.get(num).getName());
//                        VectorChildFinder vectorChildFinder = new VectorChildFinder(imageView.getContext(), pageIdList.get(num).getResNo(), imageView);
//                        VectorDrawableCompat.VFullPath path = vectorChildFinder.findPathByName("path1");
//                        path.setFillColor(tempPerson.getColor255());
//                        imageView1.setTag(pageIdList.get(num).getName());
//                        imageView.setVisibility(View.VISIBLE);
//                        progressBar.setVisibility(View.INVISIBLE);
//                    }
//                });
//
//            }
//
//            private Drawable getDrawable(int resNo) {
//                return ActivityCompat.getDrawable(getContext(), resNo);
//            }
//        });
//
//    }

    class ImageThread extends Thread {
        private Person me;
        private String aaName;
        private int resNo;
        private Handler handler;
        private ImageView imageView;

        ImageThread(Handler handler, ImageView imageView, int resNo, String aaName, Person me) {
            this.handler = handler;
            this.imageView = imageView;
            this.resNo = resNo;
            this.aaName = aaName;
            this.me =me;
        }
        @Override
        public void run() {
            super.run();
            Drawable d = getDrawable(resNo);
            CompleteThread completeThread = new CompleteThread(imageView,d,resNo,aaName,me);
            handler.post(completeThread);
        }
        private Drawable getDrawable(int resNo) {
            return ActivityCompat.getDrawable(getContext(), resNo);
        }
    }

    class CompleteThread extends Thread {
        private Person me;
        private String aaName;
        private int resNo;
        private Drawable d;
        private ImageView imageView;

        CompleteThread(ImageView imageView, Drawable d, int resNo, String aaName, Person me) {
            this.imageView = imageView;
            this.d = d;
            this.resNo =resNo;
            this.aaName = aaName;
            this.me = me;
        }

        @Override
        public void run() {
            super.run();
            imageView.setImageDrawable(d);
            imageView.setTag(aaName);
            VectorChildFinder vectorChildFinder = new VectorChildFinder(imageView.getContext(), resNo, imageView);
            VectorDrawableCompat.VFullPath path = vectorChildFinder.findPathByName("path1");
            path.setFillColor(me.getColor255());
        }
    }

    private void readAndShow2(int resNo,String aaName, ImageView imageView) {
//        imageView.setVisibility(View.INVISIBLE);
        final Handler handler = new Handler();
        ImageThread imageThread = new ImageThread(handler, imageView,resNo,aaName, tempPerson);
        imageThread.start();


//        ExecutorService executor = Executors.newCachedThreadPool();
//        executor.submit(new Runnable() {
//
//            @Override
//            public void run() {
//                final Drawable d = getDrawable(pageIdList.get(num).getResNo());
//                onLoadComp(d);
////                Handler handler = new Handler();
////                handler.post(new Runnable() {
////                    @Override
////                    public void run() {
////                        onLoadComplete(d);
////                    }
////
////                    private void onLoadComplete(final Drawable d) {
////                        getActivity().runOnUiThread(new Runnable() {
////                            @Override
////                            public void run() {
////                                imageView.setImageDrawable(d);
////                                imageView.setTag(pageIdList.get(num).getName());
////                                VectorChildFinder vectorChildFinder = new VectorChildFinder(imageView.getContext(), pageIdList.get(num).getResNo(), imageView);
////                                VectorDrawableCompat.VFullPath path = vectorChildFinder.findPathByName("path1");
////                                path.setFillColor(tempPerson.getColor255());
////                                imageView1.setTag(pageIdList.get(num).getName());
////                                imageView.setVisibility(View.VISIBLE);
////                                progressBar.setVisibility(View.INVISIBLE);
////                            }
////                        });
////                    }
////                });
//            }
//
//            private void onLoadComp(final Drawable d) {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        imageView.setImageDrawable(d);
//                        imageView.setTag(pageIdList.get(num).getName());
//                        VectorChildFinder vectorChildFinder = new VectorChildFinder(imageView.getContext(), pageIdList.get(num).getResNo(), imageView);
//                        VectorDrawableCompat.VFullPath path = vectorChildFinder.findPathByName("path1");
//                        path.setFillColor(tempPerson.getColor255());
//                        imageView1.setTag(pageIdList.get(num).getName());
//                        imageView.setVisibility(View.VISIBLE);
//                        progressBar.setVisibility(View.INVISIBLE);
//                    }
//                });
//
//            }
//
//            private Drawable getDrawable(int resNo) {
//                return ActivityCompat.getDrawable(getContext(), resNo);
//            }
//        });

    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
