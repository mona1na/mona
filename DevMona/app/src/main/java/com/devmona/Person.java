package com.devmona;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sake on 2017/06/11.
 */

public class Person implements Serializable, Parcelable {

    private Drawable drawable;
    private String shikakuRealShirotori;
    private boolean me = false;
    private FrameLayout.LayoutParams layoutParams;
    private LinearLayout linearLayout;
    //    private Bitmap aaBitmapForCanvas;
//    private Bitmap aaBitmapForLine;
//    private String comment;
    private int monaId = -1;
    private byte[] byteCanvas;
    private byte[] byteLine;
    private int aaMipMapId;
    private int layoutId;
    private int color;
    private String name;
    private String nameEllipsis;
    private int id;
    private String shirotori6;
    private String kurotori;
    private String shirotori;
    private String jyotai;

    private int startId = -1;
    private String aaname;
    private boolean hani = false;

    //private String message;

    private String date;
    private int cX;
    private int cY;
    private int cX2;
    private int cY2;
    private int cX3;
    private int cY3;
    private int cXR;
    private int cYR;
    private int x;
    private int y;
    private int red;
    private int green;
    private int blue;
    private int color255;
    private int colorInverte255;
    private int scl;
    private String stat;
    private boolean login;
    private int aaresno;
    private int loginId;
    private int roomId = -1;
    private int startseq = -1;
    private String shikakuRealKurotori;
    private boolean ignoreFlag;
    private String trip;
    private String xml;
    private int uniqueId;
    private boolean exitFlag;
    private ArrayList<TestCom> testComs;
    private String comment;
//    private Tate2Fragment.MyAdapter adapter;
    private Context context;
    private int colorHukidashi;
    private String log;
    private String comment2;


    public Person() {
        this.color255 = -1;
        this.red = -1;
        this.green = -1;
        this.blue = -1;
        this.aaresno = -1;
        this.name = "";
        this.kurotori = "";
        this.shirotori = "";
        this.jyotai = "";
        this.aaname = "";
        this.date = "";
        this.stat = "";
        this.shirotori6 = "";
        this.shikakuRealShirotori = "";
        this.login = false;
        this.shikakuRealKurotori = "";
        this.ignoreFlag = false;
        this.nameEllipsis = "";
        this.trip = null;
        this.exitFlag = false;
        this.testComs = new ArrayList<>();
        this.comment = "";
        this.cX3 = 0;
        this.cY3 = 0;
        this.colorHukidashi = -1;
        this.log = "";
        this.cXR = 0;
        this.cYR = 0;
        this.comment2 = "";
    }

//    public void setAdapter(Tate2Fragment.MyAdapter adapter) {
//        this.adapter = adapter;
//    }
//
//    public Tate2Fragment.MyAdapter getAdapter() {
//        return adapter;
//    }


    protected Person(Parcel in) {
        shikakuRealShirotori = in.readString();
        me = in.readByte() != 0;
        monaId = in.readInt();
        byteCanvas = in.createByteArray();
        byteLine = in.createByteArray();
        aaMipMapId = in.readInt();
        layoutId = in.readInt();
        color = in.readInt();
        name = in.readString();
        nameEllipsis = in.readString();
        id = in.readInt();
        shirotori6 = in.readString();
        kurotori = in.readString();
        shirotori = in.readString();
        jyotai = in.readString();
        startId = in.readInt();
        aaname = in.readString();
        hani = in.readByte() != 0;
        date = in.readString();
        cX = in.readInt();
        cY = in.readInt();
        cX2 = in.readInt();
        cY2 = in.readInt();
        cX3 = in.readInt();
        cY3 = in.readInt();
        cXR = in.readInt();
        cYR = in.readInt();
        x = in.readInt();
        y = in.readInt();
        red = in.readInt();
        green = in.readInt();
        blue = in.readInt();
        color255 = in.readInt();
        colorInverte255 = in.readInt();
        scl = in.readInt();
        stat = in.readString();
        login = in.readByte() != 0;
        aaresno = in.readInt();
        loginId = in.readInt();
        roomId = in.readInt();
        startseq = in.readInt();
        shikakuRealKurotori = in.readString();
        ignoreFlag = in.readByte() != 0;
        trip = in.readString();
        xml = in.readString();
        uniqueId = in.readInt();
        exitFlag = in.readByte() != 0;
        comment = in.readString();
        colorHukidashi = in.readInt();
        log = in.readString();
        message = in.createStringArrayList();
        comment2 = in.readString();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    public void setColorInverte255(int colorInverte255) {
        this.colorInverte255 = colorInverte255;
    }

    public int getColorInverte255() {
        return colorInverte255;
    }

    public int getColorHukidashi() {
        return colorHukidashi;
    }

    public void setColorHukidashi(int colorHukidashi) {
        this.colorHukidashi = colorHukidashi;
    }

    /**
     * レギュラーのcanvasのpixelのX
     * @param cXR
     */
    public void setcXR(int cXR) {
        this.cXR = cXR;
    }

    /**
     * レギュラーのcanvasのpixelのY
     * @param cYR
     */
    public void setcYR(int cYR) {
        this.cYR = cYR;
    }

    /**
     * レギュラーのcanvasのpixelを返す
     * @return
     */
    public int getcXR() {
        return cXR;
    }

    /**
     * レギュラーのcanvasのpixelを返す
     * @return
     */
    public int getcYR() {
        return cYR;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<TestCom> getTestComs() {
        return testComs;
    }

    public void setTestComs(TestCom testCom) {
        this.testComs.add(testCom);
//        this.testComs = testComs;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public void setcX3(int cX3) {
        this.cX3 = cX3;
    }

    public void setcY3(int cY3) {
        this.cY3 = cY3;
    }

    public int getcX3() {
        return cX3;
    }

    public int getcY3() {
        return cY3;
    }

    public boolean isExitFlag() {
        return exitFlag;
    }

    public void setExitFlag(boolean exitFlag) {
        this.exitFlag = exitFlag;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getXml() {
        return xml;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    public String getTrip() {
        return trip;
    }

    public void setNameEllipsis(String nameEllipsis) {
        this.nameEllipsis = nameEllipsis;
    }

    public String getNameEllipsis() {
        return nameEllipsis;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setIgnoreFlag(boolean ignoreFlag) {
        this.ignoreFlag = ignoreFlag;
    }

    public boolean isIgnoreFlag() {
        return ignoreFlag;
    }

    public void setcX2(int cX2) {
        this.cX2 = cX2;
    }

    public int getcY2() {
        return cY2;
    }

    public int getcX2() {
        return cX2;
    }

    public void setcY2(int cY2) {
        this.cY2 = cY2;
    }

    public void setStartId(int startId) {
        this.startId = startId;
    }

    public int getStartId() {
        return startId;
    }

    public int getStartseq() {
        return startseq;
    }

    public void setStartseq(int startseq) {
        this.startseq = startseq;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getMonaId() {
        return monaId;
    }

    public void setMonaId(int monaId) {
        this.monaId = monaId;
    }

//    public void setComment(String comment) {
//        this.comment = comment;
//    }
//
//    public String getComment() {
//        return comment;
//    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    public int getLoginId() {
        return loginId;
    }

    public boolean isMe() {
        return me;
    }

    public void setByteLine(byte[] byteLine) {
        this.byteLine = byteLine;
    }

    public void setByteCanvas(byte[] byteCanvas) {
        this.byteCanvas = byteCanvas;
    }

    public byte[] getByteCanvas() {
        return byteCanvas;
    }

    public byte[] getByteLine() {
        return byteLine;
    }

    public void setAaresno(int aaresno) {
        this.aaresno = aaresno;
    }

    public int getAaresno() {
        return aaresno;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public void setShikakuRealShirotori(String shikakuRealShirotori) {
        this.shikakuRealShirotori = shikakuRealShirotori;
    }

    public String getShikakuRealShirotori() {
        return shikakuRealShirotori;
    }

    public void setMe(boolean me) {
        this.me = me;
    }

    public boolean getMe() {
        return this.me;
    }

    public void setcX(int cX) {
        this.cX = cX;
    }

    public void setcY(int cY) {
        this.cY = cY;
    }

    public int getcX() {
        return cX;
    }

    public int getcY() {
        return cY;
    }

    public void setLayoutParams(FrameLayout.LayoutParams layoutParams) {
        this.layoutParams = layoutParams;
    }

    public FrameLayout.LayoutParams getLayoutParams() {
        return layoutParams;
    }

    public void setLinearLayout(LinearLayout linearLayout) {
        this.linearLayout = linearLayout;
    }

    public LinearLayout getLinearLayout() {
        return linearLayout;
    }

    public void test() {


        if (this.shirotori == null) {
            Log.d("me", getName());
            return;
        }
        StringBuilder sb1 = new StringBuilder(this.shirotori);
        setShirotori6(sb1.delete(0, 4).toString());
        this.shikakuRealShirotori = "◇" + shirotori6;
    }

//    public Bitmap getAaBitmapForLine() {
//        return aaBitmapForLine;
//    }
////
//    public void setAaBitmapForLine(Bitmap aaBitmapForLine) {
//        this.aaBitmapForLine = aaBitmapForLine;
//    }

    public boolean isHani() {
        return hani;
    }

    public void setHani(boolean hani) {
        this.hani = hani;
    }

    public String getShirotori6() {
        return shirotori6;
    }

    public void setShirotori6(String shirotori6) {
        this.shirotori6 = shirotori6;
    }

//    public Bitmap getAaBitmapForCanvas() {
//        return aaBitmapForCanvas;
//    }

//    public boolean isSetBitmapCanvas() {
//        if (aaBitmapForCanvas == null) {
//            return false;
//        } else {
//            return true;
//        }
//    }
//    public boolean isSetBitmapLine() {
//        if (aaBitmapForLine == null) {
//            return false;
//        } else {
//            return true;
//        }
//    }

//    public void setAaBitmapForCanvas(Bitmap aaBitmapForCanvas) {
//        this.aaBitmapForCanvas = aaBitmapForCanvas;
//    }

    public ArrayList<String> message = new ArrayList<String>();

    public int getColor255() {
        return color255;
    }

    public void setColor255(int color255) {
        this.color255 = color255;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public void setAaname(String aaname) {
        this.aaname = aaname;
    }

    public String getAaname() {
        return aaname;
    }

//    public int getAaMipMapId() {
//        return aaMipMapId;
//    }

    public int getBlue() {
        return blue;
    }

    public int getColor() {
        return color;
    }

    public int getGreen() {
        return green;
    }

    public int getId() {
        return id;
    }

    public int getRed() {
        return red;
    }

    public int getScl() {
        return scl;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getDate() {
        return date;
    }

    public String getJyotai() {
        return jyotai;
    }

    public String getKurotori() {
        return kurotori;
    }

    public String getName() {
        return name;
    }

    public String getShirotori() {
        return shirotori;
    }

    public String getStat() {
        return stat;
    }

//    public void setAaMipMapId(int aaMipMapId) {
//        this.aaMipMapId = aaMipMapId;
//    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJyotai(String jyotai) {
        this.jyotai = jyotai;
    }

    public void setKurotori(String kurotori) {
        this.kurotori = kurotori;
        this.shikakuRealKurotori = "◆" + kurotori;
    }

    public void setShikakuRealKurotori(String shikakuRealKurotori) {
        this.shikakuRealKurotori = shikakuRealKurotori;
    }

    public String getShikakuRealKurotori() {
        return shikakuRealKurotori;
    }

    public void setName(String name) {
        this.name = name;
        ellipsis();
    }

    private void ellipsis() {
        int BASE = 6;
        int len = name.length();
        if (len <= BASE) {
            this.nameEllipsis = name;
        } else if (len == BASE + 1) {
            this.nameEllipsis = name;
        } else {
            this.nameEllipsis = name.substring(0, BASE) + "...";
        }
    }

    public void setRed(int red) {
        this.red = red;
    }

    public void setScl(int scl) {
        this.scl = scl;
    }

    public void setShirotori(String shirotori) {
        this.shirotori = shirotori;
        test();
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public String getComment2() {
        return comment2;
    }

    public TestCom test2() {
        for (TestCom testCom : this.testComs) {
            if (!testCom.isTate2Disp()) {
                return testCom;
            }
        }
        return null;
    }

    public TestCom test3() {
        for (TestCom testCom : this.testComs) {
            if (!testCom.isTate3Disp()) {
                return testCom;
            }
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shikakuRealShirotori);
        dest.writeByte((byte) (me ? 1 : 0));
        dest.writeInt(monaId);
        dest.writeByteArray(byteCanvas);
        dest.writeByteArray(byteLine);
        dest.writeInt(aaMipMapId);
        dest.writeInt(layoutId);
        dest.writeInt(color);
        dest.writeString(name);
        dest.writeString(nameEllipsis);
        dest.writeInt(id);
        dest.writeString(shirotori6);
        dest.writeString(kurotori);
        dest.writeString(shirotori);
        dest.writeString(jyotai);
        dest.writeInt(startId);
        dest.writeString(aaname);
        dest.writeByte((byte) (hani ? 1 : 0));
        dest.writeString(date);
        dest.writeInt(cX);
        dest.writeInt(cY);
        dest.writeInt(cX2);
        dest.writeInt(cY2);
        dest.writeInt(cX3);
        dest.writeInt(cY3);
        dest.writeInt(cXR);
        dest.writeInt(cYR);
        dest.writeInt(x);
        dest.writeInt(y);
        dest.writeInt(red);
        dest.writeInt(green);
        dest.writeInt(blue);
        dest.writeInt(color255);
        dest.writeInt(colorInverte255);
        dest.writeInt(scl);
        dest.writeString(stat);
        dest.writeByte((byte) (login ? 1 : 0));
        dest.writeInt(aaresno);
        dest.writeInt(loginId);
        dest.writeInt(roomId);
        dest.writeInt(startseq);
        dest.writeString(shikakuRealKurotori);
        dest.writeByte((byte) (ignoreFlag ? 1 : 0));
        dest.writeString(trip);
        dest.writeString(xml);
        dest.writeInt(uniqueId);
        dest.writeByte((byte) (exitFlag ? 1 : 0));
        dest.writeString(comment);
        dest.writeInt(colorHukidashi);
        dest.writeString(log);
        dest.writeStringList(message);
        dest.writeString(comment2);
    }
}
