package com.devmona;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devmona.loadercallbacks.LoaderCallbacksDBRoom5;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import androidx.core.app.ActivityCompat;

public class ReadMainThread extends ReadAbstract {
    private final int mAasizePixel;
    private int mCharWholePixel;
    //    private BufferedInputStream in;
    public LinkedHashMap<Integer, PersonLayoutDrawable> hashLayoutByte = new LinkedHashMap<>();
    public LinkedHashMap<Integer, Person> people = new LinkedHashMap<>();
    public int uniqueId;
    public static final String TAG = "ReadMainThread";
//    private int k;
//    static final int LEN = 78000;
//    byte[] keyinbuf = new byte[LEN];

//    ArrayList<ArrayList<String>> arrayLists = new ArrayList<ArrayList<String>>();
//    private String strGrobal = "";

    protected enum Status {
        onResume,
        onPause,
        onDestroy,
        onActivityCreated,
    }

    public ReadMainThread(Context context, boolean reConnectting) {
        super(context, reConnectting);
        uniqueId = 0;
        mCharWholePixel = TestShared.getInstance(context).getCharWholePixel();
        mAasizePixel = TestShared.getInstance(context).getAasizePixel();
        this.context = context;
    }

    class MyRunnable extends Thread {
        private Tate3Fragment tate3Fragment;
        private RoomsFragment roomsFragment;
        private Base2Activity nameActivity;
        private Tate2Fragment tate2Fragment;
        private Main4Fragment main4Fragment;
        int bunki;
        private Room room;
        private ArrayList<TestIdXML> people;
        private LinkedHashMap<Integer, Room> roomsMap;
        private String log;

        public MyRunnable(int bunki, Main4Fragment main4Fragment) {
            this.bunki = bunki;
            this.main4Fragment = main4Fragment;

            this.nameActivity = (Base2Activity) main4Fragment.getActivity();
        }

        public MyRunnable(int bunki, Tate2Fragment tate2Fragment) {
            this.bunki = bunki;
            this.tate2Fragment = tate2Fragment;
        }

        public MyRunnable(int bunki, Tate3Fragment tate3Fragment) {
            this.bunki = bunki;
            this.tate3Fragment = tate3Fragment;
        }

        @Override
        public void run() {
            super.run();
            synchronized (this) {
//                Log.d("MyRunnble", log);
                if (bunki == 1) {
                    nameActivity.showRoomName(room);
                } else if (bunki == 2) {
                    if (tate2Fragment != null) {
                        tate2Fragment.doProcess(people);
                    } else {
                        Log.d("bunki 2", "");
                    }

                } else if (bunki == 3) {
                    if (tate3Fragment != null) {
                        tate3Fragment.doProcess(people);
                    } else {
                        Log.d("bunki 3", "");
                    }
                }
                this.notify();
            }
        }

        public void roomSet(Room room) {
            this.room = room;
        }

        public void peopleSet(ArrayList<TestIdXML> people) {
            this.people = people;
        }


        public void logSet(String log) {
            this.log = log;
        }
    }

    @Override
    public void run() {
        super.run();
        while (running) {
            ArrayList result;
            result = abstractRead1();
            if (result == null) {
                Log.d(TAG, "run result null");
                break;
            }

            if (result.size() != 0) {
                testMainRead2(context, result);
            }
        }
        Log.d(TAG, "finish run");
    }


    /**
     * MojaChat送信
     * メッセージの読み込み
     * ここで処理するXMLとして
     * ・+connect id=xxxxx (受信)
     * :TODO コネクションタイムアウトの処理
     * ①一発目受信するもの
     * ・<CONNECT id="11820" />
     * ②ルームリストの待機情報
     * ・<ROOM><USER red="100" name="名無しさん" id="12166" ihash="ToeEwoeab0" stat="通常" green="100" type="mona" blue="100" y="275" x="100" scl="100" /><USER ihash="gCuggdTwA4" name="Selcia" trip="YHydpHyFUM" id="11420" /></ROOM>
     * ③自分の情報
     * ・<UINFO name="名無しさん" id="11820" />
     * ④ルームリストの部屋の情報
     * ・<COUNT c="3" n="MONA8094"><ROOM c="0" n="1411" /><ROOM c="0" n="1410" /><ROOM c="0" n="75593" /><ROOM c="0" n="1415" /><ROOM c="0" n="1414" /><ROOM c="0" n="1412" /><ROOM c="0" n="1419" /><ROOM c="0" n="1417" /><ROOM c="0" n="1416" /><ROOM c="0" n="1821" /><ROOM c="0" n="1822" /><ROOM c="0" n="1820" /><ROOM c="0" n="1823" /><ROOM c="0" n="1824" /><ROOM c="0" n="1829" /><ROOM c="0" n="1827" /><ROOM c="0" n="1828" /><ROOM c="0" n="1420" /><ROOM c="0" n=" " /><ROOM c="0" n="1422" /></COUNT>
     * ⑤退出id
     * ・<EXIT id="111" />
     * ⑥部屋単体の入退室
     * ・<COUNT><ROOM c="7" n="99"/></COUNT>
     * ⑦部屋に入ったときの部屋番号と部屋ナンバー
     * <COUNT c="2" n="16" />\0
     * <p>
     * ・<SET x="101" scl="100" id="11881" y="320" />
     * ・<SET stat="あいうえお" id="627" />
     * <p>
     * ・<COM cmt="activity_characters" cnt="2" id="11719" />
     * ⑧例えば86の部屋に入室している際に、他の人が入室してきた際の送られてくるxml
     * ・<ENTER red="100" name="." id="11719" ihash="F/uM83OoUA" stat="......" green="100" type="hiyoko" blue="100" y="250" x="558" scl="100" />
     * <p>
     * //     * @param arrayList <p>
     * 受信結果のstringを処理するメソッド(loading)
     *
     * @param result <p>
     *               受信結果のstringを処理するメソッド(loading)
     * @param result
     */

    public synchronized void testMainRead2(final Context context, ArrayList result) {


        for (int i = 0; i < result.size(); i++) {
            String str = (String) result.get(i);
            Log.i(TAG + " receive", str);

            if (checkConnectId(str)) {
                MyThread myThread = TestConnect.getInstance(context).myThread;
                if (myThread != null) {
                    synchronized (myThread) {
                        myThread.notify();
                        reConnecting = false;
                        Log.d("killTest", "synchronized (myLoader)");
                    }
                }
                continue;
            }

            if (checkTimeout(str)) {
                /**
                 * 再接続する
                 */
                return;
            }

            XmlPullParser xpp = null;
            try {
                xpp = xmlPullParserGet(str);
            } catch (MyException e) {

                return;
            }
            int eventType = -2;
            try {
                eventType = xpp.getEventType();
            } catch (XmlPullParserException e) {
                return;
            }

            String elementName = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                elementName = xpp.getName();
                if (eventType == XmlPullParser.START_TAG) {
                    break;
                }
                try {
                    eventType = xpp.next();

                } catch (XmlPullParserException e) {
                    return;
                } catch (IOException e) {
                    return;
                }
            }

            if (elementName == null) {
                continue;
            }

            if (elementName.equals("CONNECT")) {
                String strId = xpp.getAttributeValue(null, "id");
                xmlConnect(xpp);
            } else if (elementName.equals("COUNT")) {
                //     * ・<COUNT c="2" n="16" />\0
                // MONA8094かそれ以外を取得する

                Log.d("xppTest", str);
                Main4Fragment main4Fragment = UtilInfo.getMain4Fragment(context);
                Log.d("killTestM", "context:" + context);
                Log.d("killTestM", "main4Fragment:" + main4Fragment);
                String strRoomNo = xpp.getAttributeValue(null, "n");
                // 個別の部屋(例えば29とか30)に入ったときに出力されるもの<COUNT c n>の形
                String strRoomCount = xpp.getAttributeValue(null, "c");

                Room room = new Room();
                int roomNo = Integer.parseInt(strRoomNo);
                int roomCount = Integer.parseInt(strRoomCount);
                // stub 101 todo
                String roomName;
                try {
                    roomName = mAppli.getRoomName(roomNo);
                } catch (NullPointerException e) {
                    roomName = "101";
                }
                room.setRoomName(roomName);
                room.setRoomNumber(roomNo);
//                        ReadThreadDB readThreadDB = new ReadThreadDB();
//                        final Room room = readThreadDB.getRoom(roomNo);
                room.setRoomCount(roomCount);

                MyRunnable myRunnable = new MyRunnable(1, main4Fragment);
                synchronized (myRunnable) {
                    Log.d("synchronized start", "synchronized start");
                    myRunnable.roomSet(room);
                    myRunnable.logSet("COUNT 入室時");
                    base2Activity.runOnUiThread(myRunnable);
                    try {
                        myRunnable.wait();
                        Log.d("wait finish", "wait finish");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                /**
                 * 人を取得する<ROOM><USER /><USER /><USER /></ROOM>
                 */
            } else if (elementName.equals("ROOM")) {

                /**
                 * important method
                 */
                final ArrayList<TestIdXML> people = memberSet2(context, xpp, eventType);

                if (people == null) {
                    Log.d("stop", "stop");
                    continue;
                }

                try {

                    Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
                    if (tate2Fragment == null) {
                        continue;
                    }

                    MyRunnable myRunnable = new MyRunnable(2, tate2Fragment);
                    synchronized (myRunnable) {
                        Log.d("synchronized start", "synchronized start");
                        myRunnable.peopleSet(people);
                        myRunnable.logSet("ROOM");
                        base2Activity.runOnUiThread(myRunnable);
                        try {
                            myRunnable.wait();
                            Log.d("wait finish", "wait finish");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }


                } catch (NullPointerException e) {
                    Log.d("not to be processed", "not to be processed");
                }

            } else if (elementName.equals(UtilInfo.ENTER)) {
                String strId = xpp.getAttributeValue(null, "id");
                if (strId != null) {
                    final int id = Integer.parseInt(strId);
                    /**
                     * 自分がENTERしたとき
                     */
                    if (MeMonaId.getMe().getMonaId() == id) {
                        /**
                         * important
                         */
                        final ArrayList<TestIdXML> testIdXMLS = memberSet2(context, xpp, eventType);

                        if (testIdXMLS == null) {
                            continue;
                        }
                        /**
                         * 帰ってきたpersonのリストをキューに追加
                         */


                        Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
                        final Tate3Fragment tate3Fragment = UtilInfo.getTate3Fragment(context);
//                        final Tate3Fragment tate3FragmentChild = UtilInfo.getTate3FragmentChild(context);

//                        base2Activity.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                tate3Fragment.doProcess(testIdXMLS);
//                                tate3FragmentChild.doProcess(testIdXMLS);
//                            }
//                        });

                        MyRunnable myRunnable3 = new MyRunnable(3, tate3Fragment);
                        try {
                            synchronized (myRunnable3) {
                                myRunnable3.peopleSet(testIdXMLS);
                                base2Activity.runOnUiThread(myRunnable3);
                                try {
                                    myRunnable3.wait();
                                    Log.d("wait finish", "wait finish");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (NullPointerException e) {

                        }


                        try {
                            MyRunnable myRunnable = new MyRunnable(2, tate2Fragment);
                            synchronized (myRunnable) {
                                Log.d("synchronized me ente", "synchronized me ente");
                                myRunnable.peopleSet(testIdXMLS);
                                myRunnable.logSet("ENTER 自分");
                                base2Activity.runOnUiThread(myRunnable);
                                try {
                                    myRunnable.wait();
                                    Log.d("wait finish", "wait finish");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }



                        } catch (NullPointerException e) {

                        }
//                        }
                        /**
                         * 他人がenterしたとき
                         */
                    } else {

                        /**
                         * important method
                         */
                        final ArrayList<TestIdXML> testIdXMLS = memberSet2(context, xpp, eventType);
                        if (people == null) {
                            Log.d("null", "null");
                            continue;
                        }

                        final Tate3Fragment tate3Fragment = UtilInfo.getTate3Fragment(context);
//                        final Tate3Fragment tate3FragmentChild = UtilInfo.getTate3FragmentChild(context);

                        MyRunnable myRunnable3 = new MyRunnable(3, tate3Fragment);
                        try {
                            synchronized (myRunnable3) {
                                myRunnable3.peopleSet(testIdXMLS);
                                base2Activity.runOnUiThread(myRunnable3);
                                try {
                                    myRunnable3.wait();
                                    Log.d("wait finish", "wait finish");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (NullPointerException e) {

                        }

                        try {
                            Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
                            MyRunnable myRunnable = new MyRunnable(2, tate2Fragment);
                            synchronized (myRunnable) {
                                Log.d("synchronized ente other", "synchronized ente other");
                                myRunnable.peopleSet(testIdXMLS);
                                myRunnable.logSet("ENTER 自分以外");
                                base2Activity.runOnUiThread(myRunnable);
                                try {
                                    myRunnable.wait();
                                    Log.d("wait finish", "wait finish");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
//                            final Tate3Fragment tate3Fragment = UtilInfo.getTate3Fragment(context);
//                            final Tate3Fragment tate3FragmentChild = UtilInfo.getTate3FragmentChild(context);
//                            base2Activity.runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    tate3Fragment.doProcess(testIdXMLS);
//                                    tate3FragmentChild.doProcess(testIdXMLS);
//                                }
//                            });
                        } catch (NullPointerException e) {

                        }

                    }
                }

            } else if (elementName.equals("EXIT")) {


                try {
                    String strId = xpp.getAttributeValue(null, "id");
                    int monaId = Integer.parseInt(strId);
                    /**
                     * important method
                     */
                    final ArrayList<TestIdXML> testIdXMLS = exit(context, xpp, eventType);
                    if (testIdXMLS == null) {
                        Log.d("exit people null", "exit people null");
                        continue;
                    } else if (testIdXMLS.size() == 0) {
                        Log.d("exit people size 0", "exit people size 0");
                        continue;
                    }

                    Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
                    if (tate2Fragment == null) {
                        continue;
                    }
                    final Tate3Fragment tate3Fragment = UtilInfo.getTate3Fragment(context);
                    MyRunnable myRunnable3 = new MyRunnable(3, tate3Fragment);
                    try {
                        synchronized (myRunnable3) {
                            myRunnable3.peopleSet(testIdXMLS);
                            base2Activity.runOnUiThread(myRunnable3);
                            try {
                                myRunnable3.wait();
                                Log.d("wait finish", "wait finish");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (NullPointerException e) {

                    }

                    MyRunnable myRunnable = new MyRunnable(2, tate2Fragment);
                    synchronized (myRunnable) {
                        Log.d("synchronized exit", "synchronized exit");
                        myRunnable.peopleSet(testIdXMLS);
                        myRunnable.logSet("EXIT");
                        base2Activity.runOnUiThread(myRunnable);
                        try {
                            myRunnable.wait();
                            Log.d("wait finish", "wait finish");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

//                    final Tate3Fragment tate3FragmentChild = UtilInfo.getTate3FragmentChild(context);
//                    base2Activity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            tate3Fragment.doProcess(testIdXMLS);
//                            tate3FragmentChild.doProcess(testIdXMLS);
//                        }
//                    });

                } catch (NullPointerException e) {
                    Log.d("EXIT", "NullPointerException");
                    continue;
                } catch (IndexOutOfBoundsException e) {
                    Log.d("EXIT", "IndexOutOfBoundsException");
                    continue;
                }
            } else if (elementName.equals("COM")) {


                final String strCmt = xpp.getAttributeValue(null, "cmt");
                String strMonaId = xpp.getAttributeValue(null, "id");
                final int monaId = Integer.parseInt(strMonaId);
                Person person = null;


                if (MeMonaId.getMe().getMonaId() == monaId) {
                    person = TestShared.getInstance(context).getMe();
                }

                for (int uniqueId : people.keySet()) {
                    Person temp = people.get(uniqueId);
                    if (temp.getMonaId() == monaId) {
                        person = temp;
                        break;
                    }
                }
                person.setComment2(strCmt);
                person.setTestComs(new TestCom(false, false, strCmt));
                TestIdXML testIdXML = new TestIdXML(person.getUniqueId(), UtilInfo.COM, monaId);
                final ArrayList<TestIdXML> testIdXMLS = new ArrayList<>();
                testIdXMLS.add(testIdXML);

                /**
                 * プッシュ通知
                 */
                if (MyApplication.base2sStatus == MyApplication.Status.onPaused) {
                    MyApplication myApplication = (MyApplication) context.getApplicationContext();
                    myApplication.notifyCOM2(person, strCmt);
                }

                /**
                 * insert comment table of database
                 * データベースに登録する
                 */
//                ReadThreadDB readThreadDB = new ReadThreadDB();
//                readThreadDB.insertComment(monaId, strCmt);
                final Tate3Fragment tate3Fragment = UtilInfo.getTate3Fragment(context);
//                final Tate3Fragment tate3FragmentChild = UtilInfo.getTate3FragmentChild(context);

                MyRunnable myRunnable3 = new MyRunnable(3, tate3Fragment);
                synchronized (myRunnable3) {
//                    tate3Fragment.doProcess(testIdXMLS);
                    myRunnable3.peopleSet(testIdXMLS);
                    myRunnable3.logSet("COM");
                    base2Activity.runOnUiThread(myRunnable3);
                    try {
                        myRunnable3.wait();
                        Log.d("wait finish", "wait finish");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                person = people.get(testIdXML.getUniqueId());
                TestCom testCom = person.test3();
                testCom.setTate3Disp(true);


                try {
                    Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
                    if (tate2Fragment == null) {
                        continue;
                    }
                    MyRunnable myRunnable = new MyRunnable(2, tate2Fragment);
                    synchronized (myRunnable) {
                        Log.d("synchronized com", "synchronized com");
                        myRunnable.peopleSet(testIdXMLS);
                        myRunnable.logSet("COM");
                        base2Activity.runOnUiThread(myRunnable);
                        try {
                            myRunnable.wait();
                            Log.d("wait finish", "wait finish");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (NullPointerException e) {

                }

                /**
                 * in tate2Fragment, show comment
                 */


                /**
                 * in tate3Fragment, show comment
                 */




//                myRunnable = new MyRunnable(3, tate3FragmentChild);
//                synchronized (myRunnable) {
////                    tate3Fragment.doProcess(testIdXMLS);
//                    myRunnable.peopleSet(testIdXMLS);
//                    myRunnable.logSet("COM");
//                    base2Activity.runOnUiThread(myRunnable);
//                    try {
//                        myRunnable.wait();
//                        Log.d("wait finish", "wait finish");
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }


//                base2Activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        tate3Fragment.doProcess(testIdXMLS);
//                        tate3FragmentChild.doProcess(testIdXMLS);
//                    }
//                });



            } else if (elementName.equals("SET")) {
                /**
                 *      * ・<SET x="101" scl="100" id="11881" y="320" />
                 *      * ・<SET stat="あいうえお" id="627" />
                 */
                String strId = xpp.getAttributeValue(null, "id");
                int id = Integer.parseInt(strId);
                String stat = xpp.getAttributeValue(null, "stat");
                String x = xpp.getAttributeValue(null, "x");
                String y = xpp.getAttributeValue(null, "y");
                String scl = xpp.getAttributeValue(null, "scl");


                JSONObject jsonObject = checkSet(stat, x, y, scl);
                String check = null;
                try {
                    check = jsonObject.getString("check");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (check.equals("stat")) {

//                        Main3Activity main3Activity = (Main3Activity) activity;
                    final String strStat = xpp.getAttributeValue(null, "stat");
                    final String strId2 = xpp.getAttributeValue(null, "id");
                    final int id2 = Integer.parseInt(strId2);

                    Person person = null;
                    if (MeMonaId.getMe().getMonaId() == id2) {
                        person = TestShared.getInstance(context).getMe();
                        TestShared.getInstance(context).setStat(strStat);
                    } else {
                        for (int uniqueId : people.keySet()) {
                            Person temp = people.get(uniqueId);
                            if (temp.getMonaId() == id2) {
                                person = temp;
                                break;
                            }
                        }
                        if (person == null) {
                            continue;
                        }
                        person.setStat(strStat);
                    }


                    TestIdXML testIdXML = new TestIdXML(person.getUniqueId(), UtilInfo.STAT, id2);

                    /**
                     * statを登録しないといけない
                     */
//                    Main2DB main2DB = new Main2DB();
//                    main2DB.updateState(id2, strStat);
                    final ArrayList<TestIdXML> arrayList = new ArrayList<>();
                    arrayList.add(testIdXML);

                    try {
                        Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
                        MyRunnable myRunnable = new MyRunnable(2, tate2Fragment);
                        synchronized (myRunnable) {
                            Log.d("synchronized stat", "synchronized stat");
                            myRunnable.peopleSet(arrayList);
                            myRunnable.logSet("stat");
                            base2Activity.runOnUiThread(myRunnable);
                            try {
                                myRunnable.wait();
                                Log.d("wait finish", "wait finish");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (NullPointerException e) {

                    }

                } else if (check.equals("xyscl")) {
                    /**
                     * importan method
                     */

                    final HashMap<String, Integer> map = Calc.calGetSetMap(
                            xpp.getAttributeValue(null, "id"),
                            xpp.getAttributeValue(null, "x"),
                            xpp.getAttributeValue(null, "y"),
                            xpp.getAttributeValue(null, "scl")
                    );
                    int tempMonaId = map.get("id");
                    int tempX = Calc.limitX(map.get("x"));
                    int tempY = Calc.limitY(map.get("y"));
                    int tempScl = map.get("scl");
                    int[] monaidcxRcyRscl = Calc.test2Cal(tempMonaId, tempX, tempY, tempScl);
                    Person person = null;

                    if (MeMonaId.getMe().getMonaId() == monaidcxRcyRscl[0]) {
                        TestShared testShared = TestShared.getInstance(context);
                        person = testShared.getMe();
                        testShared.setX(tempX);
                        testShared.setY(tempY);
                        testShared.setScl(tempScl);
                        Log.d(TAG, "" + test7(scl));
                    } else {
                        for (int uniqueId : people.keySet()) {
                            Person temp = people.get(uniqueId);
                            if (temp.getMonaId() == monaidcxRcyRscl[0]) {
                                person = temp;
                                break;
                            }
                        }
                        if (person == null) {
                            continue;
                        }
                    }

                    TestIdXML testIdXML = new TestIdXML(person.getUniqueId(), UtilInfo.XY, monaidcxRcyRscl[0]);

                    person.setcXR(monaidcxRcyRscl[1]);
                    person.setcYR(monaidcxRcyRscl[2]);
                    person.setScl(monaidcxRcyRscl[3]);
                    final ArrayList<TestIdXML> arrayList = new ArrayList<>();
                    arrayList.add(testIdXML);

                    try {
                        Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);

                        MyRunnable myRunnable = new MyRunnable(2, tate2Fragment);
                        synchronized (myRunnable) {
                            Log.d("synchronized xy", "synchronized xy");
                            myRunnable.peopleSet(arrayList);
                            myRunnable.logSet("xy");
                            base2Activity.runOnUiThread(myRunnable);
                            try {
                                myRunnable.wait();
                                Log.d("wait finish", "wait finish");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (NullPointerException e) {

                    }

                } else {

                }
            } else {
                Log.d("elementName", elementName);
            }

        }

    }

    /**
     * important method
     *
     * @param context
     * @param xpp
     * @param eventType
     */
    private ArrayList<TestIdXML> memberSet2(Context context, XmlPullParser xpp, int eventType) {
        ArrayList<TestIdXML> testIdXMLS = new ArrayList<>();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName = xpp.getName();
            if (eventType == XmlPullParser.START_TAG && (elementName.equals(UtilInfo.ENTER) || elementName.equals(UtilInfo.USER))) {

                /**
                 * 取得するだけ（id以外)
                 */
                String strOtherId = xpp.getAttributeValue(null, "id");
                int monaId = test8(strOtherId);

                // <ENTER id="164" />の時の処理 もう１回確認する
                String check = xpp.getAttributeValue(null, "r");

                String strRed;//redではなくrで
                String strGreen;
                String strBlue;
                String strName;
                String strIhash;
                String strStat;
                String strType;
                String strY;
                String strX;
                String strScl;
                String strTrip;
                if (check == null && MeMonaId.me.getMonaId() != monaId) {
                    /**
                     * important method
                     * close progress
                     */
                    strRed = "0";//redではなくrで
                    strGreen = "0";
                    strBlue = "0";
                    strName = "*stealth*";
                    strIhash ="*stealth*";
                    strStat ="*stealth*";
                    strType = "";
                    strY = "80";
                    strX = "360";
                    strScl = "100";
                    strTrip ="";


                    try {


                    } catch (NullPointerException e) {
                        Log.d("progress not close", "progress not close");
                    }

//                    return null;
                } else {
                    strRed = xpp.getAttributeValue(null, "r");//redではなくrで
                    strGreen = xpp.getAttributeValue(null, "g");
                    strBlue = xpp.getAttributeValue(null, "b");
                    strName = xpp.getAttributeValue(null, "name");
                    strIhash = xpp.getAttributeValue(null, "ihash");
                    strStat = xpp.getAttributeValue(null, "stat");
                    strType = xpp.getAttributeValue(null, "type");
                    strY = xpp.getAttributeValue(null, "y");
                    strX = xpp.getAttributeValue(null, "x");
                    strScl = xpp.getAttributeValue(null, "scl");
                    strTrip = xpp.getAttributeValue(null, "trip");
                }





                /**
                 * チェック(不正な値が入っていないか)
                 */
//        int aaMipMapId = testGetMipMapIdFromSearchtypeAA(strType);
                int red = test5(strRed);
                int green = test5(strGreen);
                int blue = test5(strBlue);
                int x = 100;
                int y = 100;
                int type = resNoFromStrType(strType);

                if (type == R.drawable.ic_svg_mona) {
                    strType = "mona";
                }

                try {
                    HashMap<String, Integer> hashMap = Calc.calGetSetMap(monaId + "", strX, strY, strScl);
                    x = hashMap.get("x");
                    y = hashMap.get("y");
                } catch (NumberFormatException e) {

                }
                int scl = test7(strScl);

                int color255 = Util.change100to255(red, green, blue);
                int colorInverted255 = Util.inverteColor(color255);
                int colorHukidashi = Util.change100toHukidashi(red, green, blue);

                /**
                 * -----------------------ここまでチェック----------------------
                 * -------------------------ここから設定-----------------------
                 */

                Person person = new Person();
                person.setUniqueId(uniqueId);

                String ignores = TestShared.getInstance(context).getIgnores();
                if (ignores.contains(","+ strIhash)) {
                    person.setIgnoreFlag(true);
                }

//                person.setAdapter(new Tate2Fragment.MyAdapter(context,R.layout.test_comment1));


                if (MeMonaId.getMe().getMonaId() == monaId) {
//                    MeMonaId.setMe(person);
                    MeMonaId.uniqueId = uniqueId;
                    person.setTrip(TestShared.getInstance(context).getTrip());
                    people.put(uniqueId, person);
                } else {
                    people.put(uniqueId, person);
                }
                testIdXMLS.add(new TestIdXML(uniqueId, elementName, monaId));
                ++uniqueId;

//                people.add(person);
                person.setXml(elementName);
                PersonLayoutDrawable personLayoutDrawable = new PersonLayoutDrawable();

//                ReadThreadDB readThreadDB = new ReadThreadDB();
//                SizeTate2 sizeTate2 = readThreadDB.getSizeTate2();

                /**
                 * 限界値チェック
                 * 限界値以上、以下なら設定する
                 */

                x = Calc.limitX(x);
                y = Calc.limitY(y);
                /**
                 * person
                 */
                personLayoutDrawable.setDrawable(ActivityCompat.getDrawable(context, type));
                person.setColorHukidashi(colorHukidashi);
                person.setColor255(color255);
                person.setColorInverte255(colorInverted255);
                person.setRed(red);
                person.setGreen(green);
                person.setBlue(blue);
                person.setY(Stub.Y);
                person.setX(x);
                person.setScl(scl);
                person.setName(strName);
                person.setMonaId(monaId);
                person.setShirotori(strIhash);
                person.setStat(strStat);
                person.setAaname(strType);
                person.setAaresno(type);

                if (strTrip == null) {
                    person.setKurotori("");
                } else {
                    person.setKurotori(strTrip);
                }


                /**
                 *
                 */
                int[] cx2cy2 = Calc.testCal(monaId, x, y, scl);
                /**
                 * もなちゃとxからレギュラーxに変換する
                 */
                int cxR = Calc.convertToRegularX(x, Calc.widthRegular);
                /**
                 * もなちゃとyからレギュラーyに変換する
                 */
                int cyR = Calc.convertToRegularY(x, Calc.heightRegular);
                // tate2 android用にxyを変換 (座標出力)
                // TODO 20180411 ※WHAT is TODO? change getHeight08 to getH08HikuPicHalf


                /**
                 * ここで逆にしているのであとはxはx yはyに代入
                 */

                person.setcX2((int) Calc.Dp2Px(cx2cy2[1], context));
                person.setcY2((int) Calc.Dp2Px(cx2cy2[2], context));

                person.setcXR(cxR);
                person.setcYR(cyR);

                /**
                 * tate2
                 */
                RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.aa_name_trip_comment2, null);
//                ListView listView = (ListView) relativeLayout.findViewById(R.id.char_comment_list);
                TextView textName = (TextView) relativeLayout.findViewById(R.id.char_name);

//                textName.setToY(Calc.aasizePixcel + 10);
                TextView textShirotori = (TextView) relativeLayout.findViewById(R.id.char_shirotori);
//                textShirotori.setToY(Calc.aasizePixcel + 20);
                TextView textKurotori = (TextView) relativeLayout.findViewById(R.id.char_kurotori);
//                textKurotori.setToY(Calc.aasizePixcel + 30);

                textName.setText(person.getName());
                textShirotori.setText(person.getShikakuRealShirotori());
                if (!person.getKurotori().equals("")) {
                    textKurotori.setText(person.getShikakuRealKurotori());
                }

                ImageView imageView2 = (ImageView) relativeLayout.findViewById(R.id.char_image);
                ViewGroup.LayoutParams layoutParams1 = imageView2.getLayoutParams();
                layoutParams1.width = mAasizePixel;
                layoutParams1.height = mAasizePixel;
                imageView2.setLayoutParams(layoutParams1);
                imageView2.setImageDrawable(ActivityCompat.getDrawable(context, type));

                if (person.getScl() == 100) {
                    imageView2.setScaleX(1);
                } else if (person.getScl() == -100) {
                    imageView2.setScaleX(-1);
                }

                // 色の設定
                if (color255 == Color.WHITE) {

                } else {

                }
                /**
                 * 2018/11/26
                 */
                /**
                 * 2018/11/27
                 * aaのサイズはNameActivityで取ってある
                 */


                //todo 20180608 雲イメージは後から追加する
                if (person.getStat().equals("通常")) {
                    TextView textStat = (TextView) relativeLayout.findViewById(R.id.char_stat);
                    textStat.setVisibility(View.INVISIBLE);
                } else {
                    TextView textStat = (TextView) relativeLayout.findViewById(R.id.char_stat);
                    textStat.setText(person.getStat());
                    textStat.setVisibility(View.VISIBLE);
                }

                personLayoutDrawable.setRelativeLayout2(relativeLayout);

                /**
                 *レギュラーのサイズをセットする
                 */
//                FrameLayout.LayoutParams regularParams = new FrameLayout.LayoutParams(Calc.aasizePixcel, Calc.aasizePixcel);
                FrameLayout.LayoutParams regularParams = new FrameLayout.LayoutParams(mAasizePixel, mCharWholePixel);
//                regularParams.setMargins(person.getcXR(),person.getcYR(),0,0);
//                regularParams.setMargins(person.getcXR(), Calc.regularTopMargin - Calc.charWholePixcel, 0, 0);
                relativeLayout.setLayoutParams(regularParams);
                personLayoutDrawable.setLayoutRegularParams(regularParams);

                /**
                 * 縦用のサイズをセットする
                 */
                int size = (int) Calc.Dp2Px(118, context);
                FrameLayout.LayoutParams testParam1 = new FrameLayout.LayoutParams(size, size);
                testParam1.setMargins(person.getcX2(), person.getcY2(), 0, 0);
                personLayoutDrawable.setLayoutParams2(testParam1);

                if (personLayoutDrawable == null) {
                    Log.d("NULLLLLLLLLLLLLLLLLLL", "NULLLLLLLLLLLLLLLLLLL");
                } else {
                    Log.d("ぬるじゃない", "ぬるじゃない");
                }

                /**
                 * 自分と部屋のメンバーを分ける
                 */
                if (MeMonaId.getMe().getMonaId() == monaId) {
                    TestShared testShared = TestShared.getInstance(context);
                    testShared.setMe(person);
                    base2Activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            base2Activity.getSupportLoaderManager().initLoader(5, new Bundle(), new LoaderCallbacksDBRoom5(base2Activity));
                        }
                    });

//                    MeMonaId.setMe(person);
//                    /**
//                     * meが登録されているか
//                     */
//                    boolean c = readThreadDB.isSetMe();
//                    /**
//                     * 登録されていない場合
//                     */
//                    if (!c) {
//                        readThreadDB.insertMe(person);
//                        /**
//                         * 登録されている場合
//                         */
//                    } else {
//                        readThreadDB.updateMe(person);
//                    }


                } else {
//                    readThreadDB.insertPerson(person);
                }
                hashLayoutByte.put(person.getUniqueId(), personLayoutDrawable);
            }
            try {
                eventType = xpp.next();
            } catch (XmlPullParserException e) {
                return null;
            } catch (IOException e) {

                return null;
            }

        }

        return testIdXMLS;
    }


    private ArrayList<TestIdXML> exit(Context context, XmlPullParser xpp, int eventType) {
        ArrayList<TestIdXML> testIdXMLS = new ArrayList<>();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName = xpp.getName();
            if (eventType == XmlPullParser.START_TAG && elementName.equals(UtilInfo.EXIT)) {

                String strIdExit = xpp.getAttributeValue(null, "id");
                final int id = Integer.parseInt(strIdExit);

                if (MeMonaId.getMe().getMonaId() == id) {
                    break;
                }

                /**
                 * フラグメントがないときにも、DBは更新しておかないといけないので
                 *
                 */
//                Main2DB main2DB = new Main2DB();
//                main2DB.updatePerson(id);
                Person person = null;

                if (MeMonaId.getMe().getMonaId() == id) {
                    person = TestShared.getInstance(context).getMe();
                } else {
                    for (int uniqueId : people.keySet()) {
                        Person temp = people.get(uniqueId);
                        if (temp.getMonaId() == id) {
                            //arrayList.add(people.get(uniqueId));
                            person = temp;
                            break;
                        }
                    }
                }

//                ArrayList<Person> arrayList = new ArrayList<>();
//                for (int uniqueId : people.keySet()) {
//                    Person temp = people.get(uniqueId);
//                    if (temp.getMonaId() == id) {
//                        arrayList.add(people.get(uniqueId));
//                    }
//                }

                person.setXml(UtilInfo.EXIT);
                TestIdXML testIdXML = new TestIdXML(person.getUniqueId(), UtilInfo.EXIT, person.getMonaId());
                testIdXMLS.add(testIdXML);


//                if (arrayList.size() == 0) {
//                    try {
//                        eventType = xpp.next();
//                    } catch (XmlPullParserException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    continue;
//                } else {
//                    for (int i = 0; i < arrayList.size(); i++) {
//                        Person checkPerson = arrayList.get(0);
//                        if (checkPerson.isExitFlag() != true) {
//                            checkPerson.setExitFlag(true);
//                            person = checkPerson;
//                            break;
//                        }
//
//                    }
//
//                }

                if (person == null) {
                    try {
                        eventType = xpp.next();
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    continue;
                }


            }
            try {
                eventType = xpp.next();
            } catch (XmlPullParserException e) {
                return null;
            } catch (IOException e) {
                return null;
            }
        }
        return testIdXMLS;
    }


    /**
     * check element
     *
     * @param stat
     * @param x
     * @param y
     * @param scl
     * @return
     */
    private JSONObject checkSet(String stat, String x, String y, String scl) {
        /**
         * <SET x="101" scl="100" id="11881" y="320" />
         */
        if (x != null && y != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("check", "xyscl");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
            /**
             * <SET stat="abcdef" id="627" />
             */
        } else if (stat != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("check", "stat");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }
        /**
         * <SET cmd="ev" pre="0" param="0" id="102" />
         */
        else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("check", "error");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }
    }


    private int[] set2(XmlPullParser xpp, String frag) {

        SetValidation setValidation = new SetValidation();
        try {
            setValidation.setDefaultSet(xpp);
        } catch (NullPointerException e) {

            return new int[0];
        } catch (NumberFormatException e1) {
            return new int[0];
        }

        //----------- end -----------

        /**
         * changes 16-decimal,etc
         * 16進新などの変換
         */
        final HashMap<String, Integer> setMap = getSet(
                xpp.getAttributeValue(null, "id"),
                xpp.getAttributeValue(null, "x"),
                xpp.getAttributeValue(null, "y"),
                xpp.getAttributeValue(null, "scl")
        );
        final int monaId = setMap.get("id");
        final int x = setMap.get("x");
        final int y = setMap.get("y");
        final int scl = setMap.get("scl");

        if (frag.equals("tate")) {


            return null;
        } else if (frag.equals("tate2")) {
            int[] cx2cy2scl = Calc.testCal(monaId, x, y, scl);
            return cx2cy2scl;
        } else {
            return null;
        }
    }

    private int test6(String position) throws NumberFormatException {

        int result = 100;
        try {
            result = Integer.parseInt(position);
            Log.d("lineStartX", "XかY" + result);
        } catch (NumberFormatException e) {
            Log.d("1021test6", position + "");
            String position16 = position.replaceAll("0x", "");
            try {
                result = Integer.parseInt(position16, 16);
                Log.d("lineStartX", "positionChangeError値 :" + position);
            } catch (NumberFormatException e1) {
                throw new NumberFormatException("Read test6");
            }
        }

        return result;
    }

    private int test8(String strId) throws NumberFormatException, NullPointerException {

        int result;
        try {
            result = Integer.parseInt(strId);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("ERROR: r0x11aa");
        } catch (NullPointerException e) {
            throw new NullPointerException("ERROR: r0x12");
        }
        return result;
    }

    private static int test5(String color) {

        int result = 100;
        try {
            result = Integer.parseInt(color);
        } catch (NumberFormatException e) {
            Log.d("test5", "colorChangeError値 :" + color);
        }
        return result;
    }

    private int test7(String strScl) {

        int result = 100;

        try {
            result = Integer.parseInt(strScl);
        } catch (NumberFormatException e) {
            Log.d("lineStopX", "sclChangeError値 :" + strScl);
            return 1;
        }

        if (result == 100) {
            return 1;
        } else if (result == -100) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * return resNo(svg) from strType
     * typeからresNoを戻す
     * もしtypeがマップされていないなら、monaを返す
     *
     * @param strType
     * @return
     */
    public static int resNoFromStrType(String strType) {

        int resNo;
        try {
            resNo = UtilInfo.aaNameaaResNoMap.get(strType);
        } catch (NullPointerException e) {
            return R.drawable.ic_svg_mona;
        }
        return resNo;
    }

    private HashMap<String, Integer> getSet(String strId, String strX, String strY, String strScl) {

        HashMap<String, Integer> hashMap = new HashMap<>();

        int id = Integer.parseInt(strId);
        hashMap.put("id", id);

        try {
            int x = Integer.parseInt(strX);
            hashMap.put("x", x);
        } catch (NumberFormatException e) {
            String position16 = strX.replaceAll("0x", "");
            int x16 = Integer.parseInt(position16, 16);
            hashMap.put("x", x16);
        }
        try {
            int y = Integer.parseInt(strY);
            hashMap.put("y", y);
        } catch (NumberFormatException e) {
            String position16 = strY.replaceAll("0x", "");
            int y16 = Integer.parseInt(position16, 16);
            hashMap.put("y", y16);
        }

        int scl = test7(strScl);
        hashMap.put("scl", scl);

        return hashMap;
    }

}
