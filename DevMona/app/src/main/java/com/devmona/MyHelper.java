package com.devmona;

import android.content.Context;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * Created by sake on 2017/09/13.
 */

public class MyHelper extends SQLiteOpenHelper {
    public MyHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        db.execSQL("CREATE TABLE mt (id INTEGER PRIMARY KEY AUTOINCREMENT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, m TEXT, flag INTEGER DEFAULT 0)");
//        db.execSQL("CREATE TABLE mt (id INTEGER PRIMARY KEY AUTOINCREMENT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, shirotori TEXT, shirotori6 TEXT, m TEXT, flag INTEGER DEFAULT 0)");
//        String query = "CREATE TABLE cmt(id INTEGER PRIMARY KEY AUTOINCREMENT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, cmt TEXT, roomno TEXT,flag INTEGER DEFAULT 0, shirotori TEXT, shirotori6 TEXT)";
//        db.execSQL(query);
//        test(db);
        db.execSQL("CREATE TABLE start ( id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME)");
        db.execSQL("CREATE TABLE start_save ( id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME)");
        db.execSQL("CREATE TABLE login (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,date DATETIME)");
        db.execSQL("CREATE TABLE login_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,date DATETIME)");
        db.execSQL("CREATE TABLE room (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER,room_no INTEGER, room_name TEXT, room_count INTEGER,  enter_date DATETIME,exit_date DATETIME)");
        db.execSQL("CREATE TABLE room_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER,room_no INTEGER, room_name TEXT, room_count INTEGER,  enter_date DATETIME,exit_date DATETIME)");
        db.execSQL("CREATE TABLE person (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT,trip TEXT,name_ellipsis TEXT,aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag INTEGER,ignore_flag INTEGER,show_flag INTEGER)");
        db.execSQL("CREATE TABLE person_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT,trip TEXT,aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag INTEGER,ignore_flag INTEGER,show_flag INTEGER)");
        db.execSQL("CREATE TABLE comment (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id, room_id INTEGER, person_id INTEGER, comment TEXT, date DATETIME, show_flag INTEGER,send_flag INTEGER)");
        db.execSQL("CREATE TABLE comment_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id, room_id INTEGER, person_id INTEGER, comment TEXT, date DATETIME, show_flag INTEGER,send_flag INTEGER)");
        db.execSQL("CREATE TABLE sentaku_me (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT,trip TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag,ignore_flag INTEGER)");
        db.execSQL("CREATE TABLE rooms_map ( id INTEGER PRIMARY KEY AUTOINCREMENT, default_no INTEGER, room_name TEXT, room_no INTEGER, room_count INTEGER, button_id INTEGER,available_flag INTEGER,update_flag INTEGER)");
        db.execSQL("CREATE TABLE dbstatus(id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,s_p_id INTEGER,s_start_id INTEGER,s_login_id INTEGER, s_room_id INTEGER, s_cmt_id INTEGER, date DATETIME)");
        db.execSQL("CREATE TABLE size_tate2 ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER,date INTEGER,delete_flag INTEGER,h08_hiku_pic_half INTEGER,w08_plus_pic_half INTEGER ,h08_hiku_pic INTEGER)");
        db.execSQL("CREATE TABLE gc (id INTEGER PRIMARY KEY AUTOINCREMENT,gc TEXT,date DATETIME,flag INTEGER)");
        db.execSQL("CREATE TABLE gc_ft (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,from_gc INTEGER,to_gc TEXT,date DATETIME)");
        db.execSQL("CREATE TABLE pass(id INTEGER PRIMARY KEY AUTOINCREMENT,pass TEXT,date DATETIME,delete_flag INTEGER)");
        db.execSQL("CREATE TABLE m_res_no(id INTEGER PRIMARY KEY AUTOINCREMENT,aa_name TEXT,aa_res_no TEXT)");
        db.execSQL("CREATE TABLE setting (id INTEGER PRIMARY KEY AUTOINCREMENT, alpha INTEGER, alpha01 TEXT, black INTEGER, touch_ok INTEGER,black_seek INTEGER,alpha_seek INTEGER,rooms_list_position INTEGER)");
        db.execSQL("CREATE TABLE current_dialog_page (id INTEGER PRIMARY KEY AUTOINCREMENT, page INTEGER)");
        db.execSQL("CREATE TABLE room_status ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER, room_scroll_y INTEGER, date DATETIME )");
        db.execSQL("CREATE TABLE p_st ( id INTEGER PRIMARY KEY AUTOINCREMENT,start_id INTEGER, pst TEXT, date DATETIME)");
        db.execSQL("CREATE TABLE `pic_size` (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,pic_width INTEGER, pic_height INTEGER)");
        db.execSQL("CREATE TABLE `pic_size_save` (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,pic_width INTEGER, pic_height INTEGER)");
        db.execSQL("CREATE TABLE `character_no_on_page` (id INTEGER PRIMARY KEY AUTOINCREMENT, page INTEGER,character_no INTEGER)");
        db.execSQL("CREATE TABLE `notify_setting` (id INTEGER PRIMARY KEY AUTOINCREMENT,notify1 TEXT,notify2 TEXT,notify3 TEXT,notify4 TEXT,notify5 TEXT,notify6 TEXT,notify7 TEXT,notify8 TEXT,notify9 TEXT,notify10 TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        test(db);
//        db.execSQL("DROP TABLE IF EXISTS mtable");
//        db.execSQL("DROP TABLE IF EXISTS proptable");

//        // start
//        db.execSQL("DROP TABLE IF EXISTS start");
//        db.execSQL("CREATE TABLE start ( id INTEGER PRIMARY KEY AUTOINCREMENT, time DATETIME)");
//        // login
//        db.execSQL("DROP TABLE IF EXISTS login");
//        db.execSQL("CREATE TABLE login ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, time DATETIME)");
//        // room
//        db.execSQL("DROP TABLE IF EXISTS room");
//        db.execSQL("CREATE TABLE room ( id INTEGER PRIMARY KEY AUTOINCREMENT, room_no INTEGER, room_name TEXT, room_count INTEGER, login_id INTEGER, time DATETIME)");
//        // person
//        db.execSQL("DROP TABLE IF EXISTS person");
//        db.execSQL("CREATE TABLE person ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, name TEXT, aa_color INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name INTEGER, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, is_me INTEGER, login_id INTEGER, date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER, mona_id INTEGER, room_id INTEGER)");
//        // sentaku_me
//        db.execSQL("DROP TABLE IF EXISTS sentaku_me");
//        db.execSQL("CREATE TABLE sentaku_me ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,name TEXT, aa_color INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name INTEGER, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, is_me INTEGER, login_id INTEGER, date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER, mona_id INTEGER, room_id INTEGER)");
//        // comment
//        db.execSQL("DROP TABLE IF EXISTS comment");
//        db.execSQL("CREATE TABLE comment ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id, room_id INTEGER, person_id INTEGER, comment TEXT, show_flag INTEGER, time DATETIME )");
//        // rooms_map
//        db.execSQL("DROP TABLE IF EXISTS rooms_map");
//        db.execSQL("CREATE TABLE rooms_map ( id INTEGER PRIMARY KEY AUTOINCREMENT, default_no INTEGER, room_name TEXT, room_no INTEGER, room_count INTEGER, button_id INTEGER )");
//
//
//        //dbstatus
//        db.execSQL("DROP TABLE IF EXISTS dbstatus");
//        db.execSQL("CREATE TABLE dbstatus ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,pseq INTEGER, loginseq INTEGER, roomseq INTEGER, cmtseq INTEGER, fromseq INTEGER, toseq INTEGER )");
//
//        // size_tate
//        db.execSQL("DROP TABLE IF EXISTS size_tate");
//        db.execSQL("CREATE TABLE size_tate ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER,time INTEGER,delete_flag INTEGER )");
//        // size_land
//        db.execSQL("DROP TABLE IF EXISTS size_land");
//        db.execSQL("CREATE TABLE size_land ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER,time INTEGER,delete_flag INTEGER )");
//        // size_canvas_tate
//        db.execSQL("DROP TABLE IF EXISTS size_canvas_tate");
//        db.execSQL("CREATE TABLE size_canvas_tate ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER,time INTEGER,delete_flag INTEGER)");
//        // size_canvas_land
//        db.execSQL("DROP TABLE IF EXISTS size_canvas_land");
//        db.execSQL("CREATE TABLE size_canvas_land ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER,time INTEGER,delete_flag INTEGER)");
//        db.execSQL("DROP TABLE IF EXISTS sizeyoko");
//        db.execSQL("CREATE TABLE `sizeyoko`(id INTEGER PRIMARY KEY,width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER)");
//        db.execSQL("DROP TABLE IF EXISTS room");
//        db.execSQL("CREATE TABLE `room`(id INTEGER PRIMARY KEY, defaultnum INTEGER, roomname TEXT, roomno INTEGER, roomcount INTEGER, buttonid TEXT, loginno INTEGER)");
//        db.execSQL("DROP TABLE IF EXISTS canvastate");

//        db.execSQL("CREATE TABLE `mtable`(id INTEGER PRIMARY KEY,m TEXT,mgettime DATETIME,mflag INTEGER)");


//        db.execSQL("CREATE TABLE `sizecanvastate`(id INTEGER PRIMARY KEY,width INTEGER, height INTEGER)");
//        db.execSQL("ALTER TABLE mt ADD shirotori TEXT");
//        db.execSQL("ALTER TABLE mt ADD shirotori6 TEXT");
//        db.execSQL("CREATE TEMPORARY TABLE tmt (id INTEGER PRIMARY KEY AUTOINCREMENT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, m TEXT, flag INTEGER DEFAULT 0,flag INTEGER)");
//        db.execSQL("INSERT INTO tmt (id, time, m, flag) SELECT id,text,m,flag FROM mt");

//        db.execSQL("DROP TABLE IF EXISTS mt");
//        db.execSQL("CREATE TABLE mt (id INTEGER PRIMARY KEY AUTOINCREMENT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, shirotori TEXT, shirotori6 TEXT, m TEXT, flag INTEGER DEFAULT 0,login INTEGER)");
//        db.execSQL("DROP TABLE IF EXISTS cmtt");

//        db.execSQL("CREATE TABLE `starttable`(`startseq`)");

//        db.execSQL("CREATE TABLE `starttable`(startseq INTEGER,starttime DATETIME DEFAULT CURRENT_TIMESTAMP)");
//        db.execSQL("CREATE TABLE `logintable`(startseq INTEGER,loginseq INTEGER, login INTEGER, logintime DATETIME DEFAULT CURRENT_TIMESTAMP)");
//        db.execSQL("CREATE TABLE `nametable`(startseq INTEGER,loginseq INTEGER, name TEXT, shirotori TEXT, shirotori6 TEXT, kurotori TEXT)");
//        db.execSQL("CREATE TABLE `roomtable`(startseq INTEGER,loginseq INTEGER,roomseq INTEGER, roomno TEXT, entertime DATETIME DEFAULT CURRENT_TIMESTAMP,flag INTEGER)");
//        db.execSQL("CREATE TABLE `cmtable`(startseq INTEGER,loginseq INTEGER,roomseq INTEGER, roomno TEXT, cmtseq INTEGER, cmt TEXT, cmttime DATETIME DEFAULT CURRENT_TIMESTAMP,flag INTEGER)");

//        String query = "CREATE TABLE cmt(id INTEGER PRIMARY KEY AUTOINCREMENT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, cmt TEXT, roomno TEXT,flag INTEGER DEFAULT 0, shirotori TEXT, shirotori6 TEXT)";
//        db.execSQL(query);
//        String query3 = "CREATE TABLE cmtt(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, cmt TEXT, roomno TEXT,flag INTEGER DEFAULT 0, shirotori TEXT, shirotori6 TEXT, androidid TEXT, deviceid TEXT, simcountryiso TEXT, simserialnumber TEXT)";
//        db.execSQL("INSERT INTO mt(id,time,m,flag) SELECT id,time,m,flag FROM tmt");
//        db.execSQL(query3);


    }

    private void test(SQLiteDatabase db) {

        /**
         * notify_setting
         */

        try {
            db.execSQL("CREATE TABLE `notify_setting` (id INTEGER PRIMARY KEY AUTOINCREMENT,notify1 TEXT,notify2 TEXT,notify3 TEXT,notify4 TEXT,notify5 TEXT,notify6 TEXT,notify7 TEXT,notify8 TEXT,notify9 TEXT,notify10 TEXT)");
        } catch(SQLiteException e) {
            e.printStackTrace();
        }

        /**
         * start
         */
        try {
//            db.rawQuery("SELECT 1 FROM start LIMIT 1", null);
            db.execSQL("CREATE TABLE start_temp AS SELECT * FROM `start`");
            db.execSQL("DROP TABLE IF EXISTS start");
            db.execSQL("CREATE TABLE start ( id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME)");
            db.execSQL("INSERT INTO start (id,date) SELECT id,date FROM start_temp");
            db.execSQL("DROP TABLE IF EXISTS start_temp");
        } catch (SQLiteException e) {
//            db.execSQL("CREATE TABLE start ( id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME)");
            e.printStackTrace();
        }

        /**
         * start_save
         */
        try {
            db.execSQL("DROP TABLE IF EXISTS start_save");
            db.execSQL("CREATE TABLE start_save ( id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME)");
        } catch (SQLiteException e) {
            e.printStackTrace();
        }


//        // start_save
//        db.rawQuery("SELECT 1 FROM start_save LIMIT 1", null);
//        db.execSQL("CREATE TABLE start_save_temp AS SELECT * FROM `start_save`");
//        db.execSQL("DROP TABLE IF EXISTS start_save");
//        db.execSQL("CREATE TABLE start_save ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,time DATETIME)");
//        db.execSQL("INSERT INTO start_save(id,start_id,time) SELECT id,start_id,time FROM start_save_temp");
//        db.execSQL("DROP TABLE IF EXISTS start_save_temp");


        /**
         * login
         */
        try {
//            db.rawQuery("SELECT 1 FROM login LIMIT 1", null);
            db.execSQL("CREATE TABLE login_temp AS SELECT * FROM `login`");
            db.execSQL("DROP TABLE IF EXISTS login");
            db.execSQL("CREATE TABLE login (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,date DATETIME)");
            db.execSQL("INSERT INTO login(id,start_id,date) SELECT id,start_id,date FROM login_temp");
            db.execSQL("DROP TABLE IF EXISTS login_temp");
        } catch (Exception e) {
//            db.execSQL("CREATE TABLE login (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,date DATETIME)");
            e.printStackTrace();
        }

        /**
         * login_save
         */
        try {
            db.execSQL("DROP TABLE IF EXISTS login_save");
            db.execSQL("CREATE TABLE login_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,date DATETIME)");
        } catch (Exception e) {
//            db.execSQL("CREATE TABLE login (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,date DATETIME)");
            e.printStackTrace();
        }


        // login_save
//        db.execSQL("DROP TABLE IF EXISTS login_save");
//        db.execSQL("CREATE TABLE login_save ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, time DATETIME)");

        /**
         * room
         */
        try {
//            db.rawQuery("SELECT 1 FROM room LIMIT 1", null);
            db.execSQL("CREATE TABLE room_temp AS SELECT * FROM room");
            db.execSQL("DROP TABLE IF EXISTS room");
            db.execSQL("CREATE TABLE room (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER,room_no INTEGER, room_name TEXT, room_count INTEGER,  enter_date DATETIME,exit_date DATETIME)");
            db.execSQL("INSERT INTO room(id,start_id,login_id,room_no,room_name,room_count,enter_date,exit_date)SELECT id,start_id,login_id,room_no,room_name,room_count,enter_date,exit_date FROM room_temp");
            db.execSQL("DROP TABLE IF EXISTS room_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE room (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER,room_no INTEGER, room_name TEXT, room_count INTEGER,  enter_date DATETIME,exit_date DATETIME)");
        }

        /**
         * room_save
         */
        try {
            db.execSQL("DROP TABLE IF EXISTS room_save");
            db.execSQL("CREATE TABLE room_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER,room_no INTEGER, room_name TEXT, room_count INTEGER,  enter_date DATETIME,exit_date DATETIME)");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        // room_save
//        db.execSQL("DROP TABLE IF EXISTS room_save");
//        db.execSQL("CREATE TABLE room_save ( id INTEGER PRIMARY KEY AUTOINCREMENT, room_no INTEGER, room_name TEXT, room_count INTEGER, start_id INTEGER, login_id INTEGER,time DATETIME)");

        /**
         * person
         */
        try {
//            db.rawQuery("SELECT 1 FROM person LIMIT 1", null);
            db.execSQL("CREATE TABLE person_temp AS SELECT * FROM person");
            db.execSQL("DROP TABLE IF EXISTS person");
            db.execSQL("CREATE TABLE person (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT, trip TEXT,name_ellipsis TEXT,aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag INTEGER,ignore_flag INTEGER,show_flag INTEGER)");
            db.execSQL("INSERT INTO person (id,start_id,login_id,room_id,mona_id,name,trip,aa_color_255,red,green,blue,aa_res_no,aa_name,stat,shirotori6,kurotori,shirotori,scl,is_me,date,x,y,cx,cy,cx2,cy2,exit_flag,ignore_flag) SELECT id,start_id,login_id,room_id,mona_id,name,trip,aa_color_255,red,green,blue,aa_res_no,aa_name,stat,shirotori6,kurotori,shirotori,scl,is_me,date,x,y,cx,cy,cx2,cy2,exit_flag,ignore_flag FROM person_temp");
            db.execSQL("DROP TABLE IF EXISTS person_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE person (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT, trip TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag,ignore_flag INTEGER,show_flag INTEGER)");
        }

        /**
         * person_save
         */
        try {
            db.execSQL("DROP TABLE IF EXISTS person_save");
            db.execSQL("CREATE TABLE person_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT,trip TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag INTEGER,ignore_flag INTEGER,show_flag INTEGER)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * comment
         */
        try {
//            db.rawQuery("SELECT 1 FROM comment LIMIT 1", null);
            db.execSQL("CREATE TABLE comment_temp AS SELECT * FROM comment");
            db.execSQL("DROP TABLE IF EXISTS comment");
            db.execSQL("CREATE TABLE comment (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id, room_id INTEGER, person_id INTEGER, comment TEXT, date DATETIME, show_flag INTEGER,send_flag INTEGER)");
            db.execSQL("INSERT INTO comment(id,start_id,login_id,room_id,person_id,comment,date,show_flag,send_flag) SELECT id,start_id,login_id,room_id,person_id,comment,date,show_flag,send_flag FROM comment_temp");
            db.execSQL("DROP TABLE IF EXISTS comment_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE comment (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id, room_id INTEGER, person_id INTEGER, comment TEXT, date DATETIME, show_flag INTEGER,send_flag INTEGER)");
        }

        /**
         * comment_save
         */
        try {
            db.execSQL("DROP TABLE IF EXISTS comment_save");
            db.execSQL("CREATE TABLE comment_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id, room_id INTEGER, person_id INTEGER, comment TEXT, date DATETIME, show_flag INTEGER,send_flag INTEGER)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //        // person_save
//        db.execSQL("DROP TABLE IF EXISTS person_save");
//        db.execSQL("CREATE TABLE person_save (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER, name TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name INTEGER, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER, mona_id INTEGER, room_id INTEGER, scl INTEGER,cx2 INTEGER,cy2 INTEGER,ignore_flag INTEGER)");


        // sentaku_me
//        db.execSQL("DROP TABLE IF EXISTS sentaku_me");
//        db.execSQL("CREATE TABLE sentaku_me ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,name TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name INTEGER, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, is_me INTEGER, login_id INTEGER, date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER, mona_id INTEGER, room_id INTEGER,scl INTEGER,cx2 INTEGER,cy2 INTEGER)");
//        // sentaku_me_save
//        db.execSQL("DROP TABLE IF EXISTS sentaku_me_save");
//        db.execSQL("CREATE TABLE sentaku_me_save ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,name TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name INTEGER, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, is_me INTEGER, login_id INTEGER, date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER, mona_id INTEGER, room_id INTEGER,scl INTEGER,cx2 INTEGER,cy2 INTEGER)");

        // sentaku_me
//        try {
////            ("SELECT 1 FROM sentaku_me LIMIT 1", null);
//            db.execSQL("CREATE TABLE sentaku_me_temp AS SELECT * FROM sentaku_me");
//            db.execSQL("DROP TABLE IF EXISTS sentaku_me");
//            db.execSQL("CREATE TABLE sentaku_me (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT,trip TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag,ignore_flag INTEGER)");
//            db.execSQL("INSERT INTO sentaku_me (id,start_id,login_id,room_id,mona_id,name,trip,aa_color_255,red,green,blue,aa_res_no,aa_name,stat,shirotori6,kurotori,shirotori,scl,is_me,date,x,y,cx,cy,cx2,cy2,exit_flag,ignore_flag) SELECT id,start_id,login_id,room_id,mona_id,name,aa_color_255,red,green,blue,aa_res_no,aa_name,stat,shirotori6,kurotori,shirotori,scl,is_me,date,x,y,cx,cy,cx2,cy2,exit_flag,ignore_flag FROM sentaku_me_temp");
//            db.execSQL("DROP TABLE IF EXISTS sentaku_me_temp");
//        } catch (Exception e) {
//            db.execSQL("CREATE TABLE sentaku_me (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, mona_id INTEGER,name TEXT,trip TEXT, aa_color_255 INTEGER, red INTEGER, green INTEGER, blue INTEGER, aa_res_no TEXT, aa_name TEXT, stat TEXT, shirotori6 TEXT, kurotori TEXT, shirotori TEXT, scl INTEGER, is_me INTEGER,  date DATETIME, x INTEGER, y INTEGER, cx INTEGER, cy INTEGER,  cx2 INTEGER,cy2 INTEGER, exit_flag,ignore_flag INTEGER)");
//        }


//        // comment_save
//        db.execSQL("DROP TABLE IF EXISTS comment_save");
//        db.execSQL("CREATE TABLE comment_save ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id, room_id INTEGER, person_id INTEGER, comment TEXT, show_flag INTEGER, time DATETIME,send_flag INTEGER)");

        // rooms_map
        try {
            // db.rawQuery("SELECT 1 FROM rooms_map LIMIT 1", null);
            db.execSQL("CREATE TABLE rooms_map_temp AS SELECT * FROM rooms_map");
            db.execSQL("DROP TABLE IF EXISTS rooms_map");
            db.execSQL("CREATE TABLE rooms_map ( id INTEGER PRIMARY KEY AUTOINCREMENT, default_no INTEGER, room_name TEXT, room_no INTEGER, room_count INTEGER, button_id INTEGER,available_flag INTEGER,update_flag INTEGER)");
            db.execSQL("INSERT INTO rooms_map(id,default_no,room_name,room_no,room_count,button_id,available_flag,update_flag) SELECT id,default_no,room_name,room_no,room_count,button_id,available_flag,update_flag FROM rooms_map_temp");
            db.execSQL("DROP TABLE IF EXISTS rooms_map_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE rooms_map ( id INTEGER PRIMARY KEY AUTOINCREMENT, default_no INTEGER, room_name TEXT, room_no INTEGER, room_count INTEGER, button_id INTEGER,available_flag INTEGER,update_flag INTEGER)");
        }

        // dbstatus
        try {
            // db.rawQuery("SELECT 1 FROM dbstatus LIMIT 1", null);
            db.execSQL("CREATE TABLE dbstatus_temp AS SELECT * FROM dbstatus");
            db.execSQL("DROP TABLE IF EXISTS dbstatus");
            db.execSQL("CREATE TABLE dbstatus(id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,s_p_id INTEGER,s_start_id INTEGER,s_login_id INTEGER, s_room_id INTEGER, s_cmt_id INTEGER, date DATETIME)");
            db.execSQL("INSERT INTO dbstatus(id,start_id,s_p_id,s_start_id,s_login_id,s_room_id,s_cmt_id,date) SELECT id,start_id,s_p_id,s_start_id,s_login_id,s_room_id,s_cmt_id,date  FROM dbstatus_temp ");
            db.execSQL("DROP TABLE IF EXISTS dbstatus_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE dbstatus(id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,s_p_id INTEGER,s_start_id INTEGER,s_login_id INTEGER, s_room_id INTEGER, s_cmt_id INTEGER, date DATETIME)");
        }

//        db.execSQL("CREATE TABLE dbstatus (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,s_p_id INTEGER,s_start_id INTEGER,s_login_id INTEGER, s_room_id INTEGER, s_cmt_id INTEGER, date DATETIME)");

        // size_tate is not currently used
//        // db.rawQuery("SELECT 1 FROM size_tate_temp LIMIT 1", null);
//        db.execSQL("CREATE TABLE size_tate_temp AS SELECT * FROM size_tate");
//        db.execSQL("DROP TABLE IF EXISTS size_tate");
//        db.execSQL("CREATE TABLE size_tate");


//        db.execSQL("DROP TABLE IF EXISTS size_tate");
//        db.execSQL("CREATE TABLE size_tate ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER,time INTEGER,delete_flag INTEGER )");

        // size_tate2
        try {
            // db.rawQuery("SELECT 1 FROM size_tate2 LIMIT 1", null);
            db.execSQL("CREATE TABLE size_tate2_temp AS SELECT * FROM size_tate2");
            db.execSQL("DROP TABLE IF EXISTS size_tate2");
            db.execSQL("CREATE TABLE size_tate2 ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER,date INTEGER,delete_flag INTEGER,h08_hiku_pic_half INTEGER,w08_plus_pic_half INTEGER ,h08_hiku_pic INTEGER)");
            db.execSQL("INSERT INTO size_tate2(width,height, width08, height08,date,delete_flag,h08_hiku_pic_half,w08_plus_pic_half,h08_hiku_pic) SELECT  width,height, width08, height08,date,delete_flag,h08_hiku_pic_half,w08_plus_pic_half,h08_hiku_pic FROM size_tate2_temp");
            db.execSQL("DROP TABLE IF EXISTS size_tate2_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE size_tate2 ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER,date INTEGER,delete_flag INTEGER,h08_hiku_pic_half INTEGER,w08_plus_pic_half INTEGER ,h08_hiku_pic INTEGER)");
        }


//        // size_land
//        db.execSQL("DROP TABLE IF EXISTS size_land");
//        db.execSQL("CREATE TABLE size_land ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER, width08 INTEGER, height08 INTEGER,time INTEGER,delete_flag INTEGER )");
//        // size_canvas_tate
//        db.execSQL("DROP TABLE IF EXISTS size_canvas_tate");
//        db.execSQL("CREATE TABLE size_canvas_tate ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER,time INTEGER,delete_flag INTEGER)");
//        // size_canvas_land
//        db.execSQL("DROP TABLE IF EXISTS size_canvas_land");
//        db.execSQL("CREATE TABLE size_canvas_land ( id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER,time INTEGER,delete_flag INTEGER)");


        // gc
        try {
            // db.rawQuery("SELECT 1 FROM gc LIMIT 1", null);
            db.execSQL("CREATE TABLE gc_temp AS SELECT * FROM gc");
            db.execSQL("DROP TABLE IF EXISTS gc");
            db.execSQL("CREATE TABLE gc (id INTEGER PRIMARY KEY AUTOINCREMENT,gc TEXT,date DATETIME,flag INTEGER)");
            db.execSQL("INSERT INTO gc (id,gc,date,flag) SELECT id,gc,date,flag FROM gc_temp");
            db.execSQL("DROP TABLE IF EXISTS gc_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE gc (id INTEGER PRIMARY KEY AUTOINCREMENT,gc TEXT,date DATETIME,flag INTEGER)");
        }


        // gc_ft
        try {
            // db.rawQuery("SELECT 1 FROM gc_ft LIMIT 1", null);
            db.execSQL("CREATE TABLE gc_ft_temp AS SELECT * FROM gc_ft");
            db.execSQL("DROP TABLE IF EXISTS gc_ft");
            db.execSQL("CREATE TABLE gc_ft (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,from_gc INTEGER,to_gc TEXT,date DATETIME)");
            db.execSQL("INSERT INTO gc_ft (start_id,from_gc,to_gc,date) SELECT start_id,from_gc,to_gc,date FROM gc_ft_temp");
            db.execSQL("DROP TABLE IF EXISTS gc_ft_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE gc_ft (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,from_gc INTEGER,to_gc TEXT,date DATETIME)");
        }


//        // gc_save
//        db.execSQL("DROP TABLE IF EXISTS gc_save");
//        db.execSQL("CREATE TABLE gc_save (id INTEGER PRIMARY KEY AUTOINCREMENT,time DATETIME,gc TEXT,flag INTEGER)");

        // pass
        try {
            // db.rawQuery("SELECT 1 FROM pass LIMIT 1", null);
            db.execSQL("CREATE TABLE pass_temp AS SELECT * FROM pass");
            db.execSQL("DROP TABLE IF EXISTS pass");
            db.execSQL("CREATE TABLE pass(id INTEGER PRIMARY KEY AUTOINCREMENT,pass TEXT,date DATETIME,delete_flag INTEGER)");
            db.execSQL("INSERT INTO pass(id,pass,date,delete_flag) SELECT id,pass,date,delete_flag FROM pass_temp");
            db.execSQL("DROP TABLE IF EXISTS pass_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE pass(id INTEGER PRIMARY KEY AUTOINCREMENT,pass TEXT,date DATETIME,delete_flag INTEGER)");
        }


        // m_res_no
        try {
            // db.rawQuery("SELECT 1 FROM m_res_no LIMIT 1", null);
            db.execSQL("CREATE TABLE m_res_no_temp AS SELECT * FROM m_res_no");
            db.execSQL("DROP TABLE IF EXISTS m_res_no");
            db.execSQL("CREATE TABLE m_res_no(id INTEGER PRIMARY KEY AUTOINCREMENT,aa_name TEXT,aa_res_no TEXT)");
            db.execSQL("INSERT INTO m_res_no(id,aa_name,aa_res_no) SELECT id,aa_name,aa_res_no FROM m_res_no_temp");
            db.execSQL("DROP TABLE IF EXISTS m_res_no_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE m_res_no(id INTEGER PRIMARY KEY AUTOINCREMENT,aa_name TEXT,aa_res_no TEXT)");
        }


        // setting
        try {
            // db.rawQuery("SELECT 1 FROM setting LIMIT 1", null);
            db.execSQL("CREATE TABLE setting_temp AS SELECT * FROM setting");
            db.execSQL("DROP TABLE IF EXISTS setting");
            db.execSQL("CREATE TABLE setting (id INTEGER PRIMARY KEY AUTOINCREMENT, alpha INTEGER, alpha01 TEXT, black INTEGER, touch_ok INTEGER,black_seek INTEGER,alpha_seek INTEGER,rooms_list_position INTEGER)");
            db.execSQL("INSERT INTO setting(id,alpha,alpha01,black,touch_ok,black_seek,alpha_seek,rooms_list_position) SELECT id,alpha,alpha01,black,touch_ok,black_seek,alpha_seek,rooms_list_position FROM setting_temp");
            db.execSQL("DROP TABLE IF EXISTS setting_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE setting (id INTEGER PRIMARY KEY AUTOINCREMENT, alpha INTEGER, alpha01 TEXT, black INTEGER, touch_ok INTEGER,black_seek INTEGER,alpha_seek INTEGER,rooms_list_position INTEGER)");
        }

        // current_status
        try {
//            // db.rawQuery("SELECT 1 FROM current_status LIMIT 1", null);
//            db.execSQL("CREATE TABLE current_status_temp AS SELECT * FROM current_status");
            db.execSQL("DROP TABLE IF EXISTS current_status");
//            db.execSQL("CREATE TABLE current_status ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, person_id INTEGER,comment_id INTEGER,start_time DATETIME, login_time DATETIME, room_enter_time, room_exit_time)");
//            db.execSQL("INSERT INTO current_status(id,start_id,login_id,room_id,person_id,comment_id,start_time,login_time,room_enter_time,room_exit_time) SELECT id,start_id,login_id,room_id,person_id,start_time,login_time,room_enter_time,room_exit_time FROM current_status_temp");
            db.execSQL("DROP TABLE IF EXISTS current_status_temp");
        } catch (Exception e) {
            e.printStackTrace();
//            db.execSQL("CREATE TABLE current_status ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, start_time DATETIME, login_time DATETIME, room_enter_time, room_exit_time)");
        }
//        db.execSQL("DROP TABLE IF EXISTS current_status");
//        db.execSQL("CREATE TABLE current_status ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER, login_id INTEGER, room_id INTEGER, start_time DATETIME, login_time DATETIME, room_time)");


        // current_dialog_page
        try {
            // db.rawQuery("SELECT 1 FROM current_dialog_page LIMIT 1", null);
            db.execSQL("CREATE TABLE current_dialog_page_temp AS SELECT * FROM current_dialog_page");
            db.execSQL("DROP TABLE IF EXISTS current_dialog_page");
            db.execSQL("CREATE TABLE current_dialog_page (id INTEGER PRIMARY KEY AUTOINCREMENT, page INTEGER)");
            db.execSQL("INSERT INTO current_dialog_page(id,page) SELECT id,page FROM current_dialog_page_temp");
            db.execSQL("DROP TABLE IF EXISTS current_dialog_page_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE current_dialog_page (id INTEGER PRIMARY KEY AUTOINCREMENT, page INTEGER)");
        }


        // room_status
        try {
            // db.rawQuery("SELECT 1 FROM room_status LIMIT 1", null);
            db.execSQL("CREATE TABLE room_status_temp AS SELECT * FROM room_status");
            db.execSQL("DROP TABLE IF EXISTS room_status");
            db.execSQL("CREATE TABLE room_status ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER, room_scroll_y INTEGER, date DATETIME )");
            db.execSQL("INSERT INTO room_status(id,start_id,login_id,room_scroll_y,date) SELECT id,start_id,login_id,room_scroll_y,date FROM room_status_temp");
            db.execSQL("DROP TABLE IF EXISTS room_status_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE room_status ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,login_id INTEGER, room_scroll_y INTEGER, date DATETIME )");
        }


        // p_st
        try {
            // db.rawQuery("SELECT 1 FROM p_st LIMIT 1", null);
            db.execSQL("CREATE TABLE p_st_temp AS SELECT * FROM p_st");
            db.execSQL("DROP TABLE IF EXISTS p_st");
            db.execSQL("CREATE TABLE p_st ( id INTEGER PRIMARY KEY AUTOINCREMENT,start_id INTEGER, pst TEXT, date DATETIME)");
            db.execSQL("INSERT INTO p_st(id,start_id, pst, date) SELECT id,start_id, pst, date FROM p_st");
            db.execSQL("DROP TABLE IF EXISTS p_st_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE p_st ( id INTEGER PRIMARY KEY AUTOINCREMENT,start_id INTEGER, pst TEXT, date DATETIME)");
        }


        /**
         * pic_size
         */
        try {
            // db.rawQuery("SELECT 1 FROM pic_size LIMIT 1", null);
            db.execSQL("CREATE TABLE pic_size_temp AS SELECT * FROM pic_size");
            db.execSQL("DROP TABLE IF EXISTS pic_size");
            db.execSQL("CREATE TABLE `pic_size` (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,pic_width INTEGER, pic_height INTEGER)");
            db.execSQL("INSERT INTO pic_size (id,start_id,pic_width,pic_height) SELECT id,start_id,pic_width,pic_height FROM pic_size_temp");
            db.execSQL("DROP TABLE IF EXISTS pic_size_temp");
        } catch (Exception e) {
            db.execSQL("CREATE TABLE `pic_size` (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,pic_width INTEGER, pic_height INTEGER)");
        }

        /**
         * pic_size_save
         */
        try {
            db.execSQL("DROP TABLE IF EXISTS pic_size_save");
            db.execSQL("CREATE TABLE `pic_size_save` (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,pic_width INTEGER, pic_height INTEGER)");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        /**
//         * current_status_first
//         */
//        try {
//            db.execSQL("DROP TABLE IF EXISTS current_status_first");
//            db.execSQL("DROP TABLE IF EXISTS current_status_init");
////            db.execSQL("CREATE TABLE `current_status_init` (id INTEGER PRIMARY KEY AUTOINCREMENT,start_id_init INTEGER, login_id_init INTEGER, room_id_init INTEGER, comment_id_init INTEGER)");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        try {
            db.execSQL("DROP TABLE IF EXISTS character_no_on_page");
            db.execSQL("CREATE TABLE `character_no_on_page` (id INTEGER PRIMARY KEY AUTOINCREMENT, page INTEGER,character_no INTEGER)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.execSQL("INSERT INTO character_no_on_page(page,character_no) VALUES(1,1)");
    }
}
