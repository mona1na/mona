package com.devmona;


import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devmona.loadercallbacks.LoaderCallbacksSendMessage15;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {


    private EditText editText;

    public MessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        editText = (EditText) view.findViewById(R.id.message_edit);
//        Button btnClose = (Button) view.findViewById(R.id.message_btnClose);
//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Base2Activity activity = (Base2Activity) getActivity();
//                if (activity != null) {
//                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
//                    fragmentTransaction.remove(MessageFragment.this).commit();
//                    if (activity != null) {
//                        activity.getSupportFragmentManager().beginTransaction().remove(MessageFragment.this).commit();
//                        TabLayout tabLayout = activity.findViewById(R.id.tabLayout);
//                        TabLayout.Tab tab = tabLayout.getTabAt(0);
//                        tab.select();
//                    }
//                }
//            }
//        });

        Button button = view.findViewById(R.id.message_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText() == null) {
                    return;
                }
                String message = editText.getText().toString();
                if (!message.equals("")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("message", editText.getText().toString());
                    Base2Activity nameActivity = (Base2Activity) getContext();
                    if (nameActivity != null) {
                        nameActivity.getSupportLoaderManager().initLoader(15, bundle, new LoaderCallbacksSendMessage15(nameActivity));
                    }
                } else {
                    Toast.makeText(getContext(), "メッセージが空です。", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }


}
