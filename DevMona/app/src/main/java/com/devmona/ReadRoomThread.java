package com.devmona;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ReadRoomThread extends ReadAbstract {
    public static final String TAG = "ReadRoomThread";

    public ReadRoomThread(Context context, boolean reConnecting) {
        super(context, reConnecting);
        this.context = context;
        this.reConnecting = reConnecting;
    }

    class MyRunnable extends Thread {
        private RoomsFragment roomsFragment;
        int bunki;
        public static final String TAG = "MyRunnable";
        private LinkedHashMap<Integer, Room> roomsMap;
        private String log;

        public MyRunnable(int bunki, RoomsFragment roomsFragment) {
            Log.d(TAG, "const");
            this.bunki = bunki;
            this.roomsFragment = roomsFragment;
        }

        @Override
        public void run() {
            super.run();
            synchronized (this) {
                Log.d(TAG, "run");
                if (roomsFragment != null) {
                    Log.d(TAG, "roomsFragment not null");
                    roomsFragment.RoomsSet(roomsMap);
                } else {
                    Log.d(TAG, "roomsFragment null");
                }


                this.notify();
            }
        }

        public void roomsSet(LinkedHashMap<Integer, Room> roomsMap) {
            this.roomsMap = roomsMap;
        }

        public void logSet(String log) {
            this.log = log;
        }
    }




    @Override
    public void run() {
        super.run();
        while (running) {
            ArrayList result;
            result = abstractRead1();
            if (result == null) {
                Log.d(TAG, "run result null");
                break;
            }
            if (result.size() != 0) {
                testRoomRead2(context, result);
            }
        }
        Log.d(TAG, "finish run");
    }


    /**
     * 受信結果のstringを処理するメソッド(loading)
     *
     * @param result
     */

    /**
     * MojaChat送信
     * メッセージの読み込み
     * ここで処理するXMLとして
     * ・+connect id=xxxxx (受信)
     * :TODO コネクションタイムアウトの処理
     * ①一発目受信するもの
     * ・<CONNECT id="11820" />
     * ②ルームリストの待機情報
     * ・<ROOM><USER red="100" name="名無しさん" id="12166" ihash="ToeEwoeab0" stat="通常" green="100" type="mona" blue="100" y="275" x="100" scl="100" /><USER ihash="gCuggdTwA4" name="Selcia" trip="YHydpHyFUM" id="11420" /></ROOM>
     * ③自分の情報
     * ・<UINFO name="名無しさん" id="11820" />
     * ④ルームリストの部屋の情報
     * ・<COUNT c="3" n="MONA8094"><ROOM c="0" n="1411" /><ROOM c="0" n="1410" /><ROOM c="0" n="75593" /><ROOM c="0" n="1415" /><ROOM c="0" n="1414" /><ROOM c="0" n="1412" /><ROOM c="0" n="1419" /><ROOM c="0" n="1417" /><ROOM c="0" n="1416" /><ROOM c="0" n="1821" /><ROOM c="0" n="1822" /><ROOM c="0" n="1820" /><ROOM c="0" n="1823" /><ROOM c="0" n="1824" /><ROOM c="0" n="1829" /><ROOM c="0" n="1827" /><ROOM c="0" n="1828" /><ROOM c="0" n="1420" /><ROOM c="0" n=" " /><ROOM c="0" n="1422" /></COUNT>
     * ⑤退出id
     * ・<EXIT id="111" />
     * ⑥部屋単体の入退室
     * ・<COUNT><ROOM c="7" n="99"/></COUNT>
     * ⑦部屋に入ったときの部屋番号と部屋ナンバー
     * <COUNT c="2" n="16" />\0
     * <p>
     * ・<SET x="101" scl="100" id="11881" y="320" />
     * ・<SET stat="あいうえお" id="627" />
     * <p>
     * ・<COM cmt="activity_characters" cnt="2" id="11719" />
     * ⑧例えば86の部屋に入室している際に、他の人が入室してきた際の送られてくるxml
     * ・<ENTER red="100" name="." id="11719" ihash="F/uM83OoUA" stat="......" green="100" type="hiyoko" blue="100" y="250" x="558" scl="100" />
     * <p>
     * //     * @param arrayList <p>
     * 受信結果のstringを処理するメソッド(loading)
     *
     * @param result <p>
     *               受信結果のstringを処理するメソッド(loading)
     * @param result
     */

    public synchronized void testRoomRead2(final Context context, ArrayList result) {

        for (int i = 0; i < result.size(); i++) {
            String str = (String) result.get(i);
            Log.i(TAG + " receive", str);

            if (checkConnectId(str)) {
                MyThread myThread = TestConnect.getInstance(context).myThread;
                synchronized (myThread) {
                    myThread.notify();
                    reConnecting = false;
                    Log.d("reConnectingFlag", reConnecting+ "");
                }
                continue;
            }

            if (checkTimeout(str)) {
                /**
                 * 再接続する
                 */
                Log.d(TAG, "checkTimeout");
                return;
            }

            XmlPullParser xpp = null;
            try {
                xpp = xmlPullParserGet(str);
            } catch (MyException e) {
                Log.d(TAG, "xmlPullParserGet exception");
                continue;
            }
            int eventType = -2;
            try {
                eventType = xpp.getEventType();
            } catch (XmlPullParserException e) {
                Log.d(TAG, "XmlPullParserException e");
                continue;
            }

            String elementName = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                elementName = xpp.getName();
                if (eventType == XmlPullParser.START_TAG) {
                    break;
                }
                try {
                    eventType = xpp.next();

                } catch (XmlPullParserException e) {
                    return;
                } catch (IOException e) {
                    return;
                }
            }

            if (elementName == null) {
                continue;
            }

            if (elementName.equals("CONNECT")) {
                String strId = xpp.getAttributeValue(null, "id");
                try {
                    int monaId = Integer.parseInt(strId);
                    /**
                     *  database accessを減らす為、monaIdに
                     */
                    Log.d(TAG, "CONNECT : setMonaId:" + monaId);
                    TestShared testShared = TestShared.getInstance(context);
                    testShared.setMeMonaId(monaId);
                } catch (NumberFormatException e) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("error", "rCO0x11");
                    } catch (JSONException e1) {
                        e.printStackTrace();
                    }
                    return;
                }
            } else if (elementName.equals("COUNT")) {
                //     * ・<COUNT c="3" n="MONA8094"><ROOM c="0" n="1411" /><ROOM c="0" n="1410" /><ROOM c="0" n="75593" /><ROOM c="0" n="1415" /><ROOM c="0" n="1414" /><ROOM c="0" n="1412" /><ROOM c="0" n="1419" /><ROOM c="0" n="1417" /><ROOM c="0" n="1416" /><ROOM c="0" n="1821" /><ROOM c="0" n="1822" /><ROOM c="0" n="1820" /><ROOM c="0" n="1823" /><ROOM c="0" n="1824" /><ROOM c="0" n="1829" /><ROOM c="0" n="1827" /><ROOM c="0" n="1828" /><ROOM c="0" n="1420" /><ROOM c="0" n=" " /><ROOM c="0" n="1422" /></COUNT>
                //     * ・<COUNT><ROOM c="7" n="99"/></COUNT>
//                Log.d("xppTest", str);

                String roomName = xpp.getAttributeValue(null, "n");
                if (roomName == null) {
                    /**
                     * <COUNT><ROOM c="7" n="99"/></COUNT>
                     */
                    if (base2Activity != null) {
                        LinkedHashMap<Integer, Room> roomsMap = COUNTRoomsSet(eventType, xpp);
                        /**
                         * roomsFragmentをゲットする
                         */
                        RoomsFragment roomsFragment = UtilInfo.getRoomsFragment(context);
                        MyRunnable myRunnable = new MyRunnable(3, roomsFragment);
                        synchronized (myRunnable) {
                            try {
                                Log.d(TAG, "synchronized start");
//                                RoomsDB roomsDB = new RoomsDB();
//                                LinkedHashMap<Integer, Room> roomsMap = roomsDB.getRoomsMapUpdate();
                                myRunnable.roomsSet(roomsMap);
                                myRunnable.logSet("COUNT 部屋待機");
                                base2Activity.runOnUiThread(myRunnable);
                                try {
                                    myRunnable.wait();
                                    Log.d(TAG, "wait count finish");
//                                    roomsFragment.RoomsUpdate();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } catch (NullPointerException e) {
                                Log.d("read2thread", "rooms count not show2");
                            }
                        }
                    }


                } else if (roomName.equals(context.getString(R.string.mona8094))) {
                    Log.d(TAG, "roomName.equals mona8094");
                    if (base2Activity == null) {
                        continue;
                    }
                    try {
                        /**
                         * important method COUNTRoomsSet 1
                         */
                        LinkedHashMap<Integer, Room> roomsMap =
                                COUNTRoomsSet(eventType, xpp);
                        /**
                         * roomsFragmentをゲットする
                         */
                        RoomsFragment roomsFragment = UtilInfo.getRoomsFragment(context);

                        MyRunnable myRunnable = new MyRunnable(3, roomsFragment);
                        synchronized (myRunnable) {
                            try {
                                Log.d(TAG, "synchronized start");
                                myRunnable.roomsSet(roomsMap);
                                myRunnable.logSet("COUNT MON8094");
                                base2Activity.runOnUiThread(myRunnable);
                                try {
                                    myRunnable.wait();
                                    Log.d(TAG, "wait finish");
//                                    roomsFragment.RoomsUpdate();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            } catch (NullPointerException e) {
                                Log.d(TAG, "rooms count not show");
                            }
                        }

                    } catch (ClassCastException e) {
                        continue;
                    } catch (NullPointerException e) {
                        continue;
                    } catch (MyException e) {
                        /**
                         * todo 2018 0327壊れた形式の問題
                         * <COUNT c="4" n="MONA8094"><ROOM c="0" n="1111" /><ROOM c="0" n="w00" /><ROOM c="0" n="90" /><ROOM c="0" n="499" /><ROOM c="0" n="2４" /><ROOM c="0" n="やまなし" /><ROOM c="0" n="4567" /><ROOM c="0" n="291" /><ROOM c="0" n="10024" /><ROOM c="0" n="3０" /><<EXIT id="564" />
                         */
                        continue;
                    }
//                    ProgressFragment progressFragment = UtilInfo.getProgressFragment(context);
//                    if (progressFragment != null) {
//                        Base2Activity base2Activity = (Base2Activity) context;
//                        Log.d(TAG, "progress remove");
//                        base2Activity.getSupportFragmentManager().beginTransaction().remove(progressFragment).commit();
//                    }
                }


            } else if (elementName.equals(UtilInfo.ENTER)) {
                String strId = xpp.getAttributeValue(null, "id");
                if (strId != null) {
                    int id = Integer.parseInt(strId);
                    if (id == MeMonaId.getMe().getMonaId()) {
//                        final ArrayList<TestIdXML> testIdXMLS = memberSet2(context, xpp, eventType);
//                        if (testIdXMLS == null) {
//                            continue;
//                        }
                        Log.d(TAG, "enter id");
                        ProgressFragment progressFragment = UtilInfo.getProgressFragment(context);
                        if (progressFragment != null) {
                            Base2Activity base2Activity = (Base2Activity) context;
                            Log.d(TAG, "progress remove");
                            base2Activity.getSupportFragmentManager().beginTransaction().remove(progressFragment).commit();
                        } else {
                            Log.d(TAG, "progress null");
                        }

                    }


                }
            }

        }
    }

    private LinkedHashMap<Integer, Room> COUNTRoomsSet(int eventType, XmlPullParser xpp) throws MyException {
        LinkedHashMap<Integer, Room> roomsMap = new LinkedHashMap<>();
        while (eventType != XmlPullParser.END_DOCUMENT) {

            if (eventType == XmlPullParser.START_TAG) {
                String name = xpp.getName();
                // 待機部屋の部屋人数を取得する
                if (name.equals("ROOM")) {
                    String strRoomNo = xpp.getAttributeValue(null, "n");
                    String strRoomCount = xpp.getAttributeValue(null, "c");
                    /*
                    バリデーション
                     */
                    RoomValidation roomValidation = new RoomValidation();
                    try {
                        roomValidation.roomCheck(strRoomNo, strRoomCount);
                    } catch (MyException e) {
                        try {
                            eventType = xpp.next();
                        } catch (XmlPullParserException e2) {
                            throw new MyException("ERROR: co0x11");
                        } catch (IOException e2) {
                            throw new MyException("ERROR: co0x12");
                        }
                        continue;
                    } catch (NumberFormatException e) {
                        try {
                            eventType = xpp.next();
                        } catch (XmlPullParserException e2) {
                            throw new MyException("ERROR: co0x13");
                        } catch (IOException e2) {
                            throw new MyException("ERROR: co0x14");
                        }
                        continue;
                    }
                    //-------------------------------------------------


                    final int roomNo = Integer.parseInt(strRoomNo);
                    int roomCount = Integer.parseInt(strRoomCount);

                    String roomName = mAppli.getRoomName(roomNo);

                    Room room = new Room();
                    room.setRoomNumber(roomNo);
                    room.setRoomCount(roomCount);
                    room.setRoomName(roomName);
                    roomsMap.put(roomNo, room);
//                    Room room = new Room();
//                    MyApplication.roomsMap.get(roomNo).setRoomCount(roomCount);
//                    room.setRoomName(roomName);
//                    room.setRoomCount(roomCount);
//                    room.setRoomNumber(roomNo);

//                    roomsMap.put(roomNo, room);
                }
            }
            try {
                eventType = xpp.next();
            } catch (XmlPullParserException e) {
                // TODO 壊れた形式で来るので後で考える必要がある
                throw new MyException("ERROR: co0x13");
            } catch (IOException e) {
                throw new MyException("ERROR: co0x13");
            }
        }
        return roomsMap;
    }
}
