package com.devmona.extendsviews;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.View;

/**
 * Created by sake on 2018/04/18.
 */

public class CustomImageView extends AppCompatImageView {

    private ClipAnimation clipAnimation;

    // progress for animation
    private float progress = 1f;

    public CustomImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * getter for ObjectAnimator
     */
    public float getProgress() {
        return progress;
    }

    /**
     * setter for ObjectAnimator
     */
    public void setProgress(float progress) {
        this.progress = progress;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (clipAnimation != null) {
            canvas.clipPath(clipAnimation.createPath(progress, getWidth(), getHeight()));
        }
        super.onDraw(canvas);
    }

    public void show(ClipAnimation clipAnimation) {

        this.clipAnimation = clipAnimation;

        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "progress", 0, 1f);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                setVisibility(View.VISIBLE);
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                invalidate();
            }
        });
        animator.setDuration(2000);
        animator.start();
    }

    public void hide(ClipAnimation clipAnimation) {

        this.clipAnimation = clipAnimation;

//        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "progress", 1f, 0);
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                setVisibility(View.INVISIBLE);
//            }
//        });
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                invalidate();
//            }
//        });
//        animator.setDuration(3000);
//        animator.start();
    }
}
