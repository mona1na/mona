package com.devmona.extendsviews;

import android.graphics.Path;

/**
 * Created by sake on 2018/04/18.
 */

public class TopToBottomClipAnimation implements ClipAnimation{
    @Override
    public Path createPath(float progress, int width, int height) {
        Path path = new Path();
        path.addRect(0, 0, width, height * progress, Path.Direction.CCW);
        return path;
    }
}
