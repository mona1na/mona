package com.devmona.extendsviews;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.devmona.SizeTate2;

/**
 * Created by sake on 2018/05/22.
 */

public class MyFrameLayout extends FrameLayout{
    private SizeTate2 sizeTate2;
//    private Main2DB main2DB;

    public MyFrameLayout(Context context) {
        super(context);
    }

    public MyFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }



//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        super.onTouchEvent(event);
//
////        switch (event.getAction())
//
//
//        performClick();
//        main2DB = new Main2DB();
//        sizeTate2 = main2DB.getSizeTate2();
//        int x;
//        int y;
//
//        int cx2;
//        int cy2;
//        if (event.getX() >= sizeTate2.getWidth08()) {
//            cx2 = sizeTate2.getWidth08();
//        } else {
//            cx2 = (int) event.getX();
//        }
//
//        if (event.getY() >= sizeTate2.getH08HikuPicHalf()) {
//            cy2 = sizeTate2.getH08HikuPicHalf();
//        } else {
//            cy2 = (int) event.getY();
//        }
//        x = convertX(cy2, sizeTate2.getH08HikuPicHalf());
//        y = convertY(cx2, sizeTate2.getWidth08());
//
//        main2DB.updateXYCXCY(x, y, cx2, cy2);
//        Bundle bundle = new Bundle();
//        bundle.putInt("x",x);
//        bundle.putInt("y",y);
//        Main3Activity main3Activity = (Main3Activity) getContext();
//        main3Activity.getSupportLoaderManager().initLoader(10, bundle, new LoaderCallbacksSendXY(main3Activity));
//
//        /**
//         * x,yを送信する
//         */
//        return true;
//    }

    /**
     * キャンバスcx2の値をYに変換する
     *
     * @param cx2
     * @param M
     * @return 本当のY
     */
    private int convertY(int cx2, int M) {
        int y;

        y = -80 * cx2 / M + 320;

        return y;
    }

    /**
     * キャンバスcy2の値をXに変換する
     *
     * @param cy2
     * @param M
     * @return 本当のX
     */
    private int convertX(int cy2, int M) {
        int x;

        x = 660 * cy2 / M + 30;

        return x;
    }

    public void init() {
//        main2DB = new Main2DB();
//        sizeTate2 = main2DB.getSizeTate2();
    }

}
