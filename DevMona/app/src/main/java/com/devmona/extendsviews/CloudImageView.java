package com.devmona.extendsviews;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.devmona.R;

/**
 * Created by sake on 2018/04/19.
 */

public class CloudImageView extends AppCompatImageView {
    private final CloudImageView cloudImageView;

    public CloudImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.cloudImageView = this;
    }

    public void show() {
        Animator animator = AnimatorInflater.loadAnimator(getContext(), R.animator.cloud);

        animator.setTarget(this);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                cloudImageView.setVisibility(VISIBLE);
                cloudImageView.setPivotY(200f);
                cloudImageView.setPivotX(100f);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                cloudImageView.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animator.start();
    }


    public void show(final FrameLayout frameTate2, final LinearLayout linearLayout2) {
        this.setPivotY(200f);
        this.setPivotX(100f);
        Animator animator = AnimatorInflater.loadAnimator(getContext(), R.animator.cloud);
        animator.setTarget(this);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                cloudImageView.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                cloudImageView.setVisibility(INVISIBLE);
                frameTate2.removeView(linearLayout2);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animator.start();
    }
}
