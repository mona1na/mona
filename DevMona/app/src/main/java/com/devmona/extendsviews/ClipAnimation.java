package com.devmona.extendsviews;

import android.graphics.Path;

/**
 * Created by sake on 2018/04/18.
 */

public interface ClipAnimation {
    Path createPath(float progress, int width, int height);
}
