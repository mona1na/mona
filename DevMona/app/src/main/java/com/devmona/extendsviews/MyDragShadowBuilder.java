package com.devmona.extendsviews;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.View;

/**
 * Created by sake on 2018/05/27.
 */

public class MyDragShadowBuilder extends View.DragShadowBuilder {

    private Point mScaleFactor;
    private Intent intent;

    public MyDragShadowBuilder(View v) {
        super(v);
    }

    @Override
    public void onProvideShadowMetrics(Point outShadowSize, Point outShadowTouchPoint) {
        super.onProvideShadowMetrics(outShadowSize, outShadowTouchPoint);
        int width;
        int height;

        width = getView().getWidth();
        height = getView().getHeight();

        outShadowSize.set(width, height);
        mScaleFactor = outShadowSize;
//        outShadowTouchPoint.set(mScaleFactor.x / 2, mScaleFactor.y);
        outShadowTouchPoint.set(intent.getIntExtra("offsetX",0), intent.getIntExtra("offsetY",0));
//        outShadowTouchPoint.set(mScaleFactor.x, mScaleFactor.y);
    }

    @Override
    public void onDrawShadow(Canvas canvas) {
//        super.onDrawShadow(canvas);
        canvas.scale(mScaleFactor.x / (float) getView().getWidth(), mScaleFactor.y / (float) getView().getHeight());
        getView().draw(canvas);
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
