package com.devmona;

public class TestIdXML {

    private int uniqueId;
    private String xml;
    private int monaId;

    public TestIdXML(int uniqueId,String xml,int monaId) {
        this.uniqueId = uniqueId;
        this.xml = xml;
        this.monaId = monaId;
    }

    public int getMonaId() {
        return monaId;
    }

    public void setMonaId(int monaId) {
        this.monaId = monaId;
    }

    public int getUniqueId() {
        return uniqueId;
    }



    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
}
