package com.devmona;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.devmona.settingfragment.StateDialogFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;


public class Base2Activity extends AppCompatActivity{

    //    private Person me;
    private FrameLayout wholeView;
    private DrawerLayout drawerLayout;
    public static final String TAG = "Base2Activity";
    private NavigationView navigationView;
    private View headerView;
    private boolean first = true;
    private int tenclick = 0;
    private FrameLayout frameLayout;

    public static final int TAB_MAIN = 0;
    public static final int TAB_RANDOM = 1;
    public static final int TAB_MESSAGE = 2;
    public static final int TAB_FUNCTION = 3;
    public static final int TAB_SETTING = 4;
    private Bundle mSavedInstanceState;
    private Window mRootWindow;
    private View mRootView;
    private Base2Activity mActivity;
    public int mRootViewSize = -1;
    public int mRootViewSizeInnerSoft = -1;
    public int mRootViewSizeInnerSoftYosoku = -1;


    public void layoutChange() {
        mRootWindow = getWindow();
        mRootView = mRootWindow.getDecorView().findViewById(android.R.id.content);
        mRootView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        Rect r = new Rect();
                        View view = mRootWindow.getDecorView();
                        view.getWindowVisibleDisplayFrame(r);
                        if (mRootViewSize == -1) {
                            mRootViewSize = r.bottom;
                            TestShared.getInstance(mActivity).setRootViewSize(r.bottom);
                        } else if(mRootViewSize > r.bottom && mRootViewSizeInnerSoftYosoku == -1) {
                            Log.d("testSoftKey", "b:"+r.bottom + "");
                            mRootViewSizeInnerSoft = r.bottom;
                            mRootViewSizeInnerSoftYosoku = 0;
                            TestShared.getInstance(mActivity).setRootViewSizeInnerSoftKey(r.bottom);
                        } else if(mRootViewSizeInnerSoft > r.bottom) {
                            Log.d("testSoftKey", "c:"+r.bottom + "");
                            mRootViewSizeInnerSoftYosoku = r.bottom;
                            TestShared.getInstance(mActivity).setRootViewSizeInnerSoftKeyYosoku(r.bottom);
                        }


                        Log.d("testSoftKey", r.bottom + "");
                        // r.left, r.top, r.right, r.bottom
                    }
                });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedInstanceState = savedInstanceState;
        mActivity = this;
        Log.d("killTest", "onCreateBase2");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }

        layoutChange();
        setContentView(R.layout.activity_name);


        /**
         * ③初期化セット
         */
        initBase();
        findViews();
        /**
         * ④設定によってアタッチするfragmentを選択する
         */

        LinearLayout tabItem1 = (LinearLayout) getLayoutInflater().inflate(R.layout.tab_item, null);
        LinearLayout tabItem2 = (LinearLayout) getLayoutInflater().inflate(R.layout.tab_item, null);
        LinearLayout tabItem3 = (LinearLayout) getLayoutInflater().inflate(R.layout.tab_item, null);
        LinearLayout tabItem4 = (LinearLayout) getLayoutInflater().inflate(R.layout.tab_item, null);
        LinearLayout tabItem5 = (LinearLayout) getLayoutInflater().inflate(R.layout.tab_item, null);
        ImageView tabImage1 = (ImageView) tabItem1.findViewById(R.id.tab_item_image);
        ImageView tabImage2 = (ImageView) tabItem2.findViewById(R.id.tab_item_image);
        ImageView tabImage3 = (ImageView) tabItem3.findViewById(R.id.tab_item_image);
        ImageView tabImage4 = (ImageView) tabItem4.findViewById(R.id.tab_item_image);
        ImageView tabImage5 = (ImageView) tabItem5.findViewById(R.id.tab_item_image);
        TextView tabText1 = (TextView) tabItem1.findViewById(R.id.tab_item_text);
        TextView tabText2 = (TextView) tabItem2.findViewById(R.id.tab_item_text);
        TextView tabText3 = (TextView) tabItem3.findViewById(R.id.tab_item_text);
        TextView tabText4 = (TextView) tabItem4.findViewById(R.id.tab_item_text);
        TextView tabText5 = (TextView) tabItem5.findViewById(R.id.tab_item_text);
        tabText1.setText("main");
        tabText2.setText("random");
        tabText3.setText("message");
        tabText4.setText("function");
        tabText5.setText("setting");
        tabImage1.setImageDrawable(getDrawable(R.drawable.ic_view_array_50_gray));
        tabImage2.setImageDrawable(getDrawable(R.drawable.ic_person_50));
        tabImage3.setImageDrawable(getDrawable(R.drawable.ic_info_50));
        tabImage4.setImageDrawable(getDrawable(R.drawable.ic_widgets_50dp));
        tabImage5.setImageDrawable(getDrawable(R.drawable.ic_settings_50));
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setCustomView(tabItem1));
        tabLayout.addTab(tabLayout.newTab().setCustomView(tabItem2));
        tabLayout.addTab(tabLayout.newTab().setCustomView(tabItem3));
        tabLayout.addTab(tabLayout.newTab().setCustomView(tabItem4));
        tabLayout.addTab(tabLayout.newTab().setCustomView(tabItem5));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                int pos = tab.getPosition();
                if (TAB_MAIN == pos) {


                    MessageFragment messageFragment = UtilInfo.getMessageFragment(Base2Activity.this);
                    if (messageFragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(messageFragment).commit();
                        tab.select();
                        return;
                    }
                    RandomFragment randomFragment = UtilInfo.getRandomFragment(Base2Activity.this);
                    if (randomFragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(randomFragment).commit();
                        tab.select();
                        return;
                    }

                    NewFutureFragment newFutureFragment = UtilInfo.getNewFutureFragment(Base2Activity.this);
                    if (newFutureFragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(newFutureFragment).commit();
                        tab.select();
                        return;
                    }
                    SettingFragment settingFragment = UtilInfo.getSettingFragment(Base2Activity.this);
                    if (settingFragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(settingFragment).commit();
                        tab.select();
                        return;
                    }

                } else if (TAB_RANDOM == pos) {
                    RandomFragment randomFragment = new RandomFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                    fragmentTransaction.replace(R.id.main4_option2, randomFragment);
                    fragmentTransaction.commit();
                } else if (TAB_MESSAGE == pos) {
                    MessageFragment messageFragment = new MessageFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                    fragmentTransaction.replace(R.id.main4_option2, messageFragment);
                    fragmentTransaction.commit();
                } else if (TAB_FUNCTION == pos) {
                    NewFutureFragment newFutureFragment = new NewFutureFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                    fragmentTransaction.replace(R.id.main4_option2, newFutureFragment);
                    fragmentTransaction.commit();
                } else if (TAB_SETTING == pos) {
                    SettingFragment settingFragment = new SettingFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                    fragmentTransaction.replace(R.id.main4_option2, settingFragment);
                    fragmentTransaction.commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    private void fragment() {
        TestShared testShared = TestShared.getInstance(Base2Activity.this);
        TestSave.rappid = testShared.getRapidCheck();

        if (mSavedInstanceState == null) {
            if (TestSave.rappid) {
                Main4Fragment main4Fragment = new Main4Fragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                Room room = testShared.getRapidRoom();
                testShared.setRoom(room);
                fragmentTransaction.replace(R.id.main4_fragment, main4Fragment, getString(R.string.fragment_main4));
                fragmentTransaction.commit();
            } else {
                /**
                 * 通常入室
                 */
                Name2Fragment name2Fragment = new Name2Fragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2, R.animator.update_animator1, R.animator.chara_animator2);
                fragmentTransaction.replace(R.id.test_fragment, name2Fragment, getString(R.string.fragment_name));
                fragmentTransaction.commit();
            }
        } else {
            TestSave.rappid = false;
        }
    }

    private void initBase() {
        sharedSetting();
    }

    private void findViews() {
        wholeView = (FrameLayout) findViewById(R.id.name_whole);
        wholeView.post(new Runnable() {
            @Override
            public void run() {

            }
        });
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.setEnabled(false);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setEnabled(false);
        headerView = navigationView.getHeaderView(0);
        LinearLayout linearLayout = (LinearLayout) headerView.findViewById(R.id.nav_linear);
        linearLayout.setBackgroundColor(MeMonaId.getMe().getColorHukidashi());
        ImageView imageView = headerView.findViewById(R.id.nav_image_person1);
        /**
         * set listener
         */
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tenclick == 20) {
                    TestShared.getInstance(Base2Activity.this).setDevelop(true);
                    Toast.makeText(Base2Activity.this, "modeDev", Toast.LENGTH_SHORT).show();
                    Menu menu = navigationView.getMenu();
                    MenuItem menuDevelop = menu.findItem(R.id.nav_develop);
                    menuDevelop.setTitle("devlog");
                    menuDevelop.setEnabled(true);
                    tenclick++;
                } else if (tenclick < 20) {
                    tenclick++;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        try {

        } catch (AndroidRuntimeException e) {

        }
        super.onResume();
        Log.d("LifeTest Base2Activity", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("LifeTest Base2Activity", "onPause");
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.d(TAG, "onWindowFocusChanged");
        if (isFirst()) {
            Calc.onWindowFocusChanged(wholeView);
            Calc.updateSizeTate2();
            Calc.updatePixel();
            test2();
            first = false;
        } else {

        }

    }

    private void test2() {
        frameLayout = (FrameLayout) findViewById(R.id.name_whole);
        final RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.aa_name_trip_comment2, frameLayout, false);
        TextView textView = relativeLayout.findViewById(R.id.char_kurotori);
        textView.setText("test");
        ImageView imageView2 = (ImageView) relativeLayout.findViewById(R.id.char_image);
        relativeLayout.setVisibility(View.INVISIBLE);
        ViewGroup.LayoutParams layoutParams1 = imageView2.getLayoutParams();
        layoutParams1.height = Calc.aasizePixcel;
        layoutParams1.width = Calc.aasizePixcel;
        imageView2.setLayoutParams(layoutParams1);
        imageView2.setImageDrawable(ActivityCompat.getDrawable(this, R.drawable.ic_svg_mona));
        frameLayout.addView(relativeLayout);
        relativeLayout.post(new Runnable() {
            @Override
            public void run() {
                Log.d("viewposttest",                 relativeLayout.getHeight() +"");

            }
        });

        ViewTreeObserver viewTreeObserver2 = imageView2.getViewTreeObserver();
        viewTreeObserver2.addOnGlobalLayoutListener(new MyO2(imageView2, relativeLayout));
    }

    class MyO2 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {

        private ImageView imageView;
        private RelativeLayout relativeLayout;
        private boolean b = false;

        MyO2(ImageView imageView, RelativeLayout relativeLayout) {
            this.imageView = imageView;
            this.relativeLayout = relativeLayout;
        }

        @Override
        public void onGlobalLayout() {
            if (b == false) {
                Log.d("onG2 relative.getHeight", relativeLayout.getHeight() + "");
                Log.d("onG2 imageView.getHeigh", imageView.getHeight() + "");
                Calc.charWholePixcel = relativeLayout.getHeight();
                TestShared.getInstance(Base2Activity.this).setCharWholePixel(relativeLayout.getHeight());
                Log.d("killTest", "Calc.charWholePixcel:" + Calc.charWholePixcel);
                Calc.aasizePixcel = imageView.getHeight();
                TestShared.getInstance(Base2Activity.this).setAasizePixel(imageView.getHeight());
                fragment();
                b = true;
            }
        }
    }

    private boolean isFirst() {
        return first;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_sentaku_menu, menu);
        MenuItem setting = menu.findItem(R.id.action_setting);
        setting.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SettingFragment settingFragment = UtilInfo.getSettingFragment(Base2Activity.this);
                if (settingFragment != null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                    fragmentTransaction.remove(settingFragment).commit();
                } else {
                    settingFragment = new SettingFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                    fragmentTransaction.replace(R.id.main4_option2, settingFragment, getString(R.string.fragment_setting));
                    fragmentTransaction.commit();
                }

                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setting:
        }
        return true;
    }

    private void sharedSetting() {
        Log.d(TAG, "sharedSetting");

        TestShared testShared = TestShared.getInstance(Base2Activity.this);
        testShared.init();
        testShared.init2();

//        // aanameとaaResNo登録する
        UtilInfo.aaNameaaResNoMap = testShared.getaaNameaaResNoMap();
        MeMonaId.setMe(testShared.getMe());
        MeMonaId.notifyArray = testShared.getNotify();
    }

    /**
     * in navigationView sets roomName, roomCount, meId
     *
     * @param room
     */
    public void showRoomName(Room room) {
        if (room.getRoomNumber() == 13) {
            Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(this);
            View view = tate2Fragment.getView();
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.testDe_line);
            if (linearLayout.findViewById(R.id.tate2_image_seishin) == null) {
                ImageView seishinImage = (ImageView) LayoutInflater.from(this).inflate(R.layout.seishin_image, linearLayout, false);
                linearLayout.addView(seishinImage);
                tate2Fragment.seishinSet(seishinImage);
            }
        }

        Log.d(TAG, "showRoomName:" + room.getRoomName());
        ProgressFragment progressFragment = UtilInfo.getProgressFragment(Base2Activity.this);
        if (progressFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(progressFragment).commit();
        }
        TextView textView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_txt_room_name);
        if (room.getRoomName().equals("部屋選択")) {
            textView.setText(room.getRoomName());
        } else {
            textView.setText(room.getRoomName() + " : " + room.getRoomCount() + "人" + "(ID:" + MeMonaId.getMe().getMonaId() + ")");
        }
    }



    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.d(TAG, "onKeyDown");
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.app_finish))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AsyncTask asyncTask = new AsyncTask() {
                                @Override
                                protected Object doInBackground(Object[] objects) {
                                    synchronized (Base2Activity.this) {
                                        Base2Activity.this.notify();
                                    }
                                    return null;
                                }
                            };
                            synchronized (Base2Activity.this) {
                                asyncTask.execute();
                                try {
                                    Base2Activity.this.wait();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            Base2Activity.this.finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO 自動生成されたメソッド・スタブ

                        }
                    })
                    .show();

            return true;
        }
        return false;

    }

    public void test_click(View view) {
        Log.d(TAG, "call");
        try {
            SettingFragment settingFragment = UtilInfo.getSettingFragment(Base2Activity.this);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
            fragmentTransaction.remove(settingFragment).commit();
        } catch (NullPointerException e) {
            Log.d(TAG, "catch test_click");
        }
    }
}
