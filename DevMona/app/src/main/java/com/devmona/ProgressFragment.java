package com.devmona;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProgressFragment extends Fragment {


    public ProgressFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("lifeTest", "progress onCreateView");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_progress, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("lifeTest", "progress onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("lifeTest", "progress onPause");
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        Log.d("lifeTest", "progress onDestroy");
    }
}
