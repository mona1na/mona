package com.devmona;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

public class MyRandomLoader extends AsyncTaskLoader<Object> {
    private Bundle bundle;
    private Context context;

    public MyRandomLoader(@NonNull Context context, Bundle bundle) {
        super(context);
        this.context = context;
        this.bundle = bundle;
    }

    @Nullable
    @Override
    public Object loadInBackground() {
        int key = bundle.getInt("key");
        if (key == 19) {
            return JsonCreate.randomSend19(context,null);
        }
        return null;
    }
    @Override
    protected void onStartLoading() {
        Log.v("MyRandomLoader", "onStartLoading");
        forceLoad();
    }
}
