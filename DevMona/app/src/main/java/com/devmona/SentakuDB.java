package com.devmona;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.wifi.ScanResult;
import androidx.core.app.ActivityCompat;

import net.sqlcipher.database.SQLiteException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Created by sake on 2018/03/13.
 */

public class SentakuDB {

    public void insertStartId() {
        String SQL = "INSERT INTO `start`(date) ";
        String VALUES = "VALUES(?)";
        String[] s = new String[]{
                UtilInfo.getNowDate()
        };
        try {
            UtilInfo.m_db.execSQL(SQL + VALUES, s);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public int getStartId() {
        String SQL = "SELECT MAX(id) FROM `start`";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        cursor.moveToFirst();

        int max = cursor.getInt(0);
//        int startId = -1;
//        while (next) {
//            startId = cursor.getInt(cursor.getColumnIndex("id"));
//            next = cursor.moveToNext();
//        }
        cursor.close();
        return max;
    }

//    public Person getSentakuMe() {
//        String sql = "SELECT * FROM sentaku_me where id = (SELECT MAX(id) from sentaku_me)";
//        Cursor cursor = UtilInfo.m_db.rawQuery(sql, null);
//        boolean next = cursor.moveToFirst();
////        HashMap<String, Object> hashMap = new HashMap<>();
//        Person person = new Person();
//        person.setId(-1);
//        while (next) {
//            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
//            person.setName(cursor.getString(cursor.getColumnIndex("name")));
//
//
//            person.setTrip(cursor.getString(cursor.getColumnIndex("trip")));
//            person.setColor255(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
//            person.setRed(cursor.getInt(cursor.getColumnIndex("red")));
//            person.setGreen(cursor.getInt(cursor.getColumnIndex("green")));
//            person.setBlue(cursor.getInt(cursor.getColumnIndex("blue")));
//            person.setScl(cursor.getInt(cursor.getColumnIndex("scl")));
////            person.setX(cursor.getInt(cursor.getColumnIndex("x")));
////            person.setX(cursor.getInt(cursor.getColumnIndex("y")));
//            Random random = new Random();
//            person.setX(random.nextInt(660) + 30);
//            person.setToY(random.nextInt(80) + 240);
//            /**
//             * 初期処理でaanameにaaresnoをマッピングした変数を使用する。
//             * because of changing aaresno every time
//             */
//            person.setAaresno(Integer.parseInt(UtilInfo.aaNameaaResNoMap.get(cursor.getString(cursor.getColumnIndex("aa_name")))));
//            person.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
//            person.setStat("通常");
//            next = cursor.moveToNext();
//        }
//        if (person.getId() == -1) {
//            person.setId(0);
//            person.setName("名無しさん");
//            person.setTrip(null);
//            person.setColor255(0xFFFFFFFF);
//            person.setRed(100);
//            person.setGreen(100);
//            person.setBlue(100);
//            person.setAaresno(R.drawable.ic_svg_mona);
//            person.setAaname("mona");
//            person.setStat("通常");
//            person.setScl(0);
//            Random random = new Random();
//            person.setX(random.nextInt(660) + 30);
//            person.setToY(random.nextInt(80) + 240);
//        }
//        cursor.close();
//        return person;
//    }

    public Person getMe() {
        String sql = "SELECT * FROM person where is_me = 1";
        Cursor cursor = UtilInfo.m_db.rawQuery(sql, null);
        boolean next = cursor.moveToFirst();
        Person person = new Person();
        person.setId(-1);
        while (next) {
            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
            person.setName(cursor.getString(cursor.getColumnIndex("name")));
            person.setColor(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
            person.setRed(cursor.getInt(cursor.getColumnIndex("red")));
            person.setGreen(cursor.getInt(cursor.getColumnIndex("green")));
            person.setBlue(cursor.getInt(cursor.getColumnIndex("blue")));
            person.setAaresno(cursor.getInt(cursor.getColumnIndex("aa_res_no")));
            person.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
            person.setScl(cursor.getInt(cursor.getColumnIndex("scl")));
            person.setX(cursor.getInt(cursor.getColumnIndex("x")));
            person.setX(cursor.getInt(cursor.getColumnIndex("y")));
            next = cursor.moveToNext();
        }
        if (person.getId() == -1) {
            person.setId(0);
            person.setName("名無しさん");
            person.setColor(0xFFFFFF);
            person.setRed(100);
            person.setGreen(100);
            person.setBlue(100);
//            person.setAaresno(R.mipmap.mona);
            person.setAaname("mona");
            person.setScl(100);
            // x30 x690 y320 y240
            Random random = new Random();
            person.setX(random.nextInt(660) + 30);
            person.setY(random.nextInt(80) + 240);
        }
        cursor.close();
        return person;
    }

    public void insertSentakuMe(Person person, int startId) {
        String SQL = "INSERT INTO `sentaku_me`(name,trip, aa_color_255, red, green, blue, aa_res_no, aa_name, is_me,start_id,scl,stat) " +
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        String[] S = new String[]{
                person.getName(),
                person.getTrip(),
                String.valueOf(person.getColor255()),
                String.valueOf(person.getRed()),
                String.valueOf(person.getGreen()),
                String.valueOf(person.getBlue()),
                String.valueOf(person.getAaresno()),
                person.getAaname(),
                String.valueOf(1),
                String.valueOf(startId),
                String.valueOf(person.getScl()),
                person.getStat()
        };

        UtilInfo.m_db.execSQL(SQL, S);
    }

    @Deprecated
    public void insertPerson(Person person, int startId) {
        String SQL = "INSERT INTO `person`(name, trip,aa_color_255, red, green, blue, aa_res_no, aa_name, is_me,start_id,stat) " +
                "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        String[] S = new String[]{
                person.getName(),
                person.getTrip(),
                String.valueOf(person.getColor255()),
                String.valueOf(person.getRed()),
                String.valueOf(person.getGreen()),
                String.valueOf(person.getBlue()),
                String.valueOf(person.getAaresno()),
                person.getAaname(),
                String.valueOf(1),
                String.valueOf(startId),
                person.getStat()
        };
        UtilInfo.m_db.execSQL(SQL, S);

    }

//    public void insertPerson2(Person person, int startId) {
//        String SQL = "INSERT INTO `person`(name, trip, is_me,start_id,aa_color_255,red,green,blue,aa_res_no,aa_name,x,y,scl,stat) " +
//                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
//        String[] S = new String[]{
//                person.getName(),
//                person.getTrip(),
//                String.valueOf(1),
//                String.valueOf(startId),
//                String.valueOf(person.getColor255()),
//                String.valueOf(person.getRed()),
//                String.valueOf(person.getGreen()),
//                String.valueOf(person.getBlue()),
//                String.valueOf(UtilInfo.aaNameaaResNoMap.get(person.getAaname())),
//                person.getAaname(),
//                String.valueOf(new Random().nextInt(660) + 30),
//                String.valueOf(new Random().nextInt(80) + 240),
//                String.valueOf(100),
//                "通常"
//        };
//
//        UtilInfo.m_db.execSQL(SQL, S);
//
//    }

    public int getRoomsMapCount() {
        String SQL = "SELECT COUNT(*) FROM rooms_map";

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        int count = -1;
        while (next) {
            count = cursor.getInt(0);
            next = cursor.moveToNext();
        }
        return count;
    }


    public void insertRoomsMap() {
        String VALUES1 = "VALUES('1','もなちゃと','1','-1','" + R.id.roomsActivity_monachat1 + "',0,0),";
        String VALUES2 = "('2','祭','2','-1','" + R.id.roomsActivity_monachat2 + "',0,0),";
        String VALUES3 = "('3','貞子','3','-1','" + R.id.roomsActivity_monachat3 + "',0,0),";
        String VALUES4 = "('4','樹海','4','-1','" + R.id.roomsActivity_monachat4 + "',0,0),";
        String VALUES5 = "('5','廃墟','5','-1','" + R.id.roomsActivity_monachat5 + "',0,0),";
        String VALUES6 = "('6','ハァ？','6','-1','" + R.id.roomsActivity_monachat6 + "',0,0),";
        String VALUES7 = "('7','キラキラ','7','-1','" + R.id.roomsActivity_monachat7 + "',0,0),";
        String VALUES8 = "('8','サッ','8','-1','" + R.id.roomsActivity_monachat8 + "',0,0),";
        String VALUES9 = "('9','食い逃げ','9','-1','" + R.id.roomsActivity_monachat9 + "',0,0),";
        String VALUES10 = "('10','学校','10','-1','" + R.id.roomsActivity_monachat10 + "',0,0),";
        String VALUES11 = "('11','喫茶','11','-1','" + R.id.roomsActivity_monachat11 + "',0,0),";
        String VALUES12 = "('12','Flash','12','-1','" + R.id.roomsActivity_monachat12 + "',0,0),";
        String VALUES13 = "('13','東京精神病院','13','-1','" + R.id.roomsActivity_monachat13 + "',0,0),";
        String VALUES14 = "('14','ヲタ','14','-1','" + R.id.roomsActivity_monachat14 + "',0,0),";
        String VALUES15 = "('15','さいたま','15','-1','" + R.id.roomsActivity_monachat15 + "',0,0),";
        String VALUES16 = "('16','BARギコ','16','-1','" + R.id.roomsActivity_monachat16 + "',0,0),";
        String VALUES17 = "('17','ワショーイ堂','17','-1','" + R.id.roomsActivity_monachat17 + "',0,0),";
        String VALUES18 = "('18','神社','18','-1','" + R.id.roomsActivity_monachat18 + "',0,0),";
        String VALUES19 = "('19','やまなし','19','-1','" + R.id.roomsActivity_monachat19 + "',0,0),";
        String VALUES20 = "('20','さすが兄弟','20','-1','" + R.id.roomsActivity_monachat20 + "',0,0),";
        String VALUES21 = "('21','もなちゃと21','21','-1','" + R.id.roomsActivity_monachat21 + "',0,0),";
        String VALUES22 = "('22','もなちゃと22','22','-1','" + R.id.roomsActivity_monachat22 + "',0,0),";
        String VALUES23 = "('23','もなちゃと23','23','-1','" + R.id.roomsActivity_monachat23 + "',0,0),";
        String VALUES24 = "('24','もなちゃと24','24','-1','" + R.id.roomsActivity_monachat24 + "',0,0),";
        String VALUES25 = "('25','もなちゃと25','25','-1','" + R.id.roomsActivity_monachat25 + "',0,0),";
        String VALUES26 = "('26','もなちゃと26','26','-1','" + R.id.roomsActivity_monachat26 + "',0,0),";
        String VALUES27 = "('27','もなちゃと27','27','-1','" + R.id.roomsActivity_monachat27 + "',0,0),";
        String VALUES28 = "('28','もなちゃと28','28','-1','" + R.id.roomsActivity_monachat28 + "',0,0),";
        String VALUES29 = "('29','もなちゃと29','29','-1','" + R.id.roomsActivity_monachat29 + "',0,0),";
        String VALUES30 = "('30','もなちゃと30','30','-1','" + R.id.roomsActivity_monachat30 + "',0,0),";
        String VALUES31 = "('31','もなちゃと31','31','-1','" + R.id.roomsActivity_monachat31 + "',0,0),";
        String VALUES32 = "('32','もなちゃと32','32','-1','" + R.id.roomsActivity_monachat32 + "',0,0),";
        String VALUES33 = "('33','もなちゃと33','33','-1','" + R.id.roomsActivity_monachat33 + "',0,0),";
        String VALUES34 = "('34','もなちゃと34','34','-1','" + R.id.roomsActivity_monachat34 + "',0,0),";
        String VALUES35 = "('35','もなちゃと35','35','-1','" + R.id.roomsActivity_monachat35 + "',0,0),";
        String VALUES36 = "('36','もなちゃと36','36','-1','" + R.id.roomsActivity_monachat36 + "',0,0),";
        String VALUES37 = "('37','もなちゃと37','37','-1','" + R.id.roomsActivity_monachat37 + "',0,0),";
        String VALUES38 = "('38','もなちゃと38','38','-1','" + R.id.roomsActivity_monachat38 + "',0,0),";
        String VALUES39 = "('39','もなちゃと39','39','-1','" + R.id.roomsActivity_monachat39 + "',0,0),";
        String VALUES40 = "('40','もなちゃと40','40','-1','" + R.id.roomsActivity_monachat40 + "',0,0),";
        String VALUES41 = "('41','もなちゃと41','41','-1','" + R.id.roomsActivity_monachat41 + "',0,0),";
        String VALUES42 = "('42','もなちゃと42','42','-1','" + R.id.roomsActivity_monachat42 + "',0,0),";
        String VALUES43 = "('43','もなちゃと43','43','-1','" + R.id.roomsActivity_monachat43 + "',0,0),";
        String VALUES44 = "('44','もなちゃと44','44','-1','" + R.id.roomsActivity_monachat44 + "',0,0),";
        String VALUES45 = "('45','もなちゃと45','45','-1','" + R.id.roomsActivity_monachat45 + "',0,0),";
        String VALUES46 = "('46','もなちゃと46','46','-1','" + R.id.roomsActivity_monachat46 + "',0,0),";
        String VALUES47 = "('47','もなちゃと47','47','-1','" + R.id.roomsActivity_monachat47 + "',0,0),";
        String VALUES48 = "('48','もなちゃと48','48','-1','" + R.id.roomsActivity_monachat48 + "',0,0),";
        String VALUES49 = "('49','もなちゃと49','49','-1','" + R.id.roomsActivity_monachat49 + "',0,0),";
        String VALUES50 = "('50','もなちゃと50','50','-1','" + R.id.roomsActivity_monachat50 + "',0,0),";
        String VALUES51 = "('51','もなちゃと51','51','-1','" + R.id.roomsActivity_monachat51 + "',0,0),";
        String VALUES52 = "('52','もなちゃと52','52','-1','" + R.id.roomsActivity_monachat52 + "',0,0),";
        String VALUES53 = "('53','もなちゃと53','53','-1','" + R.id.roomsActivity_monachat53 + "',0,0),";
        String VALUES54 = "('54','もなちゃと54','54','-1','" + R.id.roomsActivity_monachat54 + "',0,0),";
        String VALUES55 = "('55','もなちゃと55','55','-1','" + R.id.roomsActivity_monachat55 + "',0,0),";
        String VALUES56 = "('56','もなちゃと56','56','-1','" + R.id.roomsActivity_monachat56 + "',0,0),";
        String VALUES57 = "('57','もなちゃと57','57','-1','" + R.id.roomsActivity_monachat57 + "',0,0),";
        String VALUES58 = "('58','もなちゃと58','58','-1','" + R.id.roomsActivity_monachat58 + "',0,0),";
        String VALUES59 = "('59','もなちゃと59','59','-1','" + R.id.roomsActivity_monachat59 + "',0,0),";
        String VALUES60 = "('60','もなちゃと60','60','-1','" + R.id.roomsActivity_monachat60 + "',0,0),";
        String VALUES61 = "('61','もなちゃと61','61','-1','" + R.id.roomsActivity_monachat61 + "',0,0),";
        String VALUES62 = "('62','もなちゃと62','62','-1','" + R.id.roomsActivity_monachat62 + "',0,0),";
        String VALUES63 = "('63','もなちゃと63','63','-1','" + R.id.roomsActivity_monachat63 + "',0,0),";
        String VALUES64 = "('64','もなちゃと64','64','-1','" + R.id.roomsActivity_monachat64 + "',0,0),";
        String VALUES65 = "('65','もなちゃと65','65','-1','" + R.id.roomsActivity_monachat65 + "',0,0),";
        String VALUES66 = "('66','もなちゃと66','66','-1','" + R.id.roomsActivity_monachat66 + "',0,0),";
        String VALUES67 = "('67','もなちゃと67','67','-1','" + R.id.roomsActivity_monachat67 + "',0,0),";
        String VALUES68 = "('68','もなちゃと68','68','-1','" + R.id.roomsActivity_monachat68 + "',0,0),";
        String VALUES69 = "('69','もなちゃと69','69','-1','" + R.id.roomsActivity_monachat69 + "',0,0),";
        String VALUES70 = "('70','もなちゃと70','70','-1','" + R.id.roomsActivity_monachat70 + "',0,0),";
        String VALUES71 = "('71','もなちゃと71','71','-1','" + R.id.roomsActivity_monachat71 + "',0,0),";
        String VALUES72 = "('72','もなちゃと72','72','-1','" + R.id.roomsActivity_monachat72 + "',0,0),";
        String VALUES73 = "('73','もなちゃと73','73','-1','" + R.id.roomsActivity_monachat73 + "',0,0),";
        String VALUES74 = "('74','もなちゃと74','74','-1','" + R.id.roomsActivity_monachat74 + "',0,0),";
        String VALUES75 = "('75','もなちゃと75','75','-1','" + R.id.roomsActivity_monachat75 + "',0,0),";
        String VALUES76 = "('76','もなちゃと76','76','-1','" + R.id.roomsActivity_monachat76 + "',0,0),";
        String VALUES77 = "('77','もなちゃと77','77','-1','" + R.id.roomsActivity_monachat77 + "',0,0),";
        String VALUES78 = "('78','もなちゃと78','78','-1','" + R.id.roomsActivity_monachat78 + "',0,0),";
        String VALUES79 = "('79','もなちゃと79','79','-1','" + R.id.roomsActivity_monachat79 + "',0,0),";
        String VALUES80 = "('80','もなちゃと80','80','-1','" + R.id.roomsActivity_monachat80 + "',0,0),";
        String VALUES81 = "('81','もなちゃと81','81','-1','" + R.id.roomsActivity_monachat81 + "',0,0),";
        String VALUES82 = "('82','もなちゃと82','82','-1','" + R.id.roomsActivity_monachat82 + "',0,0),";
        String VALUES83 = "('83','もなちゃと83','83','-1','" + R.id.roomsActivity_monachat83 + "',0,0),";
        String VALUES84 = "('84','もなちゃと84','84','-1','" + R.id.roomsActivity_monachat84 + "',0,0),";
        String VALUES85 = "('85','もなちゃと85','85','-1','" + R.id.roomsActivity_monachat85 + "',0,0),";
        String VALUES86 = "('86','もなちゃと86','86','-1','" + R.id.roomsActivity_monachat86 + "',0,0),";
        String VALUES87 = "('87','もなちゃと87','87','-1','" + R.id.roomsActivity_monachat87 + "',0,0),";
        String VALUES88 = "('88','もなちゃと88','88','-1','" + R.id.roomsActivity_monachat88 + "',0,0),";
        String VALUES89 = "('89','もなちゃと89','89','-1','" + R.id.roomsActivity_monachat89 + "',0,0),";
        String VALUES90 = "('90','もなちゃと90','90','-1','" + R.id.roomsActivity_monachat90 + "',0,0),";
        String VALUES91 = "('91','もなちゃと91','91','-1','" + R.id.roomsActivity_monachat91 + "',0,0),";
        String VALUES92 = "('92','もなちゃと92','92','-1','" + R.id.roomsActivity_monachat92 + "',0,0),";
        String VALUES93 = "('93','もなちゃと93','93','-1','" + R.id.roomsActivity_monachat93 + "',0,0),";
        String VALUES94 = "('94','もなちゃと94','94','-1','" + R.id.roomsActivity_monachat94 + "',0,0),";
        String VALUES95 = "('95','もなちゃと95','95','-1','" + R.id.roomsActivity_monachat95 + "',0,0),";
        String VALUES96 = "('96','もなちゃと96','96','-1','" + R.id.roomsActivity_monachat96 + "',0,0),";
        String VALUES97 = "('97','もなちゃと97','97','-1','" + R.id.roomsActivity_monachat97 + "',0,0),";
        String VALUES98 = "('98','もなちゃと98','98','-1','" + R.id.roomsActivity_monachat98 + "',0,0),";
        String VALUES99 = "('99','もなちゃと99','99','-1','" + R.id.roomsActivity_monachat99 + "',0,0),";
        String VALUES100 = "('100','もなちゃと100','100','-1','" + R.id.roomsActivity_monachat100 + "',0,0)";


        String sql = "INSERT INTO `rooms_map` (default_no,room_name,room_no,room_count,button_id,available_flag,update_flag) " + VALUES1 +
                VALUES2 +
                VALUES3 +
                VALUES4 +
                VALUES5 +
                VALUES6 +
                VALUES7 +
                VALUES8 +
                VALUES9 +
                VALUES10 +
                VALUES11 +
                VALUES12 +
                VALUES13 +
                VALUES14 +
                VALUES15 +
                VALUES16 +
                VALUES17 +
                VALUES18 +
                VALUES19 +
                VALUES20 +
                VALUES21 +
                VALUES22 +
                VALUES23 +
                VALUES24 +
                VALUES25 +
                VALUES26 +
                VALUES27 +
                VALUES28 +
                VALUES29 +
                VALUES30 +
                VALUES31 +
                VALUES32 +
                VALUES33 +
                VALUES34 +
                VALUES35 +
                VALUES36 +
                VALUES37 +
                VALUES38 +
                VALUES39 +
                VALUES40 +
                VALUES41 +
                VALUES42 +
                VALUES43 +
                VALUES44 +
                VALUES45 +
                VALUES46 +
                VALUES47 +
                VALUES48 +
                VALUES49 +
                VALUES50 +
                VALUES51 +
                VALUES52 +
                VALUES53 +
                VALUES54 +
                VALUES55 +
                VALUES56 +
                VALUES57 +
                VALUES58 +
                VALUES59 +
                VALUES60 +
                VALUES61 +
                VALUES62 +
                VALUES63 +
                VALUES64 +
                VALUES65 +
                VALUES66 +
                VALUES67 +
                VALUES68 +
                VALUES69 +
                VALUES70 +
                VALUES71 +
                VALUES72 +
                VALUES73 +
                VALUES74 +
                VALUES75 +
                VALUES76 +
                VALUES77 +
                VALUES78 +
                VALUES79 +
                VALUES80 +
                VALUES81 +
                VALUES82 +
                VALUES83 +
                VALUES84 +
                VALUES85 +
                VALUES86 +
                VALUES87 +
                VALUES88 +
                VALUES89 +
                VALUES90 +
                VALUES91 +
                VALUES92 +
                VALUES93 +
                VALUES94 +
                VALUES95 +
                VALUES96 +
                VALUES97 +
                VALUES98 +
                VALUES99 +
                VALUES100;

        try {
            UtilInfo.m_db.execSQL(sql);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void deleteRoomsMap() {
        String SQL = "DELETE FROM `rooms_map`";
        try {
            UtilInfo.m_db.execSQL(SQL);
        } catch (SQLiteException e) {

        }
    }

    public void resetRoomsMap() {
        String SQL = "DELETE FROM sqlite_sequence WHERE name = 'rooms_map'";
        try {
            UtilInfo.m_db.execSQL(SQL);
        } catch (SQLiteException e) {

        }
    }

    public void insertDBStatus(int startId, int pId, int sStartId) {
        String SQL = "INSERT INTO `dbstatus`(start_id,s_p_id,s_start_id)";
        String VALUES = "VALUES(?,?,?)";
        String[] s = new String[]{
                String.valueOf(startId),
                String.valueOf(pId),
                String.valueOf(sStartId)
        };
        UtilInfo.m_db.execSQL(SQL + VALUES, s);
    }

    /**
     * setSize
     *
     * @param width
     * @param height
     * @param width08
     * @param height08
     */
    public void insertTableSizeTate(int width, int height, int width08, int height08) {
        String SQL = "INSERT INTO `size_tate`(width,height,width08,height08)VALUES (?,?,?,?)";

        String[] S = new String[]{
                String.valueOf(width),
                String.valueOf(height),
                String.valueOf(width08),
                String.valueOf(height08)
        };

        UtilInfo.m_db.execSQL(SQL, S);
    }

    public void insertTableSizeCanvasTate(int canvasRealWidth, int canvasRealheight) {
        String SQL = "INSERT INTO `size_canvas_tate`(width,height,time)VALUES (?,?,?)";

        String[] s = new String[]{
                String.valueOf(canvasRealWidth),
                String.valueOf(canvasRealheight),
                UtilInfo.getNowDate()
        };


        UtilInfo.m_db.execSQL(SQL, s);
    }

    public void insertGc(List<ScanResult> scanResults) {

        String SQL = "INSERT INTO `gc` (`gc`,`date`,`flag`) VALUES (?,?,?)";

        for (ScanResult scanResult : scanResults) {
//            ContentValues contentValues = new ContentValues();
//            contentValues.put("m", scanResult.BSSID);
//            contentValues.put("mgettime",UtilInfo.getNowDate());
            String[] s = new String[]{
                    scanResult.BSSID,
                    UtilInfo.getNowDate(),
                    "0"};
            UtilInfo.m_db.execSQL(SQL, s);
        }
    }

    public void insertPass() {

    }

    public String getPass() {
        String SQL = "SELECT id FROM `pass` WHERE id = (SELECT MAX(id) FROM pass)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        String pass = "";
        while (next) {
            pass = cursor.getString(cursor.getColumnIndex("pass"));
            next = cursor.moveToNext();
        }
        cursor.close();
        return pass;
    }


    public void deleteMResName() {

        String SQL1 = "DELETE FROM `m_res_no`";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {

        }


        String SQL2 = "DELETE FROM sqlite_sequence WHERE name = 'm_res_no'";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {

        }
    }

    public void insertMResName() {
        String SQL = "INSERT INTO m_res_no(aa_name, aa_res_no) VALUES";

//        String VALUES1 = "('mona','" + R.mipmap.mona + "'),";
//        String VALUES2 = "('marumimi','" + R.mipmap.marumimi + "'),";
//        String VALUES3 = "('shodai','" + R.mipmap.shodai + "'),";
//        String VALUES4 = "('oomimi','" + R.mipmap.oomimi + "'),";
//        String VALUES5 = "('kuromimi','" + R.mipmap.kuromimi + "'),";
//        String VALUES6 = "('polygon','" + R.mipmap.polygon + "'),";
//        String VALUES7 = "('agemona','" + R.mipmap.agemona + "'),";
//        String VALUES8 = "('anamona','" + R.mipmap.anamona + "'),";
//        String VALUES9 = "('mora','" + R.mipmap.mora + "'),";
//        String VALUES10 = "('urara','" + R.mipmap.urara + "'),";
//        String VALUES11 = "('fuun','" + R.mipmap.fuun + "'),";
//        String VALUES12 = "('iiajan','" + R.mipmap.iiajan + "'),";
//        String VALUES13 = "('nida','" + R.mipmap.nida + "'),";
//        String VALUES14 = "('gerara','" + R.mipmap.gerara + "'),";
//        String VALUES15 = "('sens','" + R.mipmap.sens + "'),";
//        String VALUES16 = "('gari','" + R.mipmap.gari + "'),";
//        String VALUES17 = "('yamazaki1','" + R.mipmap.yamazaki1 + "'),";
//        String VALUES18 = "('yamazaki2','" + R.mipmap.yamazaki2 + "'),";
//        String VALUES19 = "('boljoa3','" + R.mipmap.boljoa3 + "'),";
//        String VALUES20 = "('boljoa4','" + R.mipmap.boljoa4 + "'),";
//        String VALUES21 = "('puru','" + R.mipmap.puru + "'),";
//        String VALUES22 = "('mosamosa1','" + R.mipmap.mosamosa1 + "'),";
//        String VALUES23 = "('mosamosa2','" + R.mipmap.mosamosa2 + "'),";
//        String VALUES24 = "('mosamosa3','" + R.mipmap.mosamosa3 + "'),";
//        String VALUES25 = "('mosamosa4','" + R.mipmap.mosamosa4 + "'),";
//        String VALUES26 = "('kamemona','" + R.mipmap.kamemona + "'),";
//        String VALUES27 = "('oni','" + R.mipmap.oni + "'),";
//        String VALUES28 = "('wachoi','" + R.mipmap.wachoi + "'),";
//        String VALUES29 = "('oniini','" + R.mipmap.oniini + "'),";
//        String VALUES30 = "('tofu','" + R.mipmap.tofu + "'),";
//        String VALUES31 = "('niku','" + R.mipmap.niku + "'),";
//        String VALUES32 = "('iyou','" + R.mipmap.iyou + "'),";
//        String VALUES33 = "('ppa2','" + R.mipmap.ppa2 + "'),";
//        String VALUES34 = "('foppa','" + R.mipmap.foppa + "'),";
//        String VALUES35 = "('charhan','" + R.mipmap.charhan + "'),";
//        String VALUES36 = "('aramaki','" + R.mipmap.aramaki + "'),";
//        String VALUES37 = "('ichi','" + R.mipmap.ichi + "'),";
//        String VALUES38 = "('ichi2','" + R.mipmap.ichi2 + "'),";
//        String VALUES39 = "('ichineko','" + R.mipmap.ichineko + "'),";
//        String VALUES40 = "('hat2','" + R.mipmap.hat2 + "'),";
//        String VALUES41 = "('hati4','" + R.mipmap.hati4 + "'),";
//        String VALUES42 = "('hati','" + R.mipmap.hati + "'),";
//        String VALUES43 = "('hati3','" + R.mipmap.hati3 + "'),";
//        String VALUES44 = "('remona','" + R.mipmap.remona + "'),";
//        String VALUES45 = "('monaka','" + R.mipmap.monaka + "'),";
//        String VALUES46 = "('riru','" + R.mipmap.riru + "'),";
//        String VALUES47 = "('alice','" + R.mipmap.alice + "'),";
//        String VALUES48 = "('batu','" + R.mipmap.batu + "'),";
//        String VALUES49 = "('mina','" + R.mipmap.mina + "'),";
//        String VALUES50 = "('miwa','" + R.mipmap.miwa + "'),";
//        String VALUES51 = "('kabin','" + R.mipmap.kabin + "'),";
//        String VALUES52 = "('giko','" + R.mipmap.giko + "'),";
//        String VALUES53 = "('tatigiko','" + R.mipmap.tatigiko + "'),";
//        String VALUES54 = "('zuza','" + R.mipmap.zuza + "'),";
//        String VALUES55 = "('sugoi3','" + R.mipmap.sugoi3 + "'),";
//        String VALUES56 = "('ging','" + R.mipmap.ging + "'),";
//        String VALUES57 = "('maturi','" + R.mipmap.maturi + "'),";
//        String VALUES58 = "('usi','" + R.mipmap.usi + "'),";
//        String VALUES59 = "('nezumi','" + R.mipmap.nezumi + "'),";
//        String VALUES60 = "('abogado','" + R.mipmap.abogado + "'),";
//        String VALUES61 = "('bana','" + R.mipmap.bana + "'),";
//        String VALUES62 = "('uma','" + R.mipmap.uma + "'),";
//        String VALUES63 = "('sika','" + R.mipmap.sika + "'),";
//        String VALUES64 = "('kato','" + R.mipmap.kato + "'),";
//        String VALUES65 = "('haka','" + R.mipmap.haka + "'),";
//        String VALUES66 = "('tokei','" + R.mipmap.tokei + "'),";
//        String VALUES67 = "('gyouza','" + R.mipmap.gyouza + "'),";
//        String VALUES68 = "('papi','" + R.mipmap.papi + "'),";
//        String VALUES69 = "('tibigiko','" + R.mipmap.tibigiko + "'),";
//        String VALUES70 = "('fusa','" + R.mipmap.fusa + "'),";
//        String VALUES71 = "('suwarifusa','" + R.mipmap.suwarifusa + "'),";
//        String VALUES72 = "('tibifusa','" + R.mipmap.tibifusa + "'),";
//        String VALUES73 = "('haka','" + R.mipmap.haka + "'),";
//        String VALUES74 = "('tokei','" + R.mipmap.tokei + "'),";
//        String VALUES75 = "('gyouza','" + R.mipmap.gyouza + "'),";
//        String VALUES76 = "('papi','" + R.mipmap.papi + "'),";
//        String VALUES77 = "('tibigiko','" + R.mipmap.tibigiko + "'),";
//        String VALUES78 = "('fusa','" + R.mipmap.fusa + "'),";
//        String VALUES79 = "('suwarifusa','" + R.mipmap.suwarifusa + "'),";
//        String VALUES80 = "('tibifusa','" + R.mipmap.tibifusa + "'),";
//        String VALUES81 = "('sii2','" + R.mipmap.sii2 + "'),";
//        String VALUES82 = "('tibisii','" + R.mipmap.tibisii + "'),";
//        String VALUES83 = "('tuu','" + R.mipmap.tuu + "'),";
//        String VALUES84 = "('hokkyoku6','" + R.mipmap.hokkyoku6 + "'),";
//        String VALUES85 = "('jien','" + R.mipmap.jien + "'),";
//        String VALUES86 = "('kita','" + R.mipmap.kita + "'),";
//        String VALUES87 = "('haa','" + R.mipmap.haa + "'),";
//        String VALUES88 = "('nyog','" + R.mipmap.nyog + "'),";
//        String VALUES89 = "('gaku','" + R.mipmap.gaku + "'),";
//        String VALUES90 = "('shob','" + R.mipmap.shob + "'),";
//        String VALUES91 = "('shak','" + R.mipmap.shak + "'),";
//        String VALUES92 = "('boljoa','" + R.mipmap.boljoa + "'),";
//        String VALUES93 = "('mouk','" + R.mipmap.mouk + "'),";
//        String VALUES94 = "('mouk1','" + R.mipmap.mouk1 + "'),";
//        String VALUES95 = "('mouk2','" + R.mipmap.mouk2 + "'),";
//        String VALUES96 = "('hikk','" + R.mipmap.hikk + "'),";
//        String VALUES97 = "('dokuo','" + R.mipmap.dokuo + "'),";
//        String VALUES98 = "('dokuo2','" + R.mipmap.dokuo2 + "'),";
//        String VALUES99 = "('hosh','" + R.mipmap.hosh + "'),";
//        String VALUES100 = "('siranaiwa','" + R.mipmap.siranaiwa + "'),";
//        String VALUES101 = "('tiraneyo','" + R.mipmap.tiraneyo + "'),";
//        String VALUES102 = "('ginu','" + R.mipmap.ginu + "'),";
//        String VALUES103 = "('unko','" + R.mipmap.unko + "'),";
//        String VALUES104 = "('kasiwa','" + R.mipmap.kasiwa + "'),";
//        String VALUES105 = "('kappappa','" + R.mipmap.kappappa + "'),";
//        String VALUES106 = "('asou','" + R.mipmap.asou + "'),";
//        String VALUES107 = "('hiyoko','" + R.mipmap.hiyoko + "'),";
//        String VALUES108 = "('nin3','" + R.mipmap.nin3 + "'),";
//        String VALUES109 = "('kunoichi','" + R.mipmap.kunoichi + "'),";
//        String VALUES110 = "('osa','" + R.mipmap.osa + "'),";
//        String VALUES111 = "('cock','" + R.mipmap.cock + "'),";
//        String VALUES112 = "('coc2','" + R.mipmap.coc2 + "'),";
//        String VALUES113 = "('ri_man','" + R.mipmap.ri_man + "'),";
//        String VALUES114 = "('chichon','" + R.mipmap.chichon + "'),";
//        String VALUES115 = "('nanyo','" + R.mipmap.nanyo + "'),";
//        String VALUES116 = "('tahara','" + R.mipmap.tahara + "'),";
//        String VALUES117 = "('maji','" + R.mipmap.maji + "'),";
//        String VALUES118 = "('kikko2','" + R.mipmap.kikko2 + "'),";
//        String VALUES119 = "('sumaso2','" + R.mipmap.sumaso2 + "'),";
//        String VALUES120 = "('niraneko','" + R.mipmap.niraneko + "'),";
//        String VALUES121 = "('niraime','" + R.mipmap.niraime + "'),";
//        String VALUES122 = "('niraime2','" + R.mipmap.niraime2 + "'),";
//        String VALUES123 = "('niramusume','" + R.mipmap.niramusume + "'),";
//        String VALUES124 = "('chotto1','" + R.mipmap.chotto1 + "'),";
//        String VALUES125 = "('chotto2','" + R.mipmap.chotto2 + "'),";
//        String VALUES126 = "('chotto3','" + R.mipmap.chotto3 + "'),";
//        String VALUES127 = "('mossari','" + R.mipmap.mossari + "'),";
//        String VALUES128 = "('yokan','" + R.mipmap.yokan + "'),";
//        String VALUES129 = "('koit','" + R.mipmap.koit + "'),";
//        String VALUES130 = "('koya','" + R.mipmap.koya + "'),";
//        String VALUES131 = "('ranta','" + R.mipmap.ranta + "'),";
//        String VALUES132 = "('kyaku','" + R.mipmap.kyaku + "'),";
//        String VALUES133 = "('taxi','" + R.mipmap.taxi + "'),";
//        String VALUES134 = "('moudamepo','" + R.mipmap.moudamepo + "'),";
//        String VALUES135 = "('sai','" + R.mipmap.sai + "'),";
//        String VALUES136 = "('shaitama','" + R.mipmap.shaitama + "'),";
//        String VALUES137 = "('welneco2','" + R.mipmap.welneco2 + "'),";
//        String VALUES138 = "('kagami','" + R.mipmap.kagami + "'),";
//        String VALUES139 = "('gakuri','" + R.mipmap.gakuri + "'),";
//        String VALUES140 = "('unknown2','" + R.mipmap.unknown2 + "'),";
//        String VALUES141 = "('sira','" + R.mipmap.sira + "'),";
//        String VALUES142 = "('zonu','" + R.mipmap.zonu + "')";


        String VALUES1 = "('abogado','" + R.drawable.ic_svg_abogado + "'),";
        String VALUES2 = "('agemona','" + R.drawable.ic_svg_agemona + "'),";
        String VALUES3 = "('alice','" + R.drawable.ic_svg_alice + "'),";
        String VALUES4 = "('anamona','" + R.drawable.ic_svg_anamona + "'),";
        String VALUES5 = "('aramaki','" + R.drawable.ic_svg_aramaki + "'),";
        String VALUES6 = "('asou','" + R.drawable.ic_svg_asou + "'),";
        String VALUES7 = "('bana','" + R.drawable.ic_svg_bana + "'),";
        String VALUES8 = "('batu','" + R.drawable.ic_svg_batu + "'),";
        String VALUES9 = "('boljoa','" + R.drawable.ic_svg_boljoa + "'),";
        String VALUES10 = "('boljoa3','" + R.drawable.ic_svg_boljoa3 + "'),";
        String VALUES11 = "('boljoa4','" + R.drawable.ic_svg_boljoa4 + "'),";
        String VALUES12 = "('charhan','" + R.drawable.ic_svg_charhan + "'),";
        String VALUES13 = "('chichon','" + R.drawable.ic_svg_chichon + "'),";
        String VALUES14 = "('chotto1','" + R.drawable.ic_svg_chotto1 + "'),";
        String VALUES15 = "('chotto2','" + R.drawable.ic_svg_chotto2 + "'),";
        String VALUES16 = "('chotto3','" + R.drawable.ic_svg_chotto3 + "'),";
        String VALUES17 = "('coc2','" + R.drawable.ic_svg_coc2 + "'),";
        String VALUES18 = "('cock','" + R.drawable.ic_svg_cock + "'),";
        String VALUES19 = "('dokuo','" + R.drawable.ic_svg_dokuo + "'),";
        String VALUES20 = "('dokuo2','" + R.drawable.ic_svg_dokuo2 + "'),";
        String VALUES21 = "('foppa','" + R.drawable.ic_svg_foppa + "'),";
        String VALUES22 = "('fusa','" + R.drawable.ic_svg_fusa + "'),";
        String VALUES23 = "('fuun','" + R.drawable.ic_svg_fuun + "'),";
        String VALUES24 = "('gaku','" + R.drawable.ic_svg_gaku + "'),";
        String VALUES25 = "('gakuri','" + R.drawable.ic_svg_gakuri + "'),";
        String VALUES26 = "('gari','" + R.drawable.ic_svg_gari + "'),";
        String VALUES27 = "('gerara','" + R.drawable.ic_svg_gerara + "'),";
        String VALUES28 = "('giko','" + R.drawable.ic_svg_giko + "'),";
        String VALUES29 = "('ging','" + R.drawable.ic_svg_ging + "'),";
        String VALUES30 = "('ginu','" + R.drawable.ic_svg_ginu + "'),";
        String VALUES31 = "('gyouza','" + R.drawable.ic_svg_gyouza + "'),";
        String VALUES32 = "('haa','" + R.drawable.ic_svg_haa + "'),";
        String VALUES33 = "('haka','" + R.drawable.ic_svg_haka + "'),";
        String VALUES34 = "('hat2','" + R.drawable.ic_svg_hat2 + "'),";
        String VALUES35 = "('hati','" + R.drawable.ic_svg_hati + "'),";
        String VALUES36 = "('hati3','" + R.drawable.ic_svg_hati3 + "'),";
        String VALUES37 = "('hati4','" + R.drawable.ic_svg_hati4 + "'),";
        String VALUES38 = "('hikk','" + R.drawable.ic_svg_hikk + "'),";
        String VALUES39 = "('hiyoko','" + R.drawable.ic_svg_hiyoko + "'),";
        String VALUES40 = "('hokkyoku6','" + R.drawable.ic_svg_hokkyoku6 + "'),";
        String VALUES41 = "('hosh','" + R.drawable.ic_svg_hosh + "'),";
        String VALUES42 = "('ichi','" + R.drawable.ic_svg_ichi + "'),";
        String VALUES43 = "('ichi2','" + R.drawable.ic_svg_ichi2 + "'),";
        String VALUES44 = "('ichineko','" + R.drawable.ic_svg_ichineko + "'),";
        String VALUES45 = "('iiajan','" + R.drawable.ic_svg_iiajan + "'),";
        String VALUES46 = "('iyou','" + R.drawable.ic_svg_iyou + "'),";
        String VALUES47 = "('jien','" + R.drawable.ic_svg_jien + "'),";
        String VALUES48 = "('joruju','" + R.drawable.ic_svg_joruju + "'),";
        String VALUES49 = "('kabin','" + R.drawable.ic_svg_kabin + "'),";
        String VALUES50 = "('kagami','" + R.drawable.ic_svg_kagami + "'),";
        String VALUES51 = "('kamemona','" + R.drawable.ic_svg_kamemona + "'),";
        String VALUES52 = "('kappappa','" + R.drawable.ic_svg_kappappa + "'),";
        String VALUES53 = "('kasiwa','" + R.drawable.ic_svg_kasiwa + "'),";
        String VALUES54 = "('kato','" + R.drawable.ic_svg_kato + "'),";
        String VALUES55 = "('kikko2','" + R.drawable.ic_svg_kikko2 + "'),";
        String VALUES56 = "('kita','" + R.drawable.ic_svg_kita + "'),";
        String VALUES57 = "('koit','" + R.drawable.ic_svg_koit + "'),";
        String VALUES58 = "('koya','" + R.drawable.ic_svg_koya + "'),";
        String VALUES59 = "('kunoichi','" + R.drawable.ic_svg_kunoichi + "'),";
        String VALUES60 = "('kuromimi','" + R.drawable.ic_svg_kuromimi + "'),";
        String VALUES61 = "('kyaku','" + R.drawable.ic_svg_kyaku + "'),";
        String VALUES62 = "('maji','" + R.drawable.ic_svg_maji + "'),";
        String VALUES63 = "('marumimi','" + R.drawable.ic_svg_marumimi + "'),";
        String VALUES64 = "('maturi','" + R.drawable.ic_svg_maturi + "'),";
        String VALUES65 = "('mina','" + R.drawable.ic_svg_mina + "'),";
        String VALUES66 = "('miwa','" + R.drawable.ic_svg_miwa + "'),";
        String VALUES67 = "('mona','" + R.drawable.ic_svg_mona + "'),";
        String VALUES68 = "('monaka','" + R.drawable.ic_svg_monaka + "'),";
        String VALUES69 = "('mora','" + R.drawable.ic_svg_mora + "'),";
        String VALUES70 = "('mosamosa1','" + R.drawable.ic_svg_mosamosa1 + "'),";
        String VALUES71 = "('mosamosa2','" + R.drawable.ic_svg_mosamosa2 + "'),";
        String VALUES72 = "('mosamosa3','" + R.drawable.ic_svg_mosamosa3 + "'),";
        String VALUES73 = "('mosamosa4','" + R.drawable.ic_svg_mosamosa4 + "'),";
        String VALUES74 = "('mossari','" + R.drawable.ic_svg_mossari + "'),";
        String VALUES75 = "('moudamepo','" + R.drawable.ic_svg_moudamepo + "'),";
        String VALUES76 = "('mouk','" + R.drawable.ic_svg_mouk + "'),";
        String VALUES77 = "('mouk1','" + R.drawable.ic_svg_mouk1 + "'),";
        String VALUES78 = "('mouk2','" + R.drawable.ic_svg_mouk2 + "'),";
        String VALUES79 = "('nanyo','" + R.drawable.ic_svg_nanyo + "'),";
        String VALUES80 = "('nazoko','" + R.drawable.ic_svg_nazoko + "'),";
        String VALUES81 = "('nezumi','" + R.drawable.ic_svg_nezumi + "'),";
        String VALUES82 = "('nida','" + R.drawable.ic_svg_nida + "'),";
        String VALUES83 = "('niku','" + R.drawable.ic_svg_niku + "'),";
        String VALUES84 = "('nin3','" + R.drawable.ic_svg_nin3 + "'),";
        String VALUES85 = "('niraime','" + R.drawable.ic_svg_niraime + "'),";
        String VALUES86 = "('niraime2','" + R.drawable.ic_svg_niraime2 + "'),";
        String VALUES87 = "('niramusume','" + R.drawable.ic_svg_niramusume + "'),";
        String VALUES88 = "('niraneko','" + R.drawable.ic_svg_niraneko + "'),";
        String VALUES89 = "('nyog','" + R.drawable.ic_svg_nyog + "'),";
        String VALUES90 = "('oni','" + R.drawable.ic_svg_oni + "'),";
        String VALUES91 = "('oniini','" + R.drawable.ic_svg_oniini + "'),";
        String VALUES92 = "('oomimi','" + R.drawable.ic_svg_oomimi + "'),";
        String VALUES93 = "('osa','" + R.drawable.ic_svg_osa + "'),";
        String VALUES94 = "('papi','" + R.drawable.ic_svg_papi + "'),";
        String VALUES95 = "('polygon','" + R.drawable.ic_svg_polygon + "'),";
        String VALUES96 = "('ppa2','" + R.drawable.ic_svg_ppa2 + "'),";
        String VALUES97 = "('puru','" + R.drawable.ic_svg_puru + "'),";
        String VALUES98 = "('ranta','" + R.drawable.ic_svg_ranta + "'),";
        String VALUES99 = "('remona','" + R.drawable.ic_svg_remona + "'),";
        String VALUES100 = "('ri_man','" + R.drawable.ic_svg_ri_man + "'),";
        String VALUES101 = "('riru','" + R.drawable.ic_svg_riru + "'),";
        String VALUES102 = "('sai','" + R.drawable.ic_svg_sai + "'),";
        String VALUES103 = "('sens','" + R.drawable.ic_svg_sens + "'),";
        String VALUES104 = "('shaitama','" + R.drawable.ic_svg_shaitama + "'),";
        String VALUES105 = "('shak','" + R.drawable.ic_svg_shak + "'),";
        String VALUES106 = "('shob','" + R.drawable.ic_svg_shob + "'),";
        String VALUES107 = "('shodai','" + R.drawable.ic_svg_shodai + "'),";
        String VALUES108 = "('sii2','" + R.drawable.ic_svg_sii2 + "'),";
        String VALUES109 = "('sika','" + R.drawable.ic_svg_sika + "'),";
        String VALUES110 = "('sira','" + R.drawable.ic_svg_sira + "'),";
        String VALUES111 = "('siranaiwa','" + R.drawable.ic_svg_siranaiwa + "'),";
        String VALUES112 = "('sugoi3','" + R.drawable.ic_svg_sugoi3 + "'),";
        String VALUES113 = "('sumaso2','" + R.drawable.ic_svg_sumaso2 + "'),";
        String VALUES114 = "('suwarifusa','" + R.drawable.ic_svg_suwarifusa + "'),";
        String VALUES115 = "('tahara','" + R.drawable.ic_svg_tahara + "'),";
        String VALUES116 = "('tatigiko','" + R.drawable.ic_svg_tatigiko + "'),";
        String VALUES117 = "('taxi','" + R.drawable.ic_svg_taxi + "'),";
        String VALUES118 = "('tibifusa','" + R.drawable.ic_svg_tibifusa + "'),";
        String VALUES119 = "('tibigiko','" + R.drawable.ic_svg_tibigiko + "'),";
        String VALUES120 = "('tibisii','" + R.drawable.ic_svg_tibisii + "'),";
        String VALUES121 = "('tiraneyo','" + R.drawable.ic_svg_tiraneyo + "'),";
        String VALUES122 = "('tofu','" + R.drawable.ic_svg_tofu + "'),";
        String VALUES123 = "('tokei','" + R.drawable.ic_svg_tokei + "'),";
        String VALUES124 = "('tuu','" + R.drawable.ic_svg_tuu + "'),";
        String VALUES125 = "('uma','" + R.drawable.ic_svg_uma + "'),";
        String VALUES126 = "('unknown2','" + R.drawable.ic_svg_unknown2 + "'),";
        String VALUES127 = "('unko','" + R.drawable.ic_svg_unko + "'),";
        String VALUES128 = "('urara','" + R.drawable.ic_svg_urara + "'),";
        String VALUES129 = "('usi','" + R.drawable.ic_svg_usi + "'),";
        String VALUES130 = "('wachoi','" + R.drawable.ic_svg_wachoi + "'),";
        String VALUES131 = "('welneco2','" + R.drawable.ic_svg_welneco2 + "'),";
        String VALUES132 = "('yamazaki1','" + R.drawable.ic_svg_yamazaki1 + "'),";
        String VALUES133 = "('yamazaki2','" + R.drawable.ic_svg_yamazaki2 + "'),";
        String VALUES134 = "('yokan','" + R.drawable.ic_svg_yokan + "'),";
        String VALUES135 = "('zonu','" + R.drawable.ic_svg_zonu + "'),";
        String VALUES136 = "('zuza','" + R.drawable.ic_svg_zuza + "')";


        String result = SQL +
                VALUES1 +
                VALUES2 +
                VALUES3 +
                VALUES4 +
                VALUES5 +
                VALUES6 +
                VALUES7 +
                VALUES8 +
                VALUES9 +
                VALUES10 +
                VALUES11 +
                VALUES12 +
                VALUES13 +
                VALUES14 +
                VALUES15 +
                VALUES16 +
                VALUES17 +
                VALUES18 +
                VALUES19 +
                VALUES20 +
                VALUES21 +
                VALUES22 +
                VALUES23 +
                VALUES24 +
                VALUES25 +
                VALUES26 +
                VALUES27 +
                VALUES28 +
                VALUES29 +
                VALUES30 +
                VALUES31 +
                VALUES32 +
                VALUES33 +
                VALUES34 +
                VALUES35 +
                VALUES36 +
                VALUES37 +
                VALUES38 +
                VALUES39 +
                VALUES40 +
                VALUES41 +
                VALUES42 +
                VALUES43 +
                VALUES44 +
                VALUES45 +
                VALUES46 +
                VALUES47 +
                VALUES48 +
                VALUES49 +
                VALUES50 +
                VALUES51 +
                VALUES52 +
                VALUES53 +
                VALUES54 +
                VALUES55 +
                VALUES56 +
                VALUES57 +
                VALUES58 +
                VALUES59 +
                VALUES60 +
                VALUES61 +
                VALUES62 +
                VALUES63 +
                VALUES64 +
                VALUES65 +
                VALUES66 +
                VALUES67 +
                VALUES68 +
                VALUES69 +
                VALUES70 +
                VALUES71 +
                VALUES72 +
                VALUES73 +
                VALUES74 +
                VALUES75 +
                VALUES76 +
                VALUES77 +
                VALUES78 +
                VALUES79 +
                VALUES80 +
                VALUES81 +
                VALUES82 +
                VALUES83 +
                VALUES84 +
                VALUES85 +
                VALUES86 +
                VALUES87 +
                VALUES88 +
                VALUES89 +
                VALUES90 +
                VALUES91 +
                VALUES92 +
                VALUES93 +
                VALUES94 +
                VALUES95 +
                VALUES96 +
                VALUES97 +
                VALUES98 +
                VALUES99 +
                VALUES100 +
                VALUES101 +
                VALUES102 +
                VALUES103 +
                VALUES104 +
                VALUES105 +
                VALUES106 +
                VALUES107 +
                VALUES108 +
                VALUES109 +
                VALUES110 +
                VALUES111 +
                VALUES112 +
                VALUES113 +
                VALUES114 +
                VALUES115 +
                VALUES116 +
                VALUES117 +
                VALUES118 +
                VALUES119 +
                VALUES120 +
                VALUES121 +
                VALUES122 +
                VALUES123 +
                VALUES124 +
                VALUES125 +
                VALUES126 +
                VALUES127 +
                VALUES128 +
                VALUES129 +
                VALUES130 +
                VALUES131 +
                VALUES132 +
                VALUES133 +
                VALUES134 +
                VALUES135 +
                VALUES136;

        try {
            UtilInfo.m_db.execSQL(result);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> getAAnameAAresNo() {

        String SQL = "SELECT * FROM `m_res_no`";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        HashMap<String, String> hashMap = new HashMap<>();
        while (next) {
            String aaName = cursor.getString(cursor.getColumnIndex("aa_name"));
            String aaResNo = cursor.getString(cursor.getColumnIndex("aa_res_no"));
            hashMap.put(aaName, aaResNo);
            next = cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }

    /**
     * insert land
     *
     * @param landWidth08
     * @param landHeight08
     */
    public void insertLand(int landWidth08, int landHeight08) {
        String SQL = "INSERT INTO `size_land`(width08,height08) ";
        String VALUES = "VALUES(?,?)";
        String[] s = new String[]{
                String.valueOf(landWidth08),
                String.valueOf(landHeight08)
        };
        try {
            UtilInfo.m_db.execSQL(SQL + VALUES, s);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void getInsertSetting() {
        String SQL = "SELECT * FROM `setting` WHERE id = (SELECT MAX(id) FROM `setting`)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        if (!next) {
            SQL = "INSERT INTO `setting` (alpha,alpha01,black,touch_ok,black_seek,alpha_seek) " +
                    "VALUES(0,1,255,1,255,100)";
            UtilInfo.m_db.execSQL(SQL);
        }
        cursor.close();
    }

//    /**
//     * on current_status,insert into table
//     */
//    public void insertCurrentStatus() {
//        String SQL = "INSERT INTO `current_status`(start_id,start_time) VALUES((SELECT start_id_init FROM current_status_init),?)";
//
//        String[] S = new String[]{
//                String.valueOf(UtilInfo.getNowDate())
//        };
//        try {
//            UtilInfo.m_db.execSQL(SQL, S);
//        } catch (SQLiteException e) {
//            e.printStackTrace();
//        }
//    }

    public void insertTableSizeTate2(int width, int height, int tate2Width, int tate2Height) {
        String SQL = "INSERT INTO `size_tate2`(width,height,width08,height08)VALUES (?,?,?,?)";

        String[] S = new String[]{
                String.valueOf(width),
                String.valueOf(height),
                String.valueOf(tate2Width),
                String.valueOf(tate2Height)
        };

        UtilInfo.m_db.execSQL(SQL, S);

    }

    public List<IdAanameResno> selectPageAAResNo(int page) {

        String BETWEEN = null;
        if (page == 1) {
            BETWEEN = "BETWEEN 1 AND 9";
        } else if (page == 2) {
            BETWEEN = "BETWEEN 10 AND 18";
        } else if (page == 3) {
            BETWEEN = "BETWEEN 19 AND 27";
        } else if (page == 4) {
            BETWEEN = "BETWEEN 28 AND 36";
        } else if (page == 5) {
            BETWEEN = "BETWEEN 37 AND 45";
        } else if (page == 6) {
            BETWEEN = "BETWEEN 46 AND 54";
        } else if (page == 7) {
            BETWEEN = "BETWEEN 55 AND 63";
        } else if (page == 8) {
            BETWEEN = "BETWEEN 64 AND 72";
        } else if (page == 9) {
            BETWEEN = "BETWEEN 73 AND 81";
        } else if (page == 10) {
            BETWEEN = "BETWEEN 82 AND 90";
        } else if (page == 11) {
            BETWEEN = "BETWEEN 91 AND 99";
        } else if (page == 12) {
            BETWEEN = "BETWEEN 100 AND 108";
        } else if (page == 13) {
            BETWEEN = "BETWEEN 109 AND 117";
        } else if (page == 14) {
            BETWEEN = "BETWEEN 118 AND 126";
        } else if (page == 15) {
            BETWEEN = "BETWEEN 127 AND 135";
        } else if (page == 16) {
            BETWEEN = "BETWEEN 136 AND 142";
        }

        String SQL = "SELECT `id`,`aa_name`,`aa_res_no` FROM `m_res_no` WHERE `id` ";

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL + BETWEEN, null);
        List<IdAanameResno> pageList = new ArrayList<>();
        boolean next = cursor.moveToFirst();
        while (next) {
            IdAanameResno idAanameResno = new IdAanameResno();
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            idAanameResno.setId(id);
            String aaName = cursor.getString(cursor.getColumnIndex("aa_name"));
            int resNo = cursor.getInt(cursor.getColumnIndex("aa_res_no"));
//            Drawable drawable = UtilInfo.dialogAAMap.get(aaName);
            idAanameResno.setName(aaName);
//            idAanameResno.setDrawable(drawable);
            idAanameResno.setResNo(resNo);
            pageList.add(idAanameResno);
            next = cursor.moveToNext();
        }
        cursor.close();
        return pageList;
    }

    /**
     * @return pageNo
     */
    public int getCurrentPage() {
        String SQL = "SELECT `page` FROM `current_dialog_page`";

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        int pageNo = -1;
        boolean next = cursor.moveToFirst();
        if (!next) {
            pageNo = 1;
        } else {
            while (next) {
                pageNo = cursor.getInt(cursor.getColumnIndex("page"));
                next = cursor.moveToNext();
            }
        }
        return pageNo;
    }

    /**
     *
     */
    public void insertCurrentPage() {
        String SQL = "INSERT INTO `current_dialog_page`(`page`) VALUES(1)";

        try {
            UtilInfo.m_db.execSQL(SQL);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

    }

    public void deleteCurrentPage() {
        String SQL1 = "DELETE FROM `current_dialog_page`";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {

        }


        String SQL2 = "DELETE FROM sqlite_sequence WHERE name = 'current_dialog_page'";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {

        }

    }

    public void updateCurrentPage(int page) {
//        String SQL = "UPDATE `current_dialog_page` SET `page` = ?";
//        String[] S = new String[]{
//                String.valueOf(page)
//        };
//
//        try {
//            UtilInfo.m_db.execSQL(SQL, S);
//        } catch (SQLiteException e) {
//
//        }
    }

    public Map<String, Drawable> getAllDrawable(Context context, int color255) {
        String SQL = "SELECT * FROM `m_res_no`";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        Map<String, Drawable> map = new HashMap<>();
        while (next) {
            int aaResNo = cursor.getInt(cursor.getColumnIndex("aa_res_no"));
            String aaName = cursor.getString(cursor.getColumnIndex("aa_name"));
            map.put(aaName, ActivityCompat.getDrawable(context, aaResNo));
            next = cursor.moveToNext();
        }
        cursor.close();
        return map;
    }

    public void addColumn() {

//        String SQL = "ALTER TABLE `setting` ADD `rooms_list_position` column_definition";
//        try {
//            UtilInfo.m_db.execSQL(SQL);
//        } catch (SQLiteException e) {
//
//        }
//        String SQL2 = "ALTER TABLE `size_tate2` ADD `h08_hiku_pic` column_definition";
//        try {
//            UtilInfo.m_db.execSQL(SQL2);
//        } catch (SQLiteException e) {
//
//        }
//        String SQL3 = "ALTER TABLE `person` ADD `exit_flag` column_definition";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//
//        }
//        String SQL4 = "ALTER TABLE `person` ADD `ignore_flag` column_definition";
//        try {
//            UtilInfo.m_db.execSQL(SQL4);
//        } catch (SQLiteException e) {
//
//        }

        String SQL5 = "CREATE TABLE room_status ( id INTEGER PRIMARY KEY AUTOINCREMENT, room_scroll_y INTEGER, time DATETIME, start_id INTEGER, login_id INTEGER)";
        try {
            UtilInfo.m_db.execSQL(SQL5);
        } catch (SQLiteException e) {

        }

        String SQL6 = "CREATE TABLE gc_ft (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER ,from_gc INTEGER, to_gc INTEGER, date DATETIME)";
        try {
            UtilInfo.m_db.execSQL(SQL6);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL7 = "CREATE TABLE p_st ( id INTEGER PRIMARY KEY AUTOINCREMENT, pst TEXT, date DATETIME, start_id INTEGER)";
        try {
            UtilInfo.m_db.execSQL(SQL7);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL8 = "DROP TABLE IF EXISTS dbstatus";
        try {
            UtilInfo.m_db.execSQL(SQL8);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL9 = "CREATE TABLE dbstatus (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,s_p_id INTEGER,s_start_id INTEGER,s_login_id INTEGER, s_room_id INTEGER, s_cmt_id INTEGER, date DATETIME)";
        try {
            UtilInfo.m_db.execSQL(SQL9);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }
//        roomMatome();
        String SQL10 = "CREATE TABLE `pic_size` (id INTEGER PRIMARY KEY AUTOINCREMENT, start_id INTEGER,pic_width INTEGER, pic_height INTEGER)";
        try {
            UtilInfo.m_db.execSQL(SQL10);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }
    }

    private void roomMatome() {
        String SQL10 = "CREATE TABLE room_temp_table AS SELECT * FROM `room`";
        try {
            UtilInfo.m_db.execSQL(SQL10);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL11 = "DROP TABLE room";
        try {
            UtilInfo.m_db.execSQL(SQL11);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL12 = "CREATE TABLE room ( id INTEGER PRIMARY KEY AUTOINCREMENT, room_no INTEGER, room_name TEXT, room_count INTEGER, start_id INTEGER, login_id INTEGER,enter_date DATETIME,exit_date DATETIME)";
        try {
            UtilInfo.m_db.execSQL(SQL12);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL13 = "INSERT INTO room(id,room_no,room_name,room_count,start_id,login_id,enter_date,exit_date) SELECT id,room_no,room_name,room_count,start_id,login_id,time,null FROM room_temp_table";
        try {
            UtilInfo.m_db.execSQL(SQL13);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }
    }

//    public SizeTate2 getSizeTate2() {
//        String SQL = "SELECT * FROM `size_tate2` WHERE id = (SELECT MAX(id) FROM `size_tate2`)";
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        boolean next = cursor.moveToFirst();
//        SizeTate2 sizeTate2 = new SizeTate2();
//        while (next) {
//            sizeTate2.setWidth(cursor.getInt(cursor.getColumnIndex("width")));
//            sizeTate2.setHeight(cursor.getInt(cursor.getColumnIndex("height")));
//            sizeTate2.setWitdh08(cursor.getInt(cursor.getColumnIndex("width08")));
//            sizeTate2.setHeight08(cursor.getInt(cursor.getColumnIndex("height08")));
//            next = cursor.moveToNext();
//        }
//        cursor.close();
//        return sizeTate2;
//    }

    public void updateSizeTate2(int w08PlusPic, int h08HikuPic, int hikuPic) {
        String SQL = "UPDATE `size_tate2` SET `w08_plus_pic_half` = ? , `h08_hiku_pic_half` = ?,h08_hiku_pic = ? WHERE `id` = (SELECT MAX(id) FROM `size_tate2`)";
        String[] S = new String[]{
                String.valueOf(w08PlusPic),
                String.valueOf(h08HikuPic),
                String.valueOf(hikuPic)
        };

        try {
            UtilInfo.m_db.execSQL(SQL, S);
        } catch (SQLiteException e) {

        }

    }

    public void updateGU() {
        String SQL = "UPDATE `gc` SET `flag` = 1";
//        String[] S = new String[]{
//                String.valueOf(dbStatus.getFrommid()),
//                String.valueOf(dbStatus.getTomid())
//        };
        try {
            UtilInfo.m_db.execSQL(SQL);
        } catch (SQLiteException e) {

        }
    }

    /**
     * get gc_from_id,gc_to_id
     *
     * @return
     */
    public DBStatus getFromTo() {
        DBStatus dbStatus = new DBStatus();
        String SQL = "SELECT `from_gc`,`to_gc` FROM `gc_ft` WHERE `start_id` = (SELECT MAX(id) FROM `start`)";

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);

        boolean next = cursor.moveToFirst();
        while (next) {
            dbStatus.setFrommid(cursor.getInt(cursor.getColumnIndex("from_gc")));
            dbStatus.setTomid(cursor.getInt(cursor.getColumnIndex("to_gc")));
            next = cursor.moveToNext();
        }
        cursor.close();
        return dbStatus;
    }

    public void deleteGcFt() {
        String SQL = "DELETE FROM `gc_ft`";
        try {
            UtilInfo.m_db.execSQL(SQL);
        } catch (SQLiteException e) {

        }
    }

    public void insertGcFt() {
        String SQL = "INSERT INTO `gc_ft` (`start_id`,`date`) VALUES((SELECT MAX(id) FROM `start`),?)";
        String[] S = new String[]{
                UtilInfo.getNowDate()
        };

        try {
            UtilInfo.m_db.execSQL(SQL, S);
        } catch (SQLiteException e) {

        }
    }

    /**
     * @return
     */
    public String getPst() {

//        String SQL = "SELECT COUNT(*) FROM `p_st`";

        String SQL = "SELECT `pst` FROM `p_st` WHERE id = (SELECT MAX(id) FROM `p_st`) ";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next;
        next = cursor.moveToNext();
        String pSt = "";
        while (next) {
            pSt = cursor.getString(cursor.getColumnIndex("pst"));
            next = cursor.moveToNext();
        }
        cursor.close();
        return pSt;
    }


    public void insertPst() {
        String uuid = UUID.randomUUID().toString();
        String SQL = "INSERT INTO `p_st` (pst,date,start_id) VALUES(?,?,(SELECT MAX(id) FROM `start`))";
        String[] S = new String[]{
                uuid,
                UtilInfo.getNowDate()
        };

        try {
            UtilInfo.m_db.execSQL(SQL, S);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }
    }

//    public void updateCurrentStatus() {
//        String SQL = "UPDATE `current_status` SET `login_id` = null , `room_id` = null";
//
//        try {
//            UtilInfo.m_db.execSQL(SQL);
//        } catch (SQLiteException e) {
//
//        }
//    }

    public void insertPicSize(int size) {
        String SQL = "INSERT INTO `pic_size` (start_id,pic_width,pic_height) VALUES((SELECT MAX(id) FROM `start`),?,?)";
        String[] S = new String[]{
                String.valueOf(size),
                String.valueOf(size)
        };

        try {
            UtilInfo.m_db.execSQL(SQL, S);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }
    }

    public void initStart() {
        String SQL1 = "INSERT INTO start_save (date) SELECT date FROM start";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from start";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'start'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }


    }

    public void initLogin() {
        String SQL1 = "INSERT INTO login_save(start_id,date) SELECT start_id,date FROM login";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from login";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'login'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }


    }

    public void initRoom() {
        String SQL1 = "INSERT INTO room_save(start_id,login_id,room_no,room_name,room_count,enter_date,exit_date)SELECT start_id,login_id,room_no,room_name,room_count,enter_date,exit_date FROM room";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from room";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'room'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }
    }

    public void initPerson() {
        String SQL1 = "INSERT INTO person_save (start_id,login_id,room_id,mona_id,name,trip,aa_color_255,red,green,blue,aa_res_no,aa_name,stat,shirotori6,kurotori,shirotori,scl,is_me,date,x,y,cx,cy,cx2,cy2,exit_flag,ignore_flag) SELECT start_id,login_id,room_id,mona_id,name,trip,aa_color_255,red,green,blue,aa_res_no,aa_name,stat,shirotori6,kurotori,shirotori,scl,is_me,date,x,y,cx,cy,cx2,cy2,exit_flag,ignore_flag FROM person";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        // 消す前のperson is_meを取り出す
//        SentakuDB sentakuDB = new SentakuDB();
//        Person person = sentakuDB.getSentakuMe2();
//
//        String SQL2_5 = "UPDATE person SET aa_color_255 = ?, red = ?, green = ?, blue = ?, aa_res_no = ?, aa_name = ? WHERE id = (SELECT MAX(id) FROM person WHERE is_me)";
//
//        String[] S = new String[] {
//                String.valueOf(                person.getColor255()),
//                String.valueOf(person.getRed()),
//                String.valueOf(person.getGreen()),
//                String.valueOf()
//
//        };


        String SQL2 = "delete from person";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }


//        String SQL3 = "delete from sqlite_sequence where name= 'person'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }

    }

    public void initComment() {
        String SQL1 = "INSERT INTO comment_save(start_id,login_id,room_id,person_id,comment,date,show_flag,send_flag) SELECT start_id,login_id,room_id,person_id,comment,date,show_flag,send_flag FROM comment";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from comment";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'comment'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }
    }

    public void initPicSize() {
        String SQL1 = "INSERT INTO pic_size_save (start_id,pic_width,pic_height) SELECT start_id,pic_width,pic_height FROM pic_size";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from pic_size";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'pic_size'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }
    }

    public void initRoomStatus() {
        String SQL1 = "INSERT INTO room_status_save(start_id,login_id,room_scroll_y,date) SELECT start_id,login_id,room_scroll_y,date FROM room_status";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from room_status";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'room_status'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }
    }

    public void initSetting() {
        String SQL1 = "INSERT INTO setting_save(alpha,alpha01,black,touch_ok,black_seek,alpha_seek,rooms_list_position) SELECT alpha,alpha01,black,touch_ok,black_seek,alpha_seek,rooms_list_position FROM setting";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from setting";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'delete from setting'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }
    }

    public void initSizeTate2() {
        String SQL1 = "INSERT INTO size_tate2_save(width,height, width08, height08,date,delete_flag,h08_hiku_pic_half,w08_plus_pic_half,h08_hiku_pic) SELECT  width,height, width08, height08,date,delete_flag,h08_hiku_pic_half,w08_plus_pic_half,h08_hiku_pic FROM size_tate2";
        try {
            UtilInfo.m_db.execSQL(SQL1);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

        String SQL2 = "delete from size_tate2";
        try {
            UtilInfo.m_db.execSQL(SQL2);
        } catch (SQLiteException e) {
            String s = e.getMessage();
        }

//        String SQL3 = "delete from sqlite_sequence where name= 'size_tate2'";
//        try {
//            UtilInfo.m_db.execSQL(SQL3);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }
    }

//    public void getAAA() {
//        int startId = getInitStartId() + 1;
//        int loginId = getInitLoginId() + 1;
//        int roomId = getInitRoomId() + 1;
//        int commentId = getInitCommentId() + 1;
//
//
//        String SQL = "INSERT INTO `current_status_init` (start_id_init,login_id_init,room_id_init,comment_id_init) VALUES (?,?,?,?)";
//
//        String[] S = new String[]{
//                String.valueOf(startId),
//                String.valueOf(loginId),
//                String.valueOf(roomId),
//                String.valueOf(commentId)
//        };
//
//        try {
//            UtilInfo.m_db.execSQL(SQL,S);
//        } catch (SQLiteException e) {
//            String s = e.getMessage();
//        }
//    }

//    /**
//     * コメントセーブテーブルから得る
//     *
//     * @return
//     */
//    private int getInitCommentId() {
//        String SQL = "SELECT MAX(id) FROM comment_save";
//
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        boolean next = cursor.moveToFirst();
//        int commentId = -1;
//        if (next == false) {
//            return 0;
//        }
//
//        while (next) {
//            commentId = cursor.getInt(cursor.getColumnIndex("id"));
//            next = cursor.moveToNext();
//        }
//        return commentId;
//    }

//    /**
//     * ルームセーブテーブルから得る
//     *
//     * @return
//     */
//    private int getInitRoomId() {
//        String SQL = "SELECT MAX(id) FROM room_save";
//
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        boolean next = cursor.moveToFirst();
//        int roomId = -1;
//        if (next == false) {
//            return 0;
//        }
//
//        while (next) {
//            roomId = cursor.getInt(cursor.getColumnIndex("id"));
//            next = cursor.moveToNext();
//        }
//        return roomId;
//    }

//    /**
//     * ログインセーブテーブルから得る
//     *
//     * @return
//     */
//    private int getInitLoginId() {
//        String SQL = "SELECT MAX(id) FROM login_save";
//
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        boolean next = cursor.moveToFirst();
//        int loginId = -1;
//        if (next == false) {
//            return 0;
//        }
//
//        while (next) {
//            loginId = cursor.getInt(cursor.getColumnIndex("id"));
//            next = cursor.moveToNext();
//        }
//        return loginId;
//
//    }

//    /**
//     * スタートセーブテーブルから得る
//     * @return
//     */
//    private int getInitStartId() {
//        String SQL = "SELECT MAX(id) FROM start_save";
//
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        boolean next = cursor.moveToFirst();
//        int startId = -1;
//        if (next == false) {
//            return 0;
//        }
//
//        while (next) {
//            startId = cursor.getInt(cursor.getColumnIndex("id"));
//            next = cursor.moveToNext();
//        }
//        return startId;
//    }

//    public void deleteCurrentStatusInit() {
//        String SQL = "DELETE FROM current_status_init";
//    }

    /**
     * sqlite_sequenceにstart,login,room,person,commentカラムにstart_saveなどのidを振る
     */
    public void updateSqliteSequence() {
        String SQL1 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT `seq` FROM `sqlite_sequence` WHERE `name` = 'start_save') WHERE name = 'start'";

        String SQL2 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT `seq` FROM `sqlite_sequence` WHERE `name` = 'login_save') WHERE name = 'login'";

        String SQL3 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT `seq` FROM `sqlite_sequence` WHERE `name` = 'room_save') WHERE name = 'room'";

        String SQL4 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT `seq` FROM `sqlite_sequence` WHERE `name` = 'person_save') WHERE name = 'person'";

        String SQL5 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT `seq` FROM `sqlite_sequence` WHERE `name` = 'comment_save') WHERE name = 'comment'";
//        String SQL2 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT MAX(id) FROM `login_save`) WHERE name = 'login'";
//
//        String SQL3 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT MAX(id) FROM `room_save`) WHERE name = 'room'";
//
//        String SQL4 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT MAX(id) FROM `person_save`) WHERE name = 'person'";
//
//        String SQL5 = "UPDATE `sqlite_sequence` SET `seq` = (SELECT MAX(id) FROM `comment_save`) WHERE name = 'comment'";

        try {
            UtilInfo.m_db.execSQL(SQL1);
            UtilInfo.m_db.execSQL(SQL2);
            UtilInfo.m_db.execSQL(SQL3);
            UtilInfo.m_db.execSQL(SQL4);
            UtilInfo.m_db.execSQL(SQL5);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 起動時にテーブルの中を消す
     */
    public void deleteStartLoginRoomEtc() {
        String SQL1 = "DELETE FROM start";
        String SQL2 = "DELETE FROM login";
        String SQL3 = "DELETE FROM room";
        String SQL4 = "DELETE FROM person";
        String SQL5 = "DELETE FROM comment";

        try {
            UtilInfo.m_db.execSQL(SQL1);
            UtilInfo.m_db.execSQL(SQL2);
            UtilInfo.m_db.execSQL(SQL3);
            UtilInfo.m_db.execSQL(SQL4);
            UtilInfo.m_db.execSQL(SQL5);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public int getAaResNoWithName(String name) {
        String SQL = "SELECT * FROM `m_res_no` WHERE `aa_name` = ?";
        String[] S = new String[]{
                name
        };

        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, S);
        boolean next = cursor.moveToFirst();
        int aaResNo = -1;
        while (next) {
            aaResNo = cursor.getInt(cursor.getColumnIndex("aa_res_no"));
            next = cursor.moveToNext();
        }
        cursor.close();
        return aaResNo;
    }

    public void updateRoomsAvailable() {
        String SQL = "UPDATE  `rooms_map` SET `available_flag` = 1 WHERE `id` BETWEEN 90 AND 100";
        try {
            UtilInfo.m_db.execSQL(SQL);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

    }

//    public Person getSentakuMe2() {
//        String sql = "SELECT * FROM person_save WHERE id = (SELECT MAX(id) FROM person_save WHERE is_me = 1)";
//        Cursor cursor = UtilInfo.m_db.rawQuery(sql, null);
//        boolean next = cursor.moveToFirst();
////        HashMap<String, Object> hashMap = new HashMap<>();
//        Person person = new Person();
//        person.setId(-1);
//        while (next) {
//            person.setId(cursor.getInt(cursor.getColumnIndex("id")));
//            person.setName(cursor.getString(cursor.getColumnIndex("name")));
//            person.setTrip(cursor.getString(cursor.getColumnIndex("trip")));
//            person.setColor255(cursor.getInt(cursor.getColumnIndex("aa_color_255")));
//            person.setRed(cursor.getInt(cursor.getColumnIndex("red")));
//            person.setGreen(cursor.getInt(cursor.getColumnIndex("green")));
//            person.setBlue(cursor.getInt(cursor.getColumnIndex("blue")));
//            person.setScl(cursor.getInt(cursor.getColumnIndex("scl")));
////            person.setX(cursor.getInt(cursor.getColumnIndex("x")));
////            person.setX(cursor.getInt(cursor.getColumnIndex("y")));
//            Random random = new Random();
//            person.setX(random.nextInt(660) + 30);
//            person.setToY(random.nextInt(80) + 240);
//            /**
//             * 初期処理でaanameにaaresnoをマッピングした変数を使用する。
//             * because of changing aaresno every time
//             */
//            if (cursor.getString(cursor.getColumnIndex("aa_name")) == null) {
//                person.setAaresno(Integer.parseInt(UtilInfo.aaNameaaResNoMap.get("mona")));
//                person.setAaname("mona");
//            } else {
//                person.setAaresno(Integer.parseInt(UtilInfo.aaNameaaResNoMap.get(cursor.getString(cursor.getColumnIndex("aa_name")))));
//                person.setAaname(cursor.getString(cursor.getColumnIndex("aa_name")));
//            }
//            person.setStat("通常");
//            next = cursor.moveToNext();
//        }
//        if (person.getId() == -1) {
//            person.setId(0);
//            person.setName("名無しさん");
//            person.setTrip(null);
//            person.setColor255(0xFFFFFFFF);
//            person.setRed(100);
//            person.setGreen(100);
//            person.setBlue(100);
//            person.setAaresno(R.drawable.ic_svg_mona);
//            person.setAaname("mona");
//            person.setStat("通常");
//            person.setScl(0);
//            Random random = new Random();
//            person.setX(random.nextInt(660) + 30);
//            person.setToY(random.nextInt(80) + 240);
//        }
//        cursor.close();
//        return person;
//    }

    public void updatePerson(Person me) {
        String SQL = "UPDATE person SET name = ?, trip = ? WHERE id = (SELECT MAX(id) FROM person WHERE is_me = 1)";
        String[] S = new String[]{
                me.getName(),
                me.getTrip()
        };
        UtilInfo.m_db.execSQL(SQL, S);
    }

    public void getInsertNotify() {
        String SQL = "SELECT * FROM `notify_setting` WHERE id = (SELECT MAX(id) FROM `notify_setting`)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        if (!next) {
            SQL = "INSERT INTO `notify_setting` (id,notify1,notify2,notify3,notify4,notify5,notify6,notify7,notify8,notify9,notify10) " +
                    "VALUES(1,null,null,null,null,null,null,null,null,null,null)";
            UtilInfo.m_db.execSQL(SQL);
        }
        cursor.close();
    }
}
