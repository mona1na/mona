package com.devmona;

import android.util.SparseArray;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by sake on 2018/02/21.
 */

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    private final FragmentManager fm;
    private ArrayList<Fragment> mFragmentList;
    Fragment fragment = null;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
        mFragmentList = new ArrayList<>();
        fragment = new Tate2Fragment();
        mFragmentList.add(fragment);
        fragment = new Tate3Fragment();
        mFragmentList.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = mFragmentList.get(position);

        if (fragment != null) {
            return fragment;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
