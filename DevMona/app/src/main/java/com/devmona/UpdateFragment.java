package com.devmona;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateFragment extends Fragment {

    private UpdateProgressFragment progressFragment;
    private TextView updText;

    public UpdateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("LifeTest UpdateFragment", "onCreateView");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_update, container, false);
        updText = (TextView) v.findViewById(R.id.update_text);
        Button button = (Button) v.findViewById(R.id.update_btn_ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                fragmentTransaction.remove(UpdateFragment.this).commit();
//                getActivity().getSupportFragmentManager().beginTransaction().remove(UpdateFragment.this).commit();
            }
        });
        progressFragment = new UpdateProgressFragment();
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.update_progress, progressFragment, "updProgress").commit();
        getLoaderManager().initLoader(11, null, mCallback);
        return v;
    }

    private final LoaderManager.LoaderCallbacks<Object> mCallback =
            new LoaderManager.LoaderCallbacks<Object>() {

                @NonNull
                @Override
                public Loader<Object> onCreateLoader(int id, @Nullable Bundle args) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("key", 11);
                    MyLoader myLoader = new MyLoader(getActivity(), bundle);
                    return myLoader;
                }

                @Override
                public void onLoadFinished(@NonNull Loader<Object> loader, Object data) {
                    getActivity().getSupportLoaderManager().destroyLoader(11);
                    getActivity().getSupportFragmentManager().beginTransaction().remove(progressFragment).commit();
                    updText.setText(data.toString());
                }

                @Override
                public void onLoaderReset(@NonNull Loader<Object> loader) {

                }
            };


    @Override
    public void onResume() {
        super.onResume();
        Log.d("LifeTest UpdateFragment", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("LifeTest UpdateFragment", "onPause");
    }
}
