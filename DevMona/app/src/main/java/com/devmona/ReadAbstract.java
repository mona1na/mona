package com.devmona;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.devmona.loadercallbacks.LoaderCallbacks3FromSocket;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public abstract class ReadAbstract extends Thread {

    protected final BufferedInputStream in;
    protected boolean reConnecting;
    protected Context context;
    protected String strGrobal = "";
    byte[] byteGrobal = null;
    public Base2Activity base2Activity;
    public MyApplication mAppli;
    public boolean running = false;
    public static final String TAG = "ReadAbstract";
    protected byte[] keyinbuf = new byte[LEN];
    static final int LEN = 3000;
    protected int k;
    protected ArrayList<ArrayList<String>> arrayLists = new ArrayList<>();

    protected ReadAbstract(Context context, boolean reConnectting) {
        this.context = context;
        this.base2Activity = (Base2Activity) context;
        this.mAppli = (MyApplication) context.getApplicationContext();
        this.running = true;
        TestConnect testConnect = TestConnect.getInstance(context);
        in = testConnect.in;
        running = true;
        this.reConnecting = reConnectting;


    }

    protected ArrayList abstractRead1() {
        try {
            k = in.read(keyinbuf);
            if (k == -1) {
                throw new MyException("k == -1");
            }
        } catch (MyException e) {
            Log.d(TAG, "read k == -1");
            reconnect(context);
            return null;
        } catch (NullPointerException e) {
            Log.d(TAG, "read null");
            reconnect(context);
            return null;
        } catch (IOException e) {
            Log.d(TAG, "read IOE");
            reconnect(context);
            return null;
        }
        ArrayList result;
//        result = abstractXMLCreate(context, keyinbuf, k);
        result = abstractXMLCreate3(keyinbuf, k);
        try {
            if (result == null) {
                throw new MyException("abstractXMLCreate result null");
            }
        } catch (MyException e) {
            Log.d(TAG, "abstractXMLCreate result null");
            reconnect(context);
            return null;
        }
//        arrayLists.add(result);
        return result;
    }

//    protected ArrayList abstractRead2() {
//        ArrayList result = null;
//        result = abstractXMLCreate(context, keyinbuf, k);
//        if (result == null) {
//            NopThread.testA(context, "result null");
//            Log.d("ReadAbstract run", "result null");
//            throw new MyException("null");
//        }
//        arrayLists.add(result);
//        return result;
//    }


//    class MyThread extends Thread {
//        public static final String TAG = "MyThread";
//
//        public MyThread() {
//        }
//
//        @Override
//        public void run() {
//            super.run();
//            TestConnect testConnect = TestConnect.getInstance(context);
//            testConnect.stopAndClose(context);
//            testConnect.createConnect(context);
//            testConnect.sendTest(context);
//            try {
//                Log.d(ReadAbstract.TAG + MyThread.TAG, "run before wait");
//                synchronized (this) {
//                    this.wait();
//                }
//                Log.d(ReadAbstract.TAG + MyThread.TAG, "run before after");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//                Log.d(ReadAbstract.TAG + MyThread.TAG, "run before InterruptedException");
//                Base2Activity base2Activity = (Base2Activity) context;
//                base2Activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(context, "未接続", Toast.LENGTH_LONG).show();
//                    }
//                });
//                return;
//            }
//            testConnect.sendRoom(context);
//            Base2Activity base2Activity = (Base2Activity) context;
//            base2Activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(context, "接続", Toast.LENGTH_LONG).show();
//                }
//            });
//            return;
//
////            boolean check;
////            /**
////             * 繋がり判定をもう一つ追加する
////             * 例えば少しまって何かを受信しないとかいう判定
////             */
////            check = testConnect.sendRoom(context);
////            try {
////                wait();
////            } catch (InterruptedException e) {
////                e.printStackTrace();
////            }
////            if (!check) {
////                Base2Activity base2Activity = (Base2Activity) context;
////                base2Activity.runOnUiThread(new Runnable() {
////                    @Override
////                    public void run() {
////                        Toast.makeText(context, "接続", Toast.LENGTH_LONG).show();
////                    }
////                });
////                return;
////            } else {
////                try {
////                    Base2Activity base2Activity = (Base2Activity) context;
////                    base2Activity.runOnUiThread(new Runnable() {
////                        @Override
////                        public void run() {
////                            Toast.makeText(context, "再接続要求:" + count, Toast.LENGTH_LONG).show();
////                        }
////                    });
////                    Log.d("waitconnect", "" + count);
////                    Thread.sleep(3000);
////                } catch (InterruptedException e) {
////                    e.printStackTrace();
////                }
////            }
//
//
//        }
//    }

    protected void reconnect(final Context context) {
//        final Base2Activity base2Activity = (Base2Activity) context;
//        MyApplication myApplication = (MyApplication) context.getApplicationContext();
//        int reconnectCount = myApplication.reconnectCount;
//        int reconnectIncre = myApplication.reconnectIncre;
        MyThread myThread = TestConnect.getInstance(context).myThread;
        if (!myThread.isInterrupted()) {
            Log.d(TAG, "MyThread interrupt");
            myThread.interrupt();
        } else {
            Log.d(TAG, "MyThread not interrupt");
        }

        if (isReConnecting()) {
            Log.d(TAG, "reconnecting...");
        } else {
            Log.d(TAG, "do reconnect");
            final Base2Activity nameActivity = (Base2Activity) context;
            nameActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "reconnect run");
                    Bundle bundle = new Bundle();
                    bundle.putInt("key", 3);
                    nameActivity.getSupportLoaderManager().initLoader(3, bundle, new LoaderCallbacks3FromSocket(context));
                }
            });
        }
//        TestConnect.getInstance(context).myThread.interrupt();


//        for (; reconnectIncre < reconnectCount; reconnectIncre++) {
//            // 接続okなら
//            myThread = new MyThread();
//            myThread.start();
//        }

//        }


//        new Thread(new Runnable() {
//            @Override
//            public void run() {
////                try {
////                    Thread.sleep(5000);
////                } catch (InterruptedException e1) {
////                    e1.printStackTrace();
////                }
//                boolean check;
//                int reCount = TestShared.getInstance(context).getReconectCount();
//                for (int i = 0; i < reCount; i++) {
//                    NopThread.testA(context, "reconnect" + i);
//                    Log.d("ReadAbstract reconnect", "loop:" + i);
//                    TestConnect testConnect = TestConnect.getInstance(context);
//                    testConnect.stopAndClose(context);
//                    testConnect.createConnect(context);
//                    testConnect.sendTest(context);
////                    try {
////                        Thread.sleep(5000);
////                    } catch (InterruptedException e) {
////                        e.printStackTrace();
////                    }
//
//                    check = testConnect.sendRoom(context);
//                    if (!check) {
//                        Base2Activity base2Activity = (Base2Activity) context;
//                        base2Activity.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toast.makeText(context, "接続", Toast.LENGTH_LONG).show();
//                            }
//                        });
//                        break;
//                    } else {
//                        try {
//                            Base2Activity base2Activity = (Base2Activity) context;
//                            final int finalI = i;
//                            base2Activity.runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    Toast.makeText(context, "再接続要求:" + finalI, Toast.LENGTH_LONG).show();
//                                }
//                            });
//                            Log.d("waitconnect", "" + i);
//                            Thread.sleep(3000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();

//        Handler handler = new Handler(Looper.getMainLooper());
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        });
//        base2Activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Bundle bundle = new Bundle();
//                base2Activity.getSupportLoaderManager().initLoader(2, bundle, new LoaderCallbacks2FromSocket(context));
//            }
//        });

    }

    public boolean isReConnecting() {
        return reConnecting;
    }

    public ArrayList<String> abstractXMLCreate(Context context, byte[] keyinbuf, int k) {
        ArrayList<String> arrayResult = new ArrayList<>();
        String temp10 = null;
        temp10 = new String(keyinbuf, 0, k);

        int zerokara = 0;
        while (true) {

            int kokomade = temp10.indexOf("\0", zerokara);

            if (kokomade != -1) {
                String result = temp10.substring(zerokara, kokomade);
                if (strGrobal.equals("")) {
                    arrayResult.add(result);
                    zerokara = kokomade + 1;
                } else if (!strGrobal.equals("")) {
                    arrayResult.add(strGrobal + result);
                    zerokara = kokomade + 1;
                    strGrobal = "";
                }
            } else {

                if (zerokara == temp10.length()) {

                    return arrayResult;
                } else {
                    String result = temp10.substring(zerokara, temp10.length());
                    strGrobal = result;
                    return arrayResult;
                }
            }

        }
    }

    public ArrayList<String> abstractXMLCreate3(byte[] keyinbuf, int k) {
        byte[] result = testA(keyinbuf, k);
        ArrayList<Integer> nullNum = subByte(result);
        ArrayList<byte[]> bytesResult = byteAnalyze(nullNum, result);
        if (bytesResult != null) {
            ArrayList<String> r = byteToString(bytesResult);
            return r;
        } else {
            return new ArrayList<String>();
        }
    }

    private ArrayList<String> byteToString(ArrayList<byte[]> bytesResult) {
        ArrayList<String> result = new ArrayList<>();
        for (byte[] b : bytesResult) {
            result.add(new String(b));
        }
        return result;
    }

    private byte[] testA(byte[] keyinbuf, int k) {
        byte[] tempByte = new byte[k];
        System.arraycopy(keyinbuf, 0, tempByte, 0, k);
        return tempByte;
    }

    public ArrayList<byte[]> byteAnalyze(ArrayList<Integer> test, byte[] result) {
        // TODO 自動生成されたメソッド・スタブ


        ArrayList<byte[]> arrayList = new ArrayList<>();
        Log.d(TAG, new String(result));
        int i = 0;
        // 配列を作成して詰める
        Log.d(TAG, "test.size():" +test.size());
        if (test.size() == 0) {
//            byte[] b = new byte[result.length];
            if (byteGrobal != null) {
                byte[] newByte = new byte[byteGrobal.length + result.length];
                System.arraycopy(byteGrobal, 0, newByte, 0, byteGrobal.length);
                System.arraycopy(result, 0, newByte, byteGrobal.length, result.length);
                byteGrobal = newByte;
                return null;
            } else {
                byteGrobal = result;
                return null;
            }
        } else {
            for (int kugiri : test) {
                byte[] b = new byte[kugiri + 1 - i];
                arrayList.add(b);
                i = kugiri + 1;
            }
        }

//        if (arrayList.size() == 1 && test.size() == 0) {
//            if (byteGrobal != null) {
//                byte[] b = arrayList.get(0);
//                byte[] newByte = new byte[byteGrobal.length + b.length];
//                System.arraycopy(byteGrobal, 0, newByte, 0, byteGrobal.length);
//                System.arraycopy(b, 0, newByte, byteGrobal.length, b.length);
//                byteGrobal = newByte;
//                return null;
//            } else {
//                byte[] b = arrayList.get(0);
//                byteGrobal = b;
//                return null;
//            }
//        }


        int lastKugiri = test.get(test.size() - 1) + 1;
        byte[] lb = new byte[result.length - lastKugiri];
        arrayList.add(lb);

        // 区切りの中身をコピーしていく
        int j = 0;
        for (byte[] b : arrayList) {
            System.arraycopy(result, j, b, 0, b.length);
            j = b.length + j;
        }

        if (arrayList.size() > 1) {
            if (byteGrobal != null) {
                byte[] b = arrayList.get(0);
                byte[] newByte = new byte[byteGrobal.length + b.length];
                System.arraycopy(byteGrobal, 0, newByte, 0, byteGrobal.length);
                System.arraycopy(b, 0, newByte, byteGrobal.length, b.length);
                arrayList.set(0, newByte);
                ArrayList<byte[]> tempList = new ArrayList<byte[]>();
                for (byte[] b3 : arrayList) {
                    if (b3.length != 0) {
                        tempList.add(b3);
                    }
                }
                arrayList = tempList;
            }

            if (arrayList.size() > test.size()) {
                byte[] b2 = arrayList.get(arrayList.size() - 1);
                byteGrobal = b2;
                arrayList.remove(arrayList.size() - 1);
            } else {
                byteGrobal = null;
            }
        }

        return arrayList;
    }

    public ArrayList<Integer> subByte(byte[] result) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < result.length; i++) {
            if (result[i] == 0) {
                arrayList.add(i);
            }
        }

        // if (result.length != arrayList.get(arrayList.size()-1)) {
        //
        // }
        return arrayList;
    }

    public ArrayList<String> abstractXMLCreate2(Context context, byte[] keyinbuf, int k) {
        ArrayList<String> arrayResult = new ArrayList<>();
        String temp10 = null;
        temp10 = new String(keyinbuf, 0, k);

        byte[] tempByte = new byte[k];
        System.arraycopy(keyinbuf, 0, tempByte, 0, k);
        byteIndexOf(tempByte);

        int zerokara = 0;
        while (true) {

            int kokomade = temp10.indexOf("\0", zerokara);

            if (kokomade != -1) {
                String result = temp10.substring(zerokara, kokomade);
                if (strGrobal.equals("")) {
                    arrayResult.add(result);
                    zerokara = kokomade + 1;
                } else if (!strGrobal.equals("")) {
                    arrayResult.add(strGrobal + result);
                    zerokara = kokomade + 1;
                    strGrobal = "";
                }
            } else {

                if (zerokara == temp10.length()) {

                    return arrayResult;
                } else {
                    String result = temp10.substring(zerokara, temp10.length());
                    strGrobal = result;
                    return arrayResult;
                }
            }

        }
    }

    private void byteIndexOf(byte[] tempByte) {

    }

    protected boolean checkConnectId(String str) {
        int j = str.indexOf(context.getString(R.string.receive_connect));
        if (j == 0) {
            return true;
        }
        return false;
    }

    protected boolean checkTimeout(String str) {
        int timeout = str.indexOf("Connection timeout");
        if (timeout == 0) {
            return true;
        }
        return false;
    }

    protected XmlPullParser xmlPullParserGet(String str) throws MyException {
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            throw new MyException("ERROR: xpg0x11 ");
        }
        factory.setNamespaceAware(true);
        XmlPullParser xpp = null;
        try {
            xpp = factory.newPullParser();
        } catch (XmlPullParserException e) {
            throw new MyException("ERROR: xpg0x11");
        }

        try {
            xpp.setInput(new StringReader(str));
        } catch (XmlPullParserException e) {
            throw new MyException("ERROR: xpg0x11");
        }

        return xpp;
    }

    protected void xmlConnect(XmlPullParser xpp) {
        String strId = xpp.getAttributeValue(null, "id");
        try {
            int monaId = Integer.parseInt(strId);
            /**
             *  database accessを減らす為、monaIdに
             */
            TestShared testShared = TestShared.getInstance(context);
            testShared.setMeMonaId(monaId);
//                    MeMonaId.monaId = monaId;
        } catch (NumberFormatException e) {
            return;
        }

    }
}
