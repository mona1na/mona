package com.devmona;

import java.util.ArrayList;

/**
 * Created by sake on 2018/03/19.
 */


public class MeMonaId {
    /**
     * save monaId, reduces database access
     */
//    public static int monaId = -1;
    public static int uniqueId;
    public static Person me;
    public static ArrayList<String> notifyArray;
//    public static Room room;
//    public static int roomNo;
//    public static int roomCount;
//    public static String roomName;

    public static void setMe(Person me) {
        MeMonaId.me = me;
    }

    public static Person getMe() {
        return me;
    }

//    public static void setRoomName(String roomName) {
//        MeMonaId.roomName = roomName;
//    }
//
//    public static String getRoomName() {
//        return roomName;
//    }

//    public static void setRoom(Room room) {
//        room = room;
//    }
}
