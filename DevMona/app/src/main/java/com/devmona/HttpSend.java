package com.devmona;

import android.content.Context;
import android.os.Build;
import android.util.Log;




import org.apache.commons.codec.net.URLCodec;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.ConnectionSpec;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;

/**
 * Created by sake on 2017/11/18.
 */

public class HttpSend {

    public static String testHttpSend2(JSONObject jsonObject, String key, String urlstr) {
//        String andmona = "andmona=";
        String data = "";

        data = key + jsonObject.toString();


        HttpsURLConnection con = null;
        URL url = null;
        String result = "";
        String resultOk = "";
        try {
            url = new URL(urlstr);
//            con = (HttpsURLConnection) url.openConnection();
            con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setInstanceFollowRedirects(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            OutputStream os = con.getOutputStream();
            PrintStream ps = new PrintStream(os);
//            Log.d("HTTPSEND",data);
            ps.print(data);
            con.connect();

            InputStream in = con.getInputStream();
            byte bodyByte[] = new byte[1024];
            int k = in.read(bodyByte);

            try {
                result = new String(bodyByte, 0, k);
                resultOk = result.trim();
            } catch (StringIndexOutOfBoundsException e) {
                result = null;
            }

            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return resultOk;
    }


    public static Object testHttpSend(JSONObject jsonObject, String key, String urlstr, int timeout) {
//        String andmona = "andmona=";
        String data = "";
        if (jsonObject == null) {

        } else {
//            data = key + jsonObject.toString();
            URLCodec codec = new URLCodec("UTF-8");

            try {
                data = key + "="+codec.encode(jsonObject.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }



        HttpURLConnection con = null;
        URL url = null;
        String result = "";
        String resultOk = "";
        try {
            url = new URL(urlstr);
//            con = (HttpsURLConnection) url.openConnection();
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setInstanceFollowRedirects(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setConnectTimeout(timeout);
            OutputStream os = con.getOutputStream();
            PrintStream ps = new PrintStream(os);
//            Log.d("HTTPSEND",data);
            ps.print(data);
            con.connect();

            InputStream in = con.getInputStream();
            byte bodyByte[] = new byte[1024];
            int k = in.read(bodyByte);

            try {
                result = new String(bodyByte, 0, k);
                resultOk = result.trim();
            } catch (StringIndexOutOfBoundsException e) {
                // レスポンスが空の時にkが-1になって起こる
                // またはnullの際にも起こる
                key = key.replaceAll("=", "");
                Log.d("httpResponse", "{\"error\":\"server: error p0x1" + key + "\"}");
                return "{\"error\":\"server: error p0x1" + key + "\"}";
//                return "ERROR: server error p0x1";
            }

            in.close();
        } catch (MalformedURLException e) {
            // URLではなくhttpだったttpのようなプロトコルを指定した場合に起こる
//            e.printStackTrace();
            key = key.replaceAll("=", "");
            Log.d("httpResponse", "{\"error\":\"server: protocol error " + key + " \"}");
            return "{\"error\":\"server: protocol error " + key + " \"}";
//            return "ERROR: protocol error";
        } catch (IOException e) {
            // timeout時に起こるexception
//            e.printStackTrace();
//            return "ERROR: server down or url invalid, disconnected on device";
            key = key.replaceAll("=", "");
            Log.d("httpResponse", "{\"error\":\"server: server down or url invalid, disconnected on device "+key+"\"}");
            return "{\"error\":\"server: server down or url invalid, disconnected on device "+key+"\"}";
        }
        Log.d("httpResponse", resultOk.toString());
        return resultOk;
    }

    public static Object testHttpsSend(JSONObject jsonObject, final String key, String urlstr, int timeout, Context context)  {

//

        OkHttpClient okHttpClient = getClient();

        RequestBody body = new FormBody.Builder()
                .add(key, jsonObject.toString())
                .build();

        Request request = new Request.Builder()
                .url(urlstr)       // HTTPアクセス POST送信 テスト確認用ページ
                .post(body)
                .build();
        Response response = null;
        String result = null;
        try {
            response = okHttpClient.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            return "{\"error\":\"server error\"}";
        }
        response.body().close();

//        response.body().close();
//        int result = response.code();
//        if (result == 200) {
//            return "OK";
//        }
        return result;

//        String andmona = "andmona=";

//        OkHttpClient client = new OkHttpClient();
//        MediaType MIMEType= MediaType.parse("application/json; charset=utf-8");
//
//        Request request = new Request.Builder().url(urlstr).build();
//        Call call = client.newCall(request);


//        OkHttpClient client = new OkHttpClient();
//        MediaType MIMEType = MediaType.parse("text/plain; charset=utf-8");
//        URLCodec codec = new URLCodec("UTF-8");
//        RequestBody requestBody = null;
//        try {
//            requestBody = RequestBody.create (MIMEType, key + codec.encode(jsonObject.toString(), "UTF-8"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        Request request = new Request.Builder().url(urlstr).post(requestBody).build();
//        Response response = null;
//        try {
//            response = client.newCall(request).execute();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


//        String data = "";
//        if (jsonObject == null) {
//
//        } else {
////            data = key + jsonObject.toString();
//            URLCodec codec = new URLCodec("UTF-8");
//
//            try {
//                data = key + codec.encode(jsonObject.toString(), "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//
//        HttpsURLConnection con = null;
//        URL url = null;
//        String result = "";
//        String resultOk = "";
//        try {
//            url = new URL(urlstr);
////            con = (HttpsURLConnection) url.openConnection();
//            con = (HttpsURLConnection) url.openConnection();
//            con.setRequestMethod("POST");
//            con.setInstanceFollowRedirects(false);
//            con.setDoInput(true);
//            con.setDoOutput(true);
//            con.setConnectTimeout(timeout);
//            OutputStream os = con.getOutputStream();
//            PrintStream ps = new PrintStream(os);
////            Log.d("HTTPSEND",data);
//            ps.print(data);
//            con.connect();
//
//            InputStream in = con.getInputStream();
//            byte bodyByte[] = new byte[1024];
//            int k = in.read(bodyByte);
//
//            try {
//                result = new String(bodyByte, 0, k);
//                resultOk = result.trim();
//            } catch (StringIndexOutOfBoundsException e) {
//                // レスポンスが空の時にkが-1になって起こる
//                // またはnullの際にも起こる
//                key = key.replaceAll("=", "");
//                Log.d("httpResponse", "{\"error\":\"server: error p0x1" + key + "\"}");
//                return "{\"error\":\"server: error p0x1" + key + "\"}";
////                return "ERROR: server error p0x1";
//            }
//0
//            in.close();
//        } catch (MalformedURLException e) {
//            // URLではなくhttpだったttpのようなプロトコルを指定した場合に起こる
////            e.printStackTrace();
//            key = key.replaceAll("=", "");
//            Log.d("httpResponse", "{\"error\":\"server: protocol error " + key + " \"}");
//            return "{\"error\":\"server: protocol error " + key + " \"}";
////            return "ERROR: protocol error";
//        } catch (IOException e) {
//            e.printStackTrace();
//            // timeout時に起こるexception
////            e.printStackTrace();
////            return "ERROR: server down or url invalid, disconnected on device";
//            key = key.replaceAll("=", "");
//            Log.d("httpResponse", "{\"error\":\"server: server down or url invalid, disconnected on device "+key+"\"}");
//            return "{\"error\":\"server: server down or url invalid, disconnected on device "+key+"\"}";
//        }
//        Log.d("httpResponse", resultOk.toString());
//        return response;
    }

    public static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return client;
    }

    private static X509TrustManager getTrustManager() throws NoSuchAlgorithmException, KeyStoreException {
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance( TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init((KeyStore) null);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        }
        X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
        return trustManager;
    }
    private static OkHttpClient getClient(){
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS).build();
        OkHttpClient okHttpClient;
        try {
            okHttpClient = new OkHttpClient.Builder()
                    .connectionSpecs(Collections.singletonList(spec))
                    .sslSocketFactory(new TLSSocketFactory(), getTrustManager())
                    .build();
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new RuntimeException(e);
        }
        return okHttpClient;
    }
}
