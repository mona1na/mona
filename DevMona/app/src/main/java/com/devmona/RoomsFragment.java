package com.devmona;


import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatDelegate;

import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.devmona.extendsviews.RoomsClickListener;
import com.devmona.loadercallbacks.LoaderCallbacks2FromSocket;
import com.devmona.loadercallbacks.LoaderCallbacks3FromSocket;

import java.util.LinkedHashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class RoomsFragment extends Fragment {


    private boolean mInit;
    private GridView gridView;
    private LinearLayout linearChara;
    private LinearLayout testLeLinearLayout;
    private LinearLayout linearRooms2;
    private ConstraintLayout constraintLayout;
    private Handler m_Handler;
    private GridAdapter gridAdapter;
    private Person me;
    private String mSeni;
    private String mTestState;
    private LinearLayout linearNameChange;
    private View view;
    private LinearLayout linearUpdate;
    private static final String TAG = "RoomsFragment";
    public static int pos;


    public RoomsFragment() {
        // Required empty public constructor
        this.mTestState = "";
        this.mInit = false;
    }

    public void setSeni(String mSeni) {
        this.mSeni = mSeni;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "roomsFrag onCreateView");
        // Inflate the layout for this fragment
        /**
         * ①設定
         */
        view = inflater.inflate(R.layout.fragment_rooms, container, false);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        init2();
        findViews3(view);
        adapterSet4();
        buttonSetting5();

        /**
         * menuを設定する
         */
        Base2Activity base2Activity = (Base2Activity) getActivity();
        Room room = new Room();
        room.setRoomName("部屋選択");
        base2Activity.showRoomName(room);
        /**
         * 通信
         */
        intentGetProcess1();
        return view;
    }


    private void init2() {
        TestShared testShared = TestShared.getInstance(getContext());
        Room room = new Room();
        room.setRoomName(getString(R.string.slMONA8094));
        room.setRoomNumber(8094);
        testShared.setRoom(room);
    }

    private void findViews3(View view) {
        gridView = (GridView) view.findViewById(R.id.rooms_grid);
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d("scrollTest2", "gridView.getFirstVisiblePosition():" + gridView.getFirstVisiblePosition());
            }
        });

        linearChara = (LinearLayout) view.findViewById(R.id.rooms2_chara);
        testLeLinearLayout = (LinearLayout) view.findViewById(R.id.testLinear);
        linearRooms2 = (LinearLayout) view.findViewById(R.id.rooms2_linear);
        ProgressFragment progressFragment = new ProgressFragment();

        Base2Activity nameActivity = (Base2Activity) getActivity();
        if (nameActivity != null) {
            Log.d(TAG, "findViews3 progress start");
            nameActivity.getSupportFragmentManager().beginTransaction().add(R.id.progress_fragment, progressFragment, getContext().getString(R.string.fragment_progress)).commit();
        }
        constraintLayout = (ConstraintLayout) view.findViewById(R.id.rooms2_constraint);
        linearNameChange = (LinearLayout) view.findViewById(R.id.rooms2_name_change);
        linearUpdate = (LinearLayout) view.findViewById(R.id.rooms2_update);
        Animator animator = AnimatorInflater.loadAnimator(getContext(), R.animator.rooms2_animator);
        animator.setTarget(constraintLayout);
        animator.start();
    }

    private void adapterSet4() {
        LinkedHashMap<Integer, Room> roomsMap = MyApplication.roomsMap;

        gridAdapter = new RoomsFragment.GridAdapter(getContext(), R.layout.room2_btn);
        gridView.setAdapter(gridAdapter);
        for (int roomNo : roomsMap.keySet()) {
            gridAdapter.add(roomsMap.get(roomNo));
        }

    }

    private void buttonSetting5() {
        /**
         * キャラクター画面
         */
        linearChara.setOnClickListener(new RoomsClickListener() {
            @Override
            public void onClick(View v) {
                super.onClick(RoomsFragment.this.getView());
                Log.d(TAG, "ueclick");
                v.setEnabled(false);
                CharaSentakuFragment charaSentakuFragment = new CharaSentakuFragment();
                Base2Activity nameActivity = (Base2Activity) getActivity();
                if (nameActivity != null) {
                    FragmentTransaction fragmentTransaction = nameActivity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.animator.chara_animator1, R.animator.chara_animator2, R.animator.chara_animator1, R.animator.chara_animator2);
                    fragmentTransaction.replace(R.id.main4_option, charaSentakuFragment);
                    fragmentTransaction.commit();
                }
            }
        });


        /**
         * 名前画面
         */
        linearNameChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Name2Fragment name2Fragment = new Name2Fragment();
                name2Fragment.intentSet(getString(R.string.fragment_rooms));
                Base2Activity nameActivity = (Base2Activity) getActivity();
                if (nameActivity != null) {
                    FragmentTransaction fragmentTransaction = nameActivity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.test_fragment, name2Fragment, getString(R.string.fragment_name));
                    fragmentTransaction.commit();
                }

            }
        });

        linearUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressFragment progressFragment = new ProgressFragment();
                Base2Activity nameActivity = (Base2Activity) getActivity();
                if (nameActivity != null) {
                    nameActivity.getSupportFragmentManager().beginTransaction().add(R.id.progress_fragment, progressFragment, getString(R.string.fragment_progress)).commit();
                }
                getActivity().getSupportLoaderManager().initLoader(3, new Bundle(), new LoaderCallbacks3FromSocket((Base2Activity) getContext()));
            }
        });
    }

    /**
     * 画面から
     */
    private void intentGetProcess1() {

        /**
         * ここから全て通信
         */
        Base2Activity nameActivity = (Base2Activity) getActivity();
        if (nameActivity != null) {
            nameActivity.getSupportLoaderManager().initLoader(2, new Bundle(), new LoaderCallbacks2FromSocket(getContext()));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Base2Activity nameActivity = (Base2Activity) getActivity();
    }

    public void test() {
        Base2Activity nameActivity = (Base2Activity) getActivity();
        gridView.setSelection(pos);
        if (nameActivity != null) {
            TestShared testShared = TestShared.getInstance(getContext());
            Room room = new Room();
            room.setRoomName(getString(R.string.slMONA8094));
            room.setRoomNumber(8094);
            testShared.setRoom(room);
            Room room2 = new Room();
            room2.setRoomName("部屋選択");
            Base2Activity base2Activity = (Base2Activity) getActivity();
            base2Activity.showRoomName(room2);
            nameActivity.getSupportLoaderManager().initLoader(2, new Bundle(), new LoaderCallbacks2FromSocket(getContext()));
        }

    }

    public void setBottonEnable(boolean b) {
        linearChara.findViewById(R.id.rooms2_chara).setEnabled(true);
        linearNameChange.findViewById(R.id.rooms2_name_change).setEnabled(true);
        linearUpdate.findViewById(R.id.rooms2_update).setEnabled(true);
    }


    class GridAdapter extends ArrayAdapter<Room> {


        class ViewHolder {
            Button button;
            Room room;
        }

        //        private Map roomMap;
        private final Context context;
        private final int layoutId;
        private Map<Integer, Room> roomMap;
        private final LayoutInflater inflater;

        GridAdapter(Context context, int layoutId) {
            super(context, layoutId);
            this.inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.context = context;
            this.layoutId = layoutId;
        }


        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            final RoomsFragment.GridAdapter.ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(layoutId, parent, false);
            } else {

            }

            final Room room = getItem(position);
            Button button = (Button) convertView.findViewById(R.id.grid_btn);
            if (room.getRoomCount() == -1) {
                button.setText(room.getRoomName() + "\r\n");
            } else {
                button.setText(room.getRoomName() + "\r\n" + room.getRoomCount());
                StateListDrawable stateListDrawable = new StateListDrawable();
                stateListDrawable.addState(new int[]{-android.R.attr.state_pressed}, OptionRoomsActivity.createColorButton3(room.getRoomCount()));
                stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(Color.argb(100, 100, 100, 100)));
                button.setBackground(stateListDrawable);
                button.invalidate();
            }

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "button On Click: room no:" + room.getRoomNumber());
                    TestShared testShared = TestShared.getInstance(context);
                    String name = Util.getRoomName(room.getRoomName());
                    room.setRoomName(name);
                    testShared.setRoom(room);
                    pos = gridView.getFirstVisiblePosition();
//                    getActivity().getSupportLoaderManager().initLoader(17, new Bundle(), new LoaderCallbacksTest17((Base2Activity)getContext()));

                    Toast toast = Toast.makeText(getContext(), name + getString(R.string.roomsActivity_enter), Toast.LENGTH_LONG);
                    ViewGroup group = (ViewGroup) toast.getView();
                    TextView textView = (TextView) group.getChildAt(0);
                    textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.toast_size10));
                    toast.show();
                    /**
                     * main4Fragmentを呼び出す
                     */
                    Main4Fragment main4Fragment = new Main4Fragment();
                    Base2Activity nameActivity = (Base2Activity) getActivity();
                    if (nameActivity != null) {
//                        nameActivity.getSupportActionBar().hide();
                        FragmentTransaction fragmentTransaction = nameActivity.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.remove(RoomsFragment.this);
//                        fragmentTransaction.hide(RoomsFragment.this);
                        fragmentTransaction.replace(R.id.main4_fragment, main4Fragment, context.getString(R.string.fragment_main4));
                        fragmentTransaction.commit();
                    }
                }
            });
            button.invalidate();
            return convertView;
        }

    }

    public void RoomsSet(LinkedHashMap<Integer, Room> roomsMap) {
        Log.d(TAG, "roomsSet");
        for (int roomNo : roomsMap.keySet()) {
            Room tempRoom = roomsMap.get(roomNo);
            Room room = gridAdapter.getItem(tempRoom.getRoomNumber() - 1);
            room.setRoomCount(tempRoom.getRoomCount());
            View v = gridAdapter.getView(tempRoom.getRoomNumber() - 1, null, null);
            Button button = v.findViewById(R.id.grid_btn);
            button.setText(room.getRoomName() + "\r\n" + room.getRoomCount());
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{-android.R.attr.state_pressed}, OptionRoomsActivity.createColorButton3(room.getRoomCount()));
            stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(Color.argb(100, 100, 100, 100)));
            button.setBackground(stateListDrawable);
            gridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("lifeTest", "roomsFrag onResume");
        /**
         * onPauseからの復帰なら
         * 部屋情報を更新し反映する
         */
        mTestState = "onResume";
        if (mInit) {
            gridAdapter.notifyDataSetChanged();
        }
        mInit = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "roomsFrag onPause");
        mTestState = "onPause";
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "roomsFrag onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "roomsFrag onDestroy");
    }
}
