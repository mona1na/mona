package com.devmona;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sake on 2018/01/01.
 */

public class ErrorDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        // タイトル非表示
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // ダイアログの中身
        dialog.setContentView(R.layout.fragment_errordialog);
        ListView listView = (ListView) dialog.findViewById(R.id.list_error);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.list_error);
        listView.setAdapter(arrayAdapter);

        Bundle bundle = getArguments();
        String result = bundle.getString("error");
        try {
            JSONObject jsonObject = new JSONObject(result);
            String error = jsonObject.getString("error");
            arrayAdapter.add(error);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return dialog;
    }
}
