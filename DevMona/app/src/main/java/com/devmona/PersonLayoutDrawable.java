package com.devmona;

import android.animation.ValueAnimator;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by sake on 2018/03/07.
 */

public class PersonLayoutDrawable {

    private Drawable drawable;
    private LinearLayout linearLayout;
    private RelativeLayout linearLayout2;
//    private FrameLayout.LayoutParams layoutParams;
    /**
     * 縦用のサイズ
     */
    private FrameLayout.LayoutParams layoutParams2;

    /**
     * レギュラー用サイズ
     */
    private FrameLayout.LayoutParams layoutRegularParams;

    private ValueAnimator valueAnimator;

    public void setValueAnimator(ValueAnimator valueAnimator) {
        this.valueAnimator = valueAnimator;
    }

    public FrameLayout.LayoutParams getLayoutRegularParams() {
        return layoutRegularParams;
    }

    public void setLayoutRegularParams(FrameLayout.LayoutParams layoutRegularParams) {
        this.layoutRegularParams = layoutRegularParams;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public ValueAnimator getValueAnimator() {
        return valueAnimator;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public FrameLayout.LayoutParams getLayoutParams2() {
        return layoutParams2;
    }

    public void setLayoutParams2(FrameLayout.LayoutParams layoutParams2) {
        this.layoutParams2 = layoutParams2;
    }

    public RelativeLayout getRelativeLayout2() {
        return linearLayout2;
    }

    public void setRelativeLayout2(RelativeLayout linearLayout2) {
        this.linearLayout2 = linearLayout2;
    }



    public PersonLayoutDrawable() {
    }


//    public void setLayoutParams(FrameLayout.LayoutParams layoutParams) {
//        this.layoutParams = layoutParams;
//    }



    public void setLinearLayout(LinearLayout linearLayout) {
        this.linearLayout = linearLayout;
    }

//    public FrameLayout.LayoutParams getLayoutParams() {
//        return layoutParams;
//    }

    public LinearLayout getLinearLayout() {
        return linearLayout;
    }

}
