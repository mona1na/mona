package com.devmona;

import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;

/**
 * Created by sake on 2018/03/18.
 */

public class GCDB {

//    public int getGCCount() {
//        String SQL = "SELECT COUNT(*) FROM `gc` WHERE flag = 0";
//
//        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
//        boolean next = cursor.moveToFirst();
//        int count = -1;
//        while (next) {
//            count = cursor.getInt(0);
//            next = cursor.moveToNext();
//        }
//        cursor.close();
//        return count;
//    }

    public DBStatus getDBStatusByStartId() {
        String SQL = "SELECT * FROM `dbstatus` WHERE `id` IN (SELECT MAX(id) FROM dbstatus GROUP BY `start_id`)";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        DBStatus dbStatus = new DBStatus();
        while (next) {
            dbStatus.setsPId(cursor.getInt(cursor.getColumnIndex("s_p_id")));
            dbStatus.setsStartId(cursor.getInt(cursor.getColumnIndex("s_start_id")));
            dbStatus.setsLoginId(cursor.getInt(cursor.getColumnIndex("s_login_id")));
            dbStatus.setsRoomId(cursor.getInt(cursor.getColumnIndex("s_room_id")));
            dbStatus.setsCmtId(cursor.getInt(cursor.getColumnIndex("s_cmt_id")));
            next = cursor.moveToNext();
        }
        cursor.close();
        return dbStatus;
    }

    public JSONArray getGC0flag() {
        String SQL = "SELECT `id`,`date`,`gc` FROM `gc` WHERE `flag` = 0";
        Cursor cursor = UtilInfo.m_db.rawQuery(SQL, null);
        boolean next = cursor.moveToFirst();
        JSONArray jsonArraySoto = new JSONArray();
        while (next) {
            JSONArray jsonArrayNaka = new JSONArray();
            jsonArrayNaka.put(cursor.getInt(cursor.getColumnIndex("id")));
            jsonArrayNaka.put(cursor.getString(cursor.getColumnIndex("date")));
            jsonArrayNaka.put(cursor.getString(cursor.getColumnIndex("gc")));
            Log.d("20180704gc", cursor.getString(cursor.getColumnIndex("gc")));
            jsonArraySoto.put(jsonArrayNaka);
            next = cursor.moveToNext();
        }
        cursor.close();
        return jsonArraySoto;
    }

    public void updateGCflag() {
        String SQL = "UPDATE gc SET flag = 1 WHERE flag = 0";
        UtilInfo.m_db.execSQL(SQL);
    }
}
