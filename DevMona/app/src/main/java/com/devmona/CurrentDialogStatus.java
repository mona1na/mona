package com.devmona;

/**
 * Created by sake on 2018/04/08.
 */

public class CurrentDialogStatus {
    private int id;
    private int page;
    private String aa_name;

    public int getId() {
        return id;
    }

    public int getPage() {
        return page;
    }

    public String getAa_name() {
        return aa_name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAa_name(String aa_name) {
        this.aa_name = aa_name;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
