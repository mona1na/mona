package com.devmona;


import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devmona.loadercallbacks.LoaderCallbacksTest18;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.HashMap;

import static com.devmona.Prop.PREFERENCE_INIT;


/**
 * A simple {@link Fragment} subclass.
 */
public class Name2Fragment extends Fragment {


    private TextView txtVersionNotice;
    private TextView txtFuture;
    private EditText editText;
    private Button btnOk;
    private LinearLayout linearLayout;
    private FrameLayout wholeView;
    private LinearLayout linearUp;
    private Button btnUpdate;
    private Person me;
    private String seni;
    private View view;
    public static final String TAG = "Name2Fragment";
    private Button btnRandom;
    private ImageView imgNew;
    private ImageView mImageSetting;
    private Base2Activity mActivity;

    public Name2Fragment() {
        this.seni = "";
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("lifetest", "Name2Frag onCreateView");
        mActivity = (Base2Activity) getActivity();
        /**
         * ①init
         */
        initName();

        // Inflate the layout for this fragment

        /**
         * ②view系
         */
        findViews(inflater, container);

        /**
         * ③サーバー送受信
         */
        intentGet();

        /**
         * ④viewに設定する
         */
        initViews();
        return view;
    }

    private void findViews(LayoutInflater inflater, ViewGroup container) {
        view = inflater.inflate(R.layout.fragment_name2, container, false);
        txtVersionNotice = (TextView) view.findViewById(R.id.name_version_notice);
        imgNew = (ImageView) view.findViewById(R.id.name_img_new);
        animetionSet();
        btnRandom = (Button) view.findViewById(R.id.name_random_button);
        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                LoaderManager.getInstance(Name2Fragment.this).initLoader(19,bundle,new RandomCallbacks19(getContext(),bundle));
            }
        });
        txtVersionNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    Log.d("FirebaseInstanceId", "getInstanceId failed", task.getException());
                                    return;
                                }

                                // Get new Instance ID token
                                String token = task.getResult().getToken();

                                // Log and toast
//                                String msg = getString(R.string.msg_token_fmt, token);
                                Log.d("aiueo", token);
//                                Toast.makeText(getContext(), token, Toast.LENGTH_SHORT).show();

                            }

                        });
            }
        });
        txtFuture = (TextView) view.findViewById(R.id.name_new_future);
        txtFuture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewFutureFragment newFutureFragment = new NewFutureFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2, R.animator.update_animator1, R.animator.chara_animator2);
                fragmentTransaction.replace(R.id.main4_option, newFutureFragment);
                fragmentTransaction.commit();
            }
        });
        editText = (EditText) view.findViewById(R.id.name_edit_name);
        btnOk = (Button) view.findViewById(R.id.name_btn_ok);
        linearLayout = (LinearLayout) view.findViewById(R.id.name_linear);
        wholeView = (FrameLayout) view.findViewById(R.id.name_whole);
        linearUp = (LinearLayout) view.findViewById(R.id.name_linear_update);
        linearUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateFragment updateFragment = new UpdateFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2, R.animator.update_animator1, R.animator.chara_animator2);
                fragmentTransaction.replace(R.id.main4_option, updateFragment);
                fragmentTransaction.commit();
            }
        });

    }

    private void animetionSet() {
        Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        imgNew.startAnimation(animation);
    }

    private void initName() {
        Base2Activity nameActivity = (Base2Activity) getActivity();
//        if (!nameActivity.getSupportActionBar().isShowing()) {
////            nameActivity.getSupportActionBar().show();
//        }
    }

    private final LoaderManager.LoaderCallbacks<Object> mCallback =
            new LoaderManager.LoaderCallbacks<Object>() {


                @Override
                public Loader<Object> onCreateLoader(int id, Bundle bundle) {
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("key", 1);
                    MyLoader myLoader = new MyLoader(getContext(), bundle1);
                    return myLoader;
                }

                @Override
                public void onLoadFinished(Loader<Object> loader, Object data) {
                    LoaderManager.getInstance(getActivity()).destroyLoader(1);

                    if (Prop.DEBUG_MODE == 1) {
                        data = "OK";
                    }

                    // server is ok
                    if (data.equals(Prop.SERVE_MESSAGE)) {
                        notErrorSetBtnEnter(data.toString());
                        return;

                    }
                    // server is error
                    else {
                        Toast.makeText(getContext(),"error",Toast.LENGTH_LONG);
                        final String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
                        UtilInfo.errorSend(methodName);
                        return;
                    }
                }


                private void showErrorDialog(final String string) {
                    Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ErrorDialogFragment errorDialogFragment =
                                    new ErrorDialogFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("error", string);
                            errorDialogFragment.setArguments(bundle);
                            errorDialogFragment.show(getActivity().getFragmentManager(), "errorDialog");
                        }
                    });
                }

                private void notErrorSetBtnEnter(String result) {
//                    SentakuDB sentakuDB = new SentakuDB();
////                    DBStatus dbStatus = sentakuDB.getFromTo();
//                    sentakuDB.updateGU();

                    btnEnterSet();

                }

                private void errorSetBtnEnter() {
                    Toast.makeText(getActivity(), "サーバーエラー", Toast.LENGTH_LONG);
                }

                @Override
                public void onLoaderReset(Loader<Object> loader) {

                }
            };

    private void intentGet() {
        /**
         * versionを取得して反映する
         */
        LoaderManager.getInstance(getActivity()).initLoader(12, null, mCallback2);

        /**
         * startを送る
         */
        LoaderManager.getInstance(getActivity()).initLoader(1, null, mCallback);

        /**
         * 最新更新情報を取得する
         */
        LoaderManager.getInstance(getActivity()).initLoader(18, new Bundle(), new LoaderCallbacksTest18((Base2Activity) getActivity()));

    }

    private int getState() {
        int state;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        state = sp.getInt("initState", PREFERENCE_INIT);
        return state;
    }

    private final LoaderManager.LoaderCallbacks<Object> mCallback2 =
            new LoaderManager.LoaderCallbacks<Object>() {

                @NonNull
                @Override
                public Loader<Object> onCreateLoader(int id, @Nullable Bundle args) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("key", 12);
                    MyLoader myLoader = new MyLoader(mActivity, bundle);
                    return myLoader;
                }

                @Override
                public void onLoadFinished(@NonNull Loader<Object> loader, Object data) {
                    LoaderManager.getInstance(mActivity).destroyLoader(12);
                    txtVersionNotice.setText(data.toString());
                }

                @Override
                public void onLoaderReset(@NonNull Loader<Object> loader) {

                }
            };

    @Override
    public void onResume() {
        super.onResume();
        Log.d("lifetest", "Name2Frag onResume");
        if (PREFERENCE_INIT == getState()) {

            /**
             * ダイアログを出す
             */
            TeamsFragmentDialog teamsFragmentDialog = new TeamsFragmentDialog();
            teamsFragmentDialog.show(getActivity().getSupportFragmentManager(), "teams");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("lifetest", "Name2Frag onPause");
    }

    private void initViews() {
        Log.d("TESTCASE", "initViews");


        me = TestShared.getInstance(getContext()).getMe();


        // createMeで取得したpropprivacytableの内容を埋め込む
        btnOk.setEnabled(false);

        if (me.getTrip() == null) {
            editText.setText(me.getName());
        } else {
            editText.setText(me.getName() + "#" + me.getTrip());
        }
        // txtname

        // aa
//        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), (int) me.getAaresno());


    }

    private HashMap<String, String> mySub(String nameTrip) {
        int i = nameTrip.indexOf("#");
        HashMap<String, String> nameTripMap = new HashMap<>();
        if (i != -1) {
            String name = nameTrip.substring(0, i);
            String trip = nameTrip.substring(i + 1);
            nameTripMap.put("name", name);
            nameTripMap.put("trip", trip);

            return nameTripMap;
        } else {
            nameTripMap.put("name", nameTrip);
            return nameTripMap;
        }
    }

    private ArrayList<String> mySub2(String nameTrip) {
        ArrayList<String> arrayList = new ArrayList<>();
        int i = nameTrip.indexOf("#");
        if (i != -1) {
            arrayList.add(nameTrip.substring(0,i));
            arrayList.add(nameTrip.substring(i+1,nameTrip.length()));
        } else {
            return null;
        }
        return arrayList;
    }

    /**
     * important method
     */
    private void btnEnterSet() {
        btnOk.setEnabled(true);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"click");
//                if (!UtilInfo.clickable()) {
//                    return;
//                }

                /**
                 * ①
                 */
                ArrayList<String> arrayList = mySub2(editText.getText().toString());
                if (arrayList == null) {
                    me.setName(editText.getText().toString());
                    me.setTrip(null);
                } else {
                    me.setName(arrayList.get(0));
                    me.setTrip(arrayList.get(1));
                    Log.d(TAG, "onClick:" +me.getTrip());
                }
                TestShared testShared = TestShared.getInstance(getContext());
                testShared.setMe(me);


                /**
                 * ②RoomsFragmentを出す
                 */

                RoomsFragment roomsFragment = new RoomsFragment();
                roomsFragment.setSeni("NAME");
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.test_fragment, roomsFragment, getString(R.string.fragment_rooms));
                fragmentTransaction.commit();


//                UtilInfo.clickRelease();
            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d("lifetest", "Name2Fragment onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("lifetest", "Name2Fragment onDestroy");
    }

    public void intentSet(String seni) {
        this.seni = seni;
    }
}
