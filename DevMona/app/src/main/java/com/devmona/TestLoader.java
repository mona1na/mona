package com.devmona;

import android.content.Context;
import android.os.Bundle;
import android.content.AsyncTaskLoader;

/**
 * Created by sake on 2017/12/31.
 */

public class TestLoader extends AsyncTaskLoader {
    private Bundle bundle;

    public TestLoader(Context context, Bundle args) {
        super(context);
        bundle = args;
    }

    @Override
    public Object loadInBackground() {
        int value = bundle.getInt("key");
        if (value == 1) {
            String test = "{\"pseq\":1,\"newstartseq\":2,\"mok\":1}";
            return test;
        }
        return null;
    }
}
