package com.devmona;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * Created by sake on 2018/04/07.
 */

public class MyAADialogPagerAdapter extends FragmentStatePagerAdapter {

    private final FragmentManager fm;
    private final Bundle bundle;
    Fragment fragment = null;

    public MyAADialogPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.fm = fm;
        this.bundle = bundle;
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                fragment = AAFragment1.newInstance(1,(Person)bundle.getSerializable("tempPerson"));
                break;
            case 1:
                fragment = AAFragment1.newInstance(2,(Person) bundle.getSerializable("tempPerson"));
                break;
            case 2:
                fragment = AAFragment1.newInstance(3,(Person) bundle.getSerializable("tempPerson"));
                break;
            case 3:
                fragment = AAFragment1.newInstance(4, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 4:
                fragment = AAFragment1.newInstance(5, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 5:
                fragment = AAFragment1.newInstance(6, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 6:
                fragment = AAFragment1.newInstance(7, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 7:
                fragment = AAFragment1.newInstance(8, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 8:
                fragment = AAFragment1.newInstance(9, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 9:
                fragment = AAFragment1.newInstance(10, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 10:
                fragment = AAFragment1.newInstance(11, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 11:
                fragment = AAFragment1.newInstance(12, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 12:
                fragment = AAFragment1.newInstance(13, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 13:
                fragment = AAFragment1.newInstance(14, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 14:
                fragment = AAFragment1.newInstance(15, (Person)bundle.getSerializable("tempPerson"));
                break;
            case 15:
                fragment = AAFragment1.newInstance(16, (Person)bundle.getSerializable("tempPerson"));
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 15;
    }

}
