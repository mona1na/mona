package com.devmona;

public class TestCom {
    private boolean tate2Disp;
    private boolean tate3Disp;
    private String cmt;

    public TestCom(boolean tate2Disp, boolean tate3Disp,String cmt) {
        this.tate2Disp = tate2Disp;
        this.tate3Disp = tate3Disp;
        this.cmt = cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getCmt() {
        return cmt;
    }

    public void setTate2Disp(boolean tate2Disp) {
        this.tate2Disp = tate2Disp;
    }

    public boolean isTate2Disp() {
        return tate2Disp;
    }

    public boolean isTate3Disp() {
        return tate3Disp;
    }

    public void setTate3Disp(boolean tate3Disp) {
        this.tate3Disp = tate3Disp;
    }
}
