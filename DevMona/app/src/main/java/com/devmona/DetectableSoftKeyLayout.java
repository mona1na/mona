package com.devmona;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by sake on 2017/08/22.
 */

public class DetectableSoftKeyLayout extends LinearLayout {
    public DetectableSoftKeyLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public interface OnSoftKeyShownListener {
        public void onSoftKeyShown(boolean isShown);
    }

    private OnSoftKeyShownListener listener;

    public void setListener(OnSoftKeyShownListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        // Viewの高さ
        int viewHeight = MeasureSpec.getSize(heightMeasureSpec);

        Activity activity =(Activity) getContext();
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int statusBarHeight = rect.top;
        int screenHeight = activity.getWindowManager().getDefaultDisplay()
                .getHeight();

        int diff = (screenHeight - statusBarHeight) - viewHeight;
        if (listener != null) {
            listener.onSoftKeyShown(diff > 100);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
