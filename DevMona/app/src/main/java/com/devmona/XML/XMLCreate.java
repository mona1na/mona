package com.devmona.XML;

import android.util.Log;

import com.devmona.Person;
import com.devmona.Prop;
import com.devmona.Room;
import com.devmona.Stub;
import com.devmona.UtilInfo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by sake on 2018/06/15.
 */

public class XMLCreate {

    public static final String TAG = "XMLCreate";

    public static Document getDocument() {
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return documentBuilder.newDocument();
    }

    public static String getXMLString(Document document) {
        Transformer transformer = null;
        try {
            TransformerFactory transformerFactory = TransformerFactory
                    .newInstance();
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }

        StringWriter writer = new StringWriter();

        // Transformerの設定
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty("indent", "yes"); //改行指定
        transformer.setOutputProperty("encoding", "UTF-8"); // エンコーディング


        try {
            transformer.transform(new DOMSource(document), new StreamResult(writer));
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        String result = writer.toString() + "\0";
        return result;
    }

    public static String comXMLGet(String cmt) {
        Document document = getDocument();
        Element element = document.createElement("COM");
        element.setAttribute("cmt", cmt);
        document.appendChild(element);

        return getXMLString(document);
    }

    /**
     * String enter = "<ENTER room='/MONA8094' " + "name='" + me.getName() + "' "
     * + "attrib='no'/>\0";
     *
     * @param me
     * @return
     */
    public static String createEnter1(Person me) {
        Document document = getDocument();
        Element enter = document.createElement("ENTER");
        enter.setAttribute("room", "/MONA8094");
        enter.setAttribute("name", me.getName());
        if (me.getTrip() != null) {
            enter.setAttribute("trip", me.getTrip());
        }
        enter.setAttribute("attrib", "no");
        document.appendChild(enter);
        return getXMLString(document);
    }

    /**
     * String enter = "<ENTER room='/MONA8094/" + testRoom + "' "
     * + "umax='0' "
     * + "type='" + me.getAaname() + "' "
     * + "name='" + me.getName() + "' "
     * + "x='" + me.getX() + "' "
     * + "y='" + me.getY() + "' "
     * + "r='" + me.getRed() + "' "
     * + "g='" + me.getGreen() + "' "
     * + "b='" + me.getBlue() + "' "
     * + "scl='" + me.getScl() + "' "
     * + "stat='" + me.getStat() + "' "
     * + "/>\0";
     *
     * @param me
     */
    public static String createEnter2(Person me) {
        Document document = getDocument();
        Element element = document.createElement("ENTER");
        element.setAttribute("room", "/MONA8094/" + Prop.testRoom);
        element.setAttribute("umax", "0");
        element.setAttribute("type", me.getAaname());
        element.setAttribute("name", me.getName());
        if (me.getTrip() == null) {

        } else {
            element.setAttribute("trip",me.getTrip());
        }
        element.setAttribute("x", me.getX() + "");
        element.setAttribute("y", me.getY() + "");
        element.setAttribute("r", me.getRed() + "");
        element.setAttribute("g", me.getGreen() + "");
        element.setAttribute("b", me.getBlue() + "");
        element.setAttribute("scl", me.getScl() + "");
        element.setAttribute("stat", me.getStat() + "");
        document.appendChild(element);
        return getXMLString(document);
    }

    /**
     * String strEnter = "<ENTER room=\"/MONA8094/" + room.getRoomNumber() + "\" " +
     * "umax=\"0\" " +
     * "type=\"" + me.getAaname() + "\" " +
     * "name=\"" + me.getName() + "\" " +
     * "x=\"" + me.getX() + "\" " +
     * "y=\"" + me.getY() + "\" " +
     * "r=\"" + me.getRed() + "\" " +
     * "g=\"" + me.getGreen() + "\" " +
     * "b=\"" + me.getBlue() + "\" " +
     * "scl=\"" + me.getScl() + "\" " +
     * //                        "stat=\"" + me.getStat() + "\" " +
     * "/>\0";
     *
     * @param me
     * @param room
     * @return
     */
    public static String createEnterRoom(Person me, Room room) {
        Document document = getDocument();
        Element element = document.createElement("ENTER");
        // todo stub
        element.setAttribute("room", "/MONA8094/" + room.getRoomNumber());
//        element.setAttribute("room", "/MONA8094/" + Stub.room);
        element.setAttribute("umax", "0");
        element.setAttribute("type", me.getAaname());
        element.setAttribute("name", me.getName());
        if (me.getTrip() != null) {
            element.setAttribute("trip", me.getTrip());
            Log.d(TAG,me.getTrip());
        }
        element.setAttribute("x", me.getX() + "");
//        element.setAttribute("y", me.getY() + "");
        element.setAttribute("y", Stub.Y + "");
        element.setAttribute("r", me.getRed() + "");
        element.setAttribute("g", me.getGreen() + "");
        element.setAttribute("b", me.getBlue() + "");
        element.setAttribute("scl", UtilInfo.converScal(me.getScl()) + "");
        element.setAttribute("stat", me.getStat() );
        document.appendChild(element);

        return getXMLString(document);
    }

    /**
     * String send = "<EXIT n='" + me.getMonaId() + "' />\0";
     *
     * @param me
     * @return
     */
    public static String createEXIT(Person me) {
        Document document = getDocument();
        Element element = document.createElement("EXIT");
        element.setAttribute("n", me.getMonaId() + "");
        document.appendChild(element);
        return getXMLString(document);
    }
}


