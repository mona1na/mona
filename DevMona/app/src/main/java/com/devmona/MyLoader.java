package com.devmona;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;
import android.util.Log;

import com.devmona.XML.XMLCreate;

/**
 * Created by sake on 2017/06/15.
 */

public class MyLoader extends AsyncTaskLoader<Object> {

    private final Context context;
    Bundle bundle;
    private int i = 0;
    public static final String TAG = "MyLoader";

    public MyLoader(Context context, Bundle args) {
        super(context);
        this.context = context;
        bundle = args;
    }


    // TODO: 4
    @Override
    public Object loadInBackground() {
        int value = bundle.getInt("key");
        Log.d("loadInBack", "key:" + value);
        if (value == 1) {
            return test1();
        } else if (value == 2) {
            return test2();
        } else if (value == 3) {
            return test3();
        } else if (value == 4) {
            return test4();
        } else if (value == 5) {
//---------------------moment--------------------------
//            Object result = JsonCreate.roomsend();
//-----------------------------------------------------
//---------------------setErrorString----------------------------
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("roomseq", 1);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            Object result = jsonObject.toString();
//-----------------------------------------------------
            return test5();


//        } else if (value == 5) {
//            Person me = (Person) bundle.getSerializable(context.getString(R.string.bundle_me));
////            String comment = bundle.getString(context.getString(R.string.bundle_comment));
//            DBStatus dbStatus = (DBStatus) bundle.getSerializable(context.getString(R.string.bundle_dbstatus));
//            Object result = JsonCreate.cmtsend(bundle, me, dbStatus);
//            int i = Respons.cset(result);
//            return i;


        } else if (value == 6) {
            return test6();
        } else if (value == 7) {
//            Person me = (Person) bundle.getSerializable(context.getString(R.string.bundle_me));
//            String comment = bundle.getString(context.getString(R.string.bundle_comment));
//            DBStatus dbStatus = (DBStatus) bundle.getSerializable(context.getString(R.string.bundle_dbstatus));
//            int i = Respons.cset(result);
            return test7();
        } else if (value == 8) {
            return test8();
        } else if (value == 9) {
            test9();
        } else if (value == 10) {
            test10();
        } else if (value == 11) {
            return test11();
        } else if (value == 12) {
            return test12();
        } else if (value == 13) {
            test13();
        } else if (value == 14) {
            test14();
        } else if (value == 15) {
            return test15();
        } else if (value == 16) {
            return test16();
        } else if (value == 17) {
            return test17();
        } else if (value == 18) {
            return test18();
        } else if (value == 19) {

        }
        return 0;
    }

    /**
     * update room
     *
     * @return
     */
    private Object test17() {
        TestConnect testConnect = TestConnect.getInstance(getContext());
        testConnect.exit(context);
//        testConnect.sendRoomUpdate(context);
//        testConnect.exit(context);
        testConnect.sendMONA8094(context);

//        testConnect.test();
        return null;
    }

    /**
     * update room
     *
     * @return
     */
    private Object test18() {

        return JsonCreate.sendNewNotification(context);
    }

    private Object test16() {
//        testConnect = TestConnect.getInstance(context);
//        testConnect.createConnect(context);
//        testConnect.sendRoom(context);
        return JsonCreate.sendLp(context);
    }

    private Object test15() {
        String message = bundle.getString("message");
        return JsonCreate.messagesend(context, message);
    }

    private void test14() {
        String ihash = bundle.getString("ihash");
        String stat = bundle.getString("stat");
        TestConnect testConnect = TestConnect.getInstance(context);
        String strIg = "<IG ihash=\"" + ihash + "\" stat=\"" + stat + "\" />\0";
        testConnect.write(context, strIg, context.getResources().getString(R.string.error_mess_4));
    }

    private void test13() {
        int scl = 0;
        Log.d("testScl test13",MeMonaId.getMe().getScl() + "");
        if (MeMonaId.getMe().getScl() == 1) {
            scl = 100;
        } else if (MeMonaId.getMe().getScl() == -1) {
            scl = -100;
        }

        String strScl = "<SET x=\"100.5\" y=\"" + MeMonaId.getMe().getY() + "\" scl=\"" + scl + "\" />\0";
        TestConnect testConnect = TestConnect.getInstance(context);
        testConnect.write(context, strScl, context.getResources().getString(R.string.error_mess_4));

    }

    private Object test12() {
        return JsonCreate.sendVersionNotice(context);
    }

    private Object test11() {

        return JsonCreate.sendUpdate(context);
    }

    private void test10() {
        int x = bundle.getInt("x");
        int y = bundle.getInt("y");

        int scl = 0;
        if (MeMonaId.getMe().getScl() == 1) {
            scl = 100;
        } else if (MeMonaId.getMe().getScl() == -1) {
            scl = -100;
        }

        String xy = "<SET x=\"" + x + "\" y=\"" + y + "\" scl=\"" + scl + "\" id=\"" + MeMonaId.getMe().getMonaId() + "\" />\0";
        TestConnect testConnect = TestConnect.getInstance(context);
        testConnect.write(context, xy, context.getResources().getString(R.string.error_mess_4));
    }

    private void test9() {
        String strStat = bundle.getString("stat");
        String stat = "<SET stat=\"" + strStat + "\" />\0";
        TestConnect testConnect = TestConnect.getInstance(context);
        try {
            testConnect.write(context, stat, context.getResources().getString(R.string.error_mess_5));
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private Object test1() {
        //---------------------------moment---------------------

///------------------------------------------------------
//-----------------------TEST----------------------------
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("pseq", 1);
//                jsonObject.put("startseq", 1);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            Object result = jsonObject.toString();
//-------------------------------------------------------

        return JsonCreate.sendUUID(getContext());
    }

    private Object test2() {
        /**
         * sendMのためのもの
         */
        // TODO GCコメントアウト
//------------------------moment-------------------------
        GCDB gcdb = new GCDB();
//        int count = gcdb.getGCCount();
        Object result;
//        if (count > 0) {
//            result = JsonCreate.sendGC(getContext());
//            return result;
//        } else {
//            JSONObject jsonObject = new JSONObject();
//            try {
////                    jsonObject.put("gc", 0);
//                jsonObject.put(context.getResources().getString(R.string.sdb_gc), 0);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            result = jsonObject.toString();
            return "OK";
//        }

//---------------------------------------------------
//-------------------------setErrorString----------------------
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("status", 1);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            Object result = jsonObject.toString();
//---------------------------------------------------
    }

    private Object test3() {
        Log.d("killTest", "test3");
//        MyApplication myApplication = (MyApplication) getContext().getApplicationContext();

//        TestConnect testConnect = myApplication.testConnect.getInstance(context);
//        String result = bundle.getString("reconnect");
//        if (result != null && result.equals(context.getString(R.string.reconnect))) {
//        TestConnect.MyThread myThread = new TestConnect.MyThread(context);
//        myThread.start();
//        synchronized (myThread) {
//            try {
//                Log.d(TAG, "test3 wait before");
//                myThread.wait();
//                Log.d(TAG, "test3 wait after");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        TestConnect testConnect = TestConnect.getInstance(context);
        Log.d(TAG, "test3");
        MyThread myThread = new MyThread(context);
        TestConnect.getInstance(context).setMyThread(myThread);
        myThread.start();
//        testConnect.setLoader(this);
//        testConnect.stopAndClose(context);
//        testConnect.createConnect(context);
//        testConnect.sendTest(context);
//        synchronized (testConnect.readMainThread) {
//            try {
//                Log.d(TAG, "test3 syn wait before");
//                testConnect.readMainThread.wait();
//                Log.d(TAG, "test3 syn wait after connect");
//                Base2Activity base2Activity = (Base2Activity) context;
//                base2Activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(context, "接続", Toast.LENGTH_LONG).show();
//                    }
//                });
//            } catch (InterruptedException e) {
//                /**
//                 * こっちなら切断されました
//                 */
////                e.printStackTrace();
//                Log.d(TAG, "test3 syn catch not connect interrupted");
//                Base2Activity base2Activity = (Base2Activity) context;
//                base2Activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(context, "未接続", Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//        }




//        boolean check;
//        for (int i = 0; i < 5; i++) {
//            Log.d("MyLoader test3", "loop:" + i);
//            TestConnect testConnect = TestConnect.getInstance(context);
//            testConnect.stopAndClose(context);
//            testConnect.createConnect(context);
//            testConnect.sendTest(context);
//            synchronized (this) {
//                try {
//                    this.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            check = testConnect.sendRoom(context);
//            if (!check) {
//                Base2Activity base2Activity = (Base2Activity) context;
//                base2Activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(context, "接続", Toast.LENGTH_LONG).show();
//                    }
//                });
//                break;
//            } else {
//                try {
//                    Base2Activity base2Activity = (Base2Activity) context;
//                    final int finalI = i;
//                    base2Activity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(context, "再接続要求:" + finalI, Toast.LENGTH_LONG).show();
//                        }
//                    });
//                    Log.d("waitconnect", "" + i);
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
        return JsonCreate.sendLp(context);

    }

    private Object test4() {

        String test = null;
        Integer.parseInt(test);
//        String strExit = "<EXIT />\0";
//        TestConnect testConnect = TestConnect.getInstance(context);
//        testConnect.createConnect(context);
//        testConnect.sendTest(context);
//        testConnect.sendMONA8094(context);
//        testConnect.write(context, strExit, context.getResources().getString(R.string.error_mess_6));
//
////        UtilInfo.write(context, strExit, context.getResources().getString(R.string.error_mess_6));
//
//        TestShared testShared = TestShared.getInstance(context);
//        Person me = testShared.getMe();
//        Room room = testShared.getRoom();

//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        Room room = myLoaderDB.getRoomNumber();
//        Person me = myLoaderDB.getMe();
//        for (int i = 0; i <= 5; i++) {

//            if (me.getShirotori().equals("")) {
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    JSONObject errorJson = setsudan();
//                    try {
//                        errorJson.put("test4", "InterruptedException");
//                    } catch (JSONException e1) {
//                        e1.printStackTrace();
//                    }
//                    UtilInfo.testError((Activity) context,errorJson.toString());
//                }
//            } else {
//                break;
//            }
//            if (i == 5) {
//                try {
//                    JSONObject jsonObject = new JSONObject();
//                    jsonObject.put("error",context.getString(R.string.cjme0x02));
//                    JSONObject errorJson = setsudan();
//                    try {
//                        errorJson.put("test4", "5");
//                    } catch (JSONException e1) {
//                        e1.printStackTrace();
//                    }
//                    UtilInfo.testError((Activity) context,errorJson.toString());
//                    return jsonObject;
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        String strEnter = XMLCreate.createEnterRoom(me, room);
//        String strEnter = "<ENTER room=\"/MONA8094/" + room.getRoomNumber() + "\" " +
//                "umax=\"0\" " +
//                "type=\"" + me.getAaname() + "\" " +
//                "name=\"" + me.getName() + "\" " +
//                "x=\"" + me.getX() + "\" " +
//                "y=\"" + me.getY() + "\" " +
//                "r=\"q" + me.getRed() + "\" " +
//                "g=\"" + me.getGreen() + "\" " +
//                "b=\"" + me.getBlue() + "\" " +
//                "scl=\"" + me.getScl() + "\" " +
////                        "stat=\"" + me.getStat() + "\" " +
//                "/>\0";

//        testConnect.write(context, strEnter, context.getResources().getString(R.string.error_mess_8));

        return null;
    }

    private Object test5() {
        return JsonCreate.sendR(context);
    }

    private Object test6() {
        String cmt = bundle.getString("cmt");
        String send = XMLCreate.comXMLGet(cmt);
        TestConnect testConnect = TestConnect.getInstance(context);
        testConnect.write(context, send, context.getResources().getString(R.string.error_mess_9));
        return cmt;
    }

    private Object test7() {
        String cmt = bundle.getString("cmt");
        return JsonCreate.cmtsend(context, cmt);
    }

    private Object test8() {

//        Person me = new RoomsDB().getMe();
        TestShared testShared = TestShared.getInstance(getContext());
        Person me = testShared.getMe();

        String send = XMLCreate.createEXIT(me);
//        String send = "<EXIT n='" + me.getMonaId() + "' />\0";
        TestConnect testConnect = TestConnect.getInstance(context);
        try {
            testConnect.write(context, send, context.getResources().getString(R.string.error_mess_1));
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        String enter = UtilInfo.createEnter1(me);
        testConnect.write(context, enter, context.getResources().getString(R.string.error_mess_10));

        /**
         * ここでサーバに飛ばす
         */
        return JsonCreate.sendER(context);
    }

//    private JSONObject setsudan() {
////        HashMap<Integer,String> errorMap = UtilInfo.setsudanShori();
//        JSONObject error = UtilInfo.setsudanShori();
//        return error;
//    }

    // TODO: 3
    @Override
    protected void onStartLoading() {
        Log.v("MyLoader", "onStartLoading");
        forceLoad();
    }


}
