package com.devmona;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class RandomFragment extends Fragment {


    private EditText nameEdit;
    private EditText gunEdit;
    private EditText hitokotoEdit;
    private EditText seisokuchiEdit;
    private TextView nameVali;
    private TextView hitokotoVali;
    private TextView gunVali;
    private TextView seisokuchiVali;

    public RandomFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_random, container, false);
        if (view.getVisibility() == View.GONE) {
            LinearLayout linearLayout = new LinearLayout(getContext());
            LinearLayout.LayoutParams regularParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            linearLayout.setLayoutParams(regularParams);
            linearLayout.setBackground(getContext().getDrawable(R.color.commonWhite));
            linearLayout.setClickable(true);
            TextView textView = new TextView(getContext());
            textView.setText("準備中");
            linearLayout.addView(textView);
            return linearLayout;
        }


        nameEdit =(EditText) view.findViewById(R.id.random_edit_name);
        nameVali = (TextView) view.findViewById(R.id.random_name_vali);
        gunEdit = (EditText) view.findViewById(R.id.random_edit_gun);
        gunVali = (TextView) view.findViewById(R.id.random_gun_vali);
        seisokuchiEdit = (EditText) view.findViewById(R.id.random_edit_seisokuchi);
        seisokuchiVali = (TextView) view.findViewById(R.id.random_seisokuchi_vali);
        hitokotoEdit = (EditText) view.findViewById(R.id.random_edit_hitokoto);
        hitokotoVali = (TextView) view.findViewById(R.id.random_hitokoto_vali);
        Button ok = (Button) view.findViewById(R.id.random_btn_ok);
//        Button cancel = (Button) view.findViewById(R.id.random_btn_cancel);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    return;
                } else {
                    // サーバーに飛ばす
                    Bundle bundle = new Bundle();
                    bundle.putString(getString(R.string.random_key_name),nameEdit.getText().toString());
                    bundle.putString(getString(R.string.random_key_gun),gunEdit.getText().toString());
                    bundle.putString(getString(R.string.random_key_seisokuchi),seisokuchiEdit.getText().toString());
                    bundle.putString(getString(R.string.random_key_hitokoto),hitokotoEdit.getText().toString());
//                    LoaderManager.getInstance(RandomFragment.this).initLoader(19,bundle,new RandomCallbacks19(getContext(),bundle));
                }
            }
        });

//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        return view;
    }

    private boolean validation() {
        String name = nameEdit.getText().toString();
        String gun = gunEdit.getText().toString();
        String seisokuchi = seisokuchiEdit.getText().toString();
        String hitokoto = hitokotoEdit.getText().toString();

        boolean b = false;

        if (emptycheck(name)|| name.length() >= 16) {
            nameVali.setVisibility(View.VISIBLE);
            b = true;
        } else {
            nameVali.setVisibility(View.GONE);
        }

        if (emptycheck(gun) || gun.length() >= 11) {
            gunVali.setVisibility(View.VISIBLE);
            b = true;
        } else {
            gunVali.setVisibility(View.GONE);
        }

        if (emptycheck(seisokuchi)||seisokuchi.length() >= 11) {
            seisokuchiVali.setVisibility(View.VISIBLE);
            b = true;
        } else {
            seisokuchiVali.setVisibility(View.GONE);
        }

        if (emptycheck(hitokoto) || hitokoto.length() >= 31) {
            hitokotoVali.setVisibility(View.VISIBLE);
            b = true;
        } else {
            hitokotoVali.setVisibility(View.GONE);
        }

        return b;
    }

    /**
     *
     * @param name
     * @return
     */
    private boolean emptycheck(String name) {
        if (name.equals("")) {
            return true;
        }
        return false;
    }

}
