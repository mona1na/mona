package com.devmona;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by sake on 2018/01/31.
 */

public class SetValidation {
    public void setDefaultSet(XmlPullParser xpp) throws NullPointerException, NumberFormatException {

        String strId = xpp.getAttributeValue(null, "id");

        if (strId == null) {
            throw new NullPointerException("ERROR: sv0x11");
        }
        try {
            int id = Integer.parseInt(strId);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("ERROR: sv0x12");
        }
        String strX = xpp.getAttributeValue(null, "x");
        if (strX == null) {
            throw new NullPointerException("ERROR: sv0x13");
        }
        try {
            int x = Integer.parseInt(strX);
        } catch (NumberFormatException e) {
            String position16 = strX.replaceAll("0x", "");
            try {
                int x16 = Integer.parseInt(position16, 16);
            } catch (NumberFormatException e1) {
                throw new NumberFormatException("ERROR: sv0x14");
            }
        }

        String strY = xpp.getAttributeValue(null, "y");
        if (strY == null) {
            throw new NullPointerException("ERROR: sv0x15");
        }

        try {
            int y = Integer.parseInt(strY);
        } catch (NumberFormatException e) {
            String position16 = strY.replaceAll("0x", "");
            try {
                int x16 = Integer.parseInt(position16, 16);
            } catch (NumberFormatException e1) {
                throw new NumberFormatException("ERROR: sv0x16");
            }
        }
    }
}
