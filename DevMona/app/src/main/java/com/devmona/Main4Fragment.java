package com.devmona;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class Main4Fragment extends Fragment {

    private LinearLayout linearLayout;
    private EditText editCom;
    private Button btnSendCom;
    private Space space;
    public ViewPager pager;
    private MyPagerAdapter adapter;
    private ImageView btnMuki;
    private ToggleButton btnKeyboard;
    private ImageView home;
    private ToggleButton paging;
    public static final String TAG = "Main4Fragment";
    private View mRootView;
    private Window mRootWindow;
    private EditText mEditText;
    private Context mContext;
    private Handler mHandler;

    public Main4Fragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("killTest", "Main4Fragment_onCreateView");
        mContext = getContext();
        mHandler =new Handler();
        mRootView = inflater.inflate(R.layout.fragment_main4, container, false);
        mEditText = (EditText) mRootView.findViewById(R.id.edt_main3_com);
        mEditText.clearFocus();
        mEditText.post(new Runnable() {
            @Override
            public void run() {
                Log.d("testSata", "run");
                int rootViewSize = TestShared.getInstance(mContext).getRootViewSize();
                int rootViewSizeInnerSoftKey = TestShared.getInstance(mContext).getRootViewSizeInnerSoftKey();
                if (rootViewSize == -1 || rootViewSizeInnerSoftKey == -1) {
                    Log.d("testSata", "run checkSize");
                    mEditText.requestFocus();
                    final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(mEditText, 0,new ResultReceiver(new Handler()){
                        @Override
                        protected void onReceiveResult(int resultCode, Bundle resultData) {
                            super.onReceiveResult(resultCode, resultData);
                            Log.d("testSata", "onReceiveResult");
                            mEditText.clearFocus();
                            imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0,new ResultReceiver(new Handler(Looper.getMainLooper())){
                                @Override
                                protected void onReceiveResult(int resultCode, Bundle resultData) {
                                    super.onReceiveResult(resultCode, resultData);
                                    int rootViewSize = TestShared.getInstance(mContext).getRootViewSize();
                                    int rootViewSizeInnerSoftKey = TestShared.getInstance(mContext).getRootViewSizeInnerSoftKey();
                                    if (rootViewSize == -1 || rootViewSizeInnerSoftKey == -1) {
                                        Log.d("testSata", "onReceiveResult checkSize -1");
                                        Log.d("testSata", "onReceiveResult checkSize -1" + rootViewSize);
                                        Log.d("testSata", "onReceiveResult checkSize -1" + rootViewSizeInnerSoftKey);
                                        Toast.makeText(mContext, "boot failure", Toast.LENGTH_LONG).show();
                                    } else {
                                        Log.d("testSata", "onReceiveResult checkSize else");
                                        initViews();
                                    }
                                }
                            });
                        }
                    });
                } else {
                    initViews();
                }
            }
        });
        return mRootView;
    }
    public void initViews() {
        ProgressFragment progressFragment = new ProgressFragment();
        Base2Activity nameActivity = (Base2Activity) getActivity();
        if (nameActivity != null) {
            nameActivity.getSupportFragmentManager().beginTransaction().add(R.id.progress_fragment, progressFragment, getString(R.string.fragment_progress)).commit();
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        ViewSetting();
        findViews(mRootView);
        edit();

        pager = (ViewPager) mRootView.findViewById(R.id.pager);

        adapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        pager.setAdapter(adapter);
        Main3PagerListener main3PagerListener = new Main3PagerListener(Main4Fragment.this);
        pager.addOnPageChangeListener(main3PagerListener);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void edit() {
        btnSendCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myComSend();
            }
        });
    }

    private void myComSend() {
        boolean myCheck = Validation.mySendComCheck(editCom.getText().toString());
        if (myCheck) {
            Toast.makeText(getContext(), getResources().getString(R.string.error_mess_11), Toast.LENGTH_SHORT);
            return;
        }
        boolean myCheck2 = Validation.myEmptyCheck(editCom.getText().toString());
        if (myCheck2) {
            return;
        }
        String result = Validation.myStrConvert(editCom.getText().toString());
        UtilInfo.commonSend(getContext(), result);
        editCom.setText("");
    }

    private void findViews(View view) {
        linearLayout = (LinearLayout) view.findViewById(R.id.main3_linear);
        editCom = (EditText) view.findViewById(R.id.edt_main3_com);
        btnSendCom = (Button) view.findViewById(R.id.btn_main3_send_comment);
        space = (Space) view.findViewById(R.id.main3_space);
        /**
         * set listener
         */


        editCom.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    myComSend();
                    return true;
                } else {
                    return true;
                }
            }

        });

        setDrags();
    }

    private void setDrags() {
        linearLayout.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return false;
            }
        });
        editCom.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return false;
            }
        });
        btnSendCom.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return false;
            }
        });
        space.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }
}
