package com.devmona;

//import android.app.Dialog;
import android.app.Dialog;

import androidx.annotation.Nullable;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by sake on 2018/04/07.
 */

public class AADialogFragment2 extends Fragment{

    private ViewPager pager;
    private Dialog dialog;
    private int currentPage = 0;
    public static final String TAG = "AADialogFragment2";
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        dialog = new Dialog(getActivity());
//        // タイトル非表示
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        // ダイアログの中身
//
//        return dialog;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        Bundle bundle = getArguments();
        View view = inflater.inflate(R.layout.fragment_aadialog2,container,false);
        ViewPager viewPager = view.findViewById(R.id.aadialog_pager);

        /**
         * 現在のページ情報を取得するリスナーをセットする
         */
        viewPager.addOnPageChangeListener(new PageListener());

        MyAADialogPagerAdapter myAADialogPagerAdapter
                = new MyAADialogPagerAdapter(getChildFragmentManager(),bundle);
        viewPager.setAdapter(myAADialogPagerAdapter);
        Button button = view.findViewById(R.id.aa_dialog_cancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((SentakuActivity) getActivity()).onAADialogDismiss();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                fragmentTransaction.remove(AADialogFragment2.this).commit();

            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        List list = ((RoomsActivity)context).getSupportFragmentManager().getFragments();
//        AADialogFragment2 aaDialogFragment2 =(AADialogFragment2) list.get(0);
//        pager = aaDialogFragment2.findViewById(R.id.aa_dialog_pager);
//
//
//        MyAADialogPagerAdapter myAADialogPagerAdapter
//                = new MyAADialogPagerAdapter(((SentakuActivity) getActivity()).getSupportFragmentManager());
//        pager.setAdapter(myAADialogPagerAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        List list = ((SentakuActivity)getActivity()).getSupportFragmentManager().getFragments();
//        SentakuActivity sentakuActivity = (SentakuActivity)getActivity();
//        FragmentManager fragmentManager = sentakuActivity.getSupportFragmentManager();
    }
}
