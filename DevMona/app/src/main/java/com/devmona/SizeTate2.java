package com.devmona;

/**
 * Created by sake on 2018/04/03.
 */

public class SizeTate2{
    private int width;
    private int height;
    private int witdh08;
    private int height08;
    private int h08HikuPicHalf;
    private int h08HikuPic;
    private int w08PlusPicHalf;

    public void setH08HikuPic(int h08HikuPic) {
        this.h08HikuPic = h08HikuPic;
    }

    public int getH08HikuPic() {
        return h08HikuPic;
    }

    public int getWitdh08() {
        return witdh08;
    }

    public int getW08PlusPicHalf() {
        return w08PlusPicHalf;
    }

    public void setW08PlusPicHalf(int w08PlusPicHalf) {
        this.w08PlusPicHalf = w08PlusPicHalf;
    }

    public int getH08HikuPicHalf() {
        return h08HikuPicHalf;
    }

    public void setH08HikuPicHalf(int h08HikuPicHalf) {
        this.h08HikuPicHalf = h08HikuPicHalf;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setWitdh08(int witdh08) {
        this.witdh08 = witdh08;
    }

    public int getWidth08() {
        return witdh08;
    }


    public void setHeight08(int height08) {
        this.height08 = height08;
    }

    public int getHeight08() {
        return height08;
    }

}
