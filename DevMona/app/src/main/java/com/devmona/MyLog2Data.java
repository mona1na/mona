package com.devmona;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class MyLog2Data implements Parcelable {

    private ArrayList<Log2Fragment.PersonEnum> mLog2List;

    public MyLog2Data(){
        mLog2List = new ArrayList<>();
    }

    protected MyLog2Data(Parcel in) {
        mLog2List = in.createTypedArrayList(Log2Fragment.PersonEnum.CREATOR);
    }

    public static final Creator<MyLog2Data> CREATOR = new Creator<MyLog2Data>() {
        @Override
        public MyLog2Data createFromParcel(Parcel in) {
            return new MyLog2Data(in);
        }

        @Override
        public MyLog2Data[] newArray(int size) {
            return new MyLog2Data[size];
        }
    };

    public void setmLog2List(ArrayList<Log2Fragment.PersonEnum> mLog2List) {
        this.mLog2List = mLog2List;
    }

    public ArrayList<Log2Fragment.PersonEnum> getmLog2List() {
        return mLog2List;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mLog2List);
    }
}
