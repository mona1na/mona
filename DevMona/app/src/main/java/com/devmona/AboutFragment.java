package com.devmona;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {


    private SizeTate2 sizeTate2;
    private Dialog dialog;

    public AboutFragment() {
        // Required empty public constructor
        Main2DB main2DB = new Main2DB();
//        sizeTate2 = main2DB.getSizeTate2();
    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        dialog = new Dialog(getActivity());
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        return dialog;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("LifeTest AboutFragment", "onCreateView");
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_about, container, false);
        TextView textView1 = root.findViewById(R.id.about_title_1);
        textView1.setText("hello tab!");
//        TextView textView2 = root.findViewById(R.id.about_title_2);
//        TextView textView3 = root.findViewById(R.id.about_title_3);
//        TextView textView4 = root.findViewById(R.id.about_title_4);
//        TextView textView5 = root.findViewById(R.id.about_title_5);
//        TextView textView6 = root.findViewById(R.id.about_title_6);
////        TextView textView7 = root.findViewById(R.id.about_title_7);
//        final ImageView imageView = (ImageView) root.findViewById(R.id.about_bug1_img);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LargeImageFragment largeImageFragment = new LargeImageFragment();
//                largeImageFragment.setResourceId(R.mipmap.bug_1);
//                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.about_fragment, largeImageFragment, "largeImage").commit();
//            }
//        });
//        textView1.setPaintFlags(textView1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        textView3.setPaintFlags(textView3.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        textView4.setPaintFlags(textView4.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        textView5.setPaintFlags(textView5.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        textView6.setPaintFlags(textView6.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
////        textView7.setPaintFlags(textView7.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Button button = root.findViewById(R.id.about_close);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(AboutFragment.this).commit();

            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        Dialog dialog = getDialog();
//        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
//        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
//        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
//        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("LifeTest AboutFragment", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("LifeTest AboutFragment", "onPause");
    }
}
