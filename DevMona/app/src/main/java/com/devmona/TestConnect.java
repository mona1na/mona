package com.devmona;


import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.devmona.XML.XMLCreate;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import connect.my.myconnect.MyConnect;


public class TestConnect {

    public BufferedInputStream in;
    public ReadMainThread readMainThread;
    public ReadRoomThread readRoomThread;
    private BufferedWriter writer;
    public static TestConnect testConnect;
    private NopThread nopThread;
    public MyLoader myLoader;
    public static final String TAG = "TestConnect";
    public MyThread myThread;

    public TestConnect() {

    }

    public void setLoader(MyLoader myLoader) {
        this.myLoader = myLoader;
    }

    public void setMyThread(MyThread myThread) {
        this.myThread = myThread;
    }

    public static TestConnect getInstance(Context context) {
        if (testConnect == null) {
            Log.d(TAG, "TestConnect create");
            testConnect = new TestConnect();
        }


        return testConnect;
    }

    public JSONObject setsudanShori(Context context) {

        nopThread.running = false;
        if (readRoomThread != null) {
            readRoomThread.running = false;
        } else if (readMainThread != null) {
            readMainThread.running = false;
        }

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * ①ソケットを生成する
     * ②inputStreamを生成する
     * ③readThreadを生成して起動する
     * ④writeを生成する
     * ⑤nopThreadを生成する
     *
     * @param context
     * @param reConnecting
     */
    public boolean createConnect(Context context, boolean reConnecting) {

        MyConnect myConnect = new MyConnect();
        try {
            myConnect.createSocket();
        } catch (IOException e) {
            Log.d(TAG, "error create Socket");
            e.printStackTrace();
            return false;
        }
        try {
            myConnect.createInput();
            in = myConnect.getIn();
        } catch (IOException e) {
            Log.d(TAG, "error create input");
            e.printStackTrace();
            return false;
        }
        try {
            myConnect.createWriter();
            writer = myConnect.getWriter();
        } catch (IOException e) {
            Log.d(TAG, "error create writer");
            e.printStackTrace();
            return false;
        }


        /**
         * ③readThreadを生成して起動する
         */
        try {
            TestShared testShared = TestShared.getInstance(context);
            Room room;
            if (TestSave.rappid) {
                room = testShared.getRapidRoom();
            } else {
                room = testShared.getRoom();
            }

            if (room.getRoomNumber() == 8094) {
                Log.d(TAG, "new ReadRoomThread");
                readRoomThread = new ReadRoomThread(context,reConnecting);
                readRoomThread.start();
            } else {
                Log.d(TAG, "new ReadMainThread");
                readMainThread = new ReadMainThread(context,reConnecting);
                readMainThread.start();
            }

        } catch (NullPointerException e) {
//            final String methodName = new Object() {
//            }.getClass().getEnclosingMethod().getName();
//            error(context, methodName);
            Log.d(TAG, "createConnect: 3 null");
            return false;
        }


        /**
         * ⑤nopThreadを生成する
         */

        /**
         * {@link Base2Activity}
         */
        nopThread = new NopThread(context);
        nopThread.start();
        return true;
    }

    /**
     * mojaを送る
     *
     * @param context
     */
    public boolean sendTest(Context context) {
        // moja送信
        try {
            Log.d(TAG, "sendTest write moja");
            write(context, "MojaChat\0", context.getResources().getString(R.string.error_mess_2));
        } catch (NullPointerException e) {
            Log.d(TAG, "sendTest:Null");
            return true;
        }
        return false;
    }

    public void sendMONA8094(Context context) {
        // <enter mona8094>送信
        Person me = MeMonaId.getMe();
        String enter = XMLCreate.createEnter1(me);
        testConnect.write(context, enter, context.getResources().getString(R.string.error_mess_3));
        try {
            Thread.sleep(Prop.SENDSTOPTIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean write(Context context, String s, String string) {
        Log.d("send", s);
        try {
            writer.write(s);
            writer.flush();
        } catch (NullPointerException e) {
            Log.d(TAG, "write:null " + s);
            errorSend(TAG, "write:null " + s);
            return true;
        } catch (IOException e) {
            Log.d(TAG, "write:IOE " + s);
            errorSend(TAG, "write:IOE " + s);
            return true;
        }
        return false;
    }

    private void errorSend(String tag, String s) {
    }

    /**
     * @param context
     */
    public boolean sendRoom(Context context) {
        TestShared testShared = TestShared.getInstance(context);

        Room room;
        if (TestSave.rappid) {
            room = testShared.getRapidRoom();
            TestSave.rappid = false;
        } else {
            room = testShared.getRoom();
        }
        Person me = testShared.getMe();
        if (room.getRoomNumber() == 8094) {
            String enter = XMLCreate.createEnter1(me);
            boolean b = testConnect.write(context, enter, context.getResources().getString(R.string.error_mess_3));
            try {
                Thread.sleep(Prop.SENDSTOPTIME);
            } catch (InterruptedException e) {


                e.printStackTrace();
                final String methodName = new Object() {
                }.getClass().getEnclosingMethod().getName();
                error(context, methodName);
                return true;
            }
            return b;
        } else {
            String strEnter = XMLCreate.createEnterRoom(me, room);
            // stub todo 101 room

            boolean b = testConnect.write(context, strEnter, context.getResources().getString(R.string.error_mess_8));
            return b;
        }
    }


    public void init(Context context) {
        /**
         * 初期化
         */
//        NopThread.running = true;

    }

    /**
     * 再接続用メソッド
     * ⓪exitを送る
     * ①readThread,nopThreadを閉じる
     * ②スレッド終了まで待つ
     * ③readerとwriterを閉じる
     *
     * @param context
     */
    public void stopAndClose(final Context context) {
        Log.d(TAG, "stopAndClose");
        /**
         * ⓪exitを送る
         */
        if (writer != null) {
            exit(context);
        }

        /**
         * ①readThread,nopThreadを閉じる
         */
        ReadAbstract readAbstract = null;
        if (readRoomThread != null) {
            readAbstract = (ReadAbstract) readRoomThread;
            readAbstract.running = false;
            nopThread.running = false;
        } else if (readMainThread != null) {
            readAbstract = (ReadAbstract) readMainThread;
            readAbstract.running = false;
            nopThread.running = false;
            nopThread.interrupt();
        }

        /**
         * ②スレッド終了まで待つ
         */
        if (readAbstract != null) {
            int i = 0;
            while (readAbstract.isAlive()) {
                Log.d(TAG, "readAbstract.isAlive");
                try {
                    Thread.sleep(500);
                    if (i > 5) {
                        Log.d(TAG,"stopAndClose 2:" + i);
                        errorSend(TAG,"stopAndClose 2:" + i);
                        return;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
            }
        } else {
            Log.d(TAG, "readAbstract null");
        }
        int i = 0;
        if (nopThread != null) {
            while (nopThread.isAlive()) {
                Log.d(TAG, "teishityu:" + i);
                synchronized (nopThread) {
                    Log.d(TAG, "synchronized nopThread");
                    nopThread.interrupt();
                    try {

                        nopThread.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                i++;
            }
        } else {
            Log.d(TAG, "NopThread is null");
        }
        /**
         * ③writerとreaderを閉じる
         */
        if (writer != null) {
            try {
                Log.d(TAG, "stopAndClose writer.close()");
                writer.close();
            } catch (IOException e) {
                Log.d(TAG, "stopAndClose writer IOE");
            }
        }

        if (in != null) {
            try {
                Log.d(TAG, "stopAndClose in.close()");
                in.close();
            } catch (IOException e) {
                Log.d(TAG, "stopAndClose in.close() IOE");
                e.printStackTrace();
            }
        }

        try {
            readMainThread.hashLayoutByte.clear();
        } catch (NullPointerException e) {
            Log.d(TAG, "hashLayoutByte.clear() null");
        }
        try {
            readMainThread.people.clear();
        } catch (NullPointerException e) {
            Log.d(TAG, "people.clear() null");
        }

        final Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
        if (tate2Fragment != null) {
            Base2Activity base2Activity = (Base2Activity) context;
            synchronized (this) {
                base2Activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("testSync", "removeViewAt");
                        synchronized (TestConnect.this) {
                            Log.d("testSync", "syn");
                            Log.d("testSync", "count:" + tate2Fragment.frameTate2.getChildCount());
                            int size = tate2Fragment.frameTate2.getChildCount();
                            ArrayList<View> views = new ArrayList<>();
                            for (int i = 0; i < size; i++) {
                                View view = (View) tate2Fragment.frameTate2.getChildAt(i);
                                if (view instanceof RelativeLayout) {
                                    views.add(view);
                                    Log.d("testSync", "num:" + i);
                                }
                            }
                            for (int i = 0; i < views.size();i++) {
                                tate2Fragment.frameTate2.removeView(views.get(i));
                            }
                            Log.d("testSync", "notify");
                            TestConnect.this.notify();
                        }
                    }
                });
                try {
                    Log.d("testSync", "waitBefore");
                    this.wait();
                    Log.d("testSync", "waitAfter");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }

        readRoomThread = null;
        readMainThread = null;
    }

    public void stopAndClose2(final Context context) {
        Log.d(TAG, "stopAndClose");
        /**
         * ⓪exitを送る
         */
        if (writer != null) {
            exit(context);
        }

        /**
         * ①readThread,nopThreadを閉じる
         */
        ReadAbstract readAbstract = null;
        if (readRoomThread != null) {
            readAbstract = (ReadAbstract) readRoomThread;
            readAbstract.running = false;
            nopThread.running = false;
        } else if (readMainThread != null) {
            readAbstract = (ReadAbstract) readMainThread;
            readAbstract.running = false;
            nopThread.running = false;
            nopThread.interrupt();
        }

        /**
         * ②スレッド終了まで待つ
         */
        if (readAbstract != null) {
            int i = 0;
            while (readAbstract.isAlive()) {
                Log.d(TAG, "readAbstract.isAlive");
                try {
                    Thread.sleep(500);
                    if (i > 5) {
                        Log.d(TAG,"stopAndClose 2:" + i);
                        errorSend(TAG,"stopAndClose 2:" + i);
                        return;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
            }
        } else {
            Log.d(TAG, "readAbstract null");
        }
        int i = 0;
        if (nopThread != null) {
            while (nopThread.isAlive()) {
                Log.d(TAG, "teishityu:" + i);
                synchronized (nopThread) {
                    Log.d(TAG, "synchronized nopThread");
                    nopThread.interrupt();
                    try {

                        nopThread.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                i++;
            }
        } else {
            Log.d(TAG, "NopThread is null");
        }
        /**
         * ③writerとreaderを閉じる
         */
        if (writer != null) {
            try {
                Log.d(TAG, "stopAndClose writer.close()");
                writer.close();
            } catch (IOException e) {
                Log.d(TAG, "stopAndClose writer IOE");
            }
        }

        if (in != null) {
            try {
                Log.d(TAG, "stopAndClose in.close()");
                in.close();
            } catch (IOException e) {
                Log.d(TAG, "stopAndClose in.close() IOE");
                e.printStackTrace();
            }
        }

        try {
            readMainThread.hashLayoutByte.clear();
        } catch (NullPointerException e) {
            Log.d(TAG, "hashLayoutByte.clear() null");
        }
        try {
            readMainThread.people.clear();
        } catch (NullPointerException e) {
            Log.d(TAG, "people.clear() null");
        }

        final Tate2Fragment tate2Fragment = UtilInfo.getTate2Fragment(context);
        if (tate2Fragment != null) {

        }

        readRoomThread = null;
        readMainThread = null;
    }


    public void error(final Context context, final String methodName) {
        Log.d(TAG, "error " + methodName + "");
        final Base2Activity base2Activity = (Base2Activity) context;
        base2Activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                try {
                    Person person = new Person();
                    person.setLog(methodName);
                    DevelopFragment.PersonEnum personEnum = new DevelopFragment.PersonEnum();
                    personEnum.setPerson(person);
                    personEnum.setType(DevelopFragment.TYPE.Log);
                } catch (NullPointerException e) {

                }
            }
        });
    }

    public void exit(Context context) {
        String strExit = "<EXIT />\0";
        write(context, strExit, null);
    }
}
