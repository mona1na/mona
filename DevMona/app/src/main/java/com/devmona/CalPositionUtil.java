package com.devmona;

import android.util.Log;

/**
 * Created by sake on 2017/07/25.
 */

public class CalPositionUtil {

    //    public static float canvasHeight;
//    public static float canvasWidth;
    public static int main2Height;
    public static int main2Width;

//    public static float canvasRealWidth;
//    public static float canvasRealheight;
//    // 0.8に対しての高さ
//    public static int cheight08;
//    public static int width08;
//    public static int height08;
//    public static int width;
//
//    public static int linearAANAMETRIPX;
//    public static int linearAANAMETRIPY;

    /**
     * X座標からcanvasに描画するためのY座標を出力
     *
     * @param canvasRealWidth
     * @param xPosition
     * @return
     */
    public static int test1(float canvasRealWidth, int xPosition) {

        float b = -(30 * canvasRealWidth) / 660;

        float result = (canvasRealWidth * xPosition) / 660 + b;
        Log.d("CalPositionUtil", "width08" + result);
        return (int) result;
    }

    /**
     * Y座標からcanvasに描画するためのX座標を出力
     * <p>
     * //     * @param canvasRealHeight
     *
     * @param yPosition
     * @return
     */
    public static int test2(int widthDp,int yPosition) {

//        float result = (canvasRealHeight * yPosition) / 80 - 3 * canvasRealHeight;
//        Log.d("CalPositionUtil", "height08" + result);
//        return (int)result;
//        int result = -widthDp * yPosition / 80 + 4 * widthDp + picDip;
        float result = -(widthDp * yPosition) / 80 + (4 * widthDp);
        return (int) result;
    }

    /**
     * widthからアスペクト比
     *
     * @param width
     * @return height08
     */
    public static float asHeightfromWidth(float width) {

        //x の幅が660 yが80なので80 / 660 = 0.12....倍なので、それを横に対して掛けた際に出てくる高さ
        // y = 80 / 660 * x
        float height = (4 * width) / 33;
        return height;
    }

//    /**
//     * heightからアスペクト比
//     *
//     * @param height
//     * @return width08
//     */
//    public static int test4(int height) {
//
//        int width = (33 * height) / 4;
//        return width;
//    }

//    /**
//     * @param width
//     * @return height08
//     */
//    public static int test5(float width) {
//        int height = (int) asHeightfromWidth(width);
//        return height;
//    }

//    public static int lineStartX() {
//
//        int result = UtilInfo.me.getX() - Setting.hani;
//        if (result < 0) {
//            result = 0;
//        }
//        return result;
//    }

//    public static int lineStopX() {
//
//        int result = UtilInfo.me.getX() + Setting.hani;
//        if (result >= CalPositionUtil.canvasRealWidth) {
//            result = (int) CalPositionUtil.canvasRealWidth;
//        }
//        return result;
//    }

    public static int[] testxx(int width08, int height08, int x, int y) {

        int[] xy = new int[2];

        xy[0] = (660 * x) / width08 + 30;
        xy[1] = (80 * y) / height08 + 240;

        return xy;
    }

    /**
     * main2で使うaanametripを囲っているlinearのXを出力
     * @return
     */
//    public static int aanametripXGET() {
//
//        float x = (150f / 910f) * CalPositionUtil.width;
//
//        return (int)x;
//    }
    /**
     * main2で使うaanametripを囲っているlinearのYを出力
     * @return
     */
//    public static int aanametripYGET() {
//        float y = (203f / 250f) * CalPositionUtil.canvasRealheight;
//
//        return (int)y;
//    }
}
