package com.devmona;

import java.io.Serializable;

/**
 * Created by sake on 2017/05/15.
 */

public class Room implements Serializable{
//    private int id = -1;
//    private int id = -1;
//    private int startId = -1;
    private int defaultNum;
    private String roomName;
    private int buttonId;
    private int roomNumber;
    private int roomCount;
//    private boolean sentaku = false;
//    private int loginId = -1;
//    private int updateFlag = 0;

    public Room() {
        roomCount = -1;
    }

//    public int getUpdateFlag() {
//        return updateFlag;
//    }
//
//    public void setUpdateFlag(int updateFlag) {
//        this.updateFlag = updateFlag;
//    }

    //    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getId() {
//        return id;
//    }


//    public int getStartId() {
//        return startId;
//    }
//
//    public void setStartId(int startId) {
//        this.startId = startId;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setLoginId(int loginId) {
//        this.loginId = loginId;
//    }
//
//    public int getLoginId() {
//        return loginId;
//    }

    public void setButtonId(int buttonId) {
        this.buttonId = buttonId;
    }

    public void setDefaultNum(int defaultNum) {
        this.defaultNum = defaultNum;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getButtonId() {
        return buttonId;
    }

    public int getDefaultNum() {
        return defaultNum;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public int getRoomCount() {
        return roomCount;
    }
}
