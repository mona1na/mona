package com.devmona;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

public class RandomCallbacks19 implements LoaderManager.LoaderCallbacks<Object>{

    private Bundle bundle;
    private Context context;

    protected RandomCallbacks19(Context context, Bundle bundle) {
        this.context = context;
        this.bundle = bundle;
    }
    @NonNull
    @Override
    public Loader<Object> onCreateLoader(int id, @Nullable Bundle args) {
        bundle.putInt("key",19);
        MyRandomLoader myRandomLoader = new MyRandomLoader(context,bundle);
        return myRandomLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Object> loader, Object data) {
        String result =(String)data;
        Base2Activity base2Activity = (Base2Activity)context;
        if ("unregistered".equals(result)) {
            RandomFragment randomFragment = new RandomFragment();
            FragmentTransaction fragmentTransaction = base2Activity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
            fragmentTransaction.replace(R.id.main4_option2, randomFragment);
            fragmentTransaction.commit();

        } else if("registered".equals(result)) {

        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Object> loader) {

    }
}
