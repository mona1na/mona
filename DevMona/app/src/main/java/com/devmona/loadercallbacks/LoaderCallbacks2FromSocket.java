package com.devmona.loadercallbacks;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;
import com.devmona.Prop;

/**
 * Created by sake on 2018/01/10.
 */

public class LoaderCallbacks2FromSocket implements LoaderManager.LoaderCallbacks<Object> {

    private Context context;
    //    private RoomsActivity roomsActivity;
//    private Main3Activity main3Activity;
    private TextView txtOk;
    private TextView txtId;
    private Button btnCancel;
    private Button btnOk;

//    public LoaderCallbacks2FromSocket(Activity activity) {
//        this.roomsActivity = (RoomsActivity) activity;
//    }

    public LoaderCallbacks2FromSocket(Context context) {
        this.context = context;
    }

//    public LoaderCallbacks2FromSocket(Main3Activity main3Activity) {
//        this.main3Activity = main3Activity;
//    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle bundle) {
//        bundle = new Bundle();

        bundle.putInt("key", 2);
        MyLoader myLoader = new MyLoader(context, bundle);
        return myLoader;
        /**
         *               Object result = "{\"ok\":1}";
         return result;
         */
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
//        if (context instanceof RoomsActivity) {
//            ((RoomsActivity) context).getSupportLoaderManager().destroyLoader(2);
//        } else if (context instanceof Main3Activity) {
//            ((Main3Activity) context).getSupportLoaderManager().destroyLoader(2);
//        }

        ((Base2Activity)context).getSupportLoaderManager().destroyLoader(2);
        Base2Activity nameActivity = (Base2Activity) context;


        Log.d("onLoadFinished start", "onLoadFinished start");
        if (Prop.DEBUG_MODE == 1) {
            data = "OK";
        }

        Log.d("20180704", data.toString());
        if (data.toString().equals(Prop.SERVE_MESSAGE)) {
            notErrorSet();
            return;
        } else {
            errorSet(data.toString());
            return;
        }


//        /**
//         * 正常ケース Object data = {"gc":n}
//         * 異常ケース object data = {"error":"detail"}
//         */
//        if (Prop.DEBUG_MODE == 1) {
//            data = "{\"gc\":1}";
//        }
//
//        Validation validation = new Validation(roomsActivity);
//        try {
//            /**
//             * check json format
//             */
//            validation.checkJsonSo(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//        /**
//         * エラーチェック
//         */
//        try {
//            // jsonにerrorが存在したら
//            validation.checkErrorSo(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//        try {
//            // jsonにstatusが存在したら
//            validation.checkJsonStatus(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//        notErrorSet();
    }

    /**
     * 正常な場合
     */
    private void notErrorSet() {
        Bundle bundle = new Bundle();
        bundle.putInt("key", 3);
//        bundle.putString(context.getString(R.string.seni),context.getString(R.string.fragment_name));

//        SentakuDB sentakuDB = new SentakuDB();
//                    DBStatus dbStatus = sentakuDB.getFromTo();
//        sentakuDB.updateGU();

        Base2Activity nameActivity = (Base2Activity) context;
        nameActivity.getSupportLoaderManager().initLoader(3, bundle, new LoaderCallbacks3FromSocket(context));
//        if (context instanceof RoomsActivity) {
//            ((RoomsActivity) context).getSupportLoaderManager().initLoader(3, bundle, new LoaderCallbacks3FromSocket(context));
//        } else if (context instanceof Main3Activity) {
//            ((Main3Activity) context).getSupportLoaderManager().initLoader(3, bundle, new LoaderCallbacks3FromSocket(context));
//        }


//        // TODO テスト用に消す
//        if (roomsActivity != null) {
//            roomsActivity.getSupportLoaderManager().initLoader(3, bundle, new LoaderCallbacks3FromSocket(roomsActivity));
//        } else if (main3Activity != null) {
//            main3Activity.getSupportLoaderManager().initLoader(3, bundle, new LoaderCallbacks3FromSocket(main3Activity));
//        } else {
//            throw new RuntimeException();
//        }
    }

    private void errorSet(String json) {
//        try {
//            JSONObject jsonObject = new JSONObject(json);
//            String error = jsonObject.getString("error");
//            txtOk = (TextView) roomsActivity.findViewById(R.id.socket_result_txt_ok);
//            txtOk.setText("接続失敗[" + error + "]");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        Toast.makeText(context, "接続エラー", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }
}
