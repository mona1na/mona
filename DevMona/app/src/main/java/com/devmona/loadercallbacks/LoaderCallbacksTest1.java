package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;

public class LoaderCallbacksTest1 implements LoaderManager.LoaderCallbacks {

    private Base2Activity nameActivity;

    public LoaderCallbacksTest1(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }

    @NonNull
    @Override
    public Loader onCreateLoader(int i, @Nullable Bundle bundle) {
        bundle.putInt("key", 1);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object o) {
        nameActivity.getSupportLoaderManager().destroyLoader(1);
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {

    }
}
