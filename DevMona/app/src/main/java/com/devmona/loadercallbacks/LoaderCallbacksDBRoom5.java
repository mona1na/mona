package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;
import com.devmona.Prop;
import com.devmona.UtilInfo;

/**
 * Created by sake on 2018/03/29.
 */

public class LoaderCallbacksDBRoom5 implements LoaderManager.LoaderCallbacks {

    private final Base2Activity nameActivity;

    public LoaderCallbacksDBRoom5(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }


    @Override
    public Loader<Object> onCreateLoader(int id, Bundle bundle) {
        bundle.putInt("key", 5);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        nameActivity.getSupportLoaderManager().destroyLoader(5);

        if (Prop.DEBUG_MODE == 1) {
            data = "OK";
        }

        if (data.toString().equals(Prop.SERVE_MESSAGE)) {
            notError(data.toString());
            return;
        } else {
            errorSet(data.toString());
            return;
        }

//        /**
//         * {"s_room_id":n}の形
//         */
//        if (Prop.DEBUG_MODE == 1) {
//            data = "{\"s_room_id\":1}";
//        }
//
//        Validation validation = new Validation(main3Activity);
//
//        try {
//            /**
//             * check whether responses is Json-format
//             */
//            validation.checkJsonR(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        try {
//            /**
//             * check error
//             */
//            validation.checkErrorR(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//
//        try {
//            /**
//             * check s_room_id
//             */
//            validation.checkRoomId(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        notError(data.toString());

    }

    @Override
    public void onLoaderReset(Loader loader) {

    }


    private void notError(String s) {

        /**
         * 遅いのでしない
         * するとしたら完全に入ったというフラグをたてるくらい
         */
//        Main2DB main2DB = new Main2DB();
//        main2DB.updateCurrentStatus();
        /**
         * dbstatusにroom_idをセットする
         * put room_id on dbstatus
         */
//        try {
//            String roomid = new JSONObject(s).getString(main3Activity.getResources().getString(R.string.sdb_room_id));
//            new MyLoaderDB().updateRoomIdDBStatus(roomid);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }

    private void errorSet(final String messageJson) {
        // TODO 3/24 errorset

//        Handler handler = new Handler();
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                CommonErrorDialog commonErrorDialog = new CommonErrorDialog();
//                Bundle bundle = new Bundle();
//                bundle.putString(main3Activity.getString(R.string.bundle_json), messageJson);
//                commonErrorDialog.setArguments(bundle);
//                commonErrorDialog.show(main3Activity.getSupportFragmentManager(), "errorDialog");
        UtilInfo.testError2(nameActivity, messageJson);
//            }
//        });
    }


}
