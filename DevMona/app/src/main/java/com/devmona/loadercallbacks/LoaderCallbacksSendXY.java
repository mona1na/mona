package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;

/**
 * Created by sake on 2018/05/21.
 */

public class LoaderCallbacksSendXY implements LoaderManager.LoaderCallbacks{
    private final Base2Activity nameActivity;

    public LoaderCallbacksSendXY(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        bundle.putInt("key", 10);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        nameActivity.getSupportLoaderManager().destroyLoader(10);
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
