package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;

public class LoaderCallbacksTest16 implements LoaderManager.LoaderCallbacks {

    private Base2Activity nameActivity;

    public LoaderCallbacksTest16(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }

    @NonNull
    @Override
    public Loader onCreateLoader(int i, @Nullable Bundle bundle) {
        bundle.putInt("key", 16);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object o) {
        nameActivity.getSupportLoaderManager().destroyLoader(16);
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {

    }
}
