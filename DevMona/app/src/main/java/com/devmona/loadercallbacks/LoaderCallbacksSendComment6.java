package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;

/**
 * Created by sake on 2018/03/29.
 */

public class LoaderCallbacksSendComment6 implements LoaderManager.LoaderCallbacks{


    private final Base2Activity nameActivity;

    public LoaderCallbacksSendComment6(Base2Activity nameActivity){
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        String cmt = bundle.getString("cmt");
        bundle.putInt("key", 6);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        Bundle bundle = new Bundle();
        bundle.putString("cmt",data.toString());
        nameActivity.getSupportLoaderManager().destroyLoader(6);
        nameActivity.getSupportLoaderManager().initLoader(7, bundle, new LoaderCallbacksDBComment7(nameActivity));
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
