package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;
import com.devmona.ProgressFragment;

import java.util.List;

/**
 * Created by sake on 2018/03/29.
 */

public class LoaderCallbacksEnterRoom4 implements LoaderManager.LoaderCallbacks<Object>{
    private final Base2Activity nameActivity;

    public LoaderCallbacksEnterRoom4(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle bundle) {
        bundle.putInt("key", 4);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        nameActivity.getSupportLoaderManager().destroyLoader(4);

        Fragment progressFragment = null;
        List<Fragment> fragments = nameActivity.getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof ProgressFragment) {
                progressFragment = fragment;
            }
        }

        Bundle bundle = new Bundle();
        nameActivity.getSupportLoaderManager().initLoader(5, bundle, new LoaderCallbacksDBRoom5(nameActivity));
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }
}
