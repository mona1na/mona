package com.devmona.loadercallbacks;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;
import com.devmona.Prop;
import com.devmona.R;

/**
 * Created by sake on 2018/01/10.
 */

public class LoaderCallbacks3FromSocket implements LoaderManager.LoaderCallbacks<Object> {

    private Context context;


    private TextView txtOk;
    private TextView txtId;
    private Button btnCancel;
    private Button btnOk;
    private String mSeni;
    private Bundle bundle;

    public LoaderCallbacks3FromSocket(Context context) {
        this.context = context;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle bundle) {
        this.bundle = bundle;
        this.bundle.putInt("key", 3);
        mSeni = this.bundle.getString(context.getString(R.string.seni));
        MyLoader myLoader = new MyLoader(context, this.bundle);
        return myLoader;
    }


    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        Base2Activity nameActivity = (Base2Activity) context;
        nameActivity.getSupportLoaderManager().destroyLoader(3);

        if (Prop.DEBUG_MODE == 1) {
            data = "OK";
        }
        if (data.toString().equals(Prop.SERVE_MESSAGE)) {
            notErrorSet(data.toString());
            return;
        } else {
            errorSet(data.toString());
            return;
        }
    }

    private void notErrorSet(String result) {

    }

    private void errorSet(String json) {
        Toast.makeText(context, "接続エラー", Toast.LENGTH_LONG);

    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }
}
