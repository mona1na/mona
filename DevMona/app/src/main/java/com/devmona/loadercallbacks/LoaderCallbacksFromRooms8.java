package com.devmona.loadercallbacks;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;
import com.devmona.Prop;
import com.devmona.UtilInfo;

/**
 * Created by sake on 2018/04/10.
 */

public class LoaderCallbacksFromRooms8 implements LoaderManager.LoaderCallbacks<Object> {
    private final Base2Activity nameActivity;

    public LoaderCallbacksFromRooms8(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle bundle) {
        bundle.putInt("key", 8);
        MyLoader myLoader = new MyLoader((Context) nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        nameActivity.getSupportLoaderManager().destroyLoader(8);

        if (Prop.DEBUG_MODE == 1) {
            data = "OK";
        }

        if (data.toString().equals(Prop.SERVE_MESSAGE)) {
            notError(data.toString());
            return;
        } else {
            errorSet(data.toString());
            return;
        }

//        /**
//         *  {"status":1}
//         */
//        if (Prop.DEBUG_MODE == 1) {
//            data = "{\"status\":1}";
//        }
//
//        Validation validation = new Validation(roomsActivity);
//
//        try {
//            /**
//             * check whether responses is Json-format
//             */
//            validation.checkJsonER(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        try {
//            /**
//             * check error
//             */
//            validation.checkErrorER(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        try {
//            /**
//             * check weather there is {status:n}
//             */
//            validation.checkRoomId2(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        notError(data.toString());
    }

    private void notError(String s) {
        /**
         * 退出を反映する
         * reflect room_id null on room table
         */
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        myLoaderDB.updateCurrentStatus();
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

//        Fragment progressFragment = null;
//        List<Fragment> fragments = roomsActivity.getSupportFragmentManager().getFragments();
//        for (Fragment fragment:fragments) {
//            if (fragment instanceof ProgressFragment) {
//                progressFragment = fragment;
//            }
//        }
//        roomsActivity.getSupportFragmentManager().beginTransaction().remove(progressFragment).commit();
    }

    private void errorSet(final String messageJson) {
//        Handler handler = new Handler();
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                CommonErrorDialog commonErrorDialog = new CommonErrorDialog();
//                Bundle bundle = new Bundle();
//                bundle.putString(roomsActivity.getString(R.string.bundle_json), messageJson);
//                commonErrorDialog.setArguments(bundle);
//                commonErrorDialog.show(roomsActivity.getSupportFragmentManager(), "errorDialog");
        UtilInfo.testError2(nameActivity, messageJson);
//            }
//        });
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }
}
