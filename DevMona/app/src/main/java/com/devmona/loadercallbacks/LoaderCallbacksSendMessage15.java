package com.devmona.loadercallbacks;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.view.View;
import android.widget.TextView;

import com.devmona.Base2Activity;
import com.devmona.MessageFragment;
import com.devmona.MyLoader;
import com.devmona.R;
import com.devmona.UtilInfo;

/**
 * Created by sake on 2018/03/29.
 */

public class LoaderCallbacksSendMessage15 implements LoaderManager.LoaderCallbacks{


    private final Base2Activity nameActivity;

    public LoaderCallbacksSendMessage15(Base2Activity nameActivity){
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        String cmt = bundle.getString("message");
        bundle.putInt("key", 15);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        Bundle bundle = new Bundle();
        bundle.putString("message",data.toString());
        nameActivity.getSupportLoaderManager().destroyLoader(15);
        MessageFragment messageFragment = UtilInfo.getMessageFragment((Context) nameActivity);
        if(messageFragment != null) {
            if (messageFragment.getView() != null) {
                TextView textView = messageFragment.getView().findViewById(R.id.message_text2);
                textView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
