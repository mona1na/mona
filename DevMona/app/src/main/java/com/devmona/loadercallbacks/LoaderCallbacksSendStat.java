package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.util.Log;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;

/**
 * Created by sake on 2018/04/29.
 */

public class LoaderCallbacksSendStat implements LoaderManager.LoaderCallbacks {

    private final Base2Activity nameActivity;

    public LoaderCallbacksSendStat(Base2Activity nameActivity){
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        Log.d("onCreateLoader", "9");
        bundle.putInt("key", 9);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        nameActivity.getSupportLoaderManager().destroyLoader(9);
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
