package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;

public class LoaderCallbacksSendIgnore implements LoaderManager.LoaderCallbacks{

    private final Base2Activity nameActivity;

    public LoaderCallbacksSendIgnore(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        bundle.putInt("key", 14);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        nameActivity.getSupportLoaderManager().destroyLoader(14);
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
