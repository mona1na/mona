package com.devmona.loadercallbacks;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.devmona.Base2Activity;
import com.devmona.MyLoader;
import com.devmona.Prop;
import com.devmona.UtilInfo;

/**
 * Created by sake on 2018/03/29.
 */

public class LoaderCallbacksDBComment7 implements LoaderManager.LoaderCallbacks {

    private final Base2Activity nameActivity;

    public LoaderCallbacksDBComment7(Base2Activity nameActivity) {
        this.nameActivity = nameActivity;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        bundle.putInt("key", 7);
        MyLoader myLoader = new MyLoader(nameActivity, bundle);
        return myLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        nameActivity.getSupportLoaderManager().destroyLoader(7);

        if (Prop.DEBUG_MODE == 1) {
            data = "OK";
        }

        if (data.toString().equals(Prop.SERVE_MESSAGE)) {
            notError(data.toString());
            return;
        } else {
            errorSet(data.toString());
            return;
        }
//        /**
//         * {"s_cmt_id":n}
//         */
//        if (Prop.DEBUG_MODE == 1) {
//            data = "{\"s_cmt_id\":1}";
//        }
//
//        Validation validation = new Validation(main3Activity);
//
//        try {
//            validation.checkJsonC(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        try {
//            validation.checkErrorC(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        try {
//            /**
//             * check weather there is {"s_cmt_id":n}
//             */
//            validation.checkCmtId(data.toString());
//        } catch (JSONException e) {
//            errorSet(e.getMessage());
//            return;
//        }
//
//        notError(data.toString());
    }

    private void notError(String messageJson) {
//        try {
//            String cmtId= new JSONObject(messageJson.toString()).getString(main3Activity.getResources().getString(R.string.sdb_cmt_id));
//            MyLoaderDB myLoaderDB = new MyLoaderDB();
//            myLoaderDB.updateCommentFlag();
//            myLoaderDB.updateDBStatus(cmtId);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    private void errorSet(final String messageJson) {
//        Handler handler = new Handler();
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                CommonErrorDialog commonErrorDialog = new CommonErrorDialog();
//                Bundle bundle = new Bundle();
//                bundle.putString(main3Activity.getString(R.string.bundle_json), messageJson);
//                commonErrorDialog.setArguments(bundle);
//                commonErrorDialog.show(main3Activity.getSupportFragmentManager(), "errorDialog");
        UtilInfo.testError2(nameActivity, messageJson);
//            }
//        });

    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
