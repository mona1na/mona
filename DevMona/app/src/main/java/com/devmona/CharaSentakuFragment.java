package com.devmona;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.devs.vectorchildfinder.VectorChildFinder;
import com.devs.vectorchildfinder.VectorDrawableCompat;
import com.jaredrummler.android.colorpicker.ColorPickerView;


/**
 * A simple {@link Fragment} subclass.
 */
public class CharaSentakuFragment extends Fragment implements ColorPickerView.OnColorChangedListener {


    public static final String TAG = "CharaSentakuFragment";
    private ImageView imageViewAA;
    protected Person tempMe;
    private Button btnOk;
    private LinearLayout aaLinear;
    private Button btnCancel;
    private ColorPickerView colorPickerView;

    public CharaSentakuFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chara_sentaku, container, false);
        imageViewAA = (ImageView) v.findViewById(R.id.chara_aashowbutton);

        colorPickerView = (ColorPickerView) v.findViewById(R.id.color_picker);
        colorPickerView.setOnColorChangedListener(this);


        this.tempMe = new Person();
        this.tempMe.setAaname(MeMonaId.getMe().getAaname());
        this.tempMe.setAaresno(MeMonaId.getMe().getAaresno());
        this.tempMe.setColor255(MeMonaId.getMe().getColor255());
        this.tempMe.setRed(MeMonaId.getMe().getRed());
        this.tempMe.setGreen(MeMonaId.getMe().getGreen());
        this.tempMe.setBlue(MeMonaId.getMe().getBlue());
        aaLinear = (LinearLayout) v.findViewById(R.id.chara_aaFrame);

        int width1 = Calc.width;


        btnOk = (Button) v.findViewById(R.id.char_ok);


        ViewGroup.LayoutParams layoutParamsImage = imageViewAA.getLayoutParams();
        int width = Calc.width;
        int height = Calc.height;

        layoutParamsImage.height = width / 2;
        layoutParamsImage.width = height / 2;
        imageViewAA.setLayoutParams(layoutParamsImage);


        if (MeMonaId.getMe().getAaresno() == -1) {
            imageViewAA.setImageDrawable(ActivityCompat.getDrawable((Context) getActivity(), R.drawable.ic_svg_mona));

        } else {
            imageViewAA.setImageDrawable(ActivityCompat.getDrawable((Context) getActivity(), MeMonaId.getMe().getAaresno()));
            VectorChildFinder vectorChildFinder = new VectorChildFinder(getContext(), this.tempMe.getAaresno(), imageViewAA);
            VectorDrawableCompat.VFullPath path1 = vectorChildFinder.findPathByName("path1");
            path1.setFillColor(MeMonaId.getMe().getColor255());
            imageViewAA.invalidate();
        }

        btnCancel = (Button) v.findViewById(R.id.char_cancel);

        return v;
    }

    @Nullable
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestShared testShared = TestShared.getInstance(getContext());
                MeMonaId.getMe().setColor255(tempMe.getColor255());
                MeMonaId.getMe().setAaresno(tempMe.getAaresno());
                MeMonaId.getMe().setAaname(tempMe.getAaname());
                MeMonaId.getMe().setColorHukidashi(tempMe.getColorHukidashi());
                MeMonaId.getMe().setRed(tempMe.getRed());
                MeMonaId.getMe().setGreen(tempMe.getGreen());
                MeMonaId.getMe().setBlue(tempMe.getBlue());
                testShared.setMe(MeMonaId.getMe());
                getActivity().getSupportFragmentManager().beginTransaction().remove(CharaSentakuFragment.this).commit();
                RoomsFragment roomsFragment = UtilInfo.getRoomsFragment(getContext());
                if (roomsFragment != null) {
                    roomsFragment.setBottonEnable(true);
                }
            }
        });

        aaLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "aaLinear");
                AADialogFragment2 aaDialogFragment2 = new AADialogFragment2();
                Bundle bundle = new Bundle();
                bundle.putSerializable("tempPerson", tempMe);
                aaDialogFragment2.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.update_animator1, R.animator.chara_animator2);
                fragmentTransaction.replace(R.id.page_aa, aaDialogFragment2, getString(R.string.fragment_page_aa));
                fragmentTransaction.commit();
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(CharaSentakuFragment.this).commit();
                RoomsFragment roomsFragment = UtilInfo.getRoomsFragment(getContext());
                if (roomsFragment != null) {
                    roomsFragment.setBottonEnable(true);
                }
            }
        });



        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public void resultAA(String name) {
        int res = UtilInfo.aaNameaaResNoMap.get(name);
        tempMe.setAaresno(res);
        tempMe.setAaname(name);
        tempMe.setColorHukidashi(Util.change100toHukidashi(tempMe.getRed(), tempMe.getGreen(), tempMe.getBlue()));
        imageViewAA.setImageDrawable(ActivityCompat.getDrawable(getContext(), res));
        VectorChildFinder vectorChildFinder = new VectorChildFinder(getContext(), res, imageViewAA);
        VectorDrawableCompat.VFullPath path1 = vectorChildFinder.findPathByName("path1");
        path1.setFillColor(tempMe.getColor255());
        imageViewAA.invalidate();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onColorChanged(int newColor) {
        Log.d(TAG, "" + newColor);
        Log.d(TAG, "r:" + Color.red(newColor));
        Log.d(TAG, "g:" + Color.green(newColor));
        Log.d(TAG, "b:" + Color.blue(newColor));
//        VectorChildFinder vectorChildFinder = new VectorChildFinder(getContext(), tempMe.getAaresno(), imageViewAA);
//        VectorDrawableCompat.VFullPath path1 = vectorChildFinder.findPathByName("path1");
//        path1.setFillColor(newColor);
//        imageViewAA.invalidate();


        tempMe.setColor255(newColor);
        int[] result = Util.change255to100(Color.red(newColor), Color.green(newColor), Color.blue(newColor));
        tempMe.setRed(result[0]);
        tempMe.setGreen(result[1]);
        tempMe.setBlue(result[2]);
        tempMe.setColorHukidashi(Util.change100toHukidashi(tempMe.getRed(), tempMe.getGreen(), tempMe.getBlue()));
        VectorChildFinder vectorChildFinder = new VectorChildFinder(getContext(), tempMe.getAaresno(), imageViewAA);
        VectorDrawableCompat.VFullPath path1 = vectorChildFinder.findPathByName("path1");
        path1.setFillColor(tempMe.getColor255());
        imageViewAA.invalidate();
    }
}
