package com.devmona;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by sake on 2017/06/18.
 */

public class NopThread extends Thread {

    private Base2Activity base2Activity;
    Context context;
    //    private Base2Activity nameActivity;
    public boolean running = false;
    public static final String TAG = "NopThread";


    public NopThread(Context context) {
        this.context = context;
        running = true;
        this.base2Activity = (Base2Activity) context;
    }

    private void errorDialogShow(final String errorJson) {
        UtilInfo.testError2((Activity) context, errorJson);
    }

    @Override
    public void run() {
        super.run();
        while (running) {
//            int i = 0;
//            while (i < 5) {
//                if (running) {
//                    final int finalI = i;
//                    testA(context,finalI + "");
            Calendar calendar = Calendar.getInstance();
            boolean b = true;
//            testA(context,"nop送信前");
            while (b) {
                try {
                    Thread.sleep(1000);
                    Calendar calendar2 = Calendar.getInstance();

                    if (calendar2.getTimeInMillis() - calendar.getTimeInMillis() >= 5000) {
                        break;
                    }
                } catch (InterruptedException e) {
                    Log.d(TAG, "Interrupted finish run");
                    testA(context, "nop Interrupted finish run");
                    testMyThread();
                    running = false;
                    return;
                }
            }


//            try {
//                testA(context, "nop送信前");
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//                Log.d("nopkakunin", "Interrupted finish run");
//                testA(context, "nop Interrupted finish run");
//                return;
//            }
//                    i++;
//                } else {
//                    Log.d("nopkakunin", "else finish run");
//                    testA(context,"finish run");
//                    return;
//                }


            if (Prop.DEBUG_MODE == 1) {
                String serverResult = "{\"server\":\"OK\"}";
                String strServer = serverResult.toString();


                Validation validation = new Validation(context);

                try {
                    /**
                     * check json format,check "error" mapped,check "server" mapped
                     */
                    validation.checkJsonN(strServer);
                    validation.checkErrorN(strServer);
                    validation.checkServerN(strServer);
                } catch (JSONException e) {
                    errorDialogShow(e.getMessage());
                    return;
                }

                /**
                 * nopを送信する
                 */
                testNop();
            }
            /**
             * 本環境
             */
            else {
                /**
                 * process of checking server
                 */
                testServer();

                /**
                 * process GC
                 */
                testGC();

                /**
                 * send NOP
                 */
                testNop();
                Calendar calendar3 = Calendar.getInstance();
                int result = (int) ((calendar3.getTimeInMillis() - calendar.getTimeInMillis()) / 1000);
//                testA(context, "nop送信経過時間:" + result);
            }
//            /**
//             * --サーバーが起動しているか確認---
//             */
//            //---------------------moment------------------------------
////            Object serverResult = HttpSend.testHttpSend(null, null, Prop.surl, Prop.TIMEOUTSEC2);
//            //---------------------------------------------------------
//            String serverResult = "{\"server\":\"OK\"}";
//            String strServer = serverResult.toString();
//
//
//            Validation validation = new Validation(activity);
//
//            try {
//                /**
//                 * check json format,check "error" mapped,check "server" mapped
//                 */
//                validation.checkJsonN(strServer);
//                validation.checkErrorN(strServer);
//                validation.checkServerN(strServer);
//            } catch (JSONException e) {
//                errorDialogShow(e.getMessage());
//                return;
//            }


//            try {
//                /**
//                 * check
//                 */
//                validation.serverValidation(strServer);
//            } catch (Exception e) {
//                JSONObject jsonObject = setsudan();
//                try {
//                    String temp = jsonObject.getString("error");
//                    String result = temp + "\r\n" + e.getMessage();
//                    jsonObject.put("error",result);
//                    errorDialogShow(jsonObject);
//                } catch (JSONException e1) {
//                    e1.printStackTrace();
//                }
//                return;
//            }
//            /**
//             * ---------------サーバ確認終了--------------------
//             */
//
//            /**
//             * ---------------sendGC-----------------------------
//             */
//            GCDB gcdb = new GCDB();
//            Object result = "{\"gc\":\"empty\"}";
            //TODO GCコメントアウト
//            int count = gcdb.getGCCount();
//            if (count > 0) {
//                // pseq,startseqを得る
//                DBStatus dbStatus = gcdb.getDBStatusByStartId();
//                // JSONObject生成
//                JSONObject jsonObject = new JSONObject();
//                try {
//                    jsonObject.put("pseq", dbStatus.getsPId());
//                    jsonObject.put("startseq", dbStatus.getsStartId());
//                    // gcの中身をJSONArrayで取得する
//                    JSONArray jsonArray = gcdb.getGC0flag();
//                    jsonObject.put("ms",jsonArray);
//                    // 送信する
//                    result = HttpSend.testHttpSend(jsonObject, "m=", Prop.murl, Prop.TIMEOUTSEC5);
//
//                    //updateする
//                    gcdb.updateGCflag();
//                } catch (JSONException e) {
//                    HashMap<Integer, String> errorMap = setsudan();
//                    errorMap.put(errorMap.size(), e.getMessage());
//                    errorDialogShow(errorMap);
//                    return;
//                }
//            }

//            /**
//             * 基本的なvalidateエラー
//             */
//            try {
//                validation.checkJsonG(result.toString());
//                validation.checkErrorG(result.toString());
//                validation.checkG(result.toString());
//            } catch (JSONException e) {
//                errorDialogShow(e.getMessage());
//                return;
//            }

//            /**
//             * Nopを送信する
//             */
//
//            try {
//                UtilInfo.write(UtilInfo.socket, UtilInfo.writer, "<NOP />\0");
//            } catch (NullPointerException e) {
//                JSONObject jsonObject = setsudan();
//                try {
//                    String temp = jsonObject.getString("error");
//                    String result1 = temp + "\r\n" + "n0x52";
//                    jsonObject.put("error", result1);
//                    errorDialogShow(jsonObject.toString());
//                } catch (JSONException e1) {
//                    e1.printStackTrace();
//                }
//                return;
//            } catch (IOException e) {
//                JSONObject jsonObject = setsudan();
//                try {
//                    String temp = jsonObject.getString("error");
//                    String result1 = temp + "\r\n" + "n0x53";
//                    jsonObject.put("error", result1);
//                    errorDialogShow(jsonObject.toString());
//                } catch (JSONException e1) {
//                    e1.printStackTrace();
//                }
//                return;
//            }
        }
        testMyThread();
        Log.d(TAG, "finish run");
    }

    private void testMyThread() {
        MyThread myThread = TestConnect.getInstance(context).myThread;
        synchronized (myThread) {
            try {
                Log.d(TAG, "synchronized myThread");
                myThread.interrupt();
            } catch (IllegalMonitorStateException e) {
                e.printStackTrace();
                Log.d(TAG, "IllegalMonitorStateException");
            }
        }
    }


    public static void testA(Context context, final String s) {
//        final DevelopFragment developFragment = UtilInfo.getDevelopFragment(context);
//        Base2Activity base2Activity = (Base2Activity) context;
//        base2Activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (developFragment != null) {
//                    Person person = new Person();
//                    person.setLog(MyApplication.base2sStatus.toString() + s + ":" + Util.getDate());
//                    DevelopFragment.PersonEnum personEnum = new DevelopFragment.PersonEnum();
//                    personEnum.setPerson(person);
//                    personEnum.setType(DevelopFragment.TYPE.Log);
//                    developFragment.tate3Adapter.add(personEnum);
//                }
//            }
//        });
    }

    private void testGC() {

//        GCDB gcdb = new GCDB();
//        int count = gcdb.getGCCount();
//        Object result = null;
//        if (count > 0) {
////            if (myRoomsActivity != null) {
////                result = JsonCreate.sendGC(myRoomsActivity);
////            } else if (myMain3Activity != null) {
////                result = JsonCreate.sendGC(myMain3Activity);
////            }
//            result = JsonCreate.sendGC(context);
//            String data = result.toString();
//            if (data.equals(Prop.SERVE_MESSAGE)) {
//                SentakuDB sentakuDB = new SentakuDB();
////                    DBStatus dbStatus = sentakuDB.getFromTo();
//                sentakuDB.updateGU();
//                return;
//            } else {
//                errorDialogShow(data);
//            }
//
//        } else {
////            JSONObject jsonObject = new JSONObject();
////            try {
//////                    jsonObject.put("gc", 0);
////                jsonObject.put(context.getResources().getString(R.string.sdb_gc), 0);
////            } catch (JSONException e) {
////                e.printStackTrace();
////            }
////            result = jsonObject.toString();
////            return result;
//        }

//        MyDB myDB = new MyDB();
//        JSONArray jsonArraySoto = myDB.selectM();
//        GCDB gcdb = new GCDB();
//
//        int count = gcdb.getGCCount();
//        if (count > 0) {
//            // pseq,startseqを得る
//            DBStatus dbStatus = gcdb.getDBStatusByStartId();
//            // JSONObject生成
//            JSONObject jsonObject = new JSONObject();
//            String result = null;
//            try {
////                jsonObject.put(context.getResources().getString(R.string.sdb_p_id), dbStatus.getsPId());
////                jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//                // gcの中身をJSONArrayで取得する
//                JsonCreate.gcsend(jsonObject, context);
//
////                JSONArray jsonArray = gcdb.getGC0flag();
////                jsonObject.put(context.getResources().getString(R.string.sdb_ms), jsonArray);
//                // 送信する
//                result = HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_gc), Prop.murl, Prop.TIMEOUTSEC5).toString();
//
//                //updateする
//
//            } catch (JSONException e) {
//                JSONObject errorMap = setsudan();
//                try {
//                    errorMap.put("error", e.getMessage());
//                } catch (JSONException e1) {
//                    e1.printStackTrace();
//                }
////                        errorMap.put(errorMap.toString(), e.getMessage());
//                errorDialogShow(errorMap.toString());
//                return;
//            }
        /**
         * 基本的なvalidateエラー
         */
//            try {
//                validation.checkJsonG(result.toString());
//                validation.checkErrorG(result.toString());
//                validation.checkG(result.toString());
//            } catch (JSONException e) {
//                errorDialogShow(e.getMessage());
//                return;
//            }
//            Validation validation = new Validation(context);
//            try {
//                /**
//                 * check json format
//                 */
//
//                validation.checkJsonSo(result.toString());
//            } catch (JSONException e) {
//                errorDialogShow(e.getMessage());
//                return;
//            }
//            /**
//             * エラーチェック
//             */
//            try {
//                // jsonにerrorが存在したら
//                validation.checkErrorSo(result.toString());
//            } catch (JSONException e) {
//                errorDialogShow(e.getMessage());
//                return;
//            }
//            try {
//                // jsonにstatusが存在したら
//                validation.checkJsonStatus(result.toString());
//            } catch (JSONException e) {
//                errorDialogShow(e.getMessage());
//                return;
//            }

//            if(data.toString().equals(Prop.SERVE_MESSAGE)) {
//                notErrorSet();
//                return;
//            } else {
//                errorSet(data.toString());
//                return;
//            }

//        }
//            gcdb.updateGCflag();
//        }
    }

    /**
     * サーバーが起動しているか確認する
     */
    private void testServer() {
        /**
         * --サーバーが起動しているか確認---
         */
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("send", "send");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Object data = null;
        if (Prop.DEBUG_HTTP == 1) {
            data = HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_s), Prop.surl, Prop.TIMEOUTSEC2);
        } else {
            data = HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_s), Prop.surl, Prop.TIMEOUTSEC2, context);
        }


        if (data.toString().equals(Prop.SERVE_MESSAGE)) {
//            notErrorSet();
//            return;
        } else {
            errorDialogShow(data.toString());
            return;
        }
//        String strServer = serverResult.toString();
//        Validation validation = new Validation(context);
//        try {
//            /**
//             * check json format,check "error" mapped,check "server" mapped
//             */
//            validation.checkJsonN(strServer);
//            validation.checkErrorN(strServer);
//            validation.checkServerN(strServer);
//        } catch (JSONException e) {
//            errorDialogShow(e.getMessage());
//            return;
//        }
        /**
         * ---------------サーバ確認終了--------------------
         */
    }

    private void testNop() {

        /**
         * Nopを送信する
         */
        Log.d(TAG, "Nop before");
        TestConnect testConnect = TestConnect.getInstance(context);
        boolean b = testConnect.write(context, "<NOP />\0", context.getResources().getString(R.string.error_mess_7));
//        testA(context, b + "");
        Log.d(TAG, "Nop after");
//        try {
//            if (base2Activity != null) {
//                UtilInfo.write(base2Activity, "<NOP />\0", context.getResources().getString(R.string.error_mess_7));
//            } else {
//                UtilInfo.write(null, "<NOP />\0", context.getResources().getString(R.string.error_mess_7));
//            }
//        } catch (NullPointerException e) {
//            UtilInfo.write(null, "<NOP />\0", context.getResources().getString(R.string.error_mess_7));
//        }


    }

//    private JSONObject setsudan() {
//        JSONObject jsonObject = UtilInfo.setsudanShori();
//        return jsonObject;
//    }

//    @Override
//    public void onActivityCreated(RoomsActivity roomsActivity, Bundle savedInstanceState) {
//        this.myRoomsActivity = roomsActivity;
//    }
//
//    @Override
//    public void onActivityDestroyed(RoomsActivity roomsActivity) {
//        this.myRoomsActivity = null;
//    }
//
//    @Override
//    public void onActivityPaused(RoomsActivity roomsActivity) {
////        this.myRoomsActivity = roomsActivity;
//    }
//
//    @Override
//    public void onActivityResumed(RoomsActivity roomsActivity) {
//        this.myRoomsActivity = roomsActivity;
//    }
//
//    @Override
//    public void onActivitySaveInstanceState(RoomsActivity roomsActivity, Bundle outState) {
////        this.myRoomsActivity = roomsActivity;
//    }
//
//    @Override
//    public void onActivityStarted(RoomsActivity roomsActivity) {
//        this.myRoomsActivity = roomsActivity;
//    }
//
//    @Override
//    public void onActivityStopped(RoomsActivity roomsActivity) {
//        this.myRoomsActivity = null;
//    }

//    @Override
//    public void onActivityCreated(Base2Activity nameActivity, Bundle savedInstanceState) {
//        this.nameActivity = nameActivity;
//    }
//
//    @Override
//    public void onActivityDestroyed(Base2Activity nameActivity) {
//        this.nameActivity = null;
//    }
//
//    @Override
//    public void onActivityPaused(Base2Activity nameActivity) {
////        this.myMain3Activity = main3Activity;
//    }
//
//    @Override
//    public void onActivityResumed(Base2Activity nameActivity) {
//        this.nameActivity = nameActivity;
//    }
//
//    @Override
//    public void onActivitySaveInstanceState(Base2Activity nameActivity, Bundle outState) {
////        this.myMain3Activity = main3Activity;
//    }
//
//    @Override
//    public void onActivityStarted(Base2Activity nameActivity) {
//        this.nameActivity = nameActivity;
//    }
//
//    @Override
//    public void onActivityStopped(Base2Activity nameActivity) {
//        this.nameActivity = null;
//    }


//    @Override
//    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//
//    }
//
//    @Override
//    public void onActivityStarted(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivityResumed(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivityPaused(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivityStopped(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
//
//    }
//
//    @Override
//    public void onActivityDestroyed(Activity activity) {
//
//    }

//    @Override
//    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//        this.activity = (AppCompatActivity) activity;
//    }
//
//    @Override
//    public void onActivityStarted(Activity activity) {
//        this.activity = (AppCompatActivity) activity;
//    }
//
//    @Override
//    public void onActivityResumed(Activity activity) {
//        this.activity = (AppCompatActivity) activity;
//    }
//
//    @Override
//    public void onActivityPaused(Activity activity) {
////        this.activity = null;
//    }
//
//    @Override
//    public void onActivityStopped(Activity activity) {
////        this.activity = null;
//    }
//
//    @Override
//    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
//
//    }
//
//    @Override
//    public void onActivityDestroyed(Activity activity) {
//        this.activity = null;
//    }

//    private void dbsendHttps2() {
//        JSONObject jsonObject = UtilInfo.jsonpersonset0(context);
//        String result0 = UtilInfo.testHttpSend2(jsonObject,"pt=",Prop.purl);
//        Log.d("HTTPSEND RESULT", result0);
//        if (!result0.equals("OK")) {
//            UtilInfo.setsudanShori();
//            return;
//        }
//
//        while (check) {
//            Log.d("HTTPSEND","ループ中");
//
//            if (UtilInfo.m_logincheck == true) {
//                JSONObject jsonObject1 = UtilInfo.jsonpersonset0(context);
//                String result1 = UtilInfo.testHttpSend2(jsonObject1,"pt=",Prop.purl);
//                Log.d("HTTPSEND RESULT", result1);
//                if (!result0.equals("OK")) {
//                    UtilInfo.setsudanShori();
//                    return;
//                }
//                UtilInfo.m_logincheck = false;
//            }
//
//            if (UtilInfo.isMCount()) {
//
//                Cursor cursor = UtilInfo.m_db.rawQuery("SELECT * FROM mt WHERE flag = 0 ", null);
//                JsonFlagList jsonFlagList = UtilInfo.jsonpersonset(context, cursor);
//                Log.d("HTTPSEND",jsonFlagList.jsonObjects.toString());
//                String result = UtilInfo.testHttpSend2(jsonFlagList.jsonObjects,"andmona=",Prop.murl);
//                Log.d("HTTPSEND RESULT",result);
//                if (!result.equals("OK")) {
//                    UtilInfo.setsudanShori();
//                    break;
//                }
//                for (int value : jsonFlagList.integers) {
//                    ContentValues contentValues = new ContentValues();
//                    contentValues.put("flag",1);
//                    UtilInfo.m_db.update("mt",contentValues,"id = " + value,null);
//                }
//            }
//
//            if (UtilInfo.isCommentCount()) {
//
//                Cursor cursor2 = UtilInfo.m_db.rawQuery("SELECT * FROM cmtt WHERE flag = 0 ", null);
//                JsonFlagList jsonFlagList2 = UtilInfo.jsonpersonset2(context, cursor2);
//                String result2 = UtilInfo.testHttpSend2(jsonFlagList2.jsonObjects,"cmtt=", Prop.culr);
//                Log.d("HTTPSEND RESULT",result2);
//                if (!result2.equals("OK")) {
//                    UtilInfo.setsudanShori();
//                    break;
//                }
//
//                for (int value : jsonFlagList2.integers) {
//                    ContentValues contentValues = new ContentValues();
//                    contentValues.put("flag",1);
//                    UtilInfo.m_db.update("cmtt",contentValues,"id = " + value,null);
//                }
//            }
//
//
//
//
//            try {
//                Thread.sleep(10 * 1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//        }
//    }

//    public void dbsendHttp() {
//        JSONObject jsonObject = UtilInfo.jsonpersonset0(context);
//        String result0 = UtilInfo.testHttpSend(jsonObject,"pt=",Prop.purl);
//        Log.d("HTTPSEND RESULT", result0);
//        if (!result0.equals("OK")) {
//            UtilInfo.setsudanShori();
//            return;
//        }
//
//        while (check) {
//            Log.d("HTTPSEND","ループ中");
//            if (UtilInfo.isMCount()) {
//
//                Cursor cursor = UtilInfo.m_db.rawQuery("SELECT * FROM mt WHERE flag = 0 ", null);
//                JsonFlagList jsonFlagList = UtilInfo.jsonpersonset(context, cursor);
//                Log.d("HTTPSEND",jsonFlagList.jsonObjects.toString());
//                String result = UtilInfo.testHttpSend(jsonFlagList.jsonObjects,"andmona=",Prop.murl);
//                Log.d("HTTPSEND RESULT",result);
//                if (!result.equals("OK")) {
//                    UtilInfo.setsudanShori();
//                    break;
//                }
//                for (int value : jsonFlagList.integers) {
//                    ContentValues contentValues = new ContentValues();
//                    contentValues.put("flag",1);
//                    UtilInfo.m_db.update("mt",contentValues,"id = " + value,null);
//                }
//            }
//
//            if (UtilInfo.isCommentCount()) {
//
//                Cursor cursor2 = UtilInfo.m_db.rawQuery("SELECT * FROM cmtt WHERE flag = 0 ", null);
//                JsonFlagList jsonFlagList2 = UtilInfo.jsonpersonset2(context, cursor2);
//                String result2 = UtilInfo.testHttpSend(jsonFlagList2.jsonObjects,"cmtt=", Prop.culr);
//                Log.d("HTTPSEND RESULT",result2);
//                if (!result2.equals("OK")) {
//                    UtilInfo.setsudanShori();
//                    break;
//                }
//
//                for (int value : jsonFlagList2.integers) {
//                    ContentValues contentValues = new ContentValues();
//                    contentValues.put("flag",1);
//                    UtilInfo.m_db.update("cmtt",contentValues,"id = " + value,null);
//                }
//            }
//
//
//
//
//            try {
//                Thread.sleep(10 * 1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//        }
//    }
}
