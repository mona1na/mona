package com.devmona;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.UUID;

import androidx.annotation.NonNull;

/**
 * Created by sake on 2017/11/18.
 */

public class JsonCreate {


    public static void gcsend(JSONObject jsonObject, Context context) {

//        MyDB myDB = new MyDB();
//        ArrayList<ArrayList> arrayList = myDB.selectM();
//        JSONArray jsonArraySoto = myDB.selectM();
        try {
            MyDB myDB = new MyDB();
            JSONArray jsonArraySoto = myDB.selectM();
            if (jsonArraySoto.length() == 0) {
                return;
            } else {
                jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArraySoto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        if (arrayList != null && arrayList.size() >= 0) {
//            JSONArray jsonArraySoto = new JSONArray();
//            for (int i = 0; i < arrayList.size(); i++) {
//                JSONArray jsonArray = new JSONArray();
//                jsonArray.put(arrayList.get(i).get(0));
//                jsonArray.put(arrayList.get(i).get(1));
//                jsonArray.put(arrayList.get(i).get(2));
//                jsonArraySoto.put(jsonArray);
//            }

//            try {
//                jsonObject.put("ms", jsonArraySoto);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
    }

    public static Object sendUUID(Context context) {
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();

//        String uuid = sp.getString(context.getString(R.string.shared_uuid),null);
        TestShared testShared = TestShared.getInstance(context);
        String uuid = testShared.getUuid();
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            sp.edit().putString(context.getString(R.string.shared_uuid), uuid).commit();
        }

//        String token = testShared.getToken();
//        if (token == null) {
//
//        }
        String token = null;
        try {
            token = FirebaseInstanceId.getInstance().getToken(Prop.FCM, "FCM");
            Log.d("tokyo01", "token:" + token);
        } catch (IOException e) {
            Log.d("tokyo01", "IOE");
            e.printStackTrace();
        }

//        MyTokenReceiveThread myTokenReceiveThread = new MyTokenReceiveThread();
//        synchronized (myTokenReceiveThread) {
//            try {
//                Log.d("tokyo01", "syn wait before");
//                myTokenReceiveThread.start();
//                myTokenReceiveThread.wait();
//                Log.d("tokyo01", "syn wait after");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
            jsonObject.put(context.getResources().getString(R.string.shared_token), token);
            // mを追加する
//            gcsend(jsonObject, context);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Object result = HttpSend.testHttpSend(jsonObject, "p=", Prop.purl, Prop.TIMEOUTSEC5);
//        String result0 = HttpSend.testHttpSend2(jsonObject,"p=",Prop.purl);

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_p), Prop.purl, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_p), Prop.purl, Prop.TIMEOUTSEC5, context);
        }
    }

//    static class MyTokenReceiveThread extends Thread {
//        @Override
//        public void run() {
//            super.run();
//            synchronized (this) {
//                Log.d("tokyo01", "run");
//                FirebaseInstanceId.getInstance().getInstanceId()
//                        .addOnCompleteListener(new MyOnCompleteListener());
//
//                Log.d("tokyo01", "finish");
//                this.notify();
//            }
//        }
//    }

//    static class MyOnCompleteListener implements OnCompleteListener<InstanceIdResult> {
//
//        @Override
//        public void onComplete(@NonNull Task<InstanceIdResult> task) {
//            Log.d("tokyo01 onComplete sta", "");
//            if (!task.isSuccessful()) {
//                Log.d("FirebaseInstanceId", "getInstanceId failed", task.getException());
//                return;
//            }
//
//            // Get new Instance ID token
//            String token = task.getResult().getToken();
//
//            // Log and toast
////                                String msg = getString(R.string.msg_token_fmt, token);
//            Log.d("tokyo01 onComplete fin", token);
////                                Toast.makeText(getContext(), token, Toast.LENGTH_SHORT).show();
//        }
//    }

    public static Object cmtsend(Context context, String cmt) {
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//        String uuid = sp.getString(context.getString(R.string.shared_uuid),null);
        TestShared testShared = TestShared.getInstance(context);
        String uuid = testShared.getUuid();

        if (uuid == null) {
            /**
             * エラーダイアログを出す
             */
            return null;
        }

//        GCDB gcdb = new GCDB();
//        DBStatus dbStatus = gcdb.getDBStatusByStartId();
//        String comment = myLoaderDB.getComment();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), String.valueOf(dbStatus.getsPId()));
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_login_id), dbStatus.getsLoginId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_room_id), dbStatus.getsRoomId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_cmt_id), dbStatus.getsCmtId());
            jsonObject.put(context.getResources().getString(R.string.sdb_cmt), cmt);
//            Log.d("20180614log", jsonObject.toString());
//            JSONArray jsonArray = gcdb.getGC0flag();
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {

        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_c), Prop.curl, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_c), Prop.curl, Prop.TIMEOUTSEC5, context);
        }


//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        DBStatus dbStatus = myLoaderDB.selectDBStatus();
//        String comment = myLoaderDB.getComment();
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("pseq", dbStatus.getsPId());
//            jsonObject.put("startseq", dbStatus.getsStartId());
//            jsonObject.put("loginseq", dbStatus.getsLoginId());
//            jsonObject.put("roomseq", dbStatus.getsRoomId());
//            jsonObject.put("cmt", comment);
//            jsonObject.put("cmttime", UtilInfo.getNowDate());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
////        Object result = HttpSend.testHttpSend(jsonObject, "c=", Prop.curl, Prop.TIMEOUTSEC5);
////        return result;
//        return "{\"cmtseq\":0}";
    }

    public static Object sendGC(Context context) {
//        GCDB gcdb = new GCDB();
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();

//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//        String uuid = sp.getString(context.getString(R.string.shared_uuid),null);
        TestShared testShared = TestShared.getInstance(context);
        String uuid = testShared.getUuid();

        if (uuid == null) {
            /**
             * エラーダイアログを出す
             */
            return null;
        }

        // pseq,startseqを得る
//        DBStatus dbStatus = gcdb.getDBStatusByStartId();
        // JSONObject生成
        JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), dbStatus.getsPId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
            // gcの中身をJSONArrayで取得する
//            JSONArray jsonArray = gcdb.getGC0flag();
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
            //updateする
//                    gcdb.updateGCflag();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Object result = HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_gc), Prop.murl, Prop.TIMEOUTSEC5, context);

        //updateする
//                    gcdb.updateGCflag();

        return result;
    }

    public static Object sendLp(Context context) {
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();

//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//        String uuid = sp.getString(context.getString(R.string.shared_uuid),null);
        TestShared testShared = TestShared.getInstance(context);
        String uuid = testShared.getUuid();

        if (uuid == null) {
            /**
             * エラーダイアログを出す
             */
            return null;
        }

//        GCDB gcdb = new GCDB();
////        DBStatus dbStatus = gcdb.getDBStatusByStartId();
//        if (uuid == null) {
//            // 例外処理
//        }

        Person person = null;
        person = testShared.getMe();
//        person = myLoaderDB.getMe();
//        for (int i = 0; i <= 5; i++) {
//            person = myLoaderDB.getMe();
//            if (person.getShirotori() == null || person.getShirotori().equals("")) {
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                break;
//            }
//            if (i == 5) {
//                try {
//                    JSONObject jsonObject = new JSONObject();
//                    jsonObject.put("error", context.getString(R.string.cjme0x01));
//                    return jsonObject;
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }


        JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), dbStatus.getsPId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put("shirotori", person.getShirotori());
//            jsonObject.put("shirotori6", person.getShirotori6());
            jsonObject.put("kurotori", person.getKurotori());
            jsonObject.put("mona_id", person.getMonaId());
            jsonObject.put("name", person.getName());
//            jsonObject.put("aa_color_255", String.valueOf(person.getColor255()));
//            jsonObject.put("aa_name", String.valueOf(person.getAaname()));
//            jsonObject.put("stat", person.getStat());
//            JSONArray jsonArray = gcdb.getGC0flag();
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Object result = null;
        if (Prop.DEBUG_HTTP == 1) {
            result = HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_l), Prop.lurl, Prop.TIMEOUTSEC5);
        } else {
            result = HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_l), Prop.lurl, Prop.TIMEOUTSEC5, context);
        }
        return result;
    }

    public static Object sendR(Context context) {
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        Person me = myLoaderDB.getMe();
//        String uuid = myLoaderDB.getUUID();
//        GCDB gcdb = new GCDB();
////        DBStatus dbStatus = gcdb.getDBStatusByStartId();
//        Room room = myLoaderDB.getRoomNumber();
        TestShared testShared = TestShared.getInstance(context);
        Person me = testShared.getMe();
        String uuid = testShared.getUuid();
        Room room = testShared.getRoom();

        if (uuid == null) {

            /**
             * エラーダイアログを出す
             */
            return null;
        }

//        for (int i = 0; i <= 5; i++) {
//            me = myLoaderDB.getMe();
//            if (me.getShirotori() == null || me.getShirotori().equals("")) {
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                break;
//            }
//            if (i == 5) {
//                try {
//                    JSONObject jsonObject = new JSONObject();
//                    jsonObject.put("error", context.getString(R.string.cjme0x01));
//                    return jsonObject;
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), String.valueOf(dbStatus.getsPId()));
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_login_id), dbStatus.getsLoginId());
            jsonObject.put("room_no", room.getRoomNumber());
            jsonObject.put("name", me.getName());
            jsonObject.put("aa_color_255", me.getColor255());
            jsonObject.put("aa_name", me.getAaname());
            jsonObject.put("stat", me.getStat());
            jsonObject.put("shirotori", me.getShirotori());
            jsonObject.put("shirotori6", me.getShirotori6());
            jsonObject.put("kurotori", me.getKurotori());
            jsonObject.put("mona_id", me.getMonaId());
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {

        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_r), Prop.rurl, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_r), Prop.rurl, Prop.TIMEOUTSEC5, context);
        }
    }

    public static Object sendER(Context context) {
        TestShared testShared = TestShared.getInstance(context);
//        Person me = testShared.getMe();
        String uuid = testShared.getUuid();


//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();
//        GCDB gcdb = new GCDB();
//        DBStatus dbStatus = gcdb.getDBStatusByStartId();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), String.valueOf(dbStatus.getsPId()));
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_login_id), dbStatus.getsLoginId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_room_id), dbStatus.getsRoomId());
            jsonObject.put(context.getResources().getString(R.string.sdb_exit_date), UtilInfo.getNowDate());
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {

        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_er), Prop.erurl, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_er), Prop.erurl, Prop.TIMEOUTSEC5, context);
        }
    }

    public static Object sendUpdate(Context context) {
        TestShared testShared = TestShared.getInstance(context);
//        Person me = testShared.getMe();
        String uuid = testShared.getUuid();

//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();
//        GCDB gcdb = new GCDB();
//        DBStatus dbStatus = gcdb.getDBStatusByStartId();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), String.valueOf(dbStatus.getsPId()));
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_login_id), dbStatus.getsLoginId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_room_id), dbStatus.getsRoomId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_exit_date), UtilInfo.getNowDate());
//            JSONArray jsonArray = gcdb.getGC0flag();
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {

        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_upd), Prop.updurl, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_upd), Prop.updurl, Prop.TIMEOUTSEC5, context);
        }
    }

    public static Object randomSend20(Context context, Bundle bundle) {
        String strName = context.getString(R.string.random_key_name);
        String strGun = context.getString(R.string.random_key_gun);
        String strSeisokuchi = context.getString(R.string.random_key_seisokuchi);
        String strHitokoto = context.getString(R.string.random_key_hitokoto);

        String name = bundle.getString(strName);
        String gun = bundle.getString(strGun);
        String seisokuchi = bundle.getString(strSeisokuchi);
        String hitokoto = bundle.getString(strHitokoto);


        JSONObject jsonObject = new JSONObject();
        TestShared testShared = TestShared.getInstance(context);
        String uuid = testShared.getUuid();
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            sp.edit().putString(context.getString(R.string.shared_uuid), uuid).commit();
        }

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
            jsonObject.put(strName, name);
            jsonObject.put(strGun, gun);
            jsonObject.put(strSeisokuchi, seisokuchi);
            jsonObject.put(strHitokoto, hitokoto);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.random_touroku), Prop.touroku, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.random_touroku), Prop.touroku, Prop.TIMEOUTSEC5);
        }
    }

    public static Object randomSend19(Context context, Bundle bundle) {
//        String strName = context.getString(R.string.random_key_name);
//        String strGun = context.getString(R.string.random_key_gun);
//        String strSeisokuchi = context.getString(R.string.random_key_seisokuchi);
//        String strHitokoto = context.getString(R.string.random_key_hitokoto);
//
//        String name = bundle.getString(strName);
//        String gun = bundle.getString(strGun);
//        String seisokuchi = bundle.getString(strSeisokuchi);
//        String hitokoto = bundle.getString(strHitokoto);


        JSONObject jsonObject = new JSONObject();
        TestShared testShared = TestShared.getInstance(context);
        String uuid = testShared.getUuid();
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            sp.edit().putString(context.getString(R.string.shared_uuid), uuid).commit();
        }

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(strName, name);
//            jsonObject.put(strName, name);
//            jsonObject.put(strGun, gun);
//            jsonObject.put(strSeisokuchi, seisokuchi);
//            jsonObject.put(strHitokoto, hitokoto);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.random_auth), Prop.touroku, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.random_auth), Prop.touroku, Prop.TIMEOUTSEC5);
        }
    }

    public static Object sendVersionNotice(Context context) {
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();
        TestShared testShared = TestShared.getInstance(context);
//        Person me = testShared.getMe();
        String uuid = testShared.getUuid();
//        GCDB gcdb = new GCDB();
//        DBStatus dbStatus = gcdb.getDBStatusByStartId();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), String.valueOf(dbStatus.getsPId()));
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_login_id), dbStatus.getsLoginId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_room_id), dbStatus.getsRoomId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_exit_date), UtilInfo.getNowDate());
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {

        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_ver), Prop.verurl, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_ver), Prop.verurl, Prop.TIMEOUTSEC5, context);
        }
    }


    public static Object sendNewNotification(Context context) {
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();
        TestShared testShared = TestShared.getInstance(context);
//        Person me = testShared.getMe();
        String uuid = testShared.getUuid();
//        GCDB gcdb = new GCDB();
//        DBStatus dbStatus = gcdb.getDBStatusByStartId();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), String.valueOf(dbStatus.getsPId()));
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_login_id), dbStatus.getsLoginId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_room_id), dbStatus.getsRoomId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_exit_date), UtilInfo.getNowDate());
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {

        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_new_notify), Prop.newnotification, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_new_notify), Prop.newnotification, Prop.TIMEOUTSEC5, context);
        }
    }

    public static Object messagesend(Context context, String message) {
//        MyLoaderDB myLoaderDB = new MyLoaderDB();
//        String uuid = myLoaderDB.getUUID();
        TestShared testShared = TestShared.getInstance(context);
//        Person me = testShared.getMe();
        String uuid = testShared.getUuid();
//        GCDB gcdb = new GCDB();
//        DBStatus dbStatus = gcdb.getDBStatusByStartId();
//        String comment = myLoaderDB.getComment();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(context.getResources().getString(R.string.sdb_uuid), uuid);
//            jsonObject.put(context.getResources().getString(R.string.sdb_p_id), String.valueOf(dbStatus.getsPId()));
//            jsonObject.put(context.getResources().getString(R.string.sdb_start_id), dbStatus.getsStartId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_login_id), dbStatus.getsLoginId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_room_id), dbStatus.getsRoomId());
//            jsonObject.put(context.getResources().getString(R.string.sdb_cmt_id), dbStatus.getsCmtId());
            jsonObject.put(context.getResources().getString(R.string.sdb_message), message);
//            Log.d("20180614log", jsonObject.toString());
            JSONArray jsonArray = new JSONArray();
            jsonObject.put(context.getResources().getString(R.string.sdb_gc), jsonArray);
        } catch (JSONException e) {

        }

        if (Prop.DEBUG_HTTP == 1) {
            return HttpSend.testHttpSend(jsonObject, context.getResources().getString(R.string.sdb_key_mess), Prop.messurl, Prop.TIMEOUTSEC5);
        } else {
            return HttpSend.testHttpsSend(jsonObject, context.getResources().getString(R.string.sdb_key_mess), Prop.messurl, Prop.TIMEOUTSEC5, context);
        }
    }
}
