package com.devmona;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.devmona.loadercallbacks.LoaderCallbacks2FromSocket;

import java.util.ArrayList;


///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
// * {@link OnFragmentInteractionListener} interface
// * to handle interaction events.
// * Use the {@link Tate3Fragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class DevelopFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private float dimension;
    private ReadMainThread readMainThread;
    private TestConnect testConnect;
    private ImageView imgScroll;
    private boolean mInit;
    private FrameLayout frameLayout;
    private int backColor;

    /**
     * shows comment
     * <p>
     * //     * @param monaId
     * //     * @param strCmt
     * //     * @param hashLayoutByte
     */
    public void showComment(TestIdXML testIdXML) {
        Person person;

        if (MeMonaId.getMe().getMonaId() == testIdXML.getMonaId()) {
            person = MeMonaId.getMe();
        } else {
            person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());
        }

        Person temp = testPerson(person);

        if (!person.isIgnoreFlag()) {
            PersonEnum personEnum = new PersonEnum();
            personEnum.setPerson(temp);
            personEnum.setType(TYPE.Comment);
            tate3Adapter.add(personEnum);
            checkScroll();
        }
    }

    private Person testPerson(Person person) {
        Person temp = new Person();
        TestCom testCom = person.test3();

        temp.setComment(testCom.getCmt());
        if (!person.getKurotori().equals("")) {
            temp.setKurotori(person.getKurotori());
        }
        temp.setShirotori(person.getShirotori());
        temp.setName(person.getName());
        temp.setColor255(person.getColor255());
        temp.setColorHukidashi(person.getColorHukidashi());
        return temp;
    }

    private void checkScroll() {
        if (listView.getCount() > 0) {
            Log.d("test425", "la:" + listView.getLastVisiblePosition() + " co:" + listView.getCount());
            if (listView.getLastVisiblePosition() + 2 != listView.getCount()) {
                imgScroll.setVisibility(View.VISIBLE);
            } else {
                imgScroll.setVisibility(View.INVISIBLE);
                listView.setSelection(listView.getCount());
            }
        }
    }

    public void enterMe2(TestIdXML testIdXML) {
//        Person me = ReadThread2.people.get(testIdXML.getUniqueId());
        Person me = MeMonaId.getMe();
        PersonEnum personEnum = new PersonEnum();
        personEnum.setType(TYPE.Enter);
        personEnum.setPerson(me);
        tate3Adapter.add(personEnum);
        checkScroll();
    }

    public void enterOther2(TestIdXML testIdXML) {
        Person person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());

        if (!person.isIgnoreFlag()) {
            PersonEnum personEnum = new PersonEnum();
            personEnum.setType(TYPE.Enter);
            personEnum.setPerson(person);
            tate3Adapter.add(personEnum);
            checkScroll();
        }
    }

    public void showExitOther2(TestIdXML testIdXML) {
        Person person = testConnect.readMainThread.people.get(testIdXML.getUniqueId());

        if (!person.isIgnoreFlag()) {
            PersonEnum personEnum = new PersonEnum();
            personEnum.setPerson(person);
            personEnum.setType(TYPE.Exit);
            tate3Adapter.add(personEnum);
            checkScroll();
        }

    }

    public enum TYPE {Comment, Enter, Exit, Log}


    // TODO: Rename and change types of parameters
    private OnFragmentInteractionListener mListener;
    private ListView listView;
    public Tate3Adapter tate3Adapter;

    public DevelopFragment() {
        mInit = false;
        testConnect = TestConnect.getInstance(getContext());
        this.dimension = -1;
    }

    public void setDimension(float dimension) {
        this.dimension = dimension;
    }

    //    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
//     * @return A new instance of fragment Tate3Fragment.
//     */
    // TODO: Rename and change types and number of parameters
//    public static Tate3Fragment newInstance(String param1, String param2) {
//        Tate3Fragment fragment = new Tate3Fragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.backColor = 0;
    }

    public void setBackColor(int backColor) {
        this.backColor = backColor;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tate3, container, false);
        frameLayout = (FrameLayout) view.findViewById(R.id.tate3_frame_layout);
        if (backColor != 0) {
            frameLayout.setBackgroundColor(backColor);
        }
        frameLayout.setBackgroundColor(getResources().getColor(R.color.commonGray));

        listView = (ListView) view.findViewById(R.id.list_tate3);
        if (this.dimension == -1) {
            tate3Adapter = new Tate3Adapter(getContext(), R.layout.test10, getResources().getDimension(R.dimen.cmn_size15));
        } else {
            tate3Adapter = new Tate3Adapter(getContext(), R.layout.test10, dimension);
        }

        listView.setAdapter(tate3Adapter);
        imgScroll = (ImageView) view.findViewById(R.id.image_scroll_tate3);
        imgScroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickImgScroll();
                imgScroll.setVisibility(View.INVISIBLE);
            }
        });

        return view;
    }

    private void clickImgScroll() {
        listView.setSelection(listView.getCount());
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("20180503test", "onCreate Tate3");
        Base2Activity main3Activity = (Base2Activity) getActivity();
//        main3Activity.getSupportLoaderManager().initLoader(4, new Bundle(), new LoaderCallbacksEnterRoom4((Base2Activity) getActivity()));
        Bundle bundle = new Bundle();
        main3Activity.getSupportLoaderManager().initLoader(2, new Bundle(), new LoaderCallbacks2FromSocket(getContext()));
    }

    class Tate3Adapter extends ArrayAdapter<PersonEnum> {

        private float dimension;
        private int resource;

        public Tate3Adapter(@NonNull Context context, int resource, float dimension) {
            super(context, resource);
            this.resource = resource;
            this.dimension = dimension;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            PersonEnum personEnum = getItem(position);
            if (TYPE.Comment == personEnum.getType()) {
                convertView = getLayoutInflater().inflate(R.layout.test10, null);
                Person person = personEnum.getPerson();
                LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.chat_tate3);
                linearLayout.setBackgroundColor(person.getColorHukidashi());
                TextView textDainari = (TextView) convertView.findViewById(R.id.txt_dainari_tate3);
                textDainari.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                textDainari.setTextColor(getResources().getColor(R.color.commonBlack));
                TextView textName = (TextView) convertView.findViewById(R.id.txt_name_tate3);
                textName.setTextColor(getResources().getColor(R.color.commonBlack));
                textName.setText(person.getNameEllipsis());
                textName.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                TextView textComment = (TextView) convertView.findViewById(R.id.txt_comment_tate3);
                textComment.setText(person.getComment());
                textComment.setTextColor(getResources().getColor(R.color.commonBlack));
                textComment.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                TextView textShirotori6 = (TextView) convertView.findViewById(R.id.txt_shirotori6_tate3);
                textShirotori6.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                if (!person.getKurotori().equals("")) {
                    textShirotori6.setText(person.getShikakuRealKurotori());
                } else {
                    textShirotori6.setText(person.getShikakuRealShirotori());
                }
                textShirotori6.setTextColor(getResources().getColor(R.color.commonBlack));
            } else if (TYPE.Enter == personEnum.getType()) {
                convertView = getLayoutInflater().inflate(R.layout.test11, null);

                Person person = personEnum.getPerson();

                TextView name = convertView.findViewById(R.id.txt_enter_name_tate3);
                TextView enter = convertView.findViewById(R.id.txt_enter_tate3);
                enter.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                name.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                name.setTextColor(getResources().getColor(R.color.commonWhite));
                TextView shirotori = convertView.findViewById(R.id.txt_enter_shirotori_tate3);
                shirotori.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                shirotori.setTextColor(getResources().getColor(R.color.commonWhite));
                try {
                    name.setText(person.getNameEllipsis());
                } catch (NullPointerException e) {
                    Log.d("null", "null");
                }
                if (!person.getKurotori().equals("")) {
                    shirotori.setText(person.getShikakuRealKurotori());
                } else {
                    shirotori.setText(person.getShikakuRealShirotori());
                }
            } else if (TYPE.Exit == personEnum.getType()) {
                convertView = getLayoutInflater().inflate(R.layout.test12, null);
                Person person = personEnum.getPerson();
                TextView exit = convertView.findViewById(R.id.txt_exit_tate3);
                exit.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                TextView name = convertView.findViewById(R.id.txt_exit_name_tate3);
                name.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                name.setTextColor(getResources().getColor(R.color.commonWhite));
                TextView shirotori = convertView.findViewById(R.id.txt_exit_shirotori_tate3);
                shirotori.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                shirotori.setTextColor(getResources().getColor(R.color.commonWhite));
                name.setText(person.getNameEllipsis());

                if (!person.getKurotori().equals("")) {
                    shirotori.setText(person.getShikakuRealKurotori());
                } else {
                    shirotori.setText(person.getShikakuRealShirotori());
                }
            } else if (TYPE.Log == personEnum.getType()) {
                convertView = getLayoutInflater().inflate(R.layout.test13, null);
                TextView log = convertView.findViewById(R.id.txt_log_tate3);
                log.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
                Person person = personEnum.getPerson();
                log.setText(person.getLog());
            }
            return convertView;
        }
    }

    static class PersonEnum {
        private Person person;
        private TYPE type;

        public Person getPerson() {
            return person;
        }

        public TYPE getType() {
            return type;
        }

        public void setType(TYPE type) {
            this.type = type;
        }

        public void setPerson(Person person) {
            this.person = person;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("lifeTest", "Tate3Fragment onResume");
        if (mInit) {

        }
        mInit = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("lifeTest", "Tate3Fragment onPause");
    }
}
