package test3.com.test;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static org.junit.Assert.assertNotNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest extends Instrumentation {
//    @Test
//    public void useAppContext() throws Exception {
////        // Context of the app under test.
////        Context appContext = InstrumentationRegistry.getTargetContext();
////
////        assertEquals("listtest.com.testfragment", appContext.getPackageName());
//        Intent intent = context.
//    }

    private static final String APP_PACKAGE = "meetscom.s";
    private UiDevice mDevice;

    private Intent createLaunchIntent(Context context) {

        Intent intent = context.getPackageManager().getLaunchIntentForPackage(APP_PACKAGE);
        assertNotNull("Target app is not found on your device", intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Before
    public void start_target_app() throws Exception {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        mDevice = UiDevice.getInstance(instrumentation);
        mDevice.pressHome();

        Context context = instrumentation.getTargetContext();
        context.startActivity(createLaunchIntent(context));
        mDevice.wait(Until.hasObject(By.pkg(APP_PACKAGE).depth(0)), 5000);
    }

    @Test
    public void test() throws Exception {
        String[] str = {
                "onaji!!!!!"
//                "rainnsiyou",
//                "rainnhayo",
//                "kakaodemoiiyo",
//                "hayo",
//                "rainn"
//                "yukasikainaitanomu",
//                "tanomuhonntoni",
//                "yukadakenanndatte",
//                "yukamajideonegai",
//                "onegai",
//                "hontoni",
//                "onegaitanomu",
//                "yukamajidetanomu",
//                "hontoniyuka",
//                "yukadake",
//                "majidetanomu",
//                "tanomukara",
//                "yuka",
//                "nanndemosurukara",
//                "nanndemosurukaratanomu",
//                "tanomunanndemosuru",
//                "onegaitanomu",
//                "onegaidayohontoyuka",
//                "onegaiyukaonegai",
//                "onegaihontomajide",
//                "onegaiyukamaji",
//                "yukahonntotanomu",
//                "yukasikainainndayo",
//                "yukadakenanndayo",
//                "yukadakenannda",
//                "yukainaitomuri",
//                "yukatanomutte"
        };

        for (int i = 0; i < 11110; i++) {
            UiObject text = mDevice.findObject(new UiSelector()
                    .resourceId("meetscom.s:id/messageInput"));
            Random random = new Random();
            String s = str[random.nextInt(str.length)];
            text.setText(s);


            Thread.sleep(3000);
            UiObject send = mDevice.findObject(new UiSelector()
                    .resourceId("meetscom.s:id/messageSendButton"));
            send.click();
        }
    }

}